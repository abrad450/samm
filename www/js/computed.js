var Computed = function(cultureLang, thousandsSeparator, decimalSeparator) {
    var self = this;
    var lang = cultureLang;
    var tSep = thousandsSeparator;
    var dSep = decimalSeparator;

    // Zakladni field
    this.getElementValue = function(el) {
        var tagName = el.get(0).tagName.toLowerCase();
        var v = '';
        
        if(tagName === 'div' || tagName === 'span') {
            v = el.data('fieldinfo-rawval');
        }
        else {
            v = el.val();
        }

        v = self.parseNum(v);

        if($.trim(v) === '') {
            v = self.parseNum(el.html());
        }

        return v === '' ? 0 : parseFloat(v);
    };

    this.parseNum = function(value) {
        var regexpNbsp = new RegExp('&nbsp;', 'g');
        var regexp = new RegExp(tSep === '.' ? '\\.' : tSep, 'g');
        var v = value.toString()
            .replace(regexpNbsp, '')
            .replace(regexp, '')
            .replace(dSep, '.');
        return v;
    };

    this.formatNum = function(value, size) {
        var sizeArray = size.toString().split(',');
        var roundPrec = sizeArray.length <= 1 ? 0 : parseInt(sizeArray[1]);
        var rounding = Math.pow(10, roundPrec);
        value = Math.round(value*rounding)/rounding;
        return value.toLocaleString(lang);
    };

    // compute aggregation of the given fieldPath
    this.computeAggregation = function(fieldPath) {
        var control = $('[data-fieldinfo-path="' + fieldPath + '"]');
        var sum = 0;
        control.each(function(i, c) {
            var value = self.getElementValue($(c));
            sum += value;   
        });
        return sum;
    };

    // computes formula of the given template
    this.computeFormula = function(tpl, mustContain) {
        var regexp = new RegExp('{([a-zA-Z0-9_\.\-]+)}','g');
        var matches = tpl.match(regexp);
        var realFormula = tpl;
        $.each(matches, function(i, match) {
            var fiPath = match.replace('{', '').replace('}', '');
            var field = null;
            if(mustContain) {
                // uprasneni za aktualni radek tabulky naor. "[2]"
                field = $('[data-fieldinfo-path="' + fiPath + '"][name*="' + mustContain + '"]');   
            }
            else {
                field = $('[data-fieldinfo-path="' + fiPath + '"]');
            }
            var rfVal = self.getValue(field);
            realFormula = realFormula.replace(match, rfVal);
        });
        var formulaResult = math.evaluate(realFormula);
        return formulaResult;
    };

    // returns computed or found value of the given control
    this.getValue = function(field, mustContain) {
        var cType = field.data('computed-type');
        var cValue = field.data('computed-value');

        // agregace
        if(cType == 'aggregation') {
            return self.computeAggregation(cValue);
        }
        
        // vzorec
        if(cType == 'formula') {
            return self.computeFormula(cValue, mustContain);
        }

        return self.getElementValue(field);
    };

    // Compute field value and fill it
    this.computeField = function(field, mustContain) {
        var cVal = self.getValue(field, mustContain);
        var tagName = field.get(0).tagName.toLowerCase();
        var size = field.data('fieldinfo-size');
        if(tagName === 'div' || tagName === 'span') {
            field.html(self.formatNum(cVal, size));
        }
        else {
            field.val(self.formatNum(cVal, size));
        }
    };

    // iterates over all computed fields and computes its values
    this.calculate = function() {
        var computedFields = [];
        $('[data-computed-field]').each(function(i, cf) {
            computedFields.push(cf);
            var $cf = $(cf);
            
            // is this table row field ?
            var cfName = $cf.attr('name');
            var cfMatches = cfName.match(/\[[0-9]+\]/i);
            var mustContain = cfMatches ? cfMatches[0] : null;
            
            var cVal = self.computeField($cf, mustContain);
        });
        return computedFields;
    };

};