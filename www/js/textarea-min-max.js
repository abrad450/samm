$(function() {
    
    var addCss = function() {
        $('html > head').append('<style>\n\
            .min-max-wrap { }\n\
            .min-max-wrap .min-max-label { height: 20px; line-height: 20px; font-weight: bold; text-align: left; }\n\
            .min-max-wrap.min-max-wrap-maximized { position: fixed; left: 0; top: 0; z-index: 9999; right: 0; bottom: 0; background: white; padding: 10px; display: flex; flex-direction: column; }\n\
            .min-max-textarea-wrap { position: relative; display: inline-block; }\n\
            .min-max-button { position: absolute; right: 5px; top: 5px; background-color: transparent; border: none; background-size: 75%; background-repeat: no-repeat; background-position: center; visibility: hidden; }\n\
            .min-max-textarea-wrap:hover .min-max-button { visibility: visible; } \n\
            .min-max-button.min-max-maximize { background-image: url(\'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDQ4IDQ4IiBoZWlnaHQ9IjQ4cHgiIGlkPSJMYXllcl80IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA0OCA0OCIgd2lkdGg9IjQ4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxwb2x5Z29uIGZpbGw9IiMyNDFGMjAiIHBvaW50cz0iMjAuNTM5LDE2LjgyNiA5Ljg3Nyw2LjE2NCAxNi4wMiwwIDAsMCAwLDE2LjA3NCA2LjIyMiw5LjgzMSAxNi44NzcsMjAuNDg2ICAiLz48cG9seWdvbiBmaWxsPSIjMjQxRjIwIiBwb2ludHM9IjI3LjQ2MiwxNi44MjYgMzguMTIzLDYuMTY0IDMxLjk4MSwwIDQ4LDAgNDgsMTYuMDc0IDQxLjc3OCw5LjgzMSAzMS4xMjMsMjAuNDg2ICAiLz48cG9seWdvbiBmaWxsPSIjMjQxRjIwIiBwb2ludHM9IjI3LjQ2MiwzMS4xNzYgMzguMTIzLDQxLjgzOCAzMS45ODEsNDguMDAyIDQ4LDQ4LjAwMiA0OCwzMS45MjcgNDEuNzc4LDM4LjE3IDMxLjEyMywyNy41MTYgICIvPjxwb2x5Z29uIGZpbGw9IiMyNDFGMjAiIHBvaW50cz0iMjAuNTM5LDMxLjE3NiA5Ljg3OCw0MS44MzggMTYuMDIsNDguMDAyIDAuMDAxLDQ4LjAwMiAwLjAwMSwzMS45MjcgNi4yMjMsMzguMTcgMTYuODc3LDI3LjUxNiAgICAgIi8+PC9nPjwvc3ZnPg==\'); width: 20px; height: 20px; opacity: .6; }\n\
            .min-max-button.min-max-minimize { background-image: url(\'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDQ4IDQ4IiBoZWlnaHQ9IjQ4cHgiIGlkPSJMYXllcl80IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA0OCA0OCIgd2lkdGg9IjQ4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxwb2x5Z29uIGZpbGw9IiMyNDFGMjAiIHBvaW50cz0iMCw0NC4zNCAxMC42NjIsMzMuNjc4IDQuNTIsMjcuNTE0IDIwLjUzOSwyNy41MTQgMjAuNTM5LDQzLjU4OCAxNC4zMTYsMzcuMzQ1IDMuNjYyLDQ4ICAiLz48cG9seWdvbiBmaWxsPSIjMjQxRjIwIiBwb2ludHM9IjQ3Ljk5OSw0NC4zNCAzNy4zMzgsMzMuNjc4IDQzLjQ3OSwyNy41MTQgMjcuNDYxLDI3LjUxNCAyNy40NjEsNDMuNTg4IDMzLjY4MywzNy4zNDUgNDQuMzM4LDQ4ICAgICAiLz48cG9seWdvbiBmaWxsPSIjMjQxRjIwIiBwb2ludHM9IjQ3Ljk5OSwzLjY2IDM3LjMzOCwxNC4zMjIgNDMuNDc5LDIwLjQ4NiAyNy40NjEsMjAuNDg2IDI3LjQ2MSw0LjQxMSAzMy42ODMsMTAuNjU0IDQ0LjMzOCwwICAiLz48cG9seWdvbiBmaWxsPSIjMjQxRjIwIiBwb2ludHM9IjAuMDAxLDMuNjYgMTAuNjYyLDE0LjMyMiA0LjUyMSwyMC40ODYgMjAuNTM5LDIwLjQ4NiAyMC41MzksNC40MTEgMTQuMzE3LDEwLjY1NCAzLjY2MywwICAiLz48L2c+PC9zdmc+\'); width: 30px; height: 30px; opacity: 0.4; }\n\
            .min-max-wrap-minimized .min-max-label { display: none; }\n\
            .min-max-wrap-maximized .min-max-label { display: block; }\n\
			.min-max-wrap-maximized .min-max-textarea-wrap { width: 100% !important; height: 100% !important; }\n\
            .min-max-wrap-maximized textarea { /*min-width: 100% !important; min-height: calc(100% - 20px) !important;*/ width: 100% !important; height: 100% !important; max-width: 100% !important; max-height: 100% !important }\n\
        </style>');
    };
    
    var apply = function(textArea) {
        
        var id = textArea.prop('id');
        var inTable = textArea.closest('td').length > 0;
                
        var label = $('label[for="' + id + '"]');

        var taStyle = textArea.attr('style');

        var maximizeText = textArea.data('min-max-maximize-label') ?? 'Maximize';
        var labelText = textArea.data('min-max-label') ?? '';
        
        textArea.wrap('<span class="min-max-wrap min-max-wrap-minimized"></span>');
        textArea.wrap('<span class="min-max-textarea-wrap"></span>');
        
        var taWrap = textArea.closest('.min-max-textarea-wrap');
        
        if(inTable) {
            taWrap.attr('style', taStyle);
            textArea.attr('style', 'width:100%;height:100%;');
        }
        
        var btnMinMax = $('<button type="button" class="min-max-button min-max-maximize"></button>');
        btnMinMax.attr('title', maximizeText);
        textArea.after(btnMinMax);
        
        var labelMinMax = $('<span class="min-max-label"></span>');
        labelMinMax.html(label.length > 0 ? label.html() : labelText);
        taWrap.before(labelMinMax);
    };
    
    var check = function() {
        $('textarea[data-min-max=0]').each(function() {
            var textArea = $(this);
            textArea.attr('data-min-max', 1);
            apply(textArea);
        });
    };

    addCss();
    
    $('body').on('click', '.min-max-button', function() {
        
        var btnMinMax = $(this);
        var textArea = btnMinMax.closest('.min-max-textarea-wrap').find('textarea');
        var wrap = textArea.closest('.min-max-wrap');
        var minimizeText = textArea.data('min-max-minimize-label') ?? 'Minimize';
        var maximizeText = textArea.data('min-max-maximize-label') ?? 'Maximize';
        
        if(btnMinMax.hasClass('min-max-maximize')) { 
            btnMinMax.addClass('min-max-minimize');
            btnMinMax.removeClass('min-max-maximize');
            btnMinMax.attr('title', minimizeText);
        }
        else {
            btnMinMax.addClass('min-max-maximize');
            btnMinMax.removeClass('min-max-minimize');
            btnMinMax.attr('title', maximizeText);
        }
        if((textArea.data('min-max-status') ?? 'minimized') == 'minimized') {
            textArea.data('min-max-status', 'maximized');
            wrap.addClass('min-max-wrap-maximized');
            wrap.removeClass('min-max-wrap-minimized');
        }
        else {
            textArea.data('min-max-status', 'minimized');
            wrap.addClass('min-max-wrap-minimized');
            wrap.removeClass('min-max-wrap-maximized');
        }
    });

    check();
    setInterval(check, 1000);

});