/**
 * Javascript
 */
$(function() {

	const TIMEOUT = 100;
	const TIMEOUT_MESSAGE = 2000;
    
    // Test if input contains some utf-8 characters and is not utf-8 compatible
    Nette.validators.AbraHelpers_validateEncoding = function(elem, encoding, value) {
        if(encoding === 'utf-8' || $.trim(value) === '') {
            return true;
        }
        
        var encoderDecoder = null;
        switch(encoding) {
            case 'windows-1250': 
                // windows1250 = https://github.com/mathiasbynens/windows-1250
                encoderDecoder = windows1250; 
                break;
            default: 
                // windows1250 = https://github.com/mathiasbynens/windows-1250
                encoderDecoder = windows1250;
                break;
        }
        
        var reencoded = encoderDecoder.decode(encoderDecoder.encode(value, { mode: 'html' }));
        return reencoded === value;
    };
    
    // Warn, ask and decide (do not forget update this adequately on Nette update @fixme)
    Nette.validators.AbraHelpers_warnfilled = function(elem, args, val, c) {
		var result = val !== '' && val !== false && val !== null
			&& (!Nette.isArray(val) || val.length)
			&& (!window.FileList || !(val instanceof FileList) || val.length);
        if(!result) {
            result = window.confirm(args);
        }
        return result;
    };
    // override addError (do not forget update this adequately on Nette update @fixme)
    Nette.addError = function(elem, message) {
        if (elem.focus) {
            elem.focus();
        }
        if (message && message !== 'SKIPMESSAGE') {
            alert(message);
        }
    };    
    

	// inicializace
	var init = function(elem)
	{
		if(elem === undefined || elem === null) {elem = $(document);}

        // sections type='tabs'
        $('.jqui-tabs').tabs();

		// date-picker
		$('input[data-input-type]', elem).each(function() {

			var input = $(this);
			var is = input.data('input-type');
			switch(is)
			{
				case 'dtDateTime':
					input.addClass('input-datetime');
					input.datetimepicker({
						timeFormat: 'hh:mm',
						stepHour: 1,
						stepMinute: 5,
						addSliderAccess: true,
						sliderAccessArgs: {touchonly: false}
					});
					break;

				case 'dtDate':
					input.addClass('input-datetime');
					input.datepicker();
					break;

			}
			input.trigger('change');
		});

		// Submitted By Setter
		$('input[data-submitted-by],button[data-submitted-by]', elem).click(function(event) {
			event.preventDefault();
			var frm = $(this).closest('form');
			frm.find('input[name=_submittedby_]').val($(this).data('submitted-by'));
			frm.submit();
		});

        // Cancel filter (set flag)
        $('input[data-cancel-filter]').click(function(elem) {
            event.preventDefault();
            var frm = $(this).closest('form');
            frm.find('input[name=_cancel_filter]').val('1');
            frm.submit();
        });

		// if browser < IE10
		/*if(!window.atob) {
			window.setInterval(function() {
				$('.row-fluid:has(.span-block)', elem).each(function() {
					var maxHeight = 0;
					$(this).children('.span-block').each(function() {
						var h = $(this).innerHeight();
						if(h > maxHeight) maxHeight = h;
					}).css('min-height', maxHeight);
				});
			}, 500);
		}*/

		/*$('.nav-pills a', elem).click(function (e) {
			e.preventDefault();
			$(this).tab('show');
		});*/


        // Korekce velikosti - K aktivaci je treba v flex.css vypnout flex zobrazeni
        // window.setInterval(function() {
        //     $('.row-fluid:has(.span-block)').each(function() {
        //         let maxHeight = 0, maxChildren;
        //         const children = $(this).children('.span-block');
        //
        //         // Najdeme nejvyssi
        //         children.each(function() {
        //             // Nejdrive vyresetujeme potomky (mohla se zmenit velikost z vetsiho na mensi
        //             $(this).children().css('minHeight', 'auto');
        //
        //             let h = $(this).height();
        //
        //             if(h > maxHeight){
        //                 maxHeight = h;
        //                 maxChildren = this;
        //             }
        //         });
        //
        //         // Pouze desktop
        //         if($(document).width() > 767) {
        //             // Krome nejvyssiho pridelime poslednimu prvku zbytek
        //             children.each(function(){
        //                 if (this != maxChildren){
        //                     const last =  $(this).children().last();
        //
        //                     last.css('min-height', last.height() + (maxHeight - $(this).height()));
        //                 }
        //             });
        //         }
        //     });
        // }, 500);


		$('.btn-massdelete,.btn-massform,.btn-massprint', elem).click(function(event) {
			var chb = $('.list-table tbody input[type=checkbox]:checked', elem);
			if(chb.length <= 0) {
				alert('Nejprve zaškrtněte záznamy, u kterých chcete provést hromadnou akci.');
				event.preventDefault();
			}
		});
		$('.btn-massdelete', elem).click(function(event) {
			if(!event.isDefaultPrevented()) {
				if(!window.confirm('Chcete opravdu smazat všechny označené záznamy?')) {
					event.preventDefault();
				}
			}
			else {
				event.preventDefault();
			}
		});
		$('.rec-delete', elem).click(function(event) {
			if(!window.confirm('Chcete opravdu smazat tento záznam?')) {
				event.preventDefault();
			}
		});
		$('body').on('click', '.checkall', function() {
			$('.list-table input[type=checkbox]').prop('checked', $(this).is(':checked'));
		});


		// Predvyplni fieldy, ktere nalezne
		var fillFields = function(prefix, values, filledById) {
			$.each(values, function(name, value) {
				var newPrefix = prefix + '[' + name + ']';
				if(typeof(value) === 'object' && value !== null && value !== undefined) {
					fillFields(newPrefix, value, filledById);
				}
				else {
					var field = $('[name="' + newPrefix + '"]');
					// podepiseme pole autorem predvyplneni a dame ho readonly
					field.attr('data-filled-by-id', filledById);
					field.data('original-value', field.val());
					field.data('original-readonly', field.attr('readonly'));
					if(field.closest('.hidden-fields').length <= 0) {
						field.attr('readonly', true);
					}
					field.val(value);
				}
			});
		};

		var rollbackAutocomplete = function(element) {
			// obnovime puvodni hodnoty z predvyplnenych poli pri vyberu
			$('[data-filled-by-id=' + element.attr('id') + ']').each(function() {
				var f = $(this);
				f.val(null); //f.data('original-value'));
				var orRo = f.data('original-readonly');
				f.attr('readonly', orRo === undefined || orRo === null ? false : orRo);
				f.removeAttr('data-filled-by-id');
			});
		};

        // Hleda nejblizsi field predaneho jmena
        var findClosestField = function(name, jqObject) {
            var field = null;
            while(true) {
                var parent = jqObject.parent();
                if(parent.length <= 0) {
                    break;
                }
                var jqObject = parent;
                var input = jqObject.find(
                        'input[name='+name+'],select[name='+name+'],textarea[name='+name+'],' +
                        'input[name*="['+name+']"],select[name*="['+name+']"],textarea[name*="['+name+']"]');
                if(input.length > 0) {
                    field = $(input.get(0));
                    break;
                }
            }
            return field;
        };

		// Set X_ApproveType field and submit the form
		$('button[data-x-approve-button-value]').on('click', function() {
			var field = $(this).data('x-approve-button-field');
			var value = $(this).data('x-approve-button-value');
			var frm = $(this).closest('form');
			frm.find('select[name=' + field + ']').val(value);
			frm.find('input[type=submit],button[type=submit]').trigger('click');
		});

		// AUTOCOMPLETE FIELD
		$('input[data-autocomplete-entity]', elem).each(function() {
			var element = $(this);
            (function(element) {
                var width = element.data('autocomplete-style-width');

                // if not wrapped yet (autocomplete fields in rows)
                if(element.closest('.load-wrapper').length <= 0)
                {
                    if(element.closest('td').length > 0) {
                        // it is in the table, no width specified (width = column width)
                        element.wrap('<div class="load-wrapper"></div>');
                    }
                    else {
                        element.wrap('<div class="load-wrapper" style="width: ' + width +'"></div>');
                    }
                }
                
                var wrapper = element.closest('.load-wrapper');
                var url = element.data('autocomplete-url');
                var oneUrl = element.data('autocomplete-one-url');
                var field = element.data('autocomplete-field');
                var prefillVal = element.data('autocomplete-val');
                var minLength = element.data('autocomplete-min-length');
                var minWidth = element.data('autocomplete-min-width');
                var idParam = element.attr('id');
                // row template
                var rowTemplate = element.data('autocomplete-templatebase64');
                var rowTemplateDecoded = '{DisplayName}';
                if(rowTemplate) {
                    rowTemplateDecoded = $.base64.atob(rowTemplate);
                }
                var displayField = element.data('autocomplete-display-field');

                // loadOne
                var loadOne = function(abraId, searchText, ui) {
                    $.ajax({
                        url: oneUrl.replace('__ABRAID__', abraId),
                        complete: function() {
                            wrapper.removeClass('loading');
                        },
                        success: function(response) {
                            // ulozime hledanou frazi
                            element.data('autocompleteSearch', searchText);
                            // nastavime ID do hidden pole
                            //targetIdField.val(abraId);
                            // Vytvorime (nebo nalezneme jiz vytvoreny) div s vybranou hodnotou a vlozime ho hned za INPUT
                            var asi = element.next('div.autocomplete-selected-item');
                            var asiBtn = asi.next('button.autocomplete-clear-button');
                            var asiWasThere = true;
                            if(asi.length <= 0) {
                                asi = $('<div class="autocomplete-selected-item"></div>');
                                asiBtn = $('<button type="button" class="btn btn-danger autocomplete-clear-button"><i class="icon-remove icon-white"></i></button>');
                                asiWasThere = false;
                            }
                            asi.html(ui === undefined || !ui.item.label ? response[displayField] : ui.item.label);
                            // Pokud tam DIV nebyl, nabindime CLICK event a vlozime ho za input
                            if(!asiWasThere) {
                                asi.css('min-width', element.width());
                                // Pri kliknuti obnovime do hledatka hledanou frazi
                                asi.click(function(event) {
                                    element.val(element.data('autocompleteSearch'));

                                    // obnovime predtim naseptana pole
                                    rollbackAutocomplete(element);

                                    // skryjeme DIV a zobrazime TEXTBOX
                                    asi.hide();
                                    asiBtn.hide();
                                    //targetIdField.val(prevTargetValue);
                                    element.show();
                                    element.focus();
                                    element.trigger('keydown');
                                });

                                // Pri kliknuti na button obnovime take, ale nenaseptavame
                                asiBtn.click(function() {
                                    element.val('');

                                    // obnovime predtim naseptana pole
                                    rollbackAutocomplete(element);

                                    // skryjeme DIV a zobrazime TEXTBOX
                                    asi.hide();
                                    asiBtn.hide();
                                    //targetIdField.val(prevTargetValue);
                                    element.show();
                                    element.focus();
                                });

                                element.after(asi);
                                //asi.after(asiBtn)			// NO MORE RED BUTTON;
                            }
                            // skryjeme input
                            asi.show();
                            asiBtn.show();
                            element.hide();

                            // Predvyplnime FIELDy, ktere najdeme na formulari
                            var fieldsPrefix = element.attr('name').replace('[' + field + '_autocomplete]', '');
                            fillFields(fieldsPrefix, response, element.attr('id'));
                        },
                        error: function() {
                            // TODO: LOG
                        }
                    });
                };
                //var targetIdField = $('input[type=hidden][name=' + element.attr('name').replace('_autocomplete','') + ']');
                //var prevTargetValue = targetIdField.val();
                element.autocomplete({
                    source: function(req, res) {
                        // Kaskada na naseptavaci
                        var fs = null;
                        var cascade = element.data('cascade');
                        var cascadeData = element.data('autocomplete-cascade-data');
                        if(element.data('cascade')) {
                            fs = {};
                            $.each(cascade.filters, function(i, filter) {
                                var source = cascadeData[filter.value];
                                if(source) {
                                    if(typeof(source) === 'object') {
                                        if(source.ID) {
                                            source = source.ID;
                                            fs[filter.column] = source;
                                        }
                                    }
                                    else {
                                        fs[filter.column] = source;
                                    }
                                }
                                // Live value update
                                if(filter.live) {
                                    var fcf = findClosestField(filter.live, element);
                                    fs[filter.column] = fcf.val();
                                }
                            });
                        }
                        var lnk = url
                            .replace('__FILTERS__', fs === null ? '' : JSON.stringify(fs))
                            .replace('__DISPLAY_FIELD__', displayField);
                        $.ajax({
                            url: lnk,
                            data: {
                                term: req.term
                            },
                            success: function(response) {
                                res(response);
                            }
                        });
                    },
                    minLength: minLength,
                    open: function(event, ui) {
                        var li = $('.ui-menu-item[data-for-id=' + $(this).attr('id') + ']');
                        li.closest('ul').css({minWidth: '300px'});
                        li.find('table').css({width: '100%', tableLayout: 'fixed'});
                        li.find('td,th').css({overflow: 'hidden', textOverflow: 'ellipsis' });
                        // minHeight according to where it was opened
                        $('.ui-autocomplete:visible').css({ 'max-height': window.innerHeight - $('.ui-autocomplete:visible').offset().top - 20 });
                    },
                    select: function(event, ui) {
                        var abraId = ui.item.id;
                        var searchText = this.value;
                        wrapper.addClass('loading');
                        loadOne(abraId, searchText, ui);
                    }
                });

                // template for each item in autocomplete
                element.data("ui-autocomplete")._renderItem = function( ul, object ) {

                    var content = rowTemplateDecoded.replace(/\{([a-z0-9_\-\$\.]+)\}/ig, function(match, path) {
                        // access the proper property
                        var pathExpanded = path.split('.');
                        var obj = object.item;
                        while(pathExpanded.length > 0) {
                            var part = pathExpanded.shift();
                            obj = obj[part] === null ? '' : obj[part];
                        }
                        return obj;
                    });

                    var li = $("<li>")
                                .attr("data-value", object.value)
                                .attr("data-for-id", idParam)
                                .css({ display:'block' })
                                .append('<a class="ui-corner-all" id="ui-id-' + object.id + '" tabindex="-1">' + content + '</a>')
                                .appendTo(ul);
                    return li;
                };
                if(prefillVal !== null && $.trim(prefillVal) !== '') {
                    loadOne(prefillVal, '');
                }

            })(element);
		});


		// Cascade Comboboxes
		$('select[data-cascade]', elem).each(function() {
			var el = $(this);
			var cascade = el.data('cascade');
			var cascadeSignal = el.data('cascade-signal');
			var refreshCodelist = function(name, newValue) {
				var fs = {};
				$.each(cascade.filters, function(i, filter) {
					var source = $('input[name="' + filter.value + '"],textarea[name="' + filter.value + '"],select[name="' + filter.value + '"]');
					if(source.length <= 0) {
						source = $('input[name="' + filter.value + '[ID]"],textarea[name="' + filter.value + '[ID]"],select[name="' + filter.value + '[ID]"]');
					}
					if(source.length > 0) {
						fs[filter.column] = name === filter.column ? newValue : source.val();
					}
				});
				$.ajax({
					url: cascadeSignal,
					data: {
						'logic': cascade.operator,
						'class': el.data('cascade-class'),
						'filters': JSON.stringify(fs)
					},
					success: function(response) {
						// Stare options odebrat
						el.find('option').not(':first').remove();
						// Nove options zalozit
						var selected = false;
						$.each(response, function(id, name) {
							var opt = $('<option></option>');
							opt.attr('value', id);
							opt.html(name);
							if(!selected) {
								opt.attr('selected', 'selected');
								selected = true;
							}
							el.append(opt);
						});
					}
				});
			};
			$.each(cascade.filters, function(i, filter) {
				var source = $('input[name="' + filter.value + '"],textarea[name="' + filter.value + '"],select[name="' + filter.value + '"]');
				if(source.length <= 0)
				{
					source = $('input[name="' + filter.value + '[ID]"],textarea[name="' + filter.value + '[ID]"],select[name="' + filter.value + '[ID]"]');
				}
				if(source.length > 0)
				{
					var fbi = source.attr('data-autocompleted');
					if(fbi) {
						$('#' + fbi).on('autocompleteselect', function(event, ui){
							refreshCodelist(filter.column, ui.item.id);
						});
					}
					else {
						source.on('change', function() {
							refreshCodelist(filter.column, $(this).val());
						});
					}
				}
			});
		});

	};


	// --------------------------------------------------------------------------

	var tableRowsBackup = {};
	// backup original copy of last row on each collection table
	$("table[class*='table-index-']").each(function() {
		var tr = $(this).find('tbody tr:last');
		tableRowsBackup[$(this).attr('class')] = {
			counter: 1,
			element: tr.clone(false).get(0)
		};
	});
	
	// COPY OR ADD new row
	var newCollectionRow = function(button, table, copy) {
		var tableObj = $('.' + table);
		var backRow = tableRowsBackup[tableObj.attr('class')];
		// no backuo rows for the table ??
		if(!backRow || !backRow.element) {
			return;
		}
		
		var copiedRow = copy ? button.closest('tr') : null;		
		var sourceRow = $(backRow.element);
		var newRow = sourceRow.clone(false);
		
		// get INDEX from some first input/select/textarea name
		if(copy) {
			var firstName = $(copiedRow.find('select:first,input:first,textarea:first').get(0)).attr('name');
			var firstNameIndexMatch = firstName.match(/\[([0-9]+)\]/);
			var firstNameIndex = firstNameIndexMatch[1];
		}
		
		// inputy v nove row
		var inputs = newRow.find('input,select,textarea');
		// pokud nejake jsou...
		if(inputs.length > 0)
		{
			// Podle prvniho fieldu najdeme ID v ramci radku a naklonujeme tento INPUT
			var firstInput = $(inputs.get(0));
			var form = button.closest('form');
			// nahrazeni IDcek
			var matches = firstInput.attr('name').match(/\[([0-9]+)\]/);
			if(matches.length > 1)
			{
				//console.log(matches);
				var nameParts = firstInput.attr('name').split(matches[0]);
				var rowIdField = form.find('input[type="hidden"][name="' + nameParts[0] + matches[0] + '[ID]"]');
				if(rowIdField.length > 0)
				{
					var newNum = Number(matches[1]) + backRow.counter;
					var rowIdClone = rowIdField.clone(false);
					rowIdClone.val(null);
					// pozmeneni cisla v name a ID atributu
					rowIdClone.attr('name', nameParts[0] + '[' + newNum + '][ID]');
					if(rowIdClone.attr('id')) {
						rowIdClone.attr('id', rowIdClone.attr('id').replace('-' + matches[1] + '-', '-' + newNum + '-'));
					}
					rowIdField.after(rowIdClone);
				}
			}

			// na vsech fieldech v radku zvedneme ID
			// a v pripade duplikace radku nastavime stejne hodnoty
			newRow.find('input,select,textarea').each(function() {
				var field = $(this);

				// hodnoty - kopie
				if(copy) {
					var fieldName = field.attr('name');
					var copiedSearchFieldName = fieldName.replace(/\[([0-9]+)\]/, '[' + firstNameIndex + ']');
					var copiedField = copiedRow.find('[name="' + copiedSearchFieldName + '"]');

					field.val(copiedField.val());
					
					// For Autocomplete - set value for prefill (when initializing the new row)
					if(field.data('autocomplete-entity')) {
						field.data('autocomplete-val', copiedField.val());
					}
					
				}
				
				field.attr('name', field.attr('name').replace(/\[([0-9]+)\]/, function(matches, match) {
					return '[' + (Number(match)+backRow.counter).toString() + ']';
				}));

				// pokud mame ID atribut
				if(field.attr('id')) {
					field.attr('id', field.attr('id').replace(/\-([0-9]+)\-/, function(matches, match) {
						return '-' + (Number(match)+backRow.counter).toString() + '-';
					}));
				}
				
			});

			// Increment num counter
			backRow.counter++;
		}

        // novy radek nebude smazany
        newRow.find('input[type=hidden][name*=__DELETE_FLAG__]').val(0);

		// pridame novy radek
		tableObj.append(newRow);
		//tableObj.find('button[data-delete-row]').attr('disabled', false);
				
		// novy init na novych polickach
		init(newRow);
	};	
	
	// duplicate row - ADD NEW ROW
	$('body').on('click', '[data-clone-row]', function() {
		var button = $(this);
		var table = button.data('clone-row');
		newCollectionRow(button, table, false);
	});
	
	// copy row
	$('body').on('click', '[data-copy-row]', function() {
		var button = $(this);
		var table = button.data('copy-row');		
		newCollectionRow(button, table, true);
	});
	
	// Universal confirm
	$('body').on('click', '[data-confirm-msg]', function(e) {
		var confirmMessage = $(this).data('confirm-msg');
		if(!window.confirm(confirmMessage)) {
			e.preventDefault();
			return;
		}
	});
	
	// delete row
	$('body').on('click', '[data-delete-row]', function() {
		var confirmMessage = $(this).data('confirm-message');
		var row = $(this).closest('tr');
		var input = row.find('input,select,textarea');
		var siblings = row.siblings('tr:visible');
		// if we have more rows to be left, we can safely delete this one
		if(input.length > 0)
		{
			var attrName = $(input.get(0)).attr('name');
			var matches = attrName.match(/\[([0-9]+)\]/);
			if(matches || attrName.match('^deletableEntry'))
			{
				//var part = matches.shift();
				//var attrNameParts = attrName.split(part);
				//attrNameParts.pop();
				//attrNameParts.push('[ID]');
				//var idName = attrNameParts.join(part);
                if(window.confirm(confirmMessage))
                {
                    let valEl = row.find('*[data-aggregate="sum"]');
                    valEl.val(0);
                    valEl.trigger('change');
                    
//                    if(siblings.length > 0)
//                    {
						row.find('input[type=hidden][name*=__DELETE_FLAG__]').val(1);
						row.hide();
//					}
//                    else
//                    {
//                        row.find('input,select,textarea').each(function() {
//                            $(this).val(null);
//                        });
//
//						// vymazat (tedy nastavit na -999) ID
//						var idName = row.find('input[name$="[__DELETE_FLAG__]"]').attr('name').replace('__DELETE_FLAG__', 'ID');
//						row.closest('form').find('input[name="' + idName + '"]').val('-999');
//                    }
				}
			}
		}
	});


	init();

	// ajax loader show on submit
	$('body').on('submit', 'form', function(e) {
		if($(this).data('skiploader')) {
			$(this).data('skiploader', false);
		} else {
			if(!e.isDefaultPrevented()) {
				$('.ajax-loader').show();
			}
		}
	});

	// ajax loader on A click - some responses could take much time or could be a pagination and so on...
	$('a:not(.download-link, [target=_blank])').on('click', function(e) {
		if($(this).data('skiploader')) {
			$(this).data('skiploader', false);
		} else if(!e.isDefaultPrevented()) {
			var href = $(this).attr('href');
			if($.trim(href) == '#' || $.trim(href) == '' || href.substring(0,1) == '#') {
				return;
			}
            setTimeout(function() {
                $('.ajax-loader').show();
            }, TIMEOUT);
            setTimeout(function() {
                var msg = $('body').data('loader-message');
                $('.ajax-loader p').text(msg).css('opacity', 1);
            }, TIMEOUT_MESSAGE);
		}
	});

	// ajax loader on leave - some responses could take much time or could be a pagination and so on...
//	$(window).on('beforeunload', function(){
//		setTimeout(function(){
//			$('.ajax-loader').show();
//		}, TIMEOUT);
//		setTimeout(function(){
//			var msg = $('body').data('loader-message');
//			$('.ajax-loader p').text(msg).css('opacity', 1);
//		}, TIMEOUT_MESSAGE);		
//	});

	// ajax loader
	$(document).ajaxStart(function() {
		$('.ajax-loader').show();
	});
	$(document).ajaxStop(function() {
		$('.ajax-loader').hide();
		$('.ajax-loader > p').css('opacity', 0);
	});

	$('body').on('click', '[name=massprint],.skiploader', function() {
		$(this).closest('form').data('skiploader', true);
	});

    $('body').on('click', '[data-confirm]', function(e) {
        var msg = $(this).data('confirm');
        if(!window.confirm(msg)) {
            event.preventDefault();
        }
    });
    
	// thead on top
    if($(document).width() >= 960) {
        $('.list-table').floatThead({
            position: 'fixed',
            top: $('header').height(),
            scrollContainer: function($table){
                return $table.closest('.module-content');
            }
        });
    }

    // HTTPs test
    if(location.protocol !== "https:") {
        var headerWarning = $('<div class="header-warning"></div>');
        var show = $('body header').data('http-warning-show');
        if(show == 1) {
            headerWarning.html($('body header').data('http-warning'));
            $('body .module-content').prepend(headerWarning);
        }
    }


	//-------------- Custom trigger
    //$('[data-visibility-toggle]').live('click', function(e) {
    //$("body").on("click","[data-visibility-toggle]",function(){

    $(document).on("click","[data-visibility-toggle]",function(){
        //e.preventDefault();
        var target = $(this).data("visibilityToggle");
        $("body").find("[data-visibility-target='" + target + "']").each(function() {
            if ( $(this).css('visibility') == 'hidden' )
                $(this).css('visibility','visible');
            else
                $(this).css('visibility','hidden');
        });
        $(window).trigger('resize');    // floating table header fix
    });

    /**
     * Show/Hide handler
     */
    (function ($) {
        $.each(['show', 'hide'], function (i, ev) {
            var el = $.fn[ev];
            $.fn[ev] = function () {
                this.trigger(ev);
                return el.apply(this, arguments);
            };
        });
    })(jQuery);



    var postCollapse = function(parent, setCookie) {
        var isCollapsed = parent.hasClass('collapsed');
        if(setCookie) {
            Cookies.set('modulemenu', isCollapsed ? 'collapsed' : 'full');
        }
        if(isCollapsed) {
            parent.find('.module-menu-item a').each(function() {
                var link = $(this);
                link.attr({ title: link.find('span').html() });
            });
        }
        else {
            parent.find('.module-menu-item a').each(function() {
                var link = $(this);
                link.attr({ title: null });
            });
        }
        // Safari fix
        window.setTimeout(function() {
            $('.button-form-row').css({'display': 'none'}); // 'position': 'static', 
            window.setTimeout(function() {
                $('.button-form-row').css({'display': 'block'}); // 'position': 'fixed', 
            }, 20);
        }, 20);
        
    };

    $('.expand-collapse').on('click', function(e) {
        e.preventDefault();
        var parent = $(this).parent();
        parent.toggleClass('collapsed');
        $('.logo-wrapper').toggleClass('collapsed');
        $(window).trigger('resize');
        postCollapse(parent, true);
    });
    
    postCollapse($('.module-menu'), false);

});

	
var cmptd = new Computed(culture_lang, culture_settings.thousand, culture_settings.decimal);

// agregace
let calcSum = function(aggregatedControl){
    
    let culture = $('body').data('culture');
    
    let col = aggregatedControl.closest('td').index();
    let sum = 0;
    aggregatedControl.closest('tbody').find('tr').each(function(){
        let el = $(this).find('td').eq(col).find('input');
        sum += parseFloat(el.val().replace(/\s+/g, '').replace(',', '.'));
    });
    let tfoot = aggregatedControl.closest('table').find('tfoot');
    tfoot.find('td').eq(col).find('.aggregated-value')
        .text(sum.toLocaleString(culture, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }));
    
    cmptd.calculate();

    tfoot.show();
};
// on load
let calcSumInit = function(){
    cmptd.calculate();

    $('*[data-aggregate="sum"]').each(function(){
        $(this).closest('table').attr('data-aggregated-table', true);
    });
    $('table[data-aggregated-table="true"]').each(function(){
        $(this).find('tbody tr').first().find('*[data-aggregate="sum"]').each(function(){
            calcSum($(this));
        });
    });
};

// on change
$(document).on('change', '*[data-aggregate="sum"]', function(){
    calcSum($(this));
});

// on change - recalculate formulas on any change
$(document).on('change', '[data-fieldinfo-path]', function(){
    var usedComputedFields = cmptd.calculate();
    // what if this computed field is also aggregation="sum" field in the table ?
    $.each(usedComputedFields, function(i, computed) {
        var $computed = $(computed);
        var agg = $computed.data('aggregate');
        if(agg === 'sum') {
            calcSum($computed);
        }
    });
});
