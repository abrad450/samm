<?php
// absolute filesystem path to this web root
define('WWW_DIR', __DIR__);

// absolute filesystem path to the application root
define('APP_DIR', WWW_DIR . '/../app');

// absolute filsystem path to the data directory root
define('DATA_DIR', WWW_DIR . '/../data');

// absolute filesystem path to the libraries
define('LIBS_DIR', WWW_DIR . '/../libs');

// absolute filesystem path to the log
define('LOG_DIR', WWW_DIR . '/../log_app');

// absolute filesystem path to the temp
define('TEMP_DIR', WWW_DIR . '/../temp');

// ABRA SERVICE CACHE DIR
define('ABRA_SERVICE_CACHE_DIR', TEMP_DIR.'/AbraService-classes');

// uncomment this line if you must temporarily take down your site for maintenance
if(is_file(__DIR__.'/maintenance.flag') && (empty($_SERVER['REQUEST_URI']) || strpos($_SERVER['REQUEST_URI'], 'core/debug/moff') === FALSE))
{
	require APP_DIR . '/templates/maintenance.phtml';
}

$container = require APP_DIR.'/bootstrap.php';

$container->getService('application')->run();
