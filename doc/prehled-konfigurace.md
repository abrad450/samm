# field
Pole formuláře

## field > SQLField

Používá se u formuláře pro filtry. Je to SQL pole, za které se bude v Abře filtrovat

    <field>
      <name>FilterConfirmationState</name>
      <type>dtString</type>
      <caption>Stav schválení</caption>
      <display>editable</display>
      <control>{"element": {"_class": "Select","items": [{"_class": "SelectItem","value": 0,"caption": "Neurčeno"},{"_class": "SelectItem","value": 1,"caption": "Ke kontrole"},{"_class": "SelectItem","value": 2,"caption": "Zkontrolováno"},{"_class": "SelectItem","value": 3,"caption": "Zamítnuto"}]}}</control>
      <style>
        <width>75%</width>
      </style>
      <SQLField>FilterConfirmationState</SQLField>
    </field>

---

## field > style
	
Úprava CSS stylu formulářového prvku / buňky tabulky. Je možno použít jakýkoliv CSS styl v podobě *&lt;styl&gt;hodnota&lt;/styl&gt;*

	<field>
      <style>
        <width>75%</width>
        <border-color>#F04A6A</border-color>
      </style>
    </field>

**Pozor!** Historický nános. Pokud bude předáno:

 - `<visible>N</visible>` převede se na **display: none**
 - `<visible>A</visible>` převede se na **display: table-cell**
 - `<background>blue</background>` převede se na **background-color: blue;**
 - `<border>blue</border>` převede se na **border-color: blue;**
 - `<textAlign>right</textAlign>` převede se na **text-align: right;**
 - `<width>400</width>` převede se na **width: 400px;** (přidá automaticky px jednotky)
 - `<height>600</height>` převede se na **height: 600px;** (přidá automaticky px jednotky)

---

## field > sortfield

Pro tabulkový výpis (list). Pole, podle kterého se bude řadit.

	<field>
	  <name>FirmName</name>
	  <type>dtString</type>
	  <caption>Název Firmy</caption>
	  <style>
	    <width>20%</width>
	  </style>
	  <sortfield>Firm</sortfield>
	</field>

---

## field > control
Speciální vlastnosti pole formuláře

### field > control > element
Speciální element pro vykreslení pole formuláře
		
#### field > control > element > Link
			
Obecný odkaz na definovaný href s propsáním hodnot z fieldů do placeholderů {NAZEV_FIELDU}
	
	<field>
	  <name>FileName</name>
	  <type>dtString</type>
	  <caption>Název souboru</caption>
	  <display>text    </display>
	  <control>{ "element": {"_class": "Link","href": "/raw/documentcontent/read/{ID}" }}</control>
	</field>

parametry JSONu:

 - **"_class": "Link"** - povinná identifikace objektu
 - **"href": "/raw/documentcontent/read/{ID}"** - URL, kam odkaz bude směrovat,
 - **"newWindow": true** - zda se odkaz otevře v novém okně nebo ne
 - **"type": "link"** - Buď **"link"** nebo **"download"**, podle toho, co je na cílové URL
 - **"style": {"color": "blue", "font-size": "110%", ...}** - možnost nastylovat link podle CSS pravidel


#### field > control > element > Route
			
Odkaz na routu, vykreslí se standardní &lt;a&gt;&lt;/a&gt; odkaz směrující na definovanou routu webové aplikace. Používá se v kolekci položek dokladu.
	
	<field>
	  <name>Routa</name>
	  <type>dtString</type>
	  <caption>Routa</caption>
	  <display>text    </display>
	  <control>{ "element": {"_class": "Route", "bo": "TNxReceivedInvoiceEdit", "id": "{id}" }}</control>
	</field>

parametry JSONu:

 - **\_class** - povinná identifikace objektu
 - **bo** - bussiness objekt, na který bude routa směrovat,
 - **id** - id bussiness objektu, buď konkrétní nebo "{id}" placeholder, který se nahradí za ID konkrétního záznamu položky z kolekce
 - **targetBlank** - Nepovinné (zda bude odkaz veden do nového okna) true/false
 - **module** - Nepovinné (identifikace modulu ve webové aplikaci) - výchozí je NULL (není modul)
 - **presenter** - Nepovinné (identifikace presenteru ve webové aplikaci) - výchozí je "Default" 
 - **action** - Nepovinné (akce v presenteru ve webové aplikaci) - výchozí je "default"


#### field > control > element > ActionButton
			
Akční tlačítko, které uloží formulář a přesměruje jinam.
	
	<field>
	  <name>ActionButton</name>
	  <type>dtString</type>
	  <caption>Třeba přidat přílohu</caption>
	  <display>text    </display>
	  <control>{ "element": {"_class": "ActionButton", "bo": "?", "ref": "?", "action": "?" }}</control>
	</field>

parametry JSONu:
 - **\_class** - povinná identifikace objektu
 - **bo** - TNxReceivedInvoiceEdit
 - **ref** - kam přesměrovat
 - **action** - akce v rámci presenteru ve webové aplikaci
 - **id** - ID nebo {ID} placeholder
 - **autosave** - true/false, zda se uloží data


#### field > control > element > Preview
				
Náhled PDF dokumentu nebo obrázku (png,jpg,gif)
	
	<field>
	  <name>AttachmentsPreview</name>
	  <type>dtCollection</type>
	  <caption>Přílohy dokladu</caption>
	  <display>text    </display>
	  <control>{ "element": {"_class": "Preview", "href": "/raw/documentcontent/read/{ID}", "height":600}}</control>
	</field>

parametry JSONu:

 - **"_class": "Preview"** - povinná identifikace objektu
 - **"href": "/raw/documentcontent/read/{ID}"** - URL, kam odkaz bude směrovat,
 - *"height": 600 - výška náhledu dokumentu (již se nepoužívá, dopočítává se automaticky)* 


#### field > control > element > Select

Výběr z definovaných možností

    <field>
      <name>X_ApproveType</name>
      <type>dtInteger</type>
      <caption>Stav</caption>
      <display>editable</display>
      <control>{"element": {"_class": "Select","items": [{"_class": "SelectItem","value": 2,"caption": "Zamítnout"},{"_class": "SelectItem","value": 3,"caption": "Vrátit k doplnění"},{"_class": "SelectItem","value": 1,"caption": "Schválit","buttontype":"primary" }]}}</control>
    </field>		

lépe zformátovaný JSON

	{
		"element":{
			"_class":"Select",
			"items":[
				{
					"_class":"SelectItem",
					"value":2,
					"caption":"Zamítnout"
				},
				{
					"_class":"SelectItem",
					"value":3,
					"caption":"Vrátit k doplnění"
				},
				{
					"_class":"SelectItem",
					"value":1,
					"caption":"Schválit",
					"buttontype":"primary"
				}
			]
		}
	}

parametry JSONu v items:

 - **caption** = Popisek
 - **value** = nastavovaná hodnota
 - **buttontype** = "primary"/"secondary" (obecně "cokoliv", co má v bootstrapu třídu "btn-cokoliv") - 

**Pozor!** Nastavení **buttontype** se použije pouze pro pole s názvem **X_ApproveType**, neboť toto pole se nevykreslí nikdy jako Select, ale jako sada tlačítek pro schválení / zamítnutí dokumentu. 


### field > control > settings

Nastavení pole formuláře
		
#### field > control > settings > aggregation
			
Součtový řádek bude zobrazen (a vypočten) pod tabulkou pro tento sloupec
			
	<field>
	  <name>TAmount</name>
	  <type>dtFloat</type>
	  <caption>Celkem</caption>
	  <display>text    </display>
	  <control>{"settings":{"aggregation": "sum"}}</control>
	</field>


#### field > control > settings > autocomplete

Konfigurace našeptávače pro výběr (např. firmy)

    <field>
      <name>Firm_ID.ID</name>
      <type>dtString</type>
      <caption>Výběr dodavatele</caption>
      <display>viewonly</display>
      <control>{ "settings": {"autocomplete": true,"placeholder":"Zadejte jméno nebo IČ firmy", "autocompleteMinLength":3,"autocompleteMinWidth":"400px","templateBase64":"PHRhYmxlPg0KICAgICAgICAgICAgICAgIDx0cj48dGQgd2lkdGg9IjcwJSI+PGI+e05hbWV9PC9iPjwvdGQ+PHRkIHdpZHRoPSIzMCUiPntPcmdJZGVudE51bWJlcn08L3RkPjwvdHI+DQogICAgICAgICAgICAgICAgPHRyPjx0ZCBjb2xzcGFuPSIyIj48c21hbGw+e1Jlc2lkZW5jZUFkZHJlc3NfSUQuU3RyZWV0fTxicj57UmVzaWRlbmNlQWRkcmVzc19JRC5Qb3N0Q29kZX0ge1Jlc2lkZW5jZUFkZHJlc3NfSUQuQ2l0eX08L3NtYWxsPjwvdGQ+PC90cj4NCjwvdGFibGU+"}}</control>
      <style>
        <border-color>#F04A6A</border-color>
      </style>
    </field>

Formátovaný JSON:

	{
		"settings":{
			"autocomplete":true,
			"placeholder":"Zadejte jméno nebo IČ firmy",
			"autocompleteMinLength":3,
			"autocompleteMinWidth":"400px",
			"templateBase64":"PHRhYmxlPg0KICAgICAgICAgICAgICAgIDx0cj48dGQgd2lkdGg9IjcwJSI+PGI+e05hbWV9PC9iPjwvdGQ+PHRkIHdpZHRoPSIzMCUiPntPcmdJZGVudE51bWJlcn08L3RkPjwvdHI+DQogICAgICAgICAgICAgICAgPHRyPjx0ZCBjb2xzcGFuPSIyIj48c21hbGw+e1Jlc2lkZW5jZUFkZHJlc3NfSUQuU3RyZWV0fTxicj57UmVzaWRlbmNlQWRkcmVzc19JRC5Qb3N0Q29kZX0ge1Jlc2lkZW5jZUFkZHJlc3NfSUQuQ2l0eX08L3NtYWxsPjwvdGQ+PC90cj4NCjwvdGFibGU+"
		}
	}

 - **autocomplete: true** zapíná našeptávač
 - **placeholder: "text"** zobrazí text v poli, kde se píše pro našeptání hodnot z Abry
 - **autocompleteMinLength** minimální počet znaků, při kterém se spustí našeptávání
 - **autocompleteMinWidth** minimální šířka našeptávače 
 - **templateBase64** obsahuje HTML šablonu řádku našeptávače zakódovanou v base64:

		<table>
		    <tr><td width="70%"><b>{Name}</b></td><td width="30%">{OrgIdentNumber}</td></tr>
		    <tr><td colspan="2"><small>{ResidenceAddress_ID.Street}<br>{ResidenceAddress_ID.PostCode} {ResidenceAddress_ID.City}</small></td></tr>
		</table>

	Mohou v ní být opět {Placeholder}y pro propis hodnot z našeptaného objektu (v tomto případě firmy)


#### field > control > settings > computedField

Počítaná políčka. Výpočet probíhá dynamicky v prohlížeči.

**Typ "aggregation":** Vezme všechna pole ("Rows.TAmount") a provede jejich součet. Použití pro součet buněk ve sloupci tabulky.

    <field>
      <name>SumRows</name>
      <type>dtFloat</type>
      <caption>Součet řádek</caption>
      <display>text    </display>
      <control>{ "settings": { "computedField": { "type": "aggregation", "value": "Rows.TAmount"}}}</control>
    </field>

**Typ "formula":** Obecný vzorec, který se dosadí (každý {Placeholder} se nahradí za aktuální hodnotu pole) a spočítá se.

    <field>
      <name>Difference</name>
      <type>dtFloat</type>
      <caption>Rozdíl výpočtu</caption>
      <display>text    </display>
      <control>{ "settings": { "computedField": { "type": "formula", "value": "({SumRows}-{Amount})/2 * ({SumRows}/{Amount})" }}}</control>
    </field>



#### field > control > cascade

Kaskádní selecty

    <field>
      <name>FirmOffice_ID</name>
      <type>dtStringReference</type>
      <caption>Provozovna</caption>
      <display>editable</display>
      <control>{"cascade":{"operator":"and","filters":[{"value":"Firm_ID","column":"Firm_ID","operator":"="}]}}</control>
      <style>
        <width>75%</width>
      </style>
    </field>

zformátovaný JSON:
 
	{
		"cascade":{
			"operator":"and",
			"filters":[
				{
					"value":"Firm_ID",
					"column":"Firm_ID",
					"operator":"="
				}
			]
		}
	}

Do "value" se vloží hodnota z pole, které je tam uvedeno, tj. aktuální vybraná hodnota ve Firm_ID a tím se složí filtr na číselník provozoven.


### field > control > rules

Validační pravidla pro odesílaná pole. Řídí se podle pravidel Nette, včetně podmínek:

[https://doc.nette.org/cs/3.0/form-validation](https://doc.nette.org/cs/3.0/form-validation)


    <field>
      <name>X_ApproveDescription</name>
      <type>dtNote</type>
      <caption>Komentář</caption>
      <display>editable</display>
      <control>{"rules": [{"_class": "ConditionOn","field": "X_ApproveType",
		"action": "EQUALS","value": "2",
		"rules": [{"_class": "Rule","field": "X_ApproveDescription","action":
		"FILLED","error": "Musíte zadat důvod neschválení!"}]}]}
	  </control>
      <style>
        <width>75%</width>
        <height>50px</height>
      </style>
    </field>

Lépe zformátovaný JSON

	{
		"rules":[
			{
				"_class":"ConditionOn",
				"field":"X_ApproveType",
				"action":"EQUALS",
				"value":"2",
				"rules":[
					{
						"_class":"Rule",
						"field":"X_ApproveDescription",
						"action":"FILLED",
						"error":"Musíte zadat důvod neschválení!"
					}
				]
			}
		]
	}

***Vysvětlení:** Pokud je pole X_ApproveType (což je v tomto případě selectbox s hodnotami) rovno hodnotě 2 (je vybrána položka "Zamítnout"), aplikují se pravidla definovaná v "rules". Zde je jedno pravidlo a sice, že pole X_ApproveDescription musí být vyplněno. Pokud nebude vyplněno, vyskočí validační hláška, která je definovaná v "error".*




