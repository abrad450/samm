<?php

use Nette\Utils\Json;

/**
 * Style
 */
class Style extends \Abra\Service\Entity
{

	/**
	 * Podminky, kdy se styl uplatni
	 * 
	 * @var Abra\Service\FilterGroup
	 */
	protected $filterGroup = NULL;
	
	/**
	 * Style hash
	 * 
	 * @var array
	 */
	protected $styleHash = array();
	
    /**
     * Style name
     * 
     * @var string
     */
    protected $styleName = '';
    
    /**
     * Get Style name
     * 
     * @return string
     */
    public function getStyleName()
    {
        return $this->styleName;
    }
       
    
	/**
	 * Constructor
	 * 
	 * @param mixed $data
	 * @param string $namespace
	 */
	public function __construct($data, $namespace)
	{
		parent::__construct($data, $namespace);
		if(!is_array($data)) {
			$data = Json::decode(Json::encode($data), Json::FORCE_ARRAY);
		}
		if(!empty($data['filters'])) {
			$this->filterGroup = Abra\Service\FilterGroup::parseFrom($data['filters']);
			unset($data['filters']);
		}
        if(!empty($data['name'])) {
            $this->styleName = $data['name'];
        }
		unset($data['name']);
		foreach($data as $key => $value) {
			$this->styleHash[$key] = $value;
		}
	}

	/**
	 * Has this object css style for given data ? (based of Filters)
	 * 
	 * @param \Abra\Service\Entity $data
	 * @return bool
	 */
	public function hasStyleFor($data)
	{
		// Pokud nejsou definovane zadne podminky, tak se styl uplatni
		if(empty($this->filterGroup))
		{
			return TRUE;
		}
		// Jinak musime vyhodnotit podminky
		return $this->filterGroup->evaluateFor($data);
	}

	/**
	 * @return string
	 */
	public function getCss()
	{
		$css = array();
		foreach($this->styleHash as $prop => $val)
		{
			$css[] = $prop.':'.htmlspecialchars($val);
		}
		return implode(';', $css);
	}
	
}