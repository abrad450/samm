<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Module Message
 */
class Message extends \Abra\Service\Entity
{
    
	/**
	 * @type dtString
	 * @length 20000
	 */	    
    protected $text;
    
	/**
	 * @type dtString
	 * @length 20
	 */	    
    protected $type;
    
	/**
	 * @var array
	 */	    
    protected $actions;
    
	/**
	 * @type dtBoolean
	 * @length 1
	 */
    protected $visible;
    
	/**
	 * @type dtBoolean
	 * @length 1
	 */
    protected $local = false;
    
    
	/**
	 * Constructor
	 * @param type $data
	 * @param type $namespace
	 */
	public function __construct($data)
	{
		parent::__construct($data);
        
		if(!empty($data->actions)) {
            $this->actions = explode(',', $data->actions);
		}
	}
}
