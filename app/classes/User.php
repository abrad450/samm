<?php
/**
 * Description of User
 */
class User extends \Abra\Service\Entity
{

	/**
	 * @type dtString
	 * @length 10
	 * @readonly true
	 * @id true
	 */
	protected $id;

	/**
	 * @type dtString
	 * @length 100
	 */
	protected $name;

	/**
	 * @type dtString
	 * @length 100
	 */
	protected $email;

	/**
	 * @type dtInteger
	 * @length 10
	 */
	protected $limit = 20;

	/**
	 * @type dtString
	 * @length 200
	 */
	protected $companyName;



    /**
     * Vrati inicialu/y
     *
     *
     * @return String
     */
    public function getInicials(){
        $splitted = explode(" ", $this->name);
        $result = $splitted[0][0];

        if (count($splitted) > 1){
            $result .= $splitted[count($splitted) - 1][0];
        }

        return $result;
    }
}