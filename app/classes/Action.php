<?php
/**
 * Module Action
 */
class Action extends \Abra\Service\Entity
{
	
	/**
	 * @type dtString
	 * @length 200
	 * @id true
	 */	
	protected $name;

	/**
	 * @type dtString
	 * @length 200
	 */
	protected $caption;
	
	/**
	 * @type dtBoolean
	 * @length 1
	 */
	protected $active;
	
	/**
	 * Object, se kterym se ma pracovat (prilohy)
	 * @type dtString
	 * @length 200
	 */
	protected $object;
	
	/**
	 * FilterGroup - podminene zobrazeni akce
	 * @var Abra\Service\FilterGroup
	 */
	protected $filterGroup;
	
	/**
	 * Warning message - (form => view)
	 * @type dtString
	 * @length 500
	 */
	protected $warningmessage;
	
	/**
	 * Constructor
	 * 
	 * @param stdClass $data
	 * @param string $namespace
	 */
	public function __construct($data, $namespace)
	{
		parent::__construct($data, $namespace);
		// podminky
		if(!empty($data->filters))
		{
			$this->filterGroup = Abra\Service\FilterGroup::parseFrom($data->filters);
		}
	}
	
	
	/**
	 * Is this action allowed for given data ?
	 * 
	 * @param \Abra\Service\Entity $data
	 * @return bool
	 */
	public function isAllowedFor($data)
	{
		// Pokud nejsou definovane zadne podminky, tak se styl uplatni
		if(empty($this->filterGroup))
		{
			return TRUE;
		}
		// Jinak musime vyhodnotit podminky
		return $this->filterGroup->evaluateFor($data);
	}	

}