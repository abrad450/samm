<?php

/**
 * Culture
 * 
 * @property string $lang
 * @property-read array $availableCultures
 * @property-read array $settings
 */
class Culture implements \ArrayAccess
{
    use \Nette\SmartObject;
    
    /**
     * Culture
     * 
     * @var Culture
     */
    protected $culture;
    
    /**
     * Lang
     * 
     * @var string
     */
    protected $lang;
    

    /**
     * Culture
     * 
     * @param array $culture
     */
    public function __construct($culture)
    {
        $this->culture = $culture;
    }

    /**
     * Lang code
     * 
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }
    
    /**
     * Get code
     * 
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }
    
    
    /**
     * Get available cultures
     * 
     * @return string[]
     */
    public function getAvailableCultures()
    {
        return array_keys($this->culture);
    }
    
    
    /**
     * Culture settings
     * @return array
     */
    public function getSettings()
    {
        if(!$this->isCultureAvailable($this->lang)) {
            return array(
                'timeZone' => '',
                'jQueryUiCode' => '',
                'date' => '',
                'dateTime' => '',
                'decimal' => '',
                'thousand' => ''
            );
        }
        return array_intersect_key($this->culture[$this->lang], array(
			'timeZone' => true,
			'jQueryUiCode' => true,
			'date' => true,
			'dateTime' => true,
			'decimal' => true,
			'thousand' => true
        ));
    }
    
    /**
     * Is given culture available ?
     * 
     * @param string $culture
     * @return bool
     */
    public function isCultureAvailable($culture)
    {
        return array_key_exists($culture, $this->culture);
    }
    
    
    public function offsetExists($offset)
    {
        return isset($this->culture[$this->lang][$offset]);
    }

    public function offsetGet($offset)
    {
        if(!$this->offsetExists($offset)) {
            throw new OutOfBoundsException('Invalid Culture setting "'.$offset.'"');
        }
        return $this->culture[$this->lang][$offset];
    }

    public function offsetSet($offset, $value)
    {
        throw new \Nette\NotImplementedException('Not implemented');
    }

    public function offsetUnset($offset)
    {
        throw new \Nette\NotImplementedException('Not implemented');
    }

}