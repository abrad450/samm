<?php
/**
 * Description of Module
 * 
 * @property-read array $messages Messages
 */
class Module extends \Abra\Service\Entity
{

	/**
	 * Actions HASH-MAP
	 * @var array
	 */
	protected $actionsHash = array();
	
	/**
	 * Styles collection
	 * @var array
	 */
	protected $styles = array();
	
    /**
     * MEssages collection
     * @var array<Message>
     */
    protected $messages = array();
    
	/**
	 * @type dtString
	 * @length 100
	 * @readonly true
	 * @id true
	 */	
	protected $name;	

	/**
	 * @type dtString
	 * @length 100
	 */	
	protected $caption;
    
	/**
	 * @type dtString
	 * @length 100
	 */	
	protected $iconfilename;
    
	/**
	 * @type dtBoolean
	 * @length 100
	 */	
	protected $splitbefore;
	
	/**
	 * @type dtString
	 * @length 300
	 */	
	protected $description;
	
	/**
	 * @type dtOwnedCollection
	 * @length 1
	 * @list string
	 */
	protected $collections;
	
	/**
	 * @type dtOwnedCollection
	 * @length 1
	 * @list Action
	 */
	protected $actions;
	
    /**
     * External URL
     * 
     * @type dtString
     * @length 1000
     */
    protected $externalurl;
	
	
	/**
	 * Metoda vrati true, pokud je povolena nejaka hromadna akce
	 * @return boolean
	 */
	public function isMassAllowed()
	{
		$this->fillActions();
		return $this->isAllowed('massform') || $this->isAllowed('massdelete') || $this->isAllowed('massprint');
	}
	
	/**
	 * Returns TRUE if the given $action is allowed for this module
	 * @param string $action Action ident
	 * @return boolean
	 */
	public function isAllowed($action)
	{
		$this->fillActions();
		return isset($this->actionsHash[$action]) ? $this->actionsHash[$action]->active : FALSE;
	}
	
	/**
	 * Vrati caption pro predanou akci
	 * @param string $action Ident akce
	 * @return boolean
	 */
	public function getCaption($action)
	{
		$this->fillActions();
		return isset($this->actionsHash[$action]) ? $this->actionsHash[$action]->caption : '???';
	}
	
	/**
	 * Constructor
	 * @param type $data
	 * @param type $namespace
	 */
	public function __construct($data, $namespace)
	{
		parent::__construct($data, $namespace);
		// Style array
		$styleArray = array();
		if(!empty($data->styles->style))
		{
			$styleFixed = is_array($data->styles->style) ? $data->styles->style : array($data->styles->style);
			foreach($styleFixed as $style)
			{
				$styleObj = new Style($style, $namespace);
				$styleArray[] = $styleObj;
			}
		}
		$this->styles = $styleArray;
        
        // message array
        $messageArray = array();
        if(!empty($data->messages) && !empty($data->messages->message))
		{
			$messagesFixed = is_array($data->messages->message) ? $data->messages->message : array($data->messages->message);
			foreach($messagesFixed as $message)
			{
				$messageObj = new Message($message, $namespace);
				$messageArray[] = $messageObj;
			}
		}
        $this->messages = $messageArray;
	}
	
	
	/**
	 * GETTER fixed - fillActions
	 * 
	 * @param string $name Property name
	 * @return mixed
	 */
	public function &__get($name)
	{
		if($name === 'actionsHash')
		{
			$this->fillActions();
		}
		return parent::__get($name);
	}
	
	
	/**
	 * Fill Actions
	 */
	protected function fillActions()
	{
		if(empty($this->actionsHash))
		{
			$this->actionsHash = array();
			foreach($this->actions as $act)
			{
				$this->actionsHash[$act->name] = $act;
			}
		}
	}
	
	/**
	 * Aplikuje styly podle predanych dat (podle definovanych podminek u modulu)
	 * 
	 * @param \Abra\Service\Entity $data
	 * @return string inline css style zapis
	 */
	public function applyStyles($data)
	{
		$css = array();
		foreach($this->styles as $style)
		{
			/* @var $style Style */
			if($style->hasStyleFor($data))
			{
				$css[] = $style->getCss();
			}
		}
		return empty($css) ? '' : " style=".implode(';', $css);
	}

	/**
	 * Zjisti, zda je akce povolena pro predana data (podle definovanych podminek u modulu)
	 * 
	 * @param Action $action
	 * @param \Abra\Service\Entity $data
	 * @return bool
	 */
	public function isActionAllowedFor($action, $data)
	{
		$this->fillActions();
		return isset($this->actionsHash[$action]) ? $this->actionsHash[$action]->isAllowedFor($data) : FALSE;
	}
	
}
