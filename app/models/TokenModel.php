<?php

use Nette\Database\Context;

/**
 * Token Model
 */
class TokenModel
{
    /**
     * Context
     * 
     * @var Context
     */
    protected $db;

    /**
     * HTTP request
     * 
     * @var Nette\Http\Request
     */
    protected $request;
    
    /**
     * Constructor
     * 
     * @param Context $db
     * @param Nette\Http\Request $request
     */
    public function __construct(Context $db, Nette\Http\Request $request)
    {
        $this->db = $db;
        $this->request = $request;
    }
    
    
    /**
     * Get tokens selection
     * 
     * @return \Nette\Database\Table\Selection
     */
    public function get()
    {
        return $this->db->table('token');
    }
    
    /**
     * Get one GUID
     * 
     * @param string $guid
     * @param bool $need Need ?
     * @return object
     */
    public function getOne($guid, $need = TRUE)
    {
        $token = $this->get()->where('LOWER(guid) = ?', strtolower($guid))->fetch();
        if(empty($token)) {
            if($need) {
                throw new InvalidArgumentException('Token not found: '.$guid);
            }
            return NULL;
        }
        $tokenObj = (object)$token->toArray();
        
        // DateTime columns
        $this->parseDateTimeValue($tokenObj, 'validity');
        $this->parseDateTimeValue($tokenObj, 'created');
        $this->parseDateTimeValue($tokenObj, 'used');
        
        return $tokenObj;
    }
    
    /**
     * Nastavi token o predanem guidu jako pouzity uzivatelem UserId a UserName.
     * Pokud jsou pod tokenem SUBtokeny (email => list => detail),
     * nastavi je taky jako pouzite stejnym zpusobem
     * 
     * @param string $guid
     * @param string $userId
     * @param string $userName
     * @return int Tokens count that was set as used
     */
    public function setUsed($guid, $userId, $userName)
    {
        $count = 0;
        $userIP = $this->request->getRemoteAddress();
        $this->get()->where('LOWER(guid) = ? AND used IS NULL', strtolower($guid))->update([
            'used' => new \DateTime(),
            'used_user_id' => $userId,
            'used_user_name' => $userName,
            'used_user_ip' => $userIP,
        ]);
        $count++;
        $children = $this->get()->where('LOWER(parent_guid) = ? AND used IS NULL', strtolower($guid));
        foreach($children as $subToken) {
            $count += $this->setUsed($subToken->guid, $userId, $userName);
        }
        return $count;
    }
    
    /**
     * Get the whole tree of tokens currently stored in DB
     * 
     * @param callable $modifyCallback
     * @return array
     */
    public function getTree($modifyCallback = NULL)
    {
        $tokens = $this->get();
        $parents = [];
        $tree = [];
        foreach($tokens as $token) {
            $tokenObj = (object)$token->toArray();
            $tokenObj->tokens = [];
            
            // DateTime columns
            $this->parseDateTimeValue($tokenObj, 'validity');
            $this->parseDateTimeValue($tokenObj, 'created');
            $this->parseDateTimeValue($tokenObj, 'used');
            
            if($modifyCallback !== NULL && is_callable($modifyCallback)) {
                $modifyCallback($tokenObj);
            }
            
            $parents[$tokenObj->guid] = $tokenObj;
        }
        foreach($parents as $tObj) {
            if(empty($tObj->parent_guid) || !isset($parents[$tObj->parent_guid])) {
                $tree[] = $tObj;
            }
            else{
                $parents[$tObj->parent_guid]->tokens[] = $tObj;
            }
        }
        return $tree;
    }

    
    /**
     * Save new tokens from XML. Existing token GUIDs will be deleted and inserted as new
     * 
     * @param string $xml XML file
     * @return int Amount of saved (inserted/updated) tokens
     */
    public function saveFromXml($xml)
    {
        $amount = 0;
        $tokens = $this->parseXmlTokens($xml);
        if(empty($tokens['guids'])) {
            return $amount;
        }

        $guidHash = $this->db->table('token')
                ->where('guid IN(?)', $tokens['guids'])
                ->fetchPairs('guid');
        
        foreach($tokens['tokens'] as $token) {
            $selection = $this->db->table('token');
            if(isset($guidHash[$token['guid']])) {
                $selection->where('guid = ?', $token['guid'])->update($token);
                $amount++;
            }
            else {
                $token['created'] = new \DateTime();
                $selection->insert($token);
                $amount++;
            }
        }
        
        return $amount;
    }
    
    /**
     * Truncate table token
     */
    public function clearAll()
    {
        $this->db->query('DELETE FROM token');
    }
    
    
    
    
    
    /**
     * Parse XML tokens from XML string
     * 
     * @param string $xmlString
     * @return array ['tokens' => [...], 'guids' => [...]]
     */
    protected function parseXmlTokens($xmlString)
    {
        $xml = @simplexml_load_string($xmlString, NULL, LIBXML_NOCDATA);
        if(!$xml) {
            throw new Nette\InvalidArgumentException('Invalid XML passed');
        }
        /* @var $xml SimpleXMLElement */
        $result = [];
        $guids = [];
        $queue = [$xml];
        do {
            $item = array_shift($queue);
            /* @var $item SimpleXMLElement */
            if(!empty($item->tokens)) {
                foreach($item->tokens->children() as $key => $token) {
                    if($key === 'token') {
                        /* @var $token SimpleXMLElement */
                        $token->addChild('parent_guid', (string)$item->guid);
                        $queue[] = $token;
                    }
                }
            }
            if(!empty($item->guid)) {
                $anon = (string)$item->anonymous;
                $guids[] = (string)$item->guid;
                $result[] = [
                    'guid' => (string)$item->guid,
                    'validity' => \DateTime::createFromFormat('Y-m-d H:i:s', (string)$item->validity),
                    'anonymous' => $anon === 'N' ? 0 : ($anon === 'A' || $anon === 'Y' ? 1 : (bool)$item->anonymous),
                    'type' => (string)$item->type,
                    'parent_guid' => empty($item->parent_guid) ? NULL : (string)$item->parent_guid
                ];
            }
        }
        while(count($queue) > 0);
        return [
            'tokens' => $result,
            'guids' => $guids
        ];
    }
            
    
    /**
     * Parse DateTime Value from SQLite
     * 
     * @param type $obj
     * @param type $column
     */
    protected function parseDateTimeValue($obj, $column)
    {
        if(!empty($obj->$column)) {
            if(!is_object($obj->$column) || (!$obj->$column instanceof \DateTime)) {
                $dt = new DateTime();
                $dt->setTimestamp($obj->$column);
                $obj->$column = $dt;
            }
        }
    }
}
