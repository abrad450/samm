<?php

use \Abra\Service\Service,
	\Abra\Service\Entity,
	\Abra\Service\Request,
	\Nette\Caching\Cache,
	\Nette\Caching\IStorage,
	\Nette\Reflection\ClassType,
	\Nette\Utils\Strings;

/**
 * Description of BaseModel
 */
abstract class AbraModel
{
    use \Nette\SmartObject;
    
    /**
     * Culture
     * @var Culture
     */
    protected $culture;
    
	/**
	 * @var \Abra\Service\Service
	 */
	protected $service;

	/**
	 * Cache Storage
	 * @var \Nette\Caching\Cache
	 */
	protected $cache;



	/**
	 * Vytvori instanci prislusneho modelu
	 */
	public function __construct(Culture $culture, Service $service, IStorage $cacheStorage)
	{
        $this->culture = $culture;
		$this->service = $service;
		$this->cache = new Cache($cacheStorage, 'ModelCache');
	}




	/**
	 * Vrati popis listu
	 *
	 * @param string $entity Entita, jejiz list se vrati
	 * @param string $listName Nazev listu, ktery se vrati pro entitu $entity
	 * @return \Abra\Service\ListDescription
	 */
	public function getCachedListDescription($entity, $listName = 'list')
	{
		// cache
		$cn = $this->service->getConnectionName();
		$key = array(__METHOD__, $entity, $cn, $listName);
		if(($cached = $this->cache->load($key)) !== NULL) return $cached;

		$req = new Request($entity, $listName);
		$req->type = Request::TYPE_DESCRIPTION;
		$description = $this->service->describeList($req)->data;

		$this->cache->save($key, $description, array( Cache::TAGS => array("namespace/$cn", "entity/$entity") ));
		return $description;
	}

	/**
	 * Vrati popis formulare
	 *
	 * @param string $entity Entita, jejiz form se vrati
	 * @param string $formName Nazev formu, ktery se vrati pro entitu $entity
	 * @return \Abra\Service\FormDescription
	 */
	public function getCachedFormDescription($entity, $formName = 'form')
	{
		// cache
		$cn = $this->service->getConnectionName();
		$key = array(__METHOD__, $entity, $cn, $formName);
		if(($cached = $this->cache->load($key)) !== NULL) return $cached;

		$req = new Request($entity, $formName);
		$req->type = Request::TYPE_DESCRIPTION;
		$description = $this->service->describeForm($req)->data;

		$this->cache->save($key, $description, array( Cache::TAGS => array("namespace/$cn", "entity/$entity") ));
		return $description;
	}

	/**
	 * Vrati popis filtracniho formulare
	 *
	 * @param string $entity
	 * @param string $formName
	 * @return \Abra\Service\FormDescription
	 */
	public function getCachedFilterDescription($entity, $formName = 'filter')
	{
		// cache
		$cn = $this->service->getConnectionName();
		$key = array(__METHOD__, $entity, $cn, $formName);
		if(($cached = $this->cache->load($key)) !== NULL) return $cached;

		$req = new Request($entity, $formName);
		$req->type = Request::TYPE_DESCRIPTION;
		$description = $this->service->describeForm($req)->data;

		$this->cache->save($key, $description, array( Cache::TAGS => array("namespace/$cn", "entity/$entity") ));
		return $description;
	}


	/**
	 * Vrati popis razeni
	 *
	 * @param string $entity
	 * @param string $formName
	 * @return \Abra\Service\FormDescription
	 */
	public function getCachedOrderDescription($entity, $formName = 'order')
	{
		// cache
		$cn = $this->service->getConnectionName();
		$key = array(__METHOD__, $entity, $cn, $formName);
		if(($cached = $this->cache->load($key)) !== NULL)
		{
			return $cached;
		}

		$req = new Request($entity, $formName);
		$req->type = Request::TYPE_DESCRIPTION;
		$description = $this->service->describeOrder($req)->data;

		$this->cache->save($key, $description, array( Cache::TAGS => array("namespace/$cn", "entity/$entity") ));
		return $description;
	}





	/**
	 * Vrati popis listu
	 *
	 * @param string $entity
	 * @return \Abra\Service\ListDescription
	 */
	public function getListDescription($entity)
	{
		return $this->getCachedListDescription($entity, 'list');
	}

	/**
	 * Vrati popis formulare
	 *
	 * @param string $entity
	 * @return \Abra\Service\FormDescription
	 */
	public function getFormDescription($entity)
	{
		return $this->getCachedFormDescription($entity, 'form');
	}

	/**
	 * Vrati popis formulare pro zakladani polozek
	 *
	 * @param string $entity
	 * @return \Abra\Service\FormDescription
	 */
	public function getAddDescription($entity)
	{
		return $this->getCachedFormDescription($entity, 'form');
	}

	/**
	 * Vrati popis hromadneho formulare pro editaci polozek
	 *
	 * @param string $entity
	 * @return \Abra\Service\FormDescription
	 */
	public function getMassFormDescription($entity)
	{
		return $this->getCachedFormDescription($entity, 'massform');
	}

	/**
	 * Vrati pole moznych razeni v listu pro tuto entitu
	 *
	 * @param string $entity
	 * @return \Abra\Service\OrderDescription
	 */
	public function getOrderDescription($entity)
	{
		return $this->getCachedOrderDescription($entity, 'order');
	}

	/**
	 * Vrati description filtru
	 *
	 * @param string $entity
	 * @return \Abra\Service\FormDescription
	 */
	public function getFilterDescription($entity)
	{
		return $this->getCachedFilterDescription($entity, 'filter');
	}

	/**
	 * Vrati popis formulare pro pridani prilohy k dokladu
	 *
	 * @param string $object Entita spojena s akci addAttachment
	 * @return \Abra\Service\FormDescription
	 */
	public function getAddAttachmentDescription($object)
	{
		return $this->getCachedFormDescription($object, 'form');
	}

	/**
	 * Vrati popis formulare pro pridani aktivity k dokladu
	 *
	 * @param string $object Entita spojena s akci addActivity
	 * @return \Abra\Service\FormDescription
	 */
	public function getAddActivityDescription($object)
	{
		return $this->getCachedFormDescription($object, 'form');
	}

}