<?php

use \Abra\Service\Service,
	\Abra\Service\Request;

/**
 * Description of UserModel
 */
class UserModel
{
    use \Nette\SmartObject;

	/**
	 * ABRA Service
	 *
	 * @var Service
	 */
	protected $service;

	/**
	 * User
	 *
	 * @var Nette\Security\User
	 */
	protected $user;

	/**
	 * Constructor
	 *
	 * @param \Abra\Service\Service $service
	 */
	public function __construct(Service $service, \Nette\Security\User $user)
	{
		$this->service = $service;
		$this->user = $user;
	}

	/**
	 * Zmena hesla aktualniho uzivatele
	 * @param string $old Stare heslo
	 * @param string $new nove heslo
	 */
	public function changePassword($old, $new)
	{
		$request = new Request();
		$request->type = 'changepasw';
		$request->data = array('data' => array('OldPassword' => $old, 'NewPassword' => $new));
		try
		{
			$this->service->post($request, TRUE, Service::RETURN_PARSED);
		}
		catch(ResponseException $e)
		{
			throw new AuthenticationException($e->getMessage(), $e->getCode(), $e);
		}
	}



	/**
	 * Vrati aktualne prihlaseneho uzivatele
	 *
	 * @return TNxSecurityUser
	 */
	public function getLoggedUser()
	{
		$req = new Request('TNxSecurityUser', 'read', $this->user->id);
		$data = $this->service->post($req, TRUE);
		return $data->data;
	}

}
