<?php

use Abra\Service\Request;
use Nette\Caching\Cache;
use Abra\Service\Service;
use Nette\Caching\IStorage;
use Nette\Security\User as NSUser;


/**
 * Model pro praci s ciselniky
 */
class CodelistModel extends AbraModel
{
	/**
	 * Konfigurace modelu
	 * @var array
	 */
	protected $config;

	/**
	 * Cache ciselniku pro konkretni request - aby se na tentyz ciselnik nedotazovalo do ABRY x-krat v jednom pozadavku
	 */
	protected $requestCache = array();

    /**
     * User
     * @var NSUser
     */
    protected $user;

	/**
	 * Konstruktor
	 *
	 * @param type $config Konfigurace Condelist modelu
     * @param Culture $culture Culture
	 * @param \Abra\Service\Service $service Sluzba pro WS ABRA
	 * @param \Nette\Caching\IStorage $cacheStorage Cache storage
	 */
	public function __construct($config, Culture $culture, Service $service, IStorage $cacheStorage, NSUser $user)
	{
		parent::__construct($culture, $service, $cacheStorage);
		$this->config = $config;
        $this->user = $user;
	}


	/**
	 * Vrati ciselnik o predane entite
	 * @param string $entityName Nazev entity
	 * @param array $filters Filtry pro vytahnuti ciselniku
	 * @param bool $noRequestCache TRUE = nebude se cacheovat ani pro dany request
	 * @return array ('klic' => 'hodnota')
	 */
	public function getCodelist($entityName, $filters = NULL, $noRequestCache = FALSE)
	{
		// ID aktualniho uzivatele
		$userId = $this->user->isLoggedIn() ? $this->user->id : NULL;

		// Ulozit do cache ?
        $isFiltered = !empty($filters);
		$noCacheUser = $this->config['nocacheType'] === 'user';
		$noCache = (!empty($this->config['nocache'][$entityName]) && (
				(is_array($this->config['nocache'][$entityName]) && in_array($userId, $this->config['nocache'][$entityName])) || 
				!is_array($this->config['nocache'][$entityName])
			));

		// cache
		if(!$isFiltered && (!$noCache || $noCacheUser))
		{
			$cn = $this->service->getConnectionName();
			$key = array(__METHOD__, $entityName, $cn, $userId);
			if(($cached = $this->cache->load($key)) !== NULL)
			{
				return $cached;
			}
		}
		elseif(!$noRequestCache)
		{
			// Pokud se nema ciselnik cache-ovat, tak aspon ulozime do request cache
			if(isset($this->requestCache[$entityName]))
			{
				return $this->requestCache[$entityName];
			}
		}

		$req = new Request($entityName, 'roll');
		$req->limit = 10000;
		// Pokud se predaji filtry, budeme filtrovat ciselnik
		if(!empty($filters))
		{
			$filterGroup = $req->setFilterGroup($filters->operator);
			// Filtry pouze v jedne urovni (ABRA jich stejne vic neumi 2015-07-27)
			foreach($filters->filters as $filter)
			{
				$op = isset($filter->operator) ? $filter->operator : '=';
				$filterGroup->addFilter($filter->column, $filter->value, $op);
			}
		}
		// Pustime dotaz
		$cl = $this->service->post($req, FALSE);
		$codeList = array();
		if(!empty($cl->data))
		{
			foreach($cl->data as $id => $val)
			{
				$codeList[$id] = isset($val->DisplayName) && $val->DisplayName !== NULL ? $val->DisplayName : (
									isset($val->Name) && $val->Name !== NULL ? $val->Name : (
									  isset($val->Code) && $val->Code !== NULL ? $val->Code : '???'
									)
								  );
			}
		}
		if(!$isFiltered && (!$noCache || $noCacheUser))
		{
			$cacheTags = array("namespace/$cn", "entity/$entityName");
			if($noCacheUser)
			{
				$cacheTags[] = 'foruser/'.$userId;
			}
			$this->cache->save($key, $codeList, array(Cache::TAGS => $cacheTags));
		}
		// Ulozime ciselnik do cache
		$this->requestCache[$entityName] = $codeList;
		// vratime ciselnik        
		return $codeList;
	}
	
	/**
	 * Smaze nacacheovane ciselniky ulozene pro uzivatele "po prihlaseni"
	 */
	public function clearForUserCache()
	{
		// ID aktualniho uzivatele
		$userId = $this->user->isLoggedIn() ? $this->user->id : NULL;
		if(!empty($userId)) {
			$this->cache->clean(array(Cache::TAGS => array('foruser/'.$userId)));
		}
	}

}