<?php

use \Nette\Caching\Cache;

/**
 * Model pro praci s cachi
 */
class CacheModel extends AbraModel
{

	/**
	 * Promaze veskerou cache pro predane namespace a object, nebo jen namespace, nebo ani namespace - pak promaze celou cache
	 * @param string $namespace Jmenny prostor
	 * @param string $object Objekt
	 * @return array Pole odstranenych trid, ktere se odmazaly
	 */
	public function clear($namespace = NULL, $object = NULL)
	{
		$removed = $this->service->clearEntityCache($namespace, $object);
		if($namespace === NULL)
		{
			$this->cache->clean(array( Cache::ALL => TRUE ));
		}
		elseif($object === NULL)
		{
			$this->cache->clean(array( Cache::TAGS => array("namespace/$namespace") ) );
		}
		else
		{
			$this->cache->clean(array( Cache::TAGS => array("namespace/$namespace", "entity/$object") ) );
		}
		return $removed;
	}



}