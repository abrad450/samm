<?php

use Nette\Forms\IControl;
use Nette\Utils\Strings;

/**
 * Description of AbraHelpers
 */
class AbraHelpers
{

	/**
	 * Detekuje MIME typ podle predaneho nazvu souboru
	 * @param string $fileName Nazev souboru
	 * @return string MIME typ, pokud nedetekuje vrati application/octet-stream
	 */
	static public function detectMimeType($fileName, &$contentDisposition = 'inline')
	{
		$parts = explode('.', $fileName);
		$ext = strtolower(end($parts));
		$contentDisposition = 'inline';
		if(count($parts) > 1)
		{
			switch($ext)
			{
				case 'jpg':
				case 'jpeg': return 'image/jpeg';
				case 'gif': return 'image/gif';
				case 'png': return 'image/png';
				case 'pdf': return 'application/pdf';
				case 'txt': return 'text/plain';
			}
		}
		$contentDisposition = 'attachment';
		return 'application/octet-stream';
	}


	static public function placeValues($text, $row, Culture $culture, $raw = false, $urlencode = false)
	{
		$text = preg_replace_callback('/\{([a-z\._0-9]+)\}/imsU', function($matches) use($row, $culture, $raw, $urlencode) {
			$return = $matches[0];
			if(isset($matches[1]))
			{
				$return = \AbraHelpers::objectValue($matches[1], $row, $culture, $raw);
                $return = str_replace(['<div class="string-value">','</div>'], ['',''], $return);
			}
            if($urlencode) {
                // Pokud se ma provadet URL encode, zjistime, zda se nejedna o absolutni odkaz, ty se nesmi URLENCODOVAT
                if(
                    !Strings::startsWith($return, 'http://') &&
                    !Strings::startsWith($return, 'https://') &&
                    !Strings::startsWith($return, 'ftp://') &&
                    !Strings::startsWith($return, 'ftps://')
                ) {
                    return urlencode($return);
                }
            }
            return $return;
		}, $text);
		return $text;
	}

	/**
	 * Prevod odkazů na html link
	 * Pouze pro texty bez HTML
     * Umi i markdown odkazy (2020-03-09)
     * 
	 * @param $text
	 * @return string|string[]|null
	 */
	static public function url2link($text)
	{
        $patterns = [
                '`(\[([^\]]+)\])?\(?((?:https?|ftps?|file):///?[^\s<"]+[0-9a-zA-Z_-][/\\\]?)\)?`si',
                '`((?<!//)((www\.[^\s<"]+[0-9a-zA-Z_-][/\\\]?/?)))`si'
            ];
        return preg_replace_callback($patterns, function($matches) {
            $link = preg_match('`^(https?|ftps?|file):///?.*`si', $matches[3]) ? $matches[3] : 'http://'.$matches[3];
            $text = trim($matches[2]) === '' ? $matches[3] : $matches[2];
            return '<a href="'.$link.'" target="_blank">'.$text.'</a>';
        }, $text);
        
		//$find=array('`((?:https?|ftp)://\S+[[:alnum:]]/?)`si','`((?<!//)(www\.\S+[[:alnum:]]/?))`si');
//		$replace = array('<a href="$1" target="_blank">$1</a>', '<a href="http://$1" target="_blank">$1</a>');
//		return preg_replace($find, $replace, $text);
	}

	static public function objectValue($property, $row, Culture $culture = null, $raw = false, $tableView = true)
	{
		$propPath = explode('.', $property);
		$val = NULL;
		$ref = $row;
		if(!empty($propPath))
		{
			$obj = NULL;
			do
			{
				$property = array_shift($propPath);
				if(!isset($ref->{$property}))
				{
					$val = NULL;
					return $val;
				}
				$val = $ref->{$property};
				$obj = $ref;
				$ref = $ref->{$property};
			}
			while(!empty($propPath));
            if (!$raw) {
                $refClass = new \Nette\Reflection\ClassType($obj);
                $propAnnots = $refClass->getProperty($property)->getAnnotations();
                $type = 'dtString';
                if (isset($propAnnots['type']) && is_array($propAnnots['type'])) {
                    $type = reset($propAnnots['type']);
                }
                $length = 200;
                if (isset($propAnnots['length']) && is_array($propAnnots['length'])) {
                    $length = reset($propAnnots['length']);
                }
                $val = self::abraType($culture, 'from', $val, $type, $length, $tableView);
            }
		}
		return is_scalar($val) && preg_match('/^[0-9]+$/Us', $val) ? intval($val) : $val;
	}


    /**
     * Upravi HTML prvku dle predaneho field->control->settings
     * 
     * @param string|mixed $control
     * @param Abra\Service\FormField $field
     * @param array $namePath
     * @return string|mixed
     */
    static public function modifyControl($control, $field, $namePath, $data, $entity, $namespace)
    {
        if(is_string($control)) {
            
            // Computed settings
            if(isset($field->control) && isset($field->control->settings) && isset($field->control->settings->computedField)) {
                if(!empty($field->control->settings->computedField->type) && !empty($field->control->settings->computedField->value)) {
                    $cf = $field->control->settings->computedField;
                    $control = str_replace(
                            '<div ', 
                            '<div'.
                                ' data-computed-type="'.$cf->type.'"'.
                                ' data-computed-value="'.$cf->value.'"'.
                                ' data-computed-field="'.$field->name.'"'.
                                ' data-computed-datatype="'.$field->type.'" ', 
                            $control
                        );
                }
                else {
                    throw new InvalidArgumentException('Computed field must have both "type" and "value" properties for field "'.$field->name.'"');
                }
            }
            
            // Remove indices from name path
            $namePathFiltered = array_filter($namePath, function($val) { return !is_numeric($val); });
            
            $val = self::objectValue($field->name, $data, NULL, TRUE);

            $entityField = self::describeField($namespace, $entity, $namePathFiltered);
            
            // Field info
            $control = str_replace(
                    '<div ', 
                    '<div'.
                        ' data-fieldinfo-rawval="'.(is_scalar($val) ? $val : '[OBJECT]').'"'.
                        ' data-fieldinfo-path="'.implode('.', $namePathFiltered).'"'.
                        ' data-fieldinfo-name="'.$field->name.'"'.
                        ' data-fieldinfo-type="'.$field->type.'"'.
                        ' data-fieldinfo-size="'.($entityField ? $entityField->size : '').'" ', 
                    $control
                );
        }
        return $control;
    }

    
    /**
     * Describe Field within namespace/entity of the given namePath
     * 
     * @param string $namespace
     * @param string $entity
     * @param string[] $namePath
     * @return \Abra\Service\ClassField|NULL
     */
    public static function describeField($namespace, $entity, $namePath)
    {
        if(empty($namePath)) {
            return NULL;
        }
        $obj = \Abra\Service\Entity::describeEntity($namespace.'\\'.$entity);
        $fieldObj = NULL;
        while(count($namePath) > 0) {
            $nameItem = array_shift($namePath);

            if(!isset($obj->fields[$nameItem])) {
                return NULL;
            }
            $fieldObj = $obj->fields[$nameItem];
            /* @var $fieldObj \Abra\Service\ClassField */
            if($fieldObj->ref !== NULL || $fieldObj->list !== NULL) {
                if($fieldObj->ref !== NULL) {
                    $obj = \Abra\Service\Entity::describeEntity($namespace.'\\'.$fieldObj->ref);
                }
                elseif($fieldObj->list !== NULL) {
                    $obj = \Abra\Service\Entity::describeEntity($namespace.'\\'.$fieldObj->list);
                }
            }
            
        }
        return $fieldObj;
    }


	/**
	 * Provede upravu hodnoty bud z abry nebo do abry
     * 
     * @param Culture $culture Culture
	 * @param string $action 'to', 'from', 'display'
	 * @param mixed $value
	 * @param string $type dtString, dtFloat ...
	 * @param string $length Length (bud cislo, nebo cislo,cislo)
	 * @return mixed
	 */
	public static function abraType(Culture $culture, $action, $value, $type, $length, $tableView = true)
	{
		// PODLE DATOVEHO TYPU UPRAVIME FORMAT ZOBRAZENI
		if($value === NULL) return null;
		switch($type)
		{
			case 'dtBoolean':
				if($action == 'from' || $action == 'display') {
					$value = '<span class="icon-'.($value ? 'ok' : 'remove').'"></span>';
				}
				break;

			case 'dtDateTime':
				//dump($val);
				if($action == 'from' || $action == 'display') {
					$value = trim($value) === '' ? $value : \DateTime::createFromFormat('Y-m-d H:i:s', $value)->format($culture['dateTime']);
				}
				elseif($action == 'to') {
					$value = trim($value) === '' ? NULL : \DateTime::createFromFormat($culture['dateTime'], $value)->format('Y-m-d H:i:s');
				}
				break;

			case 'dtDate':
				//dump($val);
				if($action == 'from' || $action == 'display') {
					$value = trim($value) === '' ? $value : \DateTime::createFromFormat('Y-m-d', $value)->format($culture['date']);
				}
				elseif($action == 'to') {
					$value = trim($value) === '' ? NULL : \DateTime::createFromFormat($culture['date'], $value)->format('Y-m-d');
				}
				break;

			// desetine cislo formatovat podle length dtFloat a kulturnich zvyklosti
			case 'dtFloat':
				$lenDec = explode(',', $length);
                if(!isset($lenDec[1])) {
                    $lenDec[1] = 0;
                }
				if($action == 'from') {
                    $culturedNumber = number_format($value, $lenDec[1], $culture['decimal'], $culture['thousand']);
                    $class = $tableView ? "text-right view-float-value" : "view-float-value";
					$value = '<div class="' . $class . '">'.str_replace(' ', '&nbsp;', $culturedNumber).'</div>';
				} elseif($action == 'to') {
                    $floatSearch = array($culture['thousand'], $culture['decimal']);
                    $floatReplace = array('', '.');
					$value = str_replace($floatSearch, $floatReplace, $value);
				}
				break;
                
            default:
                if($action == 'from' || $action == 'display') {
                    $value = '<div class="string-value">'.$value.'</div>';
                }
		}
		return $value;
	}


	/**
	 * Removes BOM from the given file
	 *
	 * @param string $filePath File Path
	 * @return bool TRUE if BOM was removed, FALSE if no BOM was present
	 */
	public static function removeBOM($filePath)
	{
		$string = file_get_contents($filePath);
		if(substr($string, 0,3) == pack("CCC",0xef,0xbb,0xbf))
		{
			file_put_contents($filePath, substr($string, 3));
			return TRUE;
		}
		return FALSE;
	}

    
    
    
    /**
     * Validate encoding - form validator
     * 
     * @param IControl $control
     * @param string $encoding (utf-8 | NULL)
     */
    public static function validateEncoding(IControl $control, $encoding)
    {
        $charset = trim(strtolower($encoding));
        if($charset === 'utf-8') {
            return TRUE;
        }
        else {
            $val = $control->getValue();
            $converted = @iconv($charset, 'utf-8', @iconv('utf-8', $charset.'//IGNORE', $val));
            return $converted === $val;
        }
    }
    
    /**
     * Varovani - FILLED - form validator
     * 
     * @param BaseControl $control
     * @param string $arg
     */
    public static function warnfilled($control, $arg)
    {
        // $arg = Dotaz ... ale na serveru se jiz neptame, tise prekrocime
        
        // Resi se jen v JS - app.js
        // 
        // Nette.validators.AbraHelpers_warn
        // Nette.addError
        
        return TRUE;
    }


    
    
    public static $webalizeReplacements = [
        '~' => '_TILDE_',
        '!' => '_EXCLAM_',
        '@' => '_ATSIGN_',
        '#' => '_SHARP_',
        '$' => '_DOLLAR_',
        '%' => '_PERCENT_',
        '^' => '_CARET_',
        '&' => '_AMP_',
        '*' => '_ASTER_',
        '(' => '_OBRKT_',
        ')' => '_CBRKT_',
        '-' => '_DASH_',
//        '_' => '_UNDSCR_',
        '|' => '_PIPE_',
        '/' => '_SLASH_',
        '?' => '_QM_',
        ':' => '_COLON_',
        ';' => '_SCOLON_',
        ',' => '_COMMA_',
        '.' => '_DOT_',
        '{' => '_LCURL_',
        '}' => '_RCURL_',
        '=' => '_EQ_',
        '"' => '_QUOT_',
        "'" => '_APOS_'
    ];
    
    
    public static function webalizeId($id)
    {
        return str_replace(
                array_keys(self::$webalizeReplacements),
                array_values(self::$webalizeReplacements),
                $id
            );
    }
    
    public static function unwebalizeId($webalizedId)
    {
        return str_replace(
                array_values(self::$webalizeReplacements),
                array_keys(self::$webalizeReplacements),
                $webalizedId
            );
    }
    
}
