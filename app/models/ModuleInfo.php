<?php

/**
 * Informace o modulu aplikace
 *
 * @property string $unique Retezec unikatne identifikujici modul
 * @property string $localModuleName Nazev modulu
 * @property \Module $module Modul
 */
class ModuleInfo
{
    use \Nette\SmartObject;

	/**
	 * Unikatni identifikator modulu
	 * @var string
	 */
	protected $unique;

	/**
	 * nazev modulu
	 * @var string
	 */
	protected $localModuleName;

	/**
	 * Modul tak, jak ho dostanu z ABRY
	 * @var \Module
	 */
	protected $module;


	/**
	 * Konstruktor
	 * @param string $unique Unikatni retezec pro modul
	 * @param strign $localModuleName Jmeno lokalniho modulu
	 */
	public function __construct($unique, $localModuleName = NULL)
	{
		$this->setUnique($unique);
		$this->setLocalModuleName($localModuleName);
	}

	/**
	 * Vrati unikatni identifikator pro mdul
	 * @return string
	 */
	public function getUnique()
	{
		return $this->unique;
	}

	/**
	 * Nastavi unikatni identifikator pro modul
	 * @param string $unique unikatni identifikator
	 * @return \ModuleInfo Fluent rozhrani
	 */
	public function setUnique($unique)
	{
		$this->unique = $unique;
		return $this;
	}

	/**
	 * Vrati nazev lokalniho modulu
	 * @return string
	 */
	public function getLocalModuleName()
	{
		return $this->localModuleName;
	}

	/**
	 * Nastavi jmeno lokalniho modulu
	 * @param string $localModuleName nazev lokalniho modulu
	 * @return \ModuleInfo Fluent rozhrani
	 */
	public function setLocalModuleName($localModuleName)
	{
		$this->localModuleName = $localModuleName;
		return $this;
	}

	/**
	 * Vrati lidsky citelny nazev modulu
	 * @return string
	 */
	public function getModule()
	{
		return $this->module;
	}

	/**
	 * Modul z ABRY
	 * @param \Module
	 * @return \CoreModule\ModuleInfo Fluent rozhrani
	 */
	public function setModule(\Module $module)
	{
		$this->module = $module;
		return $this;
	}

}
