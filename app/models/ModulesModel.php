<?php

use \Abra\Service\Entity,
	\Abra\Service\Service,
	\Abra\Service\Request;

/**
 * Model pro praci s moduly
 */
class ModulesModel extends AbraModel
{

	/**
	 * Vrati seznam povolenych modulu
	 * @return array
	 */
	public function getAllowedModules()
	{
		$r = new Request();
		$r->type = Request::TYPE_MODULES;
		$modules = $this->service->post($r, FALSE, Service::RETURN_PARSED);
		if(empty($modules->data->module) || !is_array($modules->data->module))
		{
			throw new \Exception('Nepodařilo se získat seznam modulů', 500);
		}
		$modList = array();
		foreach($modules->data->module as $mod)
		{
			if(empty($mod)) continue;
			$mClass = new \stdClass();
			$mClass->Module = $mod;
			$module = Entity::create($mClass);
			$modList[$module->name] = $module;
		}
		return $modList;
	}

    /**
     * Get for approve
     */
    public function getForApprove()
    {
		$r = new Request();
		$r->type = Request::TYPE_FOR_APPROVE;
		$forApprove = $this->service->post($r, FALSE, Service::RETURN_PARSED);
        if(!isset($forApprove->data) || !isset($forApprove->data->forapprovecount)) {
            return [];
        }
        $hash = [];
        foreach($forApprove->data->forapprovecount as $fa) {
            if(isset($fa->modulename)) {
                $hash[$fa->modulename] = [
                    'count' => isset($fa->count) ? $fa->count : 0
                ];
            }
        }
        return $hash;
    }
    

	/**
	 * Vytahne logo z ABRY a vrati ho, pokud v abre neni, vrati NULL
     * 
	 * @return \Abra\Service\RawDataContainer|NULL
	 */
	public function getLogo()
	{
		$rawData = $this->service->rawData(new Request('logo'));
        if(strpos($rawData, '<error>') !== FALSE) {
            return NULL;
        }
        return new \Abra\Service\RawDataContainer($rawData);
	}
}