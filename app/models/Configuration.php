<?php

/**
 * Configuration
 */
class Configuration extends \Nette\Utils\ArrayHash
{
    public function __construct(\Nette\DI\Container $di = NULL)
    {
        if(!is_null($di)) {
            $params = $di->getParameters();
            foreach($params as $key => $value) {
    			$this->$key = is_array($value) ? static::from($value, true) : $value;
    		}
        }
    }
}
