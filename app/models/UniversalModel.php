<?php

use \Abra\Service\Request,
	\Abra\Service\Entity,
	\Abra\Service\FilterGroup,
	\Nette\Caching\Cache,
	\Nette\Utils\ArrayHash;


/**
 * Univerzalni model
 */
class UniversalModel extends \AbraModel
{    
    
	/**
	 * Asociativni pole ENTITA => Filtry
	 * @var array
	 */
	protected $entityFilters = array();

	/**
	 * Priznaky, ktere filtry se jiz v tomto pozadavku nacetly
	 * @var array Hash
	 */
	protected $filtersLoaded = array();


	/**
	 * Vrati filtry pro entitu $entity
	 * @param string $entity Entita
	 * @reutrn WebFilter[]
	 */
	public function &getEntityFilters($entity)
	{
		// Cacheujeme
		if(!isset($this->filtersLoaded[$entity]))
		{
			$req = new Request('WebFilter', 'read');
			$req->setFilterGroup(FilterGroup::OPERATOR_AND)
				->addFilter('ObjectName', $entity);
			$req->addOrder('Global', 'desc');
			$req->addOrder('Name', 'asc');
			$this->entityFilters[$entity] = $this->service->post($req)->data;
			if(empty($this->entityFilters[$entity]))
			{
				$this->entityFilters[$entity] = array();
			}
			$this->filtersLoaded[$entity] = TRUE;
		}
		return $this->entityFilters[$entity];
	}

	/**
	 * Vrati konkretni filter pro entitu o predanem ID
	 * @param string $entity Entita
	 * @param string $id ABRA ID
	 * @return WebFilter|NULL
	 */
	public function getEntityFilter($entity, $id)
	{
		$filters = $this->getEntityFilters($entity);
		foreach($filters as $filter)
		{
			if($filter->ID == $id)
			{
				return $filter;
			}
		}
		return null;
	}

	/**
	 * Vrati vychozi filtr pro entitu o predanem ID
	 * @param string $entity Entita
	 * @param string $id ABRA ID
	 * @return WebFilter|NULL
	 */
	public function getEntityDefaultFilter($entity)
	{
		$filters = $this->getEntityFilters($entity);
		foreach($filters as $filter)
		{
			if($filter->Default)
			{
				return $filter;
			}
		}
		return null;
	}


	/**
	 * Ulozi entity filter
	 * @param string $entity Nazev entity
	 * @param string $name Nazev filtru
	 * @param string|array $conditions
	 * @param bool $global Zda je filtr globalni
	 * @param bool $default Zda je filtr defaultni
	 * @param bool $orderby Razeni podle pole
	 * @param bool $orderbydir Smer razeni podle pole
	 */
	public function saveEntityFilter($entity, $name, $conditions, $global = FALSE, $default = FALSE, $orderby=null, $orderbydir=null)
	{
		// najdeme existujici filtr podle jmena
		$existing = $this->findFilterByName($entity, $name, $global);
		// sestavime data pro ulozeni
		$data = array(
			'Default' => $existing === NULL ? ($default ? 'A' : 'N') : ($existing->Default ? 'A' : 'N'),
			'Global' => $global ? 'A' : 'N',
			'Name' => $name,
			'ObjectName' => $entity,
			'ID' => $existing === NULL ? NULL : $existing->ID
		);
        
        // Co se nebude ukladat (z formulare) - pomocne hodnoty
        $skipFields = array('__ref' => 1, '_submittedby_' => 1, '_cancel_filter' => 1, 'cancelFilter' => 1, 'ID' => 1);
        
		// pokud se preda stringove conditions, mame hotovo ...
		if(is_string($conditions))
		{
			$data['Conditions'] = $conditions;
		}
		else
		{
			// ... jinak musime projit asociativni pole a sestavit (XML s kodovanim aplikace (UTF-8) a exportovat jako XML, aby tam nebyly entity)
			$xml = new \DOMDocument('1.0', 'UTF-8');
			$xml->encoding = 'UTF-8';
			$xml->substituteEntities = TRUE;
			$xml->appendChild($filters = new \DOMElement('filters'));
			$filters->appendChild($and = new \DOMElement('filter-and'));
			foreach($conditions as $field => $value)
			{
                if(isset($skipFields[$field])) {
                    continue;
                }
				$and->appendChild($flt = new \DOMElement('filter'));
				$flt->appendChild(new \DOMElement('name', $field));
				$flt->appendChild(new \DOMElement('operator', '='));
				$flt->appendChild(new \DOMElement('value', $value));
			}
			$xmlFilters = trim(preg_replace('/<\?xml.*\?>/Umisu', '', $xml->saveXML()));
			$data['Conditions'] = $xmlFilters;
		}

		if(!empty($orderby))
		{
			$data['Conditions'] .= '<orderby><order><col>'.$orderby.'</col><dir>'.$orderbydir.'</dir></order></orderby>';
		}

		$req = new Request('WebFilter', 'update');
		$req->setData(array('data' => array('WebFilter' => $data)));

		$filters = $this->service->post($req)->data;
		return is_array($filters) ? reset($filters) : $filters;
	}

	/**
	 * Update entity filter
	 * @param Entity $webFilter Web Filter object
	 */
	public function updateEntityFilter($webFilter)
	{
		$req = new Request('WebFilter', 'update');
		$req->setData(array(
			'data' => array(
				'WebFilter' => $webFilter->toAbra()
			)
		));
		$filters = $this->service->post($req)->data;
		return is_array($filters) ? reset($filters) : $filters;
	}

	/**
	 * Smaze filtr o predanem ID
	 * @param string $id ID
	 */
	public function deleteEntityFilter($id)
	{
		$data = array('WebFilter' => array());
		$data['WebFilter'][1] = array('ID' => $id); // 0 => klic nula je bugovy :((
		$request = new Request('WebFilter', 'delete', NULL, array('data' => $data));
		return $this->service->post($request);
	}


	/**
	 * Najdi filtr podle nazvu a prislusnosti mezi globalnimi / mimo globalni
	 * @param string $entity Entita
	 * @param string $name Nazev filtru
	 * @param bool $global Hledame mezi globalnimi ?
	 * @return WebFilter | NULL
	 */
	protected function findFilterByName($entity, $name, $global)
	{
		$filters = $this->getEntityFilters($entity);
		if(!empty($filters))
		{
			foreach($filters as $f)
			{
				if($f->Global === $global && trim($f->Name) === trim($name))
				{
					return $f;
				}
			}
		}
		return NULL;
	}


	// ----------------------------------------------------------------------------------------------------------------

    
    /**
     * Append filter to Request
     * 
     * @param string $entity Entity
     * @param Request $request ABRA service Request
     * @param Traversable $filter Filter hashmap
     * @param bool $skipFilterFieldsCheck Nekontrolovat na povolene filtracni pole
     */
    protected function appendFilter($entity, $request, $filter, $skipFilterFieldsCheck = FALSE)
    {
        // fieldy pro BO
        $empty = $this->getEmptyInstance($entity); // kvuli nacachovani entity
        $entityDescription = Entity::describeEntity($this->service->getConnectionName().'\\'.$entity);
        $fields = $entityDescription->fields;
        // description filtracniho formulare (SQLField)
        $desc = $this->getFilterDescription($entity)->getfields();
        $filterFields = array();
        foreach($desc as $ff)
        {
            $filterFields[$ff->name] = !empty($ff->SQLField) ? $ff->SQLField : $ff->name;
        }
        $fg = $request->setFilterGroup(FilterGroup::OPERATOR_AND);
        foreach($filter as $key => $value)
        {
            if(isset($fields[$key]) && ($skipFilterFieldsCheck || isset($filterFields[$key])) && $value != '' && $value != '0') // FIXME - pokud je hodnota nula schvalne, zde ji to odfiltruje, protoze $value != '' kdyz $value = 0 je FALSE !!!
            {
                // sanitizujeme hodnotu
                $value = Entity::sanitizeValue($fields[$key]->type, $value, FALSE);
                // dame hodnotu do filtru
                $filterField = isset($filterFields[$key]) ? $filterFields[$key] : $key;
                $fg->addFilter($filterField, $value);
            }
        }
    }
    

	/**
	 * Vrati seznam polozek
	 * @param string $entity Entita
	 * @param array|SessionSection $filter Filtr do requestu
	 * @param array|SessionSection $order Razeni zaznamu
	 * @param int $limit Maximalni pocet zaznamu
	 * @param int $offset Offset, od ktereho zaznamu se budou vracet data
	 * @return \Abra\Service\Response
	 */
	public function getList($entity, $filter, $order, $limit = 100, $offset = 0)
	{
//		$req = new Request($entity, 'read');
//		$req->limit = $limit;
//		$data = $this->service->post($req);
//		return $data->data;
		$req = new Request($entity, 'read');
		// Filtr
		if(!empty($filter))
		{
			if(is_string($filter))
			{
                //	$req->setFilterXml($filter);
                // get structure from XML (the simple hacky way)
                $fStruct = simplexml_load_string($filter);
                if(isset($fStruct->{'filter-and'}) && $fStruct->{'filter-and'}->filter) {
                    $filterArray = $fStruct->{'filter-and'}->filter;
                    $filterHash = array();
                    foreach($filterArray as $filterArrayItem) {
                        /* @var $filterArrayItem SimpleXMLElement */
                        $isNull = ($filterArrayItem->value->asXml() == '<value/>');
                        $filterHash[(string)$filterArrayItem->name] = $isNull ? NULL : $filterArrayItem->value->__toString();
                    }
                    $this->appendFilter($entity, $req, $filterHash, TRUE);
                }
			}
			else
			{
                $this->appendFilter($entity, $req, $filter);
			}
		}
		// Order
		if(!empty($order['orderby']))
		{
			$req->addOrder($order['orderby'], $order['orderbydir']);
		}
		// limit
		$req->limit = $limit;
		$req->offset = $offset;
		$data = $this->service->post($req);
		return $data;
	}


	/**
	 * Vrati prazdnou instanci entity
	 * @return Entity
	 */
	public function getEmptyInstance($entity)
	{
		$className = $this->service->getConnectionName().'\\'.$entity;
		if(!class_exists($className))
		{
			$this->service->cacheEntities($this->service->getConnectionName(), $entity);
		}
		return new $className();
	}


	/**
	 * Vrati data pro entitu s predanym id
	 * @param string $id ID zaznamu
	 * @return Entity
	 */
	public function getForm($entity, $id)
	{
		$req = new Request($entity, 'read', $id);
		$data = $this->service->post($req, TRUE);
		return $data->data;
	}

	/**
	 * Vrati predvyplnujici data pro entitu s predanym id
	 * @param string $entity Entita
	 * @return Entity
	 */
	public function getPrefill($entity, $sourceObject = NULL, $sourceId = NULL)
	{
		// Data
		if($sourceObject !== NULL && $sourceId !== NULL)
		{
			$edata = array();
			$edata['SourceObject'] = $sourceObject;
			$edata['Source'] = $sourceId;
			// Prefill request
			$req = new Request($entity, 'read', '0000000000', array('data' => array($entity => $edata)));
		}
		else
		{
			$req = new Request($entity, 'read', '0000000000');
		}
		$data = $this->service->post($req, TRUE);
		return $data->data;
	}

	/**
	 * Vrati DisplayName pro objekt $sourceType a $sourceId
	 */
	public function getDisplayName($sourceType, $sourceId)
	{
		$req = new Request($sourceType, 'read', $sourceId);
		$data = $this->service->post($req, TRUE);
		return $data->data;
	}


    /**
     * Provede zalozeni dat z formulare
     * @param string $entity Entita
     * @param ArrayHash $values Hodnoty z formulare
     * @param array $postValues
     * @param string $action
     * @return \Abra\Service\Response
     * @throws \Abra\Service\ResponseException
     * @throws \Abra\Service\ServiceException
     */
	public function create($entity, ArrayHash $values, array $postValues, $action = Request::ACTION_UPDATE)
	{
		$obj = Entity::createFromForm($values, $entity, $this->service->getConnectionName());
		$data = $obj->toAbraFiltered($values);
		$dataExtended = $this->extendValues($entity, $data, $postValues);
		$request = new Request($entity, $action, NULL, array('data' => array($entity => $dataExtended)));
		return $this->service->post($request, TRUE);
	}

    /**
     * Provede update dat z formulare
     * @param string $entity Entita
     * @param ArrayHash $values Hodnoty z formulare
     * @param array $postValues
     * @param string $action
     * @throws \Abra\Service\ResponseException
     * @throws \Abra\Service\ServiceException
     */
	public function update($entity, ArrayHash $values, $postValues = array(), $action = Request::ACTION_UPDATE)
	{
		$obj = Entity::createFromForm($values, $entity, $this->service->getConnectionName());
		$data = $obj->toAbraFiltered($values);

        if(!empty($postValues)) {
            $rawData = $postValues;
        } else {
            $rawData = $data;
        }

		if(!empty($postValues)) {
			$dataExtended = $this->extendValues($entity, $data, $postValues);
		} else {
			$dataExtended = $data;
		}
		$request = new Request($entity, $action, NULL, array('data' => array($entity => $dataExtended)));
		return $this->service->post($request, TRUE);
	}

	/**
	 * Pridaly se nejake polozky do nejake kolekce na strane klienta ? (umi jen kolekce v prvi urovni)
	 *
	 * @param stdClass $values hodnoty pro odeslani do abry
	 * @param array $post Pole POST hodnot, kde muze byt o trochu vic polozek v kolekcich
	 * @return stdClass
	 */
	protected function extendValues($entity, $values, $post)
	{
		$entityDescription = Entity::describeEntity($this->service->getConnectionName().'\\'.$entity);
		//dump($post);die;

		foreach($values as $property => $value)
		{
			// pokud je hodnota property kolekce, tak zkontrolujeme POST values
			if(is_array($value))
			{
				if(!isset($entityDescription->fields[$property]) || empty($entityDescription->fields[$property]->list))
				{
					throw new Nette\InvalidStateException('Invalid definition for "'.$property.'" property of "'.$entity.'" entity.');
				}

				$items = array();
				$refItem = $this->createRefItem($value); // REF item pro odeslani kolekce
				//dump($property);
				if(!empty($post[$property]))
				{
					foreach($post[$property] as $itemIndex => $item)
					{
                        // 12.12.2019 -> rozsirit $item (postData) o $values (formData) - BOOLEAN HODNOTY
                        if(isset($value[$itemIndex])) {
                            foreach($value[$itemIndex] as $_exProp => $_exVal) {
                                if($_exVal === 'A' || $_exVal === 'N') {
                                    $item[$_exProp] = $_exVal;
                                }
                            }
                        }
                        // \\\\\\\\\\\\\\\\\\\\\ FIX pro boolean hodnoty \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                        
						// Pokud se radek nema smazat, tak ho vlozime do cilovych items
						if(!isset($item['__DELETE_FLAG__']) || empty($item['__DELETE_FLAG__'])) 
						{
							$fixedVal = $this->fixData($entityDescription->fields[$property]->list, $item, $refItem);
							array_push($items, $fixedVal);
						}
					}
				}
				$values->{$property} = $items;
			}
		}
		return $values;
	}

    /**
     * Create reference item (with all filled props) from collection
     * 
     * @param stdClass $items
     */
    protected function createRefItem($items)
    {
        $refItem = new \stdClass();
        foreach($items as $item) {
            foreach($item as $property => $val) {
                if(!property_exists($refItem, $property)) {
                    $refItem->{$property} = $val;
                }
            }
        }
        return $refItem; // reset($items);
    }

	/**
	 * Converts all arrays to stdClass instance.
	 * @param array $value
	 * @return \stdClass
	 */
	protected function fixData($entity, $value, $refItem)
	{
		// popis property
		$cFields = Entity::describeEntity($this->service->getConnectionName().'\\'.$entity);
		if(is_array($value))
		{
			$obj = new \stdClass();
			foreach($value as $prop => $val)
			{
				// je tato property v refItemu?
				if(property_exists($refItem, $prop))
				{
					if(property_exists($refItem, '__class__'))
					{
						$obj->__class__ = $refItem->__class__;
					}
					// autocomplete pole ne
					if(!\Nette\Utils\Strings::endsWith($prop, '_autocomplete'))
					{
						$propDesc = $cFields->fields[$prop];
						if(is_scalar($val))
						{
							$valFixed = AbraHelpers::abraType($this->culture, 'to', $val, $propDesc->type, $propDesc->size);
							$obj->{$prop} = $valFixed;
						}
						else
						{
							$subEntity = empty($propDesc->list) ? $propDesc->ref : $propDesc->list;
							$obj->{$prop} = $this->fixData($subEntity, $val, $refItem->{$prop});
						}
					}
				}
			}
			return $obj;
		}
		return $value;
	}



	/**
	 * Provede nahrani prilohy k entite $entity s hodnotami $values a trida prilohy $attachmentClass
	 * @param string $entity Entita
	 * @param \Nette\Utils\ArrayHash $values Hodnoty z formulare
	 * @param string $attachmentClass Trida prilohy (napr. TNxAddAttachment)
	 */
	public function addAttachment($entity, ArrayHash $values, $attachmentClass)
	{
		$obj = Entity::createFromForm($values, $attachmentClass, $this->service->getConnectionName());
		// najdeme vsechny fileuploady a zakodujeme je pomoci Base64
		$this->encodeFileUploads($obj);
		$data = $obj->toAbraFiltered($values);
		$data->SourceObject = $entity;
		$data->Source = $values['_SOURCE_ID_'];
		$request = new Request($attachmentClass, 'update', NULL, array('data' => array($attachmentClass => $data)));
		return $this->service->post($request, TRUE);
	}

	/**
	 * Provede ulozeni aktivity k entite $entity s hodnotami $values a tridou aktivity $activityClass
	 *
	 * @param string $entity Entita
	 * @param \Nette\Utils\ArrayHash $values Hodnoty z formulare
	 * @param string $activityClass Trida prilohy (napr. TNxAddActivity?)
	 */
	public function addActivity($entity, ArrayHash $values, $activityClass)
	{
		$obj = Entity::createFromForm($values, $activityClass, $this->service->getConnectionName());
		$data = $obj->toAbraFiltered($values);
		$data->SourceObject = $entity;
		$data->Source = $values['_SOURCE_ID_'];
		$request = new Request($activityClass, 'update', NULL, array('data' => array($activityClass => $data)));
		return $this->service->post($request, TRUE);
	}

	/**
	 * Provede  update dat z formulare pro vsechna predana IDs
	 * @param string $entity Entita
	 * @param \Nette\Utils\ArrayHash $values Hodnoty z formulare pro hromadny update
	 * @param array $ids IDcka
	 * @return int Pocet zaktualizovanych zaznamu
	 */
	public function massUpdate($entity, ArrayHash $values, array $ids)
	{
		$count = 0;
		$idField = $this->getEmptyInstance($entity)->getIdProperty();
		$dataArray = array();
		foreach($ids as $id)
		{
			set_time_limit(60);
			$values[$idField] = $id;

			$obj = Entity::createFromForm($values, $entity, $this->service->getConnectionName());
			$data = $obj->toAbraFiltered($values);
			$dataArray[] = $data;
			$count++;
		}

		$request = new Request($entity, 'update', NULL, array('data' => $dataArray));
		$this->service->post($request);

		return $count;
	}

	/**
	 * Vrati PDF, ktere se bude stahovat a ukladat
	 * @param string $entity Entita
	 * @param array|SessionSection $filter Filtr do requestu
	 * @param array|SessionSection $order Razeni zaznamu
	 * @param int $limit Maximalni pocet zaznamu
	 * @return \TFile
	 */
	public function massPrint($entity, $filter, $order, $limit = 100)
	{
		$req = new Request($entity, 'massprint');
		// Filtr
		if(!empty($filter))
		{
			// fieldy pro BO
			$empty = $this->getEmptyInstance($entity);
			$entityDescription = Entity::describeEntity($this->service->getConnectionName().'\\'.$entity);
			$fields = $entityDescription->fields;
			// description filtracniho formulare (SQLField)
			$desc = $this->getFilterDescription($entity)->getfields();
			$filterFields = array();
			foreach($desc as $ff)
			{
				$filterFields[$ff->name] = !empty($ff->SQLField) ? $ff->SQLField : $ff->name;
			}
			$fg = $req->setFilterGroup(FilterGroup::OPERATOR_AND);
			foreach($filter as $key => $value)
			{
				if(isset($fields[$key]) && isset($filterFields[$key]) && $value != '')
				{
					// sanitizujeme hodnotu
					$value = Entity::sanitizeValue($fields[$key]->type, $value, FALSE);
					// dame hodnotu do filtru
					$fg->addFilter($filterFields[$key], $value);
				}
			}
		}
		// Order
		if(!empty($order['orderby']))
		{
			$fg = $req->addOrder($order['orderby'], $order['orderbydir']);
		}
		// limit
		$req->limit = $limit;
		$response = $this->service->post($req, TRUE, \Abra\Service\Service::RETURN_PARSED);
        if(!empty($response->errors) && !empty($response->errors->error)) {
            throw new \ErrorException(
                $response->errors->error->message.
                (isset($response->errors->error->location) ? ' ('.$response->errors->error->location.')' : ''),
                500
            );
        }
        if(!isset($response->data->TFile[0])) {
            throw new \ErrorException('Unknown error', 500);
        }
        $tf = $response->data->TFile[0];
        $tfile = new \TFile();
        $tfile->fileName = $tf->fileName;
        $tfile->mimeType = $tf->mimeType;
        $tfile->base64data = $tf->base64data;
        return $tfile;
	}



	/**
	 * Smazani zaznamu
	 *
	 * @param string $entity Entita
	 * @param int $id ID zaznamu, ktery se bude mazat
	 */
	public function delete($entity, $id)
	{
		$data = array($entity => array());
		$data[$entity][1] = array('ID' => $id); // 0 => klic nula je bugovy :((
		$request = new Request($entity, 'delete', NULL, array('data' => $data));
		return $this->service->post($request);
	}

	/**
	 * Hromadne smazani zaznamu
	 *
	 * @param string $entity Entita
	 * @param array $ids Pole idcek zaznamu, ktere se budou mazat
	 */
	public function massDelete($entity, $ids)
	{
		$data = array($entity => array());
		$i = 1;
		foreach($ids as $id)
		{
			$data[$entity][$i] = array('ID' => $id);
			$i++;
		}
		$request = new Request($entity, 'delete', NULL, array('data' => $data));
		return $this->service->post($request);
	}


	/**
	 * Pozada o stazeni print PDF
	 *
	 * @param string $id ID zaznamu, ktery se bude tisknout
	 * @return TFile
	 */
	public function downloadPrint($entity, $id)
	{
		$data = array($entity => array());
		$data[$entity][1] = array('ID' => $id); // 0 => klic nula je bugovy :((
		$request = new Request($entity, 'print', NULL, array('data' => $data));
		$response = $this->service->post($request, TRUE, \Abra\Service\Service::RETURN_PARSED);
        if(!empty($response->errors) && !empty($response->errors->error)) {
            throw new \ErrorException(
                $response->errors->error->message.
                (isset($response->errors->error->location) ? ' ('.$response->errors->error->location.')' : ''),
                500
            );
        }
        if(!isset($response->data->TFile[0])) {
            throw new \ErrorException('Unknown error', 500);
        }        
        $tf = $response->data->TFile[0];
        $tfile = new \TFile();
        $tfile->fileName = $tf->fileName;
        $tfile->mimeType = $tf->mimeType;
        $tfile->base64data = $tf->base64data;
        return $tfile;
	}



	/**
	 * Encode FileUploads
	 * @param object $obj
	 */
	protected function encodeFileUploads($obj)
	{
		foreach($obj as $prop => $value)
		{
			if(!is_scalar($value))
			{
				if($value instanceof \Nette\Http\FileUpload)
				{
					if($value->error)
					{
						switch ($value->error)
						{
							case UPLOAD_ERR_INI_SIZE:
								$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
								break;
							case UPLOAD_ERR_FORM_SIZE:
								$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
								break;
							case UPLOAD_ERR_PARTIAL:
								$message = "The uploaded file was only partially uploaded";
								break;
							case UPLOAD_ERR_NO_FILE:
								$message = "No file was uploaded";
								break;
							case UPLOAD_ERR_NO_TMP_DIR:
								$message = "Missing a temporary folder";
								break;
							case UPLOAD_ERR_CANT_WRITE:
								$message = "Failed to write file to disk";
								break;
							case UPLOAD_ERR_EXTENSION:
								$message = "File upload stopped by extension";
								break;

							default:
								$message = "Unknown upload error";
								break;
						}
						throw new \Nette\InvalidArgumentException('Error uploading the attachment: '.$message);
					}
					/* @var $value \Nette\Http\FileUpload */
					$content = base64_encode(file_get_contents($value->getTemporaryFile()));
					$tFile = new \TFile();
					$tFile->base64data = $content;
					$tFile->fileName = $value->getName();
					$tFile->mimeType = self::detectMimeType($value->getName());
					$obj->{$prop} = $tFile;
				}
				else
				{
					// TODO: nejake zanorene kolekce ve formulari atp ???? rekurzivne volat
				}
			}
		}
	}




	/**
	 * Z predaneho nazvu souboru se pokusi detekovat MIME typ, pokud selze, vrati application/octet-stream
	 * @param type $filename
	 * @return string
	 */
	public static function detectMimeType($filename)
	{
		$mimeTypes = array(
			// others
			'csv' => 'text/csv',
			// http://snipplr.com/view.php?codeview&id=1937
			"323" => "text/h323",
			"acx" => "application/internet-property-stream",
			"ai" => "application/postscript",
			"aif" => "audio/x-aiff",
			"aifc" => "audio/x-aiff",
			"aiff" => "audio/x-aiff",
			"asf" => "video/x-ms-asf",
			"asr" => "video/x-ms-asf",
			"asx" => "video/x-ms-asf",
			"au" => "audio/basic",
			"avi" => "video/x-msvideo",
			"axs" => "application/olescript",
			"bas" => "text/plain",
			"bcpio" => "application/x-bcpio",
			"bin" => "application/octet-stream",
			"bmp" => "image/bmp",
			"c" => "text/plain",
			"cat" => "application/vnd.ms-pkiseccat",
			"cdf" => "application/x-cdf",
			"cer" => "application/x-x509-ca-cert",
			"class" => "application/octet-stream",
			"clp" => "application/x-msclip",
			"cmx" => "image/x-cmx",
			"cod" => "image/cis-cod",
			"cpio" => "application/x-cpio",
			"crd" => "application/x-mscardfile",
			"crl" => "application/pkix-crl",
			"crt" => "application/x-x509-ca-cert",
			"csh" => "application/x-csh",
			"css" => "text/css",
			"dcr" => "application/x-director",
			"der" => "application/x-x509-ca-cert",
			"dir" => "application/x-director",
			"dll" => "application/x-msdownload",
			"dms" => "application/octet-stream",
			"doc" => "application/msword",
			"dot" => "application/msword",
			"dvi" => "application/x-dvi",
			"dxr" => "application/x-director",
			"eps" => "application/postscript",
			"etx" => "text/x-setext",
			"evy" => "application/envoy",
			"exe" => "application/octet-stream",
			"fif" => "application/fractals",
			"flr" => "x-world/x-vrml",
			"gif" => "image/gif",
			"gtar" => "application/x-gtar",
			"gz" => "application/x-gzip",
			"h" => "text/plain",
			"hdf" => "application/x-hdf",
			"hlp" => "application/winhlp",
			"hqx" => "application/mac-binhex40",
			"hta" => "application/hta",
			"htc" => "text/x-component",
			"htm" => "text/html",
			"html" => "text/html",
			"htt" => "text/webviewhtml",
			"ico" => "image/x-icon",
			"ief" => "image/ief",
			"iii" => "application/x-iphone",
			"ins" => "application/x-internet-signup",
			"isp" => "application/x-internet-signup",
			"jfif" => "image/pipeg",
			"jpe" => "image/jpeg",
			"jpeg" => "image/jpeg",
			"jpg" => "image/jpeg",
			"js" => "application/x-javascript",
			"latex" => "application/x-latex",
			"lha" => "application/octet-stream",
			"lsf" => "video/x-la-asf",
			"lsx" => "video/x-la-asf",
			"lzh" => "application/octet-stream",
			"m13" => "application/x-msmediaview",
			"m14" => "application/x-msmediaview",
			"m3u" => "audio/x-mpegurl",
			"man" => "application/x-troff-man",
			"mdb" => "application/x-msaccess",
			"me" => "application/x-troff-me",
			"mht" => "message/rfc822",
			"mhtml" => "message/rfc822",
			"mid" => "audio/mid",
			"mny" => "application/x-msmoney",
			"mov" => "video/quicktime",
			"movie" => "video/x-sgi-movie",
			"mp2" => "video/mpeg",
			"mp3" => "audio/mpeg",
			"mpa" => "video/mpeg",
			"mpe" => "video/mpeg",
			"mpeg" => "video/mpeg",
			"mpg" => "video/mpeg",
			"mpp" => "application/vnd.ms-project",
			"mpv2" => "video/mpeg",
			"ms" => "application/x-troff-ms",
			"mvb" => "application/x-msmediaview",
			"nws" => "message/rfc822",
			"oda" => "application/oda",
			"p10" => "application/pkcs10",
			"p12" => "application/x-pkcs12",
			"p7b" => "application/x-pkcs7-certificates",
			"p7c" => "application/x-pkcs7-mime",
			"p7m" => "application/x-pkcs7-mime",
			"p7r" => "application/x-pkcs7-certreqresp",
			"p7s" => "application/x-pkcs7-signature",
			"pbm" => "image/x-portable-bitmap",
			"pdf" => "application/pdf",
			"pfx" => "application/x-pkcs12",
			"pgm" => "image/x-portable-graymap",
			"pko" => "application/ynd.ms-pkipko",
			"pma" => "application/x-perfmon",
			"pmc" => "application/x-perfmon",
			"pml" => "application/x-perfmon",
			"pmr" => "application/x-perfmon",
			"pmw" => "application/x-perfmon",
			"pnm" => "image/x-portable-anymap",
			"pot" => "application/vnd.ms-powerpoint",
			"ppm" => "image/x-portable-pixmap",
			"pps" => "application/vnd.ms-powerpoint",
			"ppt" => "application/vnd.ms-powerpoint",
			"prf" => "application/pics-rules",
			"ps" => "application/postscript",
			"pub" => "application/x-mspublisher",
			"qt" => "video/quicktime",
			"ra" => "audio/x-pn-realaudio",
			"ram" => "audio/x-pn-realaudio",
			"ras" => "image/x-cmu-raster",
			"rgb" => "image/x-rgb",
			"rmi" => "audio/mid",
			"roff" => "application/x-troff",
			"rtf" => "application/rtf",
			"rtx" => "text/richtext",
			"scd" => "application/x-msschedule",
			"sct" => "text/scriptlet",
			"setpay" => "application/set-payment-initiation",
			"setreg" => "application/set-registration-initiation",
			"sh" => "application/x-sh",
			"shar" => "application/x-shar",
			"sit" => "application/x-stuffit",
			"snd" => "audio/basic",
			"spc" => "application/x-pkcs7-certificates",
			"spl" => "application/futuresplash",
			"src" => "application/x-wais-source",
			"sst" => "application/vnd.ms-pkicertstore",
			"stl" => "application/vnd.ms-pkistl",
			"stm" => "text/html",
			"svg" => "image/svg+xml",
			"sv4cpio" => "application/x-sv4cpio",
			"sv4crc" => "application/x-sv4crc",
			"t" => "application/x-troff",
			"tar" => "application/x-tar",
			"tcl" => "application/x-tcl",
			"tex" => "application/x-tex",
			"texi" => "application/x-texinfo",
			"texinfo" => "application/x-texinfo",
			"tgz" => "application/x-compressed",
			"tif" => "image/tiff",
			"tiff" => "image/tiff",
			"tr" => "application/x-troff",
			"trm" => "application/x-msterminal",
			"tsv" => "text/tab-separated-values",
			"txt" => "text/plain",
			"uls" => "text/iuls",
			"ustar" => "application/x-ustar",
			"vcf" => "text/x-vcard",
			"vrml" => "x-world/x-vrml",
			"wav" => "audio/x-wav",
			"wcm" => "application/vnd.ms-works",
			"wdb" => "application/vnd.ms-works",
			"wks" => "application/vnd.ms-works",
			"wmf" => "application/x-msmetafile",
			"wps" => "application/vnd.ms-works",
			"wri" => "application/x-mswrite",
			"wrl" => "x-world/x-vrml",
			"wrz" => "x-world/x-vrml",
			"xaf" => "x-world/x-vrml",
			"xbm" => "image/x-xbitmap",
			"xla" => "application/vnd.ms-excel",
			"xlc" => "application/vnd.ms-excel",
			"xlm" => "application/vnd.ms-excel",
			"xls" => "application/vnd.ms-excel",
			"xlt" => "application/vnd.ms-excel",
			"xlw" => "application/vnd.ms-excel",
			"xof" => "x-world/x-vrml",
			"xpm" => "image/x-xpixmap",
			"xwd" => "image/x-xwindowdump",
			"z" => "application/x-compress",
			"zip" => "application/zip",

			// http://www.php.net/manual/en/function.mime-content-type.php#87856
			'txt' => 'text/plain',
			'htm' => 'text/html',
			'html' => 'text/html',
			'php' => 'text/html',
			'css' => 'text/css',
			'js' => 'application/javascript',
			'json' => 'application/json',
			'xml' => 'application/xml',
			'swf' => 'application/x-shockwave-flash',
			'flv' => 'video/x-flv',
			// images
			'png' => 'image/png',
			'jpe' => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'jpg' => 'image/jpeg',
			'gif' => 'image/gif',
			'bmp' => 'image/bmp',
			'ico' => 'image/vnd.microsoft.icon',
			'tiff' => 'image/tiff',
			'tif' => 'image/tiff',
			'svg' => 'image/svg+xml',
			'svgz' => 'image/svg+xml',
			// archives
			'zip' => 'application/zip',
			'rar' => 'application/x-rar-compressed',
			'exe' => 'application/x-msdownload',
			'msi' => 'application/x-msdownload',
			'cab' => 'application/vnd.ms-cab-compressed',
			// audio/video
			'mp3' => 'audio/mpeg',
			'qt' => 'video/quicktime',
			'mov' => 'video/quicktime',
			// adobe
			'pdf' => 'application/pdf',
			'psd' => 'image/vnd.adobe.photoshop',
			'ai' => 'application/postscript',
			'eps' => 'application/postscript',
			'ps' => 'application/postscript',
			// ms office
			'doc' => 'application/msword',
			'rtf' => 'application/rtf',
			'xls' => 'application/vnd.ms-excel',
			'ppt' => 'application/vnd.ms-powerpoint',
			// open office
			'odt' => 'application/vnd.oasis.opendocument.text',
			'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
		);
		$extension = explode('.', $filename);
		$ext = strtolower(array_pop($extension));
		if(isset($mimeTypes[$ext]))
		{
			return $mimeTypes[$ext];
		}
		else
		{
			return 'application/octet-stream';
		}
	}


}
