<?php

use \Abra\Service\Request,
	\Abra\Service\ResponseException,
	\Abra\Service\Service,
	\Nette\Security\Identity,
	\Nette\Security\IAuthenticator,
	\Nette\Security\AuthenticationException,
    \Nette\Utils\Strings;


/**
 * Overuje uzivatele proti webove sluzbe
 */
class AbraAuthenticator implements IAuthenticator
{
    use \Nette\SmartObject;

	/**
	 * @var Service
	 */
	protected $service;

    /**
     * Use NTLM authentication
     *
     * @var boolean
     */
    protected $useNtlm;

    /**
     * NTLM token
     * 
     * @var string
     */
    protected $ntlmToken;
    
    /**
     * NTLM sul
     * 
     * @var string
     */
    protected $ntlmSalt;
    
	/**
	 * Vytvori autenticator
     *
     * @param bool $useNtlm use NTLM
     * @param string $ntlmToken NTLM token
     * @param string $ntlmSalt NTLM sul
	 * @param Service $service ABRA service
	 */
	public function __construct($useNtlm, $ntlmToken, $ntlmSalt, Service $service)
	{
        $this->useNtlm = $useNtlm;
        $this->ntlmToken = $ntlmToken;
        $this->ntlmSalt = $ntlmSalt;
		$this->service = $service;
	}

	/**
	 * Performs an authentication
	 * @param  array
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials): Nette\Security\IIdentity
	{
        if($this->useNtlm) {
            return $this->authenticateNtlm($credentials);
        }
        else {
            return $this->authenticateStandard($credentials);
        }
	}

    /**
     * NTLM authentication
     *
     * @param array $credentials
     * @return Identity
     * @throws AuthenticationException
     */
    protected function authenticateNtlm(array $credentials)
    {
		$domainAndUser = $credentials[0];

        $udArray = explode('\\', $domainAndUser);
        $username = end($udArray);
        
        $token = empty($this->ntlmToken) ? $this->calculateToken($domainAndUser) : $this->ntlmToken;
        
		$request = new Request();
		$request->type = Request::TYPE_LOGIN_NTLM;
		$request->data = array(
			'loginname' => $username,
            'password' => $token,
			'ip' => $_SERVER['REMOTE_ADDR']
		);
		try {
            $this->service->setApiCredentials($username, $token);    // used for API only
			$response = $this->service->post($request, TRUE, Service::RETURN_PARSED);
		}
        catch(\Nette\Application\AbortException $e) {
            if($this->useNtlm) {
                throw new AuthenticationException("Invalid password/token for user '{$username}'", $e->getCode(), $e);
            }
            throw $e;
        }
		catch(ResponseException $e) {
			throw new AuthenticationException($e->getMessage(), $e->getCode(), $e);
		}
		catch(Abra\Service\ServiceException $se) {
			throw new AuthenticationException($se->getMessage(), $se->getCode(), $se);
		}

		// manualne vytvorime objekt User - ten nema description
		if(empty($response->data->user) || !is_array($response->data->user)) {
			throw new AuthenticationException('Chyba zadání uživatelského jména, nebo hesla', 401);
		}
		$u = new \stdClass();
		$u->User = reset($response->data->user);

		$user = \User::create($u);
		// vratime identitu
		return new Identity($user->id, null, array('user' => $user, 'auth' => $response->auth));
    }

    /**
     * Standard FORM authentication
     *
     * @param array $credentials
     * @return Identity
     * @throws AuthenticationException
     */
    protected function authenticateStandard(array $credentials)
    {
		$username = $credentials[0];
		$password = $credentials[1];

		$request = new Request();
		$request->type = Request::TYPE_LOGIN;
		$request->data = array(
			'loginname' => $username,
			'password' => $this->calculateHash($password),
			'ip' => $_SERVER['REMOTE_ADDR']
		);
		try {
            $this->service->setApiCredentials($username, $password);    // used for API only
			$response = $this->service->post($request, TRUE, Service::RETURN_PARSED);
		}
		catch(ResponseException $e) {
			throw new AuthenticationException($e->getMessage(), $e->getCode(), $e);
		}
		catch(Abra\Service\ServiceException $se) {
			throw new AuthenticationException($se->getMessage(), $se->getCode(), $se);
		}
        catch(\Exception $ee) {
            //dump($ee); die;
        }

		// manualne vytvorime objekt User - ten nema description
		if(empty($response->data->user) || !is_array($response->data->user)) {
			throw new AuthenticationException('Chyba zadání uživatelského jména, nebo hesla', 401);
		}
		$u = new \stdClass();
		$u->User = reset($response->data->user);

		$user = \User::create($u);
		// vratime identitu
		return new Identity($user->id, null, array('user' => $user, 'auth' => $response->auth));
    }

	/**
	 * Computes salted password hash.
	 * @param  string
	 * @return string
	 */
	public function calculateHash($password)
	{
		return $password; // HMMMMM PLAINTEXT !!! md5($password);
	}

    
    /**
     * Computes token from given user and domain
     * 
     * @param string $userAndDomain
     */
    public function calculateToken($userAndDomain)
    {
        $udArray = explode('\\', $userAndDomain);
        $username = end($udArray);
        return Strings::upper(hash('sha1', Strings::upper($username.$this->ntlmSalt), FALSE)); // base64_decode('KlVMVFJBVEFKTkFTVUwq')
        // 3716266F2D703D634BC0C86CDA6359CA0A24E3B8AF32493251
    }
    
}