<?php
set_time_limit(120);
ini_set('post_max_size', '128M');
ini_set('upload_max_filesize', '128M');

define('DEBUG_ENABLED', !empty($_COOKIE['debugmode']) || !empty($_COOKIE['debug']));

require LIBS_DIR.'/../vendor/autoload.php'; //Nette/nette.phar';

if(!file_exists(TEMP_DIR)) {
	$result = mkdir(TEMP_DIR);
	if($result === FALSE) {
		die("Cannot create tmp folder");
	}
}

if(!file_exists(TEMP_DIR.'/cache')) {
	$result = mkdir(TEMP_DIR.'/cache');
	if($result === FALSE) {
		die("Cannot create tmp/cache folder");
	}
}

if(!file_exists(TEMP_DIR.'/sessions')) {
	$result = mkdir(TEMP_DIR.'/sessions');
	if($result === FALSE) {
		die("Cannot create tmp/sessions folder");
	}
}

// copy empty aos.db if not exists
if(!file_exists(DATA_DIR.'/aos.db')) {
    $result = copy(APP_DIR.'/empty.db', DATA_DIR.'/aos.db');
	if($result === FALSE) {
		die("Cannot create \"aos.db\" in /data folder!");
	}
}
// test php extension
if(!in_array('pdo_sqlite', get_loaded_extensions())) {
    echo "Neni zapnuta PHP extension s nazvem php_pdo_sqlite.dll\n\n<br><br>".
        " Najdete v <code>php.ini</code> radek:\n<br>".
            "<div style='padding:10px;'><code>;extension=php_pdo_sqlite.dll</code></div>\n".
        "a odstrante na zacatku strednik:".
            "<div style='padding:10px;'><code>extension=php_pdo_sqlite.dll</code></div>\n";
    die;
}

$configurator = new Nette\Configurator();
$configurator->setDebugMode(DEBUG_ENABLED); //'23.75.345.200'); // enable for your remote IP
$configurator->enableDebugger(LOG_DIR);

\Tracy\Debugger::$maxDepth = 8;
\Tracy\Debugger::$maxLen = 80000;
\Tracy\Debugger::$strictMode = TRUE;

$configurator->setTempDirectory(TEMP_DIR);

// Pokud neni adresar pro cache, vytvorime ho
if(!is_dir(ABRA_SERVICE_CACHE_DIR)) { mkdir(ABRA_SERVICE_CACHE_DIR); }


$robotLoader = $configurator->createRobotLoader()
	->addDirectory(APP_DIR)
	->addDirectory(LIBS_DIR)
	->addDirectory(ABRA_SERVICE_CACHE_DIR)
	->register();

// Auto fix custom.neon (nette: session: => session:)
try {
    $_customNeon = Nette\Neon\Neon::decode(file_get_contents(APP_DIR.'/config/custom.neon'));
    if(!empty($_customNeon['nette'])) {
        $_customNeon += $_customNeon['nette'];
        unset($_customNeon['nette']);
        $_customNeonEncoded = Nette\Neon\Neon::encode($_customNeon, Nette\Neon\Neon::BLOCK);
        file_put_contents(APP_DIR.'/config/custom.neon', $_customNeonEncoded);
    }
}
catch(\Exception $e) { }

// Create Dependency Injection container from config.neon file
$configurator->addConfig(APP_DIR.'/config/config.neon');
if(is_file(APP_DIR.'/config/custom.neon'))
{
	$configurator->addConfig(APP_DIR.'/config/custom.neon');
}

// Odstraneni BOM - editace v notepadu
try
{
	$container = $configurator->createContainer();
	/* @var $container Nette\DI\Container */
    // Pokud je zakazany debug v configu, vypneme debug bar, zapneme production mode
    if(!$container->parameters['debug']) {
        \Tracy\Debugger::$showBar = FALSE;
        \Tracy\Debugger::enable(FALSE);
        
        Tracy\Debugger::$productionMode = TRUE;
        $container->parameters['debugMode'] = FALSE;
        $container->parameters['productionMode'] = TRUE;
    }
}
catch(\Nette\Neon\Exception $ex)
{
	if(Nette\Utils\Strings::startsWith($ex->getMessage(), "Invalid UTF-8"))
	{
		AbraHelpers::removeBOM(APP_DIR.'/config/config.neon');	// Config fix
		AbraHelpers::removeBOM(APP_DIR.'/config/custom.neon');	// Custom config fix
		die('Konfigurační soubory byly opraveny po editaci v poznámkovém bloku. Obnovte prosím stránku stiskem F5.');
	}
	else
	{
		throw $ex;
	}
}

// Nastavení maximálního času běhu PHP scriptu
if(!empty($container->parameters['maxExecutionTime'])){
    set_time_limit($container->parameters['maxExecutionTime']);
}

\Tracy\Debugger::getBar()->addPanel(new SessionPanel($container->getByType('Nette\Http\Session')));

// Configure and run the application!
$container->getByType('Nette\Application\Application')->catchExceptions = !DEBUG_ENABLED;
$container->getByType('Nette\Application\Application')->errorPresenter = 'Error';

// Culture
$culture = $container->getByType('Culture');

// Translate shortcut
function __($message, $form = 1)
{
    global $container;
    return $container->getByType(\SammTranslator\Gettext::class)->translate($message, $form);
//	return \Nette\Environment::getContext()->getByType('\SammTranslator\Gettext')
}

return $container;
