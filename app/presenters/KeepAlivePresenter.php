<?php

/**
 * Keep-Alive Presenter
 */
class KeepAlivePresenter extends BasePresenter
{
    /**
     * Keep-alive session
     */
	public function actionKeepAlive()
    {
        $lInfo = $this->session->getSection('Nette.Http.UserStorage/');
        $exp =  date('d.m.Y H:i:s', $lInfo->expireTime);
        $this->sendJson(array('expire' => $exp));
    }
}