<?php

use Abra\Service\Request;
use Abra\Service\ResponseException;
use \Nette\Application\UI\Form,
	\Nette\Utils\Strings;

/**
 * Vychozi prezenter pro jadro
 *
 * @secured
 */
class DefaultPresenter extends BasePresenter
{

	/**
	 * Nazev business objektu
	 * @persistent
	 */
	public $bo;


	/**
	 * Popis formulare pro editaci
	 * @var \Abra\Service\FormDescription
	 */
	protected $formDescription;

	/**
	 * Data do formulare
	 * @var mixed
	 */
	protected $formData;

	/**
	 * Sekce session pro filtr
	 * @var SessionSection
	 */
	protected $ssFilter;

	/**
	 * Sekce pro zvolene ID filtru
	 * @var \Nette\Http\SessionSection
	 */
	protected $ssFilterId;

	/**
	 * Sekce session pro razeni
	 * @var SessionSection
	 */
	protected $ssOrder;

	/**
	 * Sekce session pro strankovani
	 * @var SessionSection
	 */
	protected $ssPage;


	/**
	 * Entita
	 * @var string
	 */
	protected $entity;



	/**
	 * Startup
	 */
	public function startup()
	{        
		parent::startup();
        
		// pokud je dostupny jeden modul, rovnou na neho presmerujeme
		$this->template->singleModule = FALSE;
		if(count($this->modules) == 1)
		{
			$this->template->singleModule = TRUE;
			$mod = reset($this->modules);
			$key = key($this->modules);
			//dump($availMods); dump($mod); dump($modKey); die;
			if($this->currentModule !== $mod && $mod !== NULL)
			{
				//dump(':'.$mod->localModuleName.':Default:'); die;
				$this->redirect('Default:default', array('bo' => $key));
			}
		}
        
		$this->entity = $this->getParameter('bo', NULL);
		$this->ssFilter = $this->session->getSection('filter_'.$this->entity);
		$this->ssFilterId = $this->session->getSection('filterId_'.$this->entity);
		$this->ssOrder = $this->session->getSection('order_'.$this->entity);
		$this->ssPage = $this->session->getSection('page_'.$this->entity);
		if(!empty($this->entity) && $this->entity !== 's')
		{
			if(!isset($this->ssUser['allowedModules'][$this->entity]))
			{
				throw new \Exception('Module "'.$this->entity.'" is not contained in allowed modules');
			}
			$this->currentModule = $this->ssUser['allowedModules'][$this->entity];
			$this->template->currentModule = $this->currentModule;
            
            $this->template->entityNamespace = $this->abraService->getConnectionName();
            $this->template->entityName = $this->entity;
		}
        
        // pri nacteni listu se dotazeme na "forapprove" tj. kolik je u ktereho modulu dokladu cekajicich na uzivatele
        if($this->action === 'list') {
            $this->template->forApproveHash = $this->readForApprove(TRUE);
        }
        else {
            $this->template->forApproveHash = $this->readForApprove();
        }
        
        $this->redefineLocalMessages();
	}

    /**
     * Predefinuje lokalni messages pro aktualni modul
     */
    protected function redefineLocalMessages()
    {
        if(!isset($this->currentModule)) {
            return;
        }
        
        // remove old local messages
        $messages = $this->currentModule->messages;
        $newMessages = [];
        foreach($messages as $msg) {
            if(!$msg->local) {
                $newMessages[] = $msg;
            }
        }
        // are there any local messages defined in configuration
        if(!empty($this->configuration->messages[$this->currentModule->name])) {
            foreach($this->configuration->messages[$this->currentModule->name] as $m) {
                $newMessages[] = new Message((object)[
                    'actions' => $m->actions,
                    'text' => $m->text,
                    'type' => $m->type,
                    'visible' => $m->visible,
                    'local' => 'A'
                ]);
            }
        }
        // set redefined messages
        $this->currentModule->messages = $newMessages;
    }
    
	/**
	 * Cascade inputs
	 *
	 * @param string $logic Logika (and / or)
	 * @param string $class Class
	 * @param string $filters JSON string of filters
	 */
	public function handleCascade($logic, $class, $filters)
	{
		$fs = \Nette\Utils\Json::decode($filters, \Nette\Utils\Json::FORCE_ARRAY);
		$fg = NULL;
		if(!empty($fs))
		{
			$fg = new Abra\Service\FilterGroup($logic);
			foreach($fs as $filterName => $filterValue)
			{
				$fg->addFilter($filterName, $filterValue);
			}
		}
		$codelist = $this->codelistModel->getCodeList($class, $fg, TRUE);
		$this->sendResponse(new \Nette\Application\Responses\JsonResponse($codelist));
	}


	/**
	 * Razeni podle zvoleneho fieldu
	 * @param string $field
	 */
	public function handleSort($field)
	{
		$description = $this->universalModel->getListDescription($this->entity);
		$sortField = NULL;
		foreach($description->fields as $f)
		{
			if($f->name === $field)
			{
				$sortField = $f->sortfield;
				break;
			}
		}
		if(isset($sortField))
		{
			$this->ssOrder['orderbydir'] = $this->ssOrder['orderby'] === $sortField ? ($this->ssOrder['orderbydir'] == 'asc' ? 'desc' : 'asc') : 'asc';
			$this->ssOrder['orderby'] = $sortField;
			$this->ssOrder['manual'] = TRUE;
		}
		$this->redirect('this');
	}


	private function setSort($field, $dir)
	{
		if(!isset($this->ssOrder['manual']))
		{
			$this->ssOrder['orderby'] = $field;
			$this->ssOrder['orderbydir'] = $dir;
		}
	}

	private function getOrderFromCondition($conditions)
	{
		// HACK na order by
		if(!empty($conditions) && is_string($conditions) && strrpos($conditions, '<orderby>') != FALSE)
		{
			$parts = explode('<orderby>', $conditions);
			$conditions = $parts[0];
			$xmlstring = '<orderby>'.$parts[1];
			$xml = simplexml_load_string($xmlstring);
			$json = json_encode($xml);
			$data = json_decode($json,TRUE);
			$this->setSort($data['order']['col'], $data['order']['dir']);

		}
		return $conditions;
	}


	/**
	 * Smaze zaznam o predanem ID
	 * @param int $id ID zaznamu`
	 */
	public function handleDelete($id)
	{
		try
		{
			$this->universalModel->delete($this->entity, $id);
			$ok = TRUE;
		}
		catch(\Exception $ex)
		{
			$ok = FALSE;
			$this->flashMessage(__($ex->getMessage()), 'error');
		}
		if($ok)
		{
			$this->flashMessage(__('Úspěšně smazáno.'), 'success');
			$this->redirect('this');
		}
	}

    /**
     * Alias pro handlePrint
     * @param int $id 
     * @param string $ref Ref URL
     */
    public function actionPrint($id, $ref = NULL)
    {
        try {
            $this->handlePrint($id);
        }
        catch(Nette\Application\AbortException $e) {
            throw $e;
        }
        
        !empty($ref) ? $this->redirectUrl($ref) : $this->redirect('list');        
    }    
    
	/**
	 * Vytiskne zaznam o predanem ID
	 * @param int $id ID zaznamu
	 */
	public function handlePrint($id)
	{
		try
		{
			$file = $this->universalModel->downloadPrint($this->entity, $id);
			$ok = TRUE;
		}
		catch(\Exception $ex)
		{
			$ok = FALSE;
			$this->flashMessage(__($ex->getMessage()), 'error');
		}
		if($ok)
		{
			$response = $this->getHttpResponse();
			/* @var $response \Nette\Http\Response */
			$response->addHeader('Content-Type', $file->mimeType);
			$response->addHeader('Content-Disposition', 'attachment; filename="'.\Nette\Utils\Strings::webalize($file->fileName,'.').'"');
			print base64_decode($file->base64data);
			$this->terminate();
		}
	}

	/**
	 * Set per page
	 * @param int $pp Per Page
	 */
	public function handleSetPerPage($pp)
	{
		$this->ssUser['perpage-'.$this->entity] = $pp;
		$this->redirect('this');
	}

	/**
	 * Default
	 */
	public function actionDefault()
	{
		$bo = $this->getParameter('bo', NULL);
		if(!empty($bo) && $bo !== 's')
		{
			$this->redirect('list', array('bo' => $bo));
		}
		else
		{
			if (!empty($this->modules))
			{
				reset($this->modules);
				$defaultOpen = key($this->modules);
				$this->redirect("default", array("bo"=>$defaultOpen));
			}
		}
	}

    /**
     * Prehled zaznamu (LIST)
     * @param array $ids pole ID pro predfiltraci vypisu
     * @param bool $checkall zaskrtnou vsechny radky
     * @throws Exception
     */
	public function actionList(array $ids, $checkall = false)
	{        
        // bud parametr, nebo session nebo vychozi prvni stranka
		$page = $this->getParameter('p', !empty($this->ssPage['page']) ? $this->ssPage['page'] : 1);
        // Save page to session
        if($this->getParameter('p') !== NULL) {
            $this->ssPage['page'] = $page;
        }
        elseif(!empty($this->ssPage['page'])) {
            // Pokud je stranka v session ale neni v URL, presmerovat na spravnou URL
            //$this->redirect('this', array('p' => $page));
        }
        
        
        // Zvolen filtr, bude se obsluhovat filterListFormSuccess - zde je zbytecne pokracovat
		$flf = $this['filterListForm'];
        /* @var $flf Form */
        if($flf->isSubmitted()) {
            return;
        }
        
		// rozpeti viditelnych stranek + select menu
		$spread = $this->configuration['pagingSpread'];

		// limit (per page) bud se vezme z promenne, nebo default na nastaveny limit uzivatele
		$limit = empty($this->ssUser['perpage-'.$this->entity]) ? $this->user->identity->data['user']->limit : $this->ssUser['perpage-'.$this->entity];
        
		// read description + data
		$description = $this->universalModel->getListDescription($this->entity);


		// FILTERS (bud se do getList preda string nebo objekty => zvladne oboji)
		$conditions = $this->ssFilter->getIterator()->getArrayCopy();
		// Pokud nemame nikterak zafiltrovano a nemame vybrany zadny filtr (coz spolu souvisi, tak vytahneme defaultni filtr)
		if($this->ssFilter->getIterator()->count() == 0 && empty($this->ssFilterId['id']))
		{
			$filter = $this->universalModel->getEntityDefaultFilter($this->entity);
			if(!empty($filter))
			{
				$this->applyEntityFilter($this->entity, $filter->ID);
			}
			$conditions = empty($filter) ? NULL : $filter->Conditions;
		}
		elseif(!empty($this->ssFilterId['id']))
		{
			// Pokud je nastavene ID prednastaveneho filtru, vytahneme ho a dame ho do listu
			$filter = $this->universalModel->getEntityFilter($this->entity, $this->ssFilterId['id']);
			$conditions = empty($filter) ? NULL : $filter->Conditions;
		}
        
		$conditions = $this->getOrderFromCondition($conditions);

		$response = $this->universalModel->getList($this->entity, $conditions, $this->ssOrder, $limit, $page*$limit-$limit);

        // Pokud se nevrati data a jsme na strance > 1, mohl se zmenit filtr, presmerujeme na prvni stranku manualne
        if($response->data === NULL && $page > 1) {
            $this->redirect('this', array('p' => 1));
        }

		// description + data do sablony
		$this->template->description = $description;
		$this->template->data = $response->data;
		$this->template->limit = $limit;

		// dame do pole stranek uzivateluv limit, aby si ho mohl vybrat
		$userLimit = $this->user->identity->data['user']->limit;
		$perPages = array();
		foreach($this->configuration['perPages'] as $ppgs)
		{
			if($ppgs > $userLimit)
			{
				$perPages[$userLimit] = $userLimit;
			}
			$perPages[$ppgs] = $ppgs;
		}
		$this->template->perPages = array_values($perPages);

		// Strankovac
		$paginator = new \Nette\Utils\Paginator();
		$paginator->setPage($page);
		$paginator->setItemCount($response->total);
		$paginator->setItemsPerPage($limit);
		// generate steps
		if($paginator->pageCount < $spread-1)
		{
			$steps = range(1, $paginator->pageCount);
		}
		else
		{
			$arr = range(max($paginator->firstPage, $page - $spread), min($paginator->lastPage, $page + $spread));
			$count = $spread + 1; // 4
			$quotient = ($paginator->pageCount - 1) / $count;
			for ($i = 0; $i <= $count; $i++)
			{
				$arr[] = round($quotient * $i) + $paginator->firstPage;
			}
			sort($arr);
			$steps = array_values(array_unique($arr));
		}
		$this->template->steps = $steps;
		$this->template->paginator = $paginator;

		if($paginator->pageCount > 1)
		{
			$this->template->subheading = __('Strana ').$page;
		}

		// Je povolena nejaka mass akce
		if($this->currentModule->isMassAllowed())
		{
			$massForm = $this['massForm'];
		}

		try
		{
			$this->getComponent('filterForm');
			$this->template->filterFormName = 'filterForm';
		}
		catch(Exception $e)
		{
			$this->template->filterFormName = NULL;
			// kdyz neni, tak neni
			throw $e;
		}

		$this->template->orderby = isset($this->ssOrder['orderby']) ? $this->ssOrder['orderby'] : null;
		$this->template->orderbydir = isset($this->ssOrder['orderbydir']) ? $this->ssOrder['orderbydir'] : null;

		$this->template->checkall = $checkall;
	}

	/**
	 * Formular s nahledem zaznamu (akce VIEW / Detail)
	 * @param int $id Item ID
	 * @param bool $showMessage if true => will show message (for example redirected from edit Link to view where edit is not allowed)
	 */
	public function actionView($id, $showMessage = false)
	{
        $this->template->moduleMenuCookieValue = 'collapsed';
                
		$form = $this->getComponent('form');
		// Is this action allowed for current record ?
		/*if(!$this->currentModule->isActionAllowedFor('view', $this->formData))
		{
			throw new Nette\Application\BadRequestException('Not allowed for this record!', 403);
		}*/
		/* @var $form Form */
		// vynutime pouze zobrazeni
		$this->template->forceViewOnly = TRUE;
		$this->template->showMessage = $showMessage;
		if($showMessage) {
			$this->template->showMessageText = __($this->currentModule->actionsHash['form']->warningmessage);
		}
		if(!$form->isSubmitted())
		{
			$valz = $this->prepareFormValues($this->formData);
			$form->setDefaults($valz);
		}
	}


	/**
	 * Formular s editaci zaznamu (akce FORM)
	 * @param int $id Item ID
	 */
	public function actionForm($id)
	{
        $this->template->moduleMenuCookieValue = 'collapsed';
		$form = $this->getComponent('form');
        
		// Is this action allowed for current record ?
        $idProp = $this->formData->getIdProperty();
        $f = '<filters><filter-and><filter><name>'.$idProp.'</name><value>'.$id.'</value><operator>=</operator></filter></filter-and></filters>';
        $resp = $this->universalModel->getList($this->entity, $f, NULL, 1);
//        dump($resp); die;
        if(!isset($resp->data[$id])) {
            throw new Nette\Application\BadRequestException('Invalid document "'.$id.'"', 404);
        }
        $listData = $resp->data[$id];
		if(!$this->currentModule->isActionAllowedFor('form', $listData)) {
		//if(1) {
            // If form is disallowed - redirect to view
            $this->redirect('view', array('id' => $id)); //, 'showMessage' => true
			// throw new Nette\Application\BadRequestException('Not allowed for this record!', 403);
		}
        
		/* @var $form Form */
		if(!$form->isSubmitted())
		{
			//dump($this->formData);
			//dump($this->formData->getDescription());
			$valz = $this->prepareFormValues($this->formData);
			//dump($valz);
			$form->setDefaults($valz);
		}
	}

	/**
	 * Formular s editaci - ovsem s kopii
	 * @param type $id
	 */
	public function actionCopy($id)
	{
		$form = $this->getComponent('form');
		// Is this action allowed for current record ?
		/*if(!$this->currentModule->isActionAllowedFor('copy', $this->formData))
		{
			throw new Nette\Application\BadRequestException('Not allowed for this record!', 403);
		}*/
		/* @var $form Form */
		if(!$form->isSubmitted())
		{
			// Odebrat ID a DisplayName
			if(isset($this->formData->ID))
			{
				$this->formData->ID = NULL;
			}
			if(isset($this->formData->DisplayName))
			{
				$this->formData->DisplayName = NULL;
			}
			$valz = $this->prepareFormValues($this->formData);
			// Odebrat ID a DisplayName
			if(isset($valz['ID']))
			{
				unset($valz['ID']);
			}
			if(isset($valz['DisplayName']))
			{
				unset($valz['DisplayName']);
			}
			$form->setDefaults($valz);
		}
	}


	/**
	 * Formular pro zalozeni nove polozky
	 */
	public function actionAdd()
	{
		$form = $this->getComponent('add');
		/* @var $form Form */
		if(!$form->isSubmitted())
		{
			//dump($this->formData);
			//dump($this->formData->getDescription());
			$valz = $this->prepareFormValues($this->formData);
//			dump($valz);
			$form->setDefaults($valz);
		}
	}

	/**
	 * Formular pro upload prilohy k dokladu
	 */
	public function actionAddAttachment()
	{
		$form = $this->getComponent('addAttachment');
		// Is this action allowed for current record ?
		/*if(!$this->currentModule->isActionAllowedFor('addattachment', $this->formData))
		{
			throw new Nette\Application\BadRequestException('Not allowed for this record!', 403);
		}*/
		$this->formData->_Parent = $this->universalModel->getDisplayName($this->currentModule->name, $this->getParameter('id'));
		/* @var $form Form */
		if(!$form->isSubmitted())
		{
			//dump($this->formData);
			//dump($this->formData->getDescription());
			$valz = $this->prepareFormValues($this->formData);
			//dump($valz);
			$form->setDefaults($valz);
		}
	}

	/**
	 * Formular pro zadani aktivity k dokladu
	 */
	public function actionAddactivity()
	{
		$form = $this->getComponent('addActivity');
		// Is this action allowed for current record ?
		/*if(!$this->currentModule->isActionAllowedFor('addactivity', $this->formData))
		{
			throw new Nette\Application\BadRequestException('Not allowed for this record!', 403);
		}*/
		$this->formData->_Parent = $this->universalModel->getDisplayName($this->currentModule->name, $this->getParameter('id'));
		/* @var $form Form */
		if(!$form->isSubmitted())
		{
			$valz = $this->prepareFormValues($this->formData);
			$form->setDefaults($valz);
		}
	}


	/**
	 * Vytvori formular pro fitraci
	 * @param string $name
	 * @return Form
	 */
	public function createComponentFilterForm($name)
	{
		$this->template->filterDescription = $this->universalModel->getFilterDescription($this->entity);
		// empty instance
		$form = $this->createForm($name, $this->template->filterDescription, $this->universalModel->getEmptyInstance($this->entity));
        $form->addHidden('_cancel_filter', '0');
		unset($form['submit']);
		$form->addSubmit('submit', 'Filtrovat');
		if($this->currentModule->isAllowed('massprint'))
		{
			$form->addSubmit('massprint', $this->currentModule->actionsHash['massprint']->caption);
		}
		$form->addButton('cancelFilter', 'Zrušit');

		// Skryte pole pro zadany nazev ulozeneho filtru
		$form->addHidden('_filterName', '');
		// ukladaci tlacitka pro vlastni a globalni filtr
		if($this->currentModule->isAllowed('filteredit'))
		{
			$form->addSubmit('saveMainSubmit', 'Uložit vlastní filtr');
		}
		if($this->currentModule->isAllowed('filterglobaledit'))
		{
			$form->addSubmit('saveGlobalSubmit', 'Uložit společný filtr');
		}

		$form->addHidden('orderby');
		$form->addHidden('orderbydir');

		$form->onSuccess[] = [$this, 'filterFormSuccess'];
		$form->onError[] = [$this, 'filterFormError'];

		if(!$form->isSubmitted())
		{
			// Fix pro hodnoty, ktere v ciselniku jeste nebyly
			foreach($this->ssFilter as $filter => $value)
			{
				if(trim($value) === '')
				{
					unset($this->ssFilter[$filter]);
				}
			}
			// Set Filter Values
            $values = $this->ssFilter->getIterator()->getArrayCopy();
			$form->setValues($values, TRUE);
		}
		$form->setTranslator($this->formTranslator);
		// Form
		return $form;
	}

	/**
	 * Obsluha fitlracniho formulare
	 * @param Form $form Formular
	 */
	public function filterFormSuccess($form)
	{
        $values = $form->getValues();
		// zda se ma odebrat predvolba
		$removePreset = TRUE;

		// odebrat stare filtry
		$this->ssFilter->remove();

		// pokud nebyl zruseny filtr, zafiltrujeme
		if(empty($values['_cancel_filter']))
		{
			foreach($values as $key => $val)
			{
				if($key != '_filterName' && $key != 'orderby' && $key != 'orderbydir')
				{
					$this->ssFilter[$key] = $val;
				}
			}
            
			// Pokud bylo odeslano ukladacim tlacitkem, tak ulozime filtr
			// ----------------------------------------------------------

			// Ulozit vlastni filtr
			if(isset($form['saveMainSubmit']) && $form['saveMainSubmit']->isSubmittedBy() && $values['_filterName'] != '')
			{
				$flt = $this->universalModel->saveEntityFilter($this->entity, $values['_filterName'], $this->ssFilter, FALSE, FALSE, $values['orderby'], $values['orderbydir']);
				$this->ssFilterId['id'] = $flt->ID;
				$removePreset = FALSE;
				$this->flashMessage(__('Vlastní filtr byl uložen.'), 'success');
			}
			if(isset($form['saveGlobalSubmit']) && $form['saveGlobalSubmit']->isSubmittedBy() && $values['_filterName'] != '')
			{
				$flt = $this->universalModel->saveEntityFilter($this->entity, $values['_filterName'], $this->ssFilter, TRUE, FALSE, $values['orderby'], $values['orderbydir']);
				$this->ssFilterId['id'] = $flt->ID;
				$removePreset = FALSE;
				$this->flashMessage(__('Společný filtr byl uložen.'), 'success');
			}
		}
        
		// pokud se odeslal formular massprint tlacitkem, tak stahneme soubor
		if(isset($form['massprint']) && $form['massprint']->isSubmittedBy())
		{
			// Byl odeslan hromadny tisk
			if(!$this->currentModule->isAllowed('massprint'))
			{
				throw new \Nette\InvalidStateException('Mass print není povolen');
			}
			try
			{
				$file = $this->universalModel->massPrint($this->entity, $this->ssFilter, $this->ssOrder);
				$ok = TRUE;
			}
			catch(\Exception $ex)
			{
				$ok = FALSE;
				$this->flashMessage(__($ex->getMessage()), 'error');
			}
			if($ok)
			{
				$response = $this->getHttpResponse();
				/* @var $response \Nette\Http\Response */
				$response->addHeader('Content-Type', $file->mimeType);
				$response->addHeader('Content-Disposition', 'attachment; filename="'.\Nette\Utils\Strings::webalize($file->fileName).'"');
				print base64_decode($file->base64data);
				$this->terminate();
			}
		}
		else
		{
			// odebrat predvolbu !
			if($removePreset)
			{
				$this->ssFilterId->remove();
			}
		}
		// presmerujeme na this
		$this->redirect('this');
	}


	/**
	 * Formular se seznamem filtru
	 *
	 * @param string $name
	 * @return Form
	 */
	public function createComponentFilterListForm($name)
	{
		// Filters for this entity
		$filters = $this->universalModel->getEntityFilters($this->entity);
		if(empty($filters))
		{
			$filtersAll = array();
			$filtersHash = array();
		}
		else
		{
			$filtersGlobal = array();
			$filtersCustom = array();
			$filtersHash = array();
			// rozdelime na filtry globalni a uzivatelske
			foreach($filters as $filter)
			{
				/* @var $filter WebFilter */
				if($filter->Global == 'A')
				{
					$filtersGlobal[$filter->ID] = $this->sammTranslator->translate($filter->Name);
				}
				else
				{
					$filtersCustom[$filter->ID] = $this->sammTranslator->translate($filter->Name);
				}
				$filtersHash[$filter->ID] = $filter;
			}
			$filtersAll = array();
			if(!empty($filtersGlobal))
			{
				$filtersAll['Společné filtry'] = $filtersGlobal;
			}
			if(!empty($filtersCustom))
			{
				$filtersAll['Vlastní filtry'] = $filtersCustom;
			}
		}
		$form = new Form($this, $name);
		$form->setTranslator($this->formTranslator);
		$form->addSelect('filter', 'Zvolte filtr:', $filtersAll)->setPrompt('');
		// akce
		$form->addSubmit('submit', 'Použít');
		$form->addSubmit('delete', 'Odebrat filtr');
		$form->onSuccess[] = [$this, 'filterListFormSuccess'];

		// Nastavime hodnotu pro filtr
		if($this->ssFilter->getIterator()->count() == 0 && empty($this->ssFilterId['id']))
		{
			$defaultFilter = $this->universalModel->getEntityDefaultFilter($this->entity);
			$form['filter']->setDefaultValue($defaultFilter === NULL ? $this->ssFilterId['id'] : $defaultFilter->ID);
		}
		else
		{
			if(isset($form['filter']->items[$this->ssFilterId['id']]))
			{
				$form['filter']->setDefaultValue($this->ssFilterId['id']);
			}
		}
		// Mega hack
		$this->template->entityFilters = $filtersAll;
		$this->template->entityFiltersHash = $filtersHash;
		// Vratime form
		return $form;
	}


	/**
	 * Uspesne odeslani formulare se seznamem filtru
	 * @param Form $form
	 */
	public function filterListFormSuccess($form)
	{
		$values = $form->getValues();

		unset($this->ssOrder['manual']);

		// odebrat stary filter ID
		$this->ssFilterId->remove();
		// byl vybran filtr ?
		if($form['submit']->isSubmittedBy())
		{
			// vytahneme si ho z ABRY a pokud se opravdu vrati nastavime ho do sessions
			$this->applyEntityFilter($this->entity, $values['filter']);
		}
		elseif($form['delete']->isSubmittedBy())
		{
			$filter = $this->universalModel->getEntityFilter($this->entity, $values['filter']);
			// Test jestli mohu mazat (podle toho jake akce mam povolene pro tento modul)
			if(($filter->Global === 'A' && $this->currentModule->isAllowed('filterglobaledit')) || ($filter->Global !== 'A' && $this->currentModule->isAllowed('filteredit')))
			{
				$this->universalModel->deleteEntityFilter($values['filter']);
				$this->flashMessage(__('Filtr "'.$filter->Name.'" byl odebrán.'), "success");
			}
			else
			{
				$this->flashMessage(__('Nemáte oprávnění smazat tento filtr.'), "error");
			}
		}
		else
		{
			// vytahneme si ho z ABRY a pokud se opravdu vrati nastavime ho do sessions
			$this->applyEntityFilter($this->entity, $values['filter']); // hack pro povoleni on select change
		}

		$this->redirect('this');
	}

	/**
	 * Apply-kuje entity filter
	 * @param string $entity Entita
	 * @param string $filterId Filter ID
	 */
	protected function applyEntityFilter($entity, $filterId)
	{
		$entityFilter = $this->universalModel->getEntityFilter($entity, $filterId);

		if(!empty($entityFilter) && !empty($entityFilter->Conditions))
		{

			$this->ssFilterId['id'] = $filterId;
			$this->ssFilter->remove();
            $this->ssOrder->remove();
            $this->ssPage->remove();

			$entityFilter->Conditions = $this->getOrderFromCondition($entityFilter->Conditions);

			// nandame filtr do ssFilter
			$xml = new \DOMDocument();
			$xml->loadXML($entityFilter->Conditions);
			foreach($xml->documentElement->firstChild->childNodes as $flt)
			{
				/* @var $flt \DOMNode */
				$children = array();
				foreach($flt->childNodes as $chNode)
				{
					/* @var $chNode \DOMNode */
					$children[$chNode->nodeName] = $chNode->nodeValue;
				}
				if(isset($children['name']) && isset($children['value']))
				{
					$val = $children['value'];
					if(Strings::endsWith(Strings::lower($children['name']), '_id') && $val === '')
					{
						$val = NULL;
					}
					$this->ssFilter[$children['name']] = $val;
				}
			}
		}
	}


	/**
	 * Nastgavi filtr o predanem ID jako vychozi
	 * @param string $filterId
	 */
	public function handleSetDefaultFilterPreset($filterId)
	{
		$filter = $this->universalModel->getEntityFilter($this->entity, $filterId);
		if(!$filter->Global)
		{
			$this->flashMessage(__('Vlastní filtr není možné nastavit jako výchozí!'), "error");
		}
		else
		{
			$filter->Default = TRUE;
			$this->universalModel->updateEntityFilter($filter);
			$this->flashMessage(__('Filtr byl nastaven jako výchozí!'), "success");
		}
		$this->redirect('this');
	}

	// ------------------------------------------------------------------- AKCE: add ---------------------------------------------------------------------

	/**
	 * Vytvori formular pro editaci
	 * @param string $name
	 */
	public function createComponentAdd($name)
	{
		$pSource = $this->getParameter('prefillsource', NULL);
		$pSourceObject = $this->getParameter('prefillsourceobject', NULL);

		$this->template->description = $this->formDescription = $this->universalModel->getAddDescription($this->entity);
		$this->template->data = $this->formData = $this->universalModel->getPrefill($this->entity, $pSourceObject, $pSource);

		$form = $this->createForm($name, $this->formDescription, $this->formData, TRUE);
        if($this->bo === "TNxIssuedOrderAdd")
        {
            $form->addSubmit('validate', 'Validovat');
        }
		$form->onSuccess[] = [$this, 'addSuccessHandler'];
		$form->onError[] = [$this, 'formErrorHandler'];
		$form->setTranslator($this->formTranslator);

		return $form;
	}


	/**
	 * Formular byl uspesne odeslan
	 * @param Form $form
	 */
	public function addSuccessHandler(Form $form)
	{
		$values = $form->getValues();
		$data = $form->getHttpData();
        $isValidate = isset($form['validate']) && $form['validate']->isSubmittedBy();
        $action = $isValidate ? Request::ACTION_VALIDATE : Request::ACTION_UPDATE;
        //echo($action); die;
		try
		{
            $response = $this->universalModel->create($this->entity, $values, $this->getHttpRequest()->getPost(), $action);
			if($isValidate)
			{				
				// clear main doc ID when creating
				$response->data->ID = NULL;
				
				$this->template->data = $this->formData = $response->data;

            	$form->setValues($this->prepareFormValues($response->data));
				return;
			}			
		}
		catch(ResponseException $e)
		{
			foreach($e->getErrors() as $err)
			{
				$this->flashMessage(__($err->message), 'error');
			}
            if($this->isAjax()) { $this->redrawControl('flash'); }
			return;
		}
		catch(\Exception $e)
		{
			$this->flashMessage(__($e->getMessage()), 'error');
            if($this->isAjax()) { $this->redrawControl('flash'); }
			return;
		}
		$this->flashMessage(__('Záznam byl úspěšně vložen'), 'info');
		// ref policko ve formulari
		if(!empty($values['__ref']))
		{
			$this->redirectUrl($values['__ref']);
		}
		elseif(!empty($values['_submittedby_']) && isset($data[$values['_submittedby_'].'_goto']))
		{
			// Akcni tlacitko, kterym byl form odeslan
			$this->redirectUrl($data[$values['_submittedby_'].'_goto']);
		}
		else
		{
			// Default redirect
			$this->redirect('default');
		}
	}


	// ------------------------------------------------------------------- AKCE: add activity -----------------------------------------------------------


	/**
	 * Vytvori formular pro pridani aktivity
	 *
	 * @param string $name Nazev komponenty
	 * @return Form
	 */
	public function createComponentAddActivity($name)
	{
		if(!isset($this->template->modules[$this->entity]))
		{
			throw new \Exception('No entity: '.$this->entity.' found in allowed modules.');
		}
		if(!isset($this->template->modules[$this->entity]->actionsHash['addactivity']))
		{
			throw new \Exception('No action addActivity found for '.$this->entity);
		}
		if(!isset($this->template->modules[$this->entity]->actionsHash['addactivity']->object))
		{
			throw new \Exception('Action addActivity for entity '.$this->entity.' has no object defined!');
		}
		$object = $this->template->modules[$this->entity]->actionsHash['addactivity']->object;

		// precteme si description formulare a prefill
		$this->template->description = $this->formDescription = $this->universalModel->getAddActivityDescription($object);

		$bo = $this->getParameter('bo');
		$boId = $this->getParameter('id');
        if(empty($boId)) {
            $boId = $this->getParameter('prefillsource');
        }        
		$this->template->data = $this->formData = $this->universalModel->getPrefill($object, $bo, $boId);

		$form = $this->createForm($name, $this->formDescription, $this->formData, TRUE);
		$form->setAction($this->link('this', array('id' => $boId)));

		// Do supertajneho skryteho pole si dame ID dokladu, ke kteremu budeme uploadovat dokument
		$form->addHidden('_SOURCE_ID_', $boId);

		$form->onSuccess[] = [$this, 'addActivitySuccessHandler'];
		$form->onError[] = [$this, 'formErrorHandler'];
		$form->setTranslator($this->formTranslator);

		return $form;
	}

	/**
	 * Formular s prilohou byl uspesne odeslan
	 * @param Form $form
	 */
	public function addActivitySuccessHandler(Form $form)
	{
		$values = $form->getValues();
		try
		{
			$object = $this->template->modules[$this->entity]->actionsHash['addactivity']->object;
			$this->universalModel->addActivity($this->entity, $values, $object);
			if(DEBUG_ENABLED && isset($form['dbgsubmit']) && $form['dbgsubmit']->isSubmittedBy())
			{
				return;
			}
		}
		catch(ResponseException $e)
		{
			foreach($e->getErrors() as $err)
			{
				$this->flashMessage(__($err->message), 'error');
			}
            if($this->isAjax()) { $this->redrawControl('flash'); }
			return;
		}
		catch(\Exception $e)
		{
			$this->flashMessage(__($e->getMessage()), 'error');
            if($this->isAjax()) { $this->redrawControl('flash'); }
			return;
		}
		$this->flashMessage(__('Úspěšně uloženo.'), 'success');
		$this->redirect('default');
	}


	// ------------------------------------------------------------------- AKCE: add attachment -----------------------------------------------------------

	/**
	 * Vytvori formular pro editaci
	 *
	 * @param string $name Nazev komponenty
	 * @return Form
	 */
	public function createComponentAddAttachment($name)
	{
		if(!isset($this->template->modules[$this->entity]))
		{
			throw new \Exception('No entity: '.$this->entity.' found in allowed modules.');
		}
		if(!isset($this->template->modules[$this->entity]->actionsHash['addattachment']))
		{
			throw new \Exception('No action addAttachment found for '.$this->entity);
		}
		if(!isset($this->template->modules[$this->entity]->actionsHash['addattachment']->object))
		{
			throw new \Exception('Action addattachment for entity '.$this->entity.' has no object defined!');
		}
		$object = $this->template->modules[$this->entity]->actionsHash['addattachment']->object;

		// precteme si description formulare a prefill
		$this->template->description = $this->formDescription = $this->universalModel->getAddAttachmentDescription($object);

		$bo = $this->getParameter('bo');
		$boId = $this->getParameter('id');
        if(empty($boId)) {
            $boId = $this->getParameter('prefillsource');
        }
		$this->template->data = $this->formData = $this->universalModel->getPrefill($object, $bo, $boId);

		$form = $this->createForm($name, $this->formDescription, $this->formData, TRUE);
		$form->setAction($this->link('this', array('id' => $boId)));

		// Do supertajneho skryteho pole si dame ID dokladu, ke kteremu budeme uploadovat dokument
		$form->addHidden('_SOURCE_ID_', $boId);

		$form->onSuccess[] = [$this, 'addAttachmentSuccessHandler'];
		$form->onError[] = [$this, 'formErrorHandler'];
		$form->setTranslator($this->formTranslator);

		return $form;
	}


	/**
	 * Formular s prilohou byl uspesne odeslan
	 * @param Form $form
	 */
	public function addAttachmentSuccessHandler(Form $form)
	{
		$values = $form->getValues();
		try
		{
			$object = $this->template->modules[$this->entity]->actionsHash['addattachment']->object;
			
			$keyFile = NULL;
			foreach($values as $k => $v) {
				if(is_array($v) && !empty($v) && $v[0] instanceof \Nette\Http\FileUpload) {
					$keyFile = $k;
				}
			}
			if(!$keyFile) {
				throw new Exception('No files uploaded!');
			}
			foreach($values[$keyFile] as $oneFile) {
				$vals = clone $values;
				$vals[$keyFile] = $oneFile;
				$this->universalModel->addAttachment($this->entity, $vals, $object);
			}
			if(DEBUG_ENABLED && isset($form['dbgsubmit']) && $form['dbgsubmit']->isSubmittedBy())
			{
				return;
			}
		}
		catch(ResponseException $e)
		{
			foreach($e->getErrors() as $err)
			{
				$this->flashMessage(__($err->message), 'error');
			}
            if($this->isAjax()) { $this->redrawControl('flash'); }
			return;
		}
		catch(\Exception $e)
		{
			$this->flashMessage(__($e->getMessage()), 'error');
            if($this->isAjax()) { $this->redrawControl('flash'); }
			return;
		}
        
        $documentName = '';
		try
		{        
            $dstEntity = $this->universalModel->getForm($this->entity, $values['_SOURCE_ID_']);
            $documentName = (string)$dstEntity;
        }
        catch(\Exception $e) {
            \Tracy\Debugger::log($e);
        }
		$this->flashMessage(__('Soubor byl úspěšně přiložen k dokladu ').$documentName, 'success');
        
        if(!empty($values->__ref)) {
            $this->redirectUrl($values->__ref);
        }
        else {
            $this->redirect('default');
        }
	}





	// ------------------------------------------------------------------- AKCE: form ---------------------------------------------------------------------

    
    /**
     * Rozsiri kolekce (POUZE V PRVNI UROVNI)
     * 
     * @param mixed $abra
     * @param array $httpData
     * @return Data s rozsirenymi kolekcemi
     */
    protected function extendCollections($abra, $httpData)
    {
        foreach($abra as $prop => $val) {
            if(is_array($val)) {
                // Pokud bylo v odeslanych POST datech vice polozek v kolekci nez 
                if(isset($httpData[$prop]) && count($val) < count($httpData[$prop])) {
                    if(count($val) > 0) {
                        $first = reset($val);
                        $clone = clone $first;
                        $i = count($val);
                        while(isset($httpData[$prop][$i])) {
                            foreach($httpData[$prop][$i] as $prop2 => $val2) {
                                if(property_exists($clone, $prop2)) {
                                    $clone->{$prop2} = $val2;
                                }
                            }
                            $abra->{$prop}[] = $clone;
                            $i++;
                        }
                    }
                }
            }
        }
        return $abra;
    }
    
	/**
	 * Vytvori formular pro editaci
	 * @param string $name
	 */
	public function createComponentForm($name)
	{
		$this->template->description = $this->formDescription = $this->universalModel->getFormDescription($this->entity);
		$this->template->data = $this->formData = $this->universalModel->getForm($this->entity, $this->getParameter('id'));

		$form = $this->createForm($name, $this->formDescription, $this->formData);

        if($this->bo === "TNxIssuedOrderAdd")
        {
            $form->addSubmit('validate', 'Validovat');
        }
        if($form->isSubmitted()) {
            $this->template->data = $this->extendCollections($this->template->data, $form->getHttpData());
        }

		$form->onSuccess[] = [$this, 'formSuccessHandler'];
		$form->onError[] = [$this, 'formErrorHandler'];
		$form->setTranslator($this->formTranslator);

		return $form;
	}


	/**
	 * Formular byl uspesne odeslan
	 * @param Form $form
	 */
	public function formSuccessHandler(Form $form)
	{
		$values = $form->getValues();
		$data = $form->getHttpData();
        $this->template->sentValues = $data;
		$isNotice = FALSE;
		$isValidate = isset($form['validate']) && $form['validate']->isSubmittedBy();
		$action = $isValidate ? Request::ACTION_VALIDATE : Request::ACTION_UPDATE;
		
		
		try
		{
			$response = $this->universalModel->update($this->entity, $values, $this->getHttpRequest()->getPost(), $action);

			if($isValidate)
			{

				$this->template->data = $this->formData = $response->data;

            	$form->setValues($this->prepareFormValues($response->data));
				return;
			}
		}
		catch(ResponseException $e)
		{
			$isNotice = TRUE;
			foreach($e->getErrors() as $err)
			{
				// @var $err \Abra\Service\Error
				$this->flashMessage($err->getMessage(), 'error');
			}
            if($this->isAjax()) {
                $this->redrawControl('flash');
                return;
            }
			if($e->isFatal())
			{
				return;
			}
		}
		catch(\Exception $e)
		{
			$this->flashMessage(__($e->getMessage()), 'error');
            if($this->isAjax()) { $this->redrawControl('flash'); }
			return;
		}

		if(!$isNotice)
		{
			$this->flashMessage(__('Záznam byl úspěšně uložen'), 'info');
		}
		// ref policko ve formulari
		if(!empty($values['__ref']))
		{
			$this->redirectUrl($values['__ref']);
		}
		elseif(!empty($values['_submittedby_']) && isset($data[$values['_submittedby_'].'_goto']))
		{
			// Akcni tlacitko, kterym byl form odeslan
			$this->redirectUrl($data[$values['_submittedby_'].'_goto']);
		}
		else
		{
			// Default redirect
			$this->redirect('default');
		}
	}






	// ------------------------------------------------------------------- AKCE: massform ---------------------------------------------------------------------

	/**
	 * Vytvori komponentu pro hromadne akce
	 * @param string $name Nazev komponenty
	 * @return Form
	 */
	public function createComponentMassForm($name)
	{
		// Data (v defaultu predana do sablony)
		$data = $this->template->data;

		// Vytvoreni formulare
		$form = new Form($this, $name);
		$form->setTranslator($this->formTranslator);
		if($data === NULL)
		{
			return $form;
		}

		// ID container pro checkboxy
		$ids = $form->addContainer('id');
		foreach($data as $row)
		{
            $webRowId = AbraHelpers::webalizeId($row->getId());
			$ids->addCheckbox($webRowId, $row->getId());
		}


		// massform - POKUD JE POVOLENA MASS FORM - sahneme si pro description MASS FORMu a VYTVORIME FORMULAR
		if($this->currentModule->isAllowed('massform'))
		{
			// TODO - az to tomas dodela, tak to tady dopisu :)
			$massDescription = $this->universalModel->getMassFormDescription($this->entity);
			//\Tracy\Debugger::$maxDepth = 18; dump($massDescription); die;

			$emptyInstance = $this->universalModel->getEmptyInstance($this->entity);
			$entityNS = explode('\\', get_class($emptyInstance));
			$entityClass = array_pop($entityNS);
			$namespace = empty($entityNS) ? NULL : implode('\\', $entityNS);
			// Pole ve formulari pouzitych kontejneru
			$containers = array('#' => $form);
			// Polozky
			$fields = $massDescription->getFields();
			$this->addFields($name, $entityClass, $fields, $containers, $entityClass, $namespace, $emptyInstance);
			// Validacni pravidla
			$allRules = $massDescription->getAllRules();
			$this->addRules($form, $allRules);
			// submit tlacitko
			$form->addSubmit('massform', $this->currentModule->getCaption('massform'));

			$this->template->massDescription = $massDescription;
			$this->template->massData = $emptyInstance;
		}


		// Pokud je povolena akce MASS PRINT, tak dame tlacitko pro hromadny tisk
		if($this->currentModule->isAllowed('massprint'))
		{
			$form->addSubmit('massprint', $this->currentModule->getCaption('massprint'));
		}
		// Pokud je povolena akce MASS DELETE, tak dame tlacitko pro hromadne smazani
		if($this->currentModule->isAllowed('massdelete'))
		{
			$form->addSubmit('massdelete', $this->currentModule->getCaption('massdelete'));
		}
		// On Success
		$form->onSuccess[] = [$this, 'massFormSuccess'];
		// Vratime formular
		return $form;
	}

	/**
	 * Uspesne odeslani formulare s hromadnou akci
	 * @param Form $form
	 */
	public function massFormSuccess($form)
	{
		$values = $form->getValues();
		// Vsechna ID
		$allIds = array_keys(array_filter((array)$_POST['id'])); // HACK $values['id'] not work on page=x
        // unwebalize IDs
        foreach($allIds as $aidIndex => $aidValue) {
            $allIds[$aidIndex] = AbraHelpers::unwebalizeId($aidValue);
        }

		// Bylo odeslano hromadne smazani
		if($this->currentModule->isAllowed('massdelete') && $form['massdelete']->isSubmittedBy())
		{
			try
			{
				$this->universalModel->massDelete($this->entity, $allIds);
				$this->flashMessage(__('Záznamy byly úspěšně smazány'), 'info');
				$ok = TRUE;
			}
			catch(\Exception $ex)
			{
				$this->flashMessage(__($ex->getMessage()), 'error');
				$ok = FALSE;
			}
			if($ok)
			{
				$this->redirect('this');
			}
			return;
		}

		// Bylo odeslano hromadne schvaleni
		if($this->currentModule->isAllowed('massform') && $form['massform']->isSubmittedBy())
		{
			unset($values['id']);
			$this->universalModel->massUpdate($this->entity, $values, $allIds);
			$this->flashMessage(__('Úspěšně uloženo.'), 'info');
			$this->redirect('this');
		}
	}

	// ------------------------------------------------------------------- AKCE: massform ---------------------------------------------------------------------




	// ------------------------------------------------------------------- AKCE: add attachment --------------------------------------------------------------

	/**
	 * Vytvori komponentu pro pripojeni prilohy
	 * @param string $name Component name
	 * @return Form
	 */
	public function createComponentAttachmentForm($name)
	{
		$form = new Form($this, $name);

		$form->addUpload('document', 'Document');

		$form->onSuccess[] = [$this, 'attachmentFormSuccess'];
		return $form;
	}

	/**
	 * Uspesne odeslani formulare pro pripojeni prilohy
	 * @param Form $form
	 */
	public function attachmentFormSuccess($form)
	{
		$values = $form->getValues();
		dump($values);
		die;
	}


	// ------------------------------------------------------------------- AKCE: add attachment --------------------------------------------------------------

}
