<?php

use \Nette\Application\UI\Form,
	\Abra\Service\Entity,
	\Nette\Utils\Strings,
	\Abra\Service\EntityLoader;

/**
 * Predek pro vsechny
 */
abstract class BasePresenter extends \Nette\Application\UI\Presenter
{
	/**
	 * Nazev fieldu pro vyber schvalit / zamitnout / vratit
	 */
	const APPROVE_FIELD_NAME = 'X_ApproveType';

    /**
     * Culture
     * 
     * @var Culture
     */
    protected $culture;
    
	/**
	 * Translator
	 *
	 * @var \SammTranslator\Gettext
	 */
	protected $sammTranslator;

    /**
     * Form Translator
     * 
     * @var \SammTranslator\FormTranslator
     */
    protected $formTranslator;
    
	/**
	 * Entity loader
	 *
	 * @var Abra\Service\EntityLoader
	 */
	protected $entityLoader;

	/**
	 * ABRA Web Service
	 *
	 * @var \Abra\Service\XmlService
	 */
	protected $abraService;

	/**
	 * Codelist model
	 *
	 * @var CodelistModel
	 */
	protected $codelistModel;

	/**
	 * Modules Model
	 *
	 * @var ModulesModel
	 */
	protected $modulesModel;

	/**
	 * User Model
	 *
	 * @var UserModel
	 */
	protected $userModel;

	/**
	 * Universal Model
	 *
	 * @var UniversalModel
	 */
	protected $universalModel;

	/**
	 * Cache Model
	 * @var CacheModel
	 */
	protected $cacheModel;



	/**
	 * Sekce
	 * @var Nette\Http\SessionSection
	 */
	protected $ssUser;

    /**
     * For Approve
     * @var Nette\Http\SessionSection
     */
    protected $ssForApprove;
    
	/**
	 * Pole dostupnych modulu
	 * @var array
	 */
	protected $modules;


	/**
	 * Aktualni modul
	 * @var \Module
	 */
	protected $currentModule;


	/**
	 * Lang default CS
	 * @persistent
	 */
	public $lang;
    

    /**
     * 
     * @var Configuration
     * @inject
     */
    public $configuration;
    
    /**
     * Culture
     * @param Culture $culture
     */
    public function injectCulture(Culture $culture)
    {
        $this->culture = $culture;
    }

	/**
	 * Nette Translator
	 * @param \SammTranslator\Gettext $translator
	 */
	public function injectTranslator(\SammTranslator\Gettext $translator)
	{
		$this->sammTranslator = $translator;
        $this->formTranslator = new \SammTranslator\FormTranslator($this->sammTranslator);
	}

	/**
	 * Modules Model Injectpr
	 * @param ModulesModel $modulesModel
	 */
	public function injectModulesModel(ModulesModel $modulesModel)
	{
		$this->modulesModel = $modulesModel;
	}

	/**
	 * Codelist Model
	 * @param CodelistModel $codelistModel
	 */
	public function injectCodelistModel(CodelistModel $codelistModel)
	{
		$this->codelistModel = $codelistModel;
	}

	/**
	 * User Model
	 * @param UserModel $userModel
	 */
	public function injectUserModel(UserModel $userModel)
	{
		$this->userModel = $userModel;
	}

	/**
	 * Universal Model
	 * @param UniversalModel $universalModel
	 */
	public function injectUniversalModel(UniversalModel $universalModel)
	{
		$this->universalModel = $universalModel;
	}

	/**
	 * ABRA Service
	 * @param \Abra\Service\Service $service
	 */
	public function injectAbraService(\Abra\Service\Service $service)
	{
		$that = $this;
		// ABRA service
		$this->abraService = $service;
		// nastaveni cache directory
		$this->abraService->setEntityCachePath(ABRA_SERVICE_CACHE_DIR);
        
        $uniqId = uniqid('r-');
        
        $asteriskPassw = function($requestHeaders) {
            // vyhodit heslo - nechat jen username - 2020-08-05
            foreach($requestHeaders as $index => $rHeader) {
                if(Strings::startsWith(Strings::lower($rHeader), 'authorization: basic ')) {
                    $rHeaderParts = explode(':', $rHeader);
                    if(isset($rHeaderParts[1])) {
                        $rHeaderPartsParts = explode(' ', trim($rHeaderParts[1]));
                        if(isset($rHeaderPartsParts[1])) {
                            $userPass = @base64_decode($rHeaderPartsParts[1]);
                            $userPassParts = explode(':', $userPass);
                            if(isset($userPassParts[1])) {
                                $requestHeaders[$index] = 'Authorization: Basic '.$userPassParts[0].':'.str_repeat('*', Strings::length($userPassParts[1]));
                            }
                        }
                    }
                }
            }
            return $requestHeaders;
        };
        
        // Log all
        $this->abraService->onLog[] = function($counter, $url, $requestHeaders, $request, $responseHeaders, $response) use($that, $uniqId, $asteriskPassw) {
            
            $requestHeaders = $asteriskPassw($requestHeaders);
            
            if(!$that->user->isLoggedIn() || $that->configuration['logPerUser'] === FALSE) {
                return;
            }
            $userId = $that->user->id;
            if(
                $that->configuration['logPerUser'] === TRUE || 
                (is_array($that->configuration['logPerUser']) && in_array($userId, $that->configuration['logPerUser']))
            ) {
                $dir = LOG_DIR.'/'.$userId;
                if(!is_dir($dir)) {
                    \Nette\Utils\FileSystem::createDir($dir);
                }

                $content = [];
                $content[] = $url;
                $content[] = '';
                $content[] = implode("\n", $requestHeaders);
                $content[] = '';
                $content[] = $request;
                $content[] = '';
                $content[] = '--------------------------------------------------------------------------------';
                $content[] = '';
                $content[] = implode("\n", $responseHeaders);
                $content[] = '';
                $content[] = $response;
                
                file_put_contents($dir.'/'.$uniqId.'_['.$counter.']_'.date('Y-m-d_H-i-s').'.log', implode("\n", $content));
            }
        };
        
        
        // On RAW - check if there was an error returned from service
        $this->abraService->onRawResponse[] = function($sender, $isXml, $url, $method, $rawRequest, $rawResponse, $requestHeaders, $responseHeaders) use ($that, $asteriskPassw) {
            if($isXml) {
                $xmlResponse = simplexml_load_string($rawResponse);
                if($xmlResponse->getName() === 'response' && $xmlResponse->children()->getName() === 'errors') {
					// ulozit chybu
                    $requestHeaders = $asteriskPassw($requestHeaders);
                    $that->logResponseError($method, $url, $requestHeaders, $rawRequest, $responseHeaders, $rawResponse);
                }
            }
        };
        
		// Pri chybe odpovedi ji zalogujeme
		$this->abraService->onResponseError[] = function($sender, \Exception $e, $url, $method, $rawRequest, $rawResponse, $requestHeaders, $responseHeaders) use ($that, $asteriskPassw) {
            
            $requestHeaders = $asteriskPassw($requestHeaders);
            
			// Zda je to chyba prihlaseni (zadano spatne heslo)
			$badPassword = FALSE;
            
			if($e instanceof \Abra\Service\ResponseException)
			{
				$errors = $e->getResponse()->getErrors();
				foreach($errors as $err)
				{
					// uzivatel neni prihlasen nebo ma neplatny token
					if($err->code == 5 || $err->code == 4)
					{
						$that->redirect('Sign:out');
					}
					if($err->code == 3)
					{
						$badPassword = TRUE;
					}
				}
				if(!$badPassword)
				{
					// ulozit chybu
                    $that->logResponseError($method, $url, $requestHeaders, $rawRequest, $responseHeaders, $rawResponse);
				}
			}
			elseif($e instanceof \Abra\Service\ServiceException)
			{
				// Do not log login attempts
				if(!isset($_POST['do']) || $_POST['do'] !== 'signInForm-submit') {
					// ulozit chybu z API
                    $that->logResponseError($method, $url, $requestHeaders, $rawRequest, $responseHeaders, $rawResponse);
					/* @var $e \Abra\Service\ServiceException */
					\Tracy\Debugger::log($e);
				}
                if(!\Tracy\Debugger::$productionMode) {
                    // Neni produkcni rezim, vypiseme chybove hlaseni
                    $that->flashMessage(__($e->getMessage()).' '.__($e->getServiceContentText()), 'error');
                }
				$that->redirect('Sign:out');
			}
		};

		// Pokid se nejaka entita teprve v tomto pozadavku vygenerovala,
		// robot loader o ni nevi a musime mu ji explicitne vnutit
		/*$this->abraService->onEntityCached[] = function($sender, \Abra\Service\ClassDescription $class, $namespace, $fileName) use($that) {
			$nsClass = $namespace.'\\'.$class->className;
			if(!class_exists($nsClass))
			{
				$that->robotLoader->getCacheStorage()->clean(array(Nette\Caching\Cache::ALL => TRUE));
				$that->robotLoader->rebuild();
				$that->robotLoader->tryLoad($nsClass);
				//dump(class_exists($nsClass), $class, $namespace, $fileName); die;
			}
		};*/
	}

	/**
	 * Entity Loader Injector
	 *
	 * @param Abra\Service\EntityLoader $entityLoader
	 */
	public function injectEntityLoader(EntityLoader $entityLoader)
	{
		$this->entityLoader = $entityLoader;
	}

	/**
	 * Cache Model Injector
	 *
	 * @param CacheModel $cacheModel
	 */
	public function injectCacheModel(CacheModel $cacheModel)
	{
		$this->cacheModel = $cacheModel;
	}

    
	/**
	 * Presenter Startup Method
	 */
	public function startup()
	{
		parent::startup();
        
        // Default link with language in mind to prefix for ABRA LINKS
        $this->template->defaultBaseLink = rtrim($this->link('Default:default', ['bo' => NULL]), '/');

        $this->template->showHttpWarning = '0';
        $this->template->moduleMenuCookieValue = $this->getHttpRequest()->getCookie('modulemenu') ?? '';
        
        // Cookie secured only on HTTPs        
        $this->session->setCookieParameters('/', NULL, $this->getHttpRequest()->isSecured());

		// X_ApproveType constant
		$this->template->APPROVE_FIELD_NAME = self::APPROVE_FIELD_NAME;

        // Remove APP framework signature (security - Sorry Mr. Grudl)
        //$this->getHttpResponse()->setHeader('X-Powered-By', NULL);
        
        // is HTTPs ?
        $this->template->isHttps = $this->request->hasFlag(\Nette\Application\Request::SECURED);
        
		// is NTLM ?
		$this->template->isNtlm = $isNtlm = $this->configuration['ntlm'];

		// Alert auto-close after this time (sec)
		$this->template->alertVisibleTime = $this->configuration['alertVisibleTime'];

		// is PHP login
		$isLoginPhp = \Nette\Utils\Strings::endsWith($_SERVER['PHP_SELF'], 'login.php');

		// Session section - USER
		$this->ssUser = $this->session->getSection('user');
        
		// Session section - USER
		$this->ssForApprove = $this->session->getSection('forApprove');
        
        // Connection name
        $this->template->connectionName = !empty($this->ssUser['connection']) ? $this->configuration['connections'][$this->ssUser['connection']] : '';

		// Define default lang based on config not by default persit param
		$this->lang = $this->configuration['lang'];

        // One connection & isNtlm
		$this->template->oneConnection = !empty($this->configuration['connections']) && count($this->configuration['connections']) === 1;
        $this->template->editProfile = $this->configuration['editProfile'];

        // Field max width
        $this->template->fieldMaxWidth = $this->configuration['fieldMaxWidth'];
        
		// xApproveTypeButtons
		$this->template->xApproveTypeButtons = [];
		
		// jazyk z ROUTy
		$this->sammTranslator->setLang($this->getParameter('lang', $this->lang)); // nastavíme jazyk
		$this->lang = $this->sammTranslator->getLang();

		// sparovani dostupnych modulu s povolenymi moduly ktere dostanu z ABRY
		$this->modules = $this->ssUser['allowedModules'];
		$bo = $this->getParameter('bo', NULL);
		$this->currentModule = isset($this->modules[$bo]) ? $this->modules[$bo] : NULL;


		// test if guide PDF exist
		$guideFiles = glob(WWW_DIR.'/manual/*.[Pp][Dd][Ff]');
		$this->template->guide = array();
		if(!empty($guideFiles)) {
            foreach($guideFiles as $guideFile) {
                $guideFileName = pathinfo($guideFile, PATHINFO_BASENAME);
                $this->template->guide[str_replace(WWW_DIR, '', $guideFile)] = $guideFileName;
            }
		}

		// Test na stary prohlizec
		$ua = strtolower($this->getHttpRequest()->getHeader('user-agent') ?? '');
		if(preg_match('/msie [345678]{1}\./', $ua))
		{
			$this->template->oldBrowser = TRUE;
		}

		/* @var $that BasePresenter */
		$lang = $this->getParameter('lang');
		$this->lang = $this->template->lang = empty($lang) ? $this->lang : $lang;
		$this->template->langs = $this->configuration['langs'];
		$this->template->translator = $this->sammTranslator;
        $this->template->culture = $this->culture;
        $this->culture->setLang($this->lang);
            
		// Set app version
		$version  = $this->configuration['version'];
		$this->template->version = $version;

		if($this->user->isLoggedIn())
		{
			if(isset($this->ssUser['auth']) && substr($this->ssUser['auth']->version, 0, 3) !== substr($version, 0, 3))
			{
				$this->template->version .= '<strong title="'.__('New version ('.$version.') was released. Please contact your ABRA consultant.').'">*</strong>';
			}
		}

		// Sablona
		$this->template->cfgTemplate = $this->configuration['template'];
        $this->template->cfgNextCss = $this->configuration['nextCss'];

		// neprihlasene uzivatele nepustime na secured presentery
		$secured = $this->getReflection()->getAnnotation('secured');
		if($secured && !$this->user->isLoggedIn())
		{
			$refLink = $this->link('this', array(
				'id' => $this->getParameter('id', NULL),
				'lang' => $this->getParameter('lang', NULL),
				'bo' => $this->getParameter('bo', NULL),
			));
            
			if($isNtlm) {
				
				// 2) login.php - prohlizec odeslal uzivatele, ulozime si ho do session a presmerujeme na ../index.php
				if($isLoginPhp) {

                    $this->fixHttpAuth();

					$remoteUser = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : NULL;
                    //$remoteUser = 'ABRA\\xxxx.xxxxx';

					$ssNtlmTmp = $this->getSession()->getSection('ntlm-tmp');
					$ssNtlmTmp['u4a'] = $remoteUser;
					$ssNtlmTmp['visited'] = 1;
					$baseUrl = $this->getHttpRequest()->getUrl()->getBaseUrl();
					if(\Nette\Utils\Strings::endsWith($baseUrl, 'protected/')) {
						$baseUrl = str_replace('protected/', '', $baseUrl);
					}
                    $refLinkReceived = $this->getParameter('ref');
                    if(empty($refLinkReceived)) {
                        $this->redirectUrl($baseUrl);
                    }
                    else {
                        $this->redirectUrl($refLinkReceived);
                    }
				}
				
				// 1) neprihlaseny uzivatel a neni login.php => presmerovani na login.php
				if(!$this->user->isLoggedIn() && !$isLoginPhp) {
					$ssNtlmTmp = $this->getSession()->getSection('ntlm-tmp');
					if(empty($ssNtlmTmp['visited'])) {
                        $bq = http_build_query(array(
                            'ref' => $refLink,
                            'lang' => $this->lang
                        ));
						$this->redirectUrl(rtrim($this->getHttpRequest()->getUrl()->getBaseUrl(), '/').'/protected/login.php?'.$bq);
					}
				}
			}

			$this->redirect('Sign:in', array('ref' => $refLink, 'lang'=>$this->lang));
		}

		// Secured presenter a uzivatel neni prihlasen?
		if($secured || $this->user->isLoggedIn())
		{
			// session section
			if(!isset($this->ssUser['auth']))
			{
				throw new \ErrorException(__('No AUTH section present!'));
			}

			// nastaveni connection a tokenu na ABRA service
			$this->abraService->setConnectionName($this->ssUser['connection']);
			$this->abraService->setToken($this->ssUser['auth']->token);

			// Kdyz neni zadny modul dostupny, vyhodime vyjimku
			if(count($this->modules) <= 0 && empty($_ENV['repeatedError'])) {
				// error presenter dedi z tohoto BasePresenteru, nesmi se poustet 2x, proto kontrola na _ENV
				$_ENV['repeatedError'] = 'yes';
				$this->user->logout(TRUE);
				$this->flashMessage(__('No module available'), 'error');
				$this->redirect('Sign:in');
			}

			// Current module do sablony
			$parts = explode(':', $this->presenter->name);
			$this->template->presenterName = array_pop($parts);
			$this->template->modules = $this->modules;
			$this->template->currentModule = $this->currentModule;
			$this->template->module = $this->currentModule;
			$this->template->logo = $this->ssUser['logo'];
		}
        
        // Language / culture check
        if(!$this->culture->isCultureAvailable($this->lang)) {
            $this->flashMessage('Jazyk "'.$this->lang.'" není nastaven v sekci "cultures" souboru "custom.neon"!');
            $cultures = $this->culture->getAvailableCultures();
            $this->redirect('this', ['lang' => reset($cultures)]);
        }
	}

    /**
     * Read for approve and store it to session
     * 
     * @param bool $force Read it even if it is already in the session
     */
    protected function readForApprove($force = FALSE)
    {
        if(empty($this->ssForApprove['forApprove']) || $force) {
            $this->ssForApprove['forApprove'] = $this->modulesModel->getForApprove();
        }
        return $this->ssForApprove['forApprove'];
    }
    

	/**
	 * Pred renderovanim dame do sablony visualni nastaveni z configu
	 */
	public function beforeRender()
	{
		parent::beforeRender();
		$this->template->visual = $this->configuration['visual'];
	}


	/**
	 * Vytvori sablonu
	 * @param name $class Trida
	 * @return ITemplate
	 */
	public function createTemplate($class = NULL): Nette\Application\UI\ITemplate
	{
		$template = parent::createTemplate($class);

		$that = $this;

		$translator = $that->sammTranslator;

		$template->setTranslator($this->sammTranslator);
        
        $template->addFilter('nl2br', 'nl2br');

        $template->addFilter('svgIcon', function($icon) {
            $defaultIcon = 'default.svg';
            $i = strtolower(empty($icon) ? $defaultIcon : $icon);
            $iconPath = WWW_DIR.'/images/modules/';
            if(is_file($iconPath.$i)) {
                $svg = file_get_contents($iconPath.$i);
            }
            else {
                $svg = file_get_contents($iconPath.$defaultIcon);
            }
            return $svg;
        });
        
        $template->addFilter('removeSpecials', function($text) {
            return preg_replace('/[^0-9A-Za-z]/', '_', $text);
        });
        
		$template->addFilter('placeValues', function($text, $row, Culture $culture, $raw = false, $urlencode = false) {
			return \AbraHelpers::placeValues($text, $row, $culture, $raw, $urlencode);
		});
        
        $template->addFilter('modifyControl', function($control, $field, $namePath, $data, $entity, $namespace) {
            return AbraHelpers::modifyControl($control, $field, $namePath, $data, $entity, $namespace);
        });

		$template->addFilter('url2link', function($text) {
			return \AbraHelpers::url2link($text);
		});

		// status aktivity
		$template->addFilter('objectValue', function($property, $row, $culture, $raw = false, $tableView = true) {
			return \AbraHelpers::objectValue($property, $row, $culture, $raw, $tableView);
		});

        $template->addFilter('abraType', function($value, Culture $culture, $action, $type, $length) {
            return \AbraHelpers::abraType($culture, $action, $value, $type, $length);
        });

		// Akce pro predany modul
		$template->addFilter('renderActions', function(\Module $module, $row) use ($that, $translator) {
			$actions = array();            
			// View (read-only edit) action
			if($module->isAllowed('view') && $module->isActionAllowedFor('view', $row))
			{
				$actions[] = array(
					'name' => 'form',
					'link' => $that->link('view', array('id' => $row->getId())),
					'title' => $translator->translate($module->getCaption('view')),
					'icon' => 'icon-search'
				);
			}
			// Form (edit) action
			if($module->isAllowed('form') && $module->isActionAllowedFor('form', $row))
			{
				$actions[] = array(
					'name' => 'form',
					'link' => $that->link('form', array('id' => $row->getId())),
					'title' => $translator->translate($module->getCaption('form')),
					'icon' => 'icon-pencil'
				);
			}
			// Vlozeni komentare (aktivity) k zaznamu
			if($module->isAllowed('addactivity') && $module->isActionAllowedFor('addactivity', $row))
			{
				$actions[] = array(
					'name' => 'addactivity',
					'link' => $that->link('addactivity', array('id' => $row->getId())),
					'title' => $translator->translate($module->getCaption('addactivity')),
					'icon' => 'icon-comment'
				);
			}
			// Vlozeni prilohy (souboru)
			if($module->isAllowed('addattachment') && $module->isActionAllowedFor('addattachment', $row))
			{
				$actions[] = array(
					'name' => 'addattachment',
					'link' => $that->link('addattachment', array('id' => $row->getId())),
					'title' => $translator->translate($module->getCaption('addattachment')),
					'icon' => 'icon-attach'
				);
			}
			// Tisk
			if($module->isAllowed('print') && $module->isActionAllowedFor('print', $row))
			{
				$actions[] = array(
					'name' => 'print',
					'link' => $that->link('print', array('id' => $row->getId())),
					'title' => $translator->translate($module->getCaption('print')),
					'icon' => 'icon-print',
                    'target' => '_blank'
				);
			}
			// Kopie
			if($module->isAllowed('copy') && $module->isActionAllowedFor('copy', $row))
			{
				$actions[] = array(
					'name' => 'copy',
					'link' => $that->link('copy', array('id' => $row->getId())),
					'title' => $translator->translate($module->getCaption('copy')),
					'icon' => 'icon-plus-sign'
				);
			}
			// Delete
			if($module->isAllowed('delete') && $module->isActionAllowedFor('delete', $row))
			{
				$actions[] = array(
					'name' => 'delete',
					'link' => $that->link('delete!', array('id' => $row->getId())),
					'title' => $translator->translate($module->getCaption('delete')),
					'icon' => 'icon-remove icon-white'
				);
			}
			$render = '';
			if(!empty($actions))
			{
				$render .= '<div class="btn-group">'; // pull-right
				$render .= '<div class="nowrap">'.
					'<a href="'.$actions[0]['link'].'"'.
                        ($actions[0]['name'] == 'delete' ? ' class="btn rec-delete"' : ' class="btn"').
                        (!empty($actions[0]['target']) ? ' target="'.$actions[0]['target'].'"' : '').
                    '>'.
                        '<span class="'.$actions[0]['icon'].'" title="'.$actions[0]['title'].'"></span>'.
                    '</a>'.
                    '<a class="btn dropdown-toggle" data-toggle="dropdown"><i class="svgi svgi-more"></i></a>'.
					'<ul class="dropdown-menu" style="left: initial; right: 0">';
					for($i = 1; $i < count($actions); $i++)
					{
						$render .=
							'<li class="text-left">'.
								'<a href="'.$actions[$i]['link'].'"'.
                                    ($actions[$i]['name'] == 'delete' ? ' class="rec-delete"' : '').
                                    (!empty($actions[$i]['target']) ? ' target="'.$actions[$i]['target'].'"' : '')
                                .'>'.
									'<span class="'.$actions[$i]['icon'].'"></span> '.
									'<span class="drop-down-action-caption">'.$actions[$i]['title'].'</span>'.
								'</a>'.
							'</li>';
					}
				$render .= '</ul>';
				$render .= '</div>';
				$render .= '</div>';

			}
			return $render;
//
//			<td n:if="$module->isAllowed('form')" class="action-cell text-right">
//				<a n:href="form id=>$row->getId()" class="btn" title="{__($module->getCaption('form'))}"><span class="icon-pencil"></span></a>
//			</td>
//			<td n:if="$module->isAllowed('addactivity')" class="action-cell text-right">
//				<a n:href="addActivity id=>$row->getId()" class="btn" title="{__($module->getCaption('addactivity'))}"><span class="icon-comment"></span></a>
//			</td>
//			<td n:if="$module->isAllowed('addattachment')" class="action-cell text-right">
//				<a n:href="addAttachment id=>$row->getId()" class="btn" title="{__($module->getCaption('addattachment'))}"><span class="icon-file"></span></a>
//			</td>
//			<td n:if="$module->isAllowed('print')" class="action-cell text-right">
//				<a n:href="print id=>$row->getId()" class="btn rec-print" title="{__($module->getCaption('print'))}"><span class="icon-print"></span></a>
//			</td>
//			<td n:if="$module->isAllowed('copy')" class="action-cell text-right">
//				<a n:href="copy id=>$row->getId()" class="btn rec-print" title="{__($module->getCaption('copy'))}"><span class="icon-plus-sign"></span></a>
//			</td>
//			<td n:if="$module->isAllowed('delete')" class="action-cell text-right">
//				<a n:href="delete! id=>$row->getId()" class="btn btn-danger rec-delete" title="{__($module->getCaption('delete'))}"><span class="icon-remove icon-white"></span></a>
//			</td>

		});

        // Style Legend
        $template->addFilter('styleLegend', function(Style $style) use($translator) {
            $csss = array();
            foreach($style->styleHash as $prop => $value) {
                $csss[] = $prop.':'.$value;
            }
            return 
                '<div>'.
                    '<span style="'.implode(';', $csss).'">&nbsp;</span>'.
                    $translator->translate($style->styleName).
                '</div>';
        });
        
		return $template;
	}






	// ----------------------------------------------------- FORMULARE ---------------------------------------------------------------

	/**
	 * Pripravi data z formulare tak, aby je bylo mozne vlozit pres setValues / setDefaults do formulare
	 *
	 *  0: result := 'dtString';
	 *	1: result := 'dtFloat';
	 *	2: result := 'dtInteger';
	 *	3: result := 'dtBoolean';
	 *	4: result := 'dtNote';
	 *	5: result := 'dtDate';
	 *	6: result := 'dtTime';
	 *	7: result := 'dtDateTime';
	 *	8: result := 'dtList';
	 *	9: result := 'dtStringReference';
	 *	10: result := 'dtIntegerReferece';
	 *	11: result := 'dtFileBlob';
	 *	12: result := 'dtImage';
	 *	13: result := 'dtCollection';
	 *	14: result := 'dtOwnedReference';
	 *	15: result := 'dtUserCollection';
	 *	16: result := 'dtOwnedCollection';
	 *	17: result := 'dtSearchReference';
	 *	18: result := 'dtXMLElement';
	 *
	 *
	 * @param array $formData
	 * @return array data
	 */
	protected function prepareFormValues($formData)
	{
		$values = array();
		//dump($formData); die;
		if($formData === NULL)
		{
			return $values;
		}
		foreach( $formData->getDescription()->getFields() as $name => $fd)
		{
			if($fd->type === 'dtList' || $fd->type === 'dtXMLElement')
			{
				throw new Nette\InvalidStateException('SAMM neumí pracovat s "'.$fd->type.'"');
			}
			// Pokud je hodnota ve formdatech ...
			if(isset($formData->{$name}))
			{
				// Pokud je to pole
				if(($fd->type == 'dtUserCollection' || $fd->type == 'dtCollection' || $fd->type == 'dtOwnedCollection') && !empty($fd->list))
				{
					foreach($formData->{$name} as $index => $record)
					{
						$values[$fd->name][$index] = $this->prepareFormValues($record);
					}
				}
				elseif($fd->type == 'dtOwnedReference')
				{
					$val = $this->prepareFormValues($formData->{$name});
					$values[$fd->name] = $val === '' ? NULL : $val;
				}
				elseif($fd->type == 'dtStringReference')
				{
					if(is_scalar($formData->{$name}))
					{
						$val = $formData->{$name};
						$values[$fd->name] = $val === '' ? NULL : $val;
					}
					else
					{
						// Pokud odkaz na referenci neni NULL, vezmeme ID z objektu
						if($formData->{$name} !== NULL)
						{
							$values[$fd->name] = $formData->{$name}->getId();
						}
						else
						{
							// jinak nastavime NULL
							$values[$fd->name] = NULL;
						}
					}
				}
				else
				{
					if(!is_object($formData->{$name}))
					{
						// html_entity_decode kvuli konvertovani entit 2014-01-07
						$values[$fd->name] = html_entity_decode($formData->{$name});
					}
					else
					{
						// Object? wat???
						\Tracy\Debugger::barDump($formData, 'Formulářová data');
						$clsParts = explode('\\', get_class($formData));
						throw new Nette\InvalidStateException(
								'Pole "'.$name.'" je objekt třídy "'.get_class($formData->{$name}).'", ale podle description "'.end($clsParts).'/object" to má být skalární hodnota.'
							);
					}
				}
			}
		}
		return $values;
	}



	/**
	 * Vytvori obecny formular podle description
	 * @param string $name Nazev formulare
	 * @param \Abra\Service\FormDescription $formDescription Form Description
	 * @param \Abra\Service\Entity|null $formData Data do formulare
	 * @return Form Formular
	 */
	protected function createForm($name, $formDescription, $formData, $useIdField = TRUE)
	{
		$entityNS = explode('\\', get_class($formData));
		$entityClass = array_pop($entityNS);
		$namespace = empty($entityNS) ? NULL : implode('\\', $entityNS);
		// vytvoreni formulare
		$form = new Form($this, $name);
		$form->addHidden('__ref', $this->getParameter('ref', NULL));
		if($formData->hasId() && $useIdField)
		{
			$form->addHidden($formData->getIdProperty());
		}
		// Pole ve formulari pouzitych kontejneru
		$containers = array('#' => $form);
		// Polozky
		$fields = $formDescription->getFields();
		$this->addFields($name, $entityClass, $fields, $containers, $entityClass, $namespace, $formData);
		// Validacni pravidla
		$allRules = $formDescription->getAllRules();
		$this->addRules($form, $allRules);
		// pridame tlacitka
		$form->addSubmit('submit', 'Uložit');
		// Javascriptem nastavovane pole pro predani tlacitka, kterym byl formular odeslan
		$form->addHidden('_submittedby_', '');
		$form->setTranslator($this->sammTranslator);
		return $form;
	}

	/**
	 * Nastavi validacni pravidla dotcenym polim formulare
	 * @param Form $form Formular
	 * @param array $allRules Validacni pravidla
	 */
	protected function addRules($form, $allRules)
	{
		foreach($allRules as $fieldName => $ruleset)
		{
			$nameParts = explode('.', $fieldName);
			$foundFields = array();
			$this->findFields($foundFields, $nameParts, $form, '');
			foreach($foundFields as $path => $f)
			{
				$rpParts = explode('.', $path);
				array_pop($rpParts);
				$rpPath = count($rpParts) > 0 ? implode('.', $rpParts).'.' : '';
				$this->addRulesetToField($form, $f, $ruleset, $rpPath);
			}
		}
	}

	/**
	 * Field, kteremu se aplikuje ruleset
	 * @param \Nette\Forms\Controls\BaseControl $field Pole formulare
	 * @param array $ruleset Pole pravidel
	 */
	protected function addRulesetToField($form, $field, $ruleset, $realPathPrefix)
	{
		foreach($ruleset as $rule)
		{
			if($rule instanceof \Abra\Service\ConditionOn)
			{
				/* @var $rule \Abra\Service\ConditionOn */
				$refField = $this->getFormComponentByPath($form, $realPathPrefix.$rule->field);
				// @fixme - odkaz na konstanty
                $operation = $this->getFormRuleOperation($rule->action);
				$condition = $field->addConditionOn($refField, $operation, $rule->value);
				// pouze jedno zanoreni podminek - @fixme
				foreach($rule->getRules() as $subRule)
				{
                    $this->addRuleToFieldOrCondition($condition, $subRule);
				}
			}
			elseif($rule instanceof \Abra\Service\Rule)
			{
				/* @var $rule \Abra\Service\Rule */
				$refField = $this->getFormComponentByPath($form, $realPathPrefix.$rule->field);
                $this->addRuleToFieldOrCondition($refField, $rule);
			}
		}
	}
    
    /**
     * 
     * @param \Nette\Forms\Controls\BaseControl|\Nette\Forms\Rules $dst Destination
     * @param \Abra\Service\Rule $rule
     */
    protected function addRuleToFieldOrCondition($dst, $rule)
    {
        $operation = $this->getFormRuleOperation($rule->action);
        
        if($operation == 'AbraHelpers::warnfilled') {
            $rule->value = $rule->error;
            $rule->error = 'SKIPMESSAGE';
        }
        
        $dst->addRule($operation, $rule->error, $rule->value);        
    }
        

	/**
	 * Operace EQUAL, FILLED ...
	 * @param string $constName
	 * @return string
	 */
	protected function getFormRuleOperation($constName)
	{
		$constName = strtolower($constName);
		// FIX
		if($constName == 'equals')
		{
			$constName = 'equal';
		}
        
        // TODO: vlastní validační pravidlo pro WARN FILLED
        if($constName == 'warn' || $constName == 'warnfilled') {
            return 'AbraHelpers::warnfilled';
        }
        
		// vrati $constName
		return ':'.$constName;
	}


	/**
	 * Vytahne komponentu z formulare podle prsne teckove cesty
	 * @param Form $form
	 * @param string $path
	 * @return \Nette\Forms\Controls\BaseControl
	 * @throws \Exception
	 */
	protected function getFormComponentByPath($form, $path)
	{
		$refFieldParts = explode('.', $path);
		$refField = $form;
		foreach($refFieldParts as $refFieldPart)
		{
			if(!isset($refField[$refFieldPart]))
			{
				throw new \Exception('Nenalezeno odkazované políčko "'.$path.'" v podmínce formuláře');
			}
			$refField = $refField[$refFieldPart];
		}
		return $refField;
	}

	/**
	 * najde pole
	 * @param array $fields Pole fieldu
	 * @param array $nameParts Cast nazvu
	 * @param array $from Od ktereho fieldu hledat
	 */
	protected function findFields(&$fields, $nameParts, $from, $realPathPrefix)
	{
		$foundPathEnd = TRUE;
		$realPath = $realPathPrefix;
		while(count($nameParts) > 0)
		{
			$part = array_shift($nameParts);
			// je to pole komponent ?
			if(isset($from[$part]))
			{
				$from = $from[$part];
				$realPath .= $realPath == '' ? $part : '.'.$part;
			}
			elseif(isset($from[0]))
			{
				$foundPathEnd = FALSE;
				if(isset($from[0][$part]))
				{
					$i = 0;
					while(isset($from[$i]))
					{
						if(isset($from[$i][$part]))
						{
							$subPath = $realPath.'.'.$i.'.'.$part;
							$this->findFields($fields, $nameParts, $from[$i][$part], $subPath);
						}
						$i++;
					}
				}
				break;
			}
		}
		// Pokud jsme prosli cestou az na konec, naslo se jedno pole
		if($foundPathEnd && $realPath != '')
		{
			$fields[$realPath] = $from;
		}
	}






	/**
	 * Prida $fields do nejakeho kontejneru
	 *
	 * @param string $formName Form Name
	 * @param string $baseClass
	 * @param array $fields Kolekce nebo pole fieldu
	 * @param array $containers Pole pouzitych kontejneru, pro spravne zacleneni fieldu
	 * @param string $entityClass Trida entity
	 * @param string $namespace Namespace
	 * @param Entity $formData Data pro predvyplneni formulare
	 * @param string $namePrefix Prefix pro nazvy polozek
	 */
	protected function addFields($formName, $baseClass, $fields, &$containers, $entityClass, $namespace, $formData, $namePrefix = '')
	{
//	    dump($formData, $fields);

		// Pokud se ve formData preda prazdny string, tak ukoncime pridavani poli (<TNxDocumentContentPDM/>)
		if(!is_object($formData))
		{
			throw new \Nette\InvalidArgumentException('No data found for object: '.$entityClass.' (maybe there is empty <'.$entityClass.'/> element)');
		}
		//\Tracy\Debugger::$maxDepth = 5;
		$startEntityClass = $entityClass;
		/* @var $section \Abra\Service\FormSection */
		foreach($fields as $field)
		{
			$input = NULL;
			// TOUR empty <field/> fix
			if($field->name === NULL)
			{
				continue;
			}

			$entityClass = $startEntityClass;
			// vytvorime formularove sekce podle teckove notace a podle nazvu FIELDU urcime, v jakem containeru se ma formulrove pole vyskytnout
			$nameParts = explode('.', $namePrefix.$field->name);
//			if(!empty($namePrefix))
//			{
//				dump($nameParts);
//				die;
//			}

			$cont = $containers['#']; // vychozim kontejnerem pro policka je formular samotny
			$partsPath = array();
//			$descriptions = array();
//			$entityClassUp = $baseClass;
			if(count($nameParts) > 1)
			{
				// postupne projdeme nazev rozsekany teckami
				do
				{
					$part = array_shift($nameParts);
					$partsPath[] = $part;
					$partsPathImploded = implode('.', $partsPath);

                	// Pokud je predan $namePrefix, musime adresovat fieldy od $baseClass ??? - coz nemusi byt uplne spravne asi
					if(!empty($namePrefix))
					{
						$whichClass = $cont instanceof \Nette\Application\UI\Form ? $baseClass : $entityClass;
						$classField = Entity::describeField($whichClass, $part, $namespace); // $partsPathImploded
					}
					else
					{
						$classField = Entity::describeField($entityClass, $part, $namespace); // $partsPathImploded
					}
					// pokud dana cesta neexistuje, tak zalozime novy naming kontejner
					if(!isset($containers[$partsPathImploded]))
					{
						$newCont = $cont->addContainer($part);    
						// IDcko 							COLLECTION ITEM           COLLECTION SAMOTNA
						if($formData instanceOf Entity && $formData->hasId() && (empty($classField) || ($classField->type != 'dtCollection' && $classField->type != 'dtOwnedCollection')))
						{
							// podle cesty se dostaneme k datum, ktere potrebujeme
							$formDataPart = $formData;
							foreach($partsPath as $pathPart)
							{
								if(isset($formDataPart->{$pathPart}))
								{
									$formDataPart = $formDataPart->{$pathPart};
								}
							}
							//dump($formDataPart);
							if(is_object($formDataPart) && method_exists($formDataPart, 'getIdProperty'))
							{
								$newCont->addHidden($formDataPart->getIdProperty(), $formDataPart->getId());
							}
							else 
							{
								// Quick hack - pro nove pridane radky toto neni objekt ale pole, bere se ID a jeho hodnota
								$newCont->addHidden('ID', $formDataPart['ID']);
							}
						}
//						dump($newCont); die;
						$containers[$partsPathImploded] = $newCont;
						//dump($newCont); die;
					}
					// Popis objektu
					//dump($entityClass); dump($partsPathImploded); echo 'a';

					//$classField = Entity::describeField($entityClass, $part, $namespace); // $partsPathImploded
					// Pokud je pole REF jinam, tak entityClass posuneme vys
					if(!empty($classField->ref))
					{
						$entityClass = $classField->ref;
					}

					$cont = $containers[$partsPathImploded];
				}
				while(count($nameParts) > 1);
			}
			// Nejsvrchnejsi cast cesty.v.teckove.notaci je nazev samotneho fieldu
			$fieldName = array_shift($nameParts);
//			$names = explode('.', $field->name);

			/* @var $field \Abra\Service\FormField */
			$classField = Entity::describeField($entityClass, $fieldName, $namespace); // $field->name
			//dump($containers['Firm_ID'] == $cont); die;

			// Data z FormData objektu pro aktualni field
			//dump($field);
            $fieldValue = NULL;
            $fieldPostValue = NULL;
            if(!empty($formData) && method_exists($formData, 'getPropertyPathData')) {
                $fieldValue = $formData->getPropertyPathData($field->name);
                $fieldPostValue = $this->getPostValue($namePrefix.$field->name);
                if($fieldPostValue !== NULL)
                {
                    $fieldValue = $fieldPostValue;
                    $dsv = explode('.', $field->name);
                    $obj = $formData;
                    foreach($dsv as $dsvPart)
                    {
						if(is_object($obj))
						{						
							if(is_scalar($obj->{$dsvPart}))
							{
								$obj->{$dsvPart} = $fieldPostValue;
							}
							else
							{
								$obj = $obj->{$dsvPart};
							}
						}
                    }
                }
            }            

			// pokud je to autocomplete pole, udelame textbox s data atributama pro naseptavac
			if(!empty($field->control->settings->autocomplete))
			{
				// Hidden pole se zvolenym IDckem (muze uz existovat - ID vnorenych zaznamu se zakladaji automaticky)
				if(!isset($cont[$fieldName]))
				{
					$hidden = $cont->addHidden($fieldName);
				}
				else
				{
					$hidden = $cont[$fieldName];
				}
				$val = $hidden->getValue();
				// dump($cont);
				// dump($fieldValue);
                $fmw = $this->configuration['fieldMaxWidth'];
				// textove pole s vyhledavanim entity
				$acf = $cont->addText($fieldName.'_autocomplete', $field->caption);
				$acf->setAttribute('placeholder', isset($field->control->settings->placeholder) ? $field->control->settings->placeholder : NULL);
				$acf->setAttribute('data-autocomplete-min-length', empty($field->control->settings->autocompleteMinLength) ? 2 : $field->control->settings->autocompleteMinLength);
				$acf->setAttribute('data-autocomplete-min-width', empty($field->control->settings->autocompleteMinWidth) ? '100px' : $field->control->settings->autocompleteMinWidth);
                $acf->setAttribute('data-autocomplete-style-width', empty($field->style->width) ? $fmw : $field->style->width);
				$acf->setAttribute('data-autocomplete-templateBase64', empty($field->control->settings->templateBase64) ? NULL : $field->control->settings->templateBase64);
				$acf->setAttribute('data-autocomplete-display-field', empty($field->control->settings->displayField) ? 'DisplayName' : $field->control->settings->displayField);
				$acf->setAttribute('data-autocomplete-val', empty($val) ? $fieldValue : $val);
				$acf->setAttribute('data-autocomplete-entity', $entityClass);
				$acf->setAttribute('data-autocomplete-field', $fieldName);
				$acf->setAttribute('data-autocomplete-url', $this->link('autocomplete!', array('e' => $entityClass, 'filters' => '__FILTERS__', 'displayField' => '__DISPLAY_FIELD__')));
				$acf->setAttribute('data-autocomplete-one-url', $this->link('autocompleteOne!', array('e' => $entityClass, 'abraId' => '__ABRAID__')));

				// Pokud je definovana kaskada dame do data atributu info
				if(!empty($field->control->cascade))
				{
					$acf->setAttribute('data-cascade', $field->control->cascade->toArray(TRUE));
                    $formDataEncoded = $formData instanceof \stdClass ? \Nette\Utils\Json::encode($formData) : \Nette\Utils\Json::encode($formData->toArray());
					$acf->setAttribute('data-autocomplete-cascade-data', $formDataEncoded);
				}

				$hidden->setAttribute('data-autocompleted', $acf->getHtmlId());
			}
			// je to kolekce polozek? tak rekurzivne plnime dal (podle poctu polozek v datech
			elseif($classField->type == 'dtCollection' || $classField->type == 'dtOwnedCollection')
			{
				// Pokud v datech v teto kolekci neco vubec je, tak pridame pole
				if($fieldValue !== NULL)
				{
					$i = 0;

                    
					// Pridaly se nejake polozky do kolekce na strane klkienta ? ???
					$post = $this->getHttpRequest()->getPost();
					if(!empty($post[$field->name]))
					{
						// Pokud je posledni item bez ID (prednastaveny od Tomase - coz uz delat nebude)... tak ho odstranime
						$lastItem = end($fieldValue);
						if(empty($lastItem->ID))
						{
							array_pop($fieldValue);
						}

						$prefillCount = count($fieldValue);
						$postCount = count($post[$field->name]);

						if($prefillCount < $postCount)
						{
							$index = $prefillCount; //$postCount - $prefillCount;
                            
							while(isset($post[$field->name][$index]))
							{
                                if(empty($fieldValue)) {
                                  $newObj = json_decode(json_encode($post[$field->name][$index]));                                  
                                }
                                else {
                                  $first = reset($fieldValue);
                                  $newObj = clone $first;
                                }
                                foreach($post[$field->name][$index] as $xProp => $xVal) {
                                    if(property_exists($newObj, $xProp)) {
                                        $newObj->{$xProp} = $xVal;
                                    }
                                }                                
								array_push($fieldValue, $newObj); // $post[$field->name][$index]
								$index++;
							}
						}
					}

					// Pro vsechny polozky kolekce vytvorime formularova pole
					foreach($fieldValue as $fieldValueOne)
					{
						/* @var $d Entity */
						$this->addFields($formName, $baseClass, $field->fields, $containers, $classField->list, $namespace, $fieldValueOne, $field->name.'.'.$i.'.');
						$i++;
					}
				}
			}
			// je to reference a je potreba editable ?
			elseif(!empty($classField->ref) && $field->display == 'editable')
			{
//				if($fieldName == 'FirmOffice_ID')
//				{
//					dump($field, $formData);
//				}

				// Pokud je potreba (cascade), zafiltrujeme codelist
				if(isset($field->control->cascade))
				{
					$codelistFilter = $this->getCascadeFilter($field, $formData);
					$codelist = $this->codelistModel->getCodeList($classField->ref, $codelistFilter);
				}
				else
				{
					$codelist = $this->codelistModel->getCodeList($classField->ref);
				}
				if($fieldValue !== NULL && isset($fieldValue->ID) && isset($fieldValue->DisplayName))
				{
					$codelist[$fieldValue->ID] = $fieldValue->DisplayName;
				}
				$input = $cont->addSelect($fieldName, $field->caption, $codelist)->setPrompt('');
				$html = $input->getControlPrototype();
			}
			// je to reference a neni potreba editable, staci vytahnout pouze jeden zaznam, ktery je ve form-datech a zobrazit ho do selectu
			elseif(!empty($classField->ref) && $field->display != 'text')
			{
				$codelist = array();
				// je to dtStringReference - ma ID a DisplayName
				if($fieldValue !== NULL && isset($fieldValue->ID) && isset($fieldValue->DisplayName))
				{
					$codelist[$fieldValue->ID] = $fieldValue->DisplayName;
				}
				//dump($codelist); die;
				$input = $cont->addSelect($fieldName, $field->caption, $codelist);
				$html = $input->getControlPrototype();
			}
			// jinak je to normalni typ formularoveho pole, podle typu policko
			else
			{
				if($field->display != 'text')
				{
					if(empty($field->control->element))
					{
						switch($classField->type)
						{
                            case 'dtSplitter':
                                die('ok');
                                break;
                            
							case 'dtFloat':
								$input = $cont->addText($fieldName, $field->caption, NULL);
								$input->addRule(Form::PATTERN, 'Hodnota v poli "'.$fieldName.'" musí být číslo.', '^[\-0-9 \.,]*$');
								$html = $input->getControlPrototype();
								$html->attrs['data-input-type'] = 'dtFloat';
								$html->attrs['data-input-length'] = $classField->size;
								if($field->display == 'readonly')
								{
									$input->setAttribute('readonly', TRUE);
								}
								break;

							case 'dtInteger':
								$input = $cont->addText($fieldName, $field->caption, NULL);
								$input->addRule(Form::INTEGER, 'Hodnota v poli "'.$fieldName.'" musí být číslo.');
								$html = $input->getControlPrototype();
								if($field->display == 'readonly')
								{
									$input->setAttribute('readonly', TRUE);
								}
								break;

							case 'dtDateTime':
								$input = $cont->addText($fieldName, $field->caption);
								$html = $input->getControlPrototype();
								$html->attrs['data-input-type'] = 'dtDateTime';
								if($field->display == 'readonly')
								{
									$input->setAttribute('readonly', TRUE);
								}
								break;

							case 'dtDate':
								$input = $cont->addText($fieldName, $field->caption);
								$html = $input->getControlPrototype();
								$html->attrs['data-input-type'] = 'dtDate';
								if($field->display == 'readonly')
								{
									$input->setAttribute('readonly', TRUE);
								}
								break;

							case 'dtTime':
								$input = $cont->addText($fieldName, $field->caption);
								$html = $input->getControlPrototype();
								$html->attrs['data-input-type'] = 'dtTime';
								if($field->display == 'readonly')
								{
									$input->setAttribute('readonly', TRUE);
								}
								break;

							case 'dtString':
                                if(isset($cont[$fieldName])) {
                                    // Pokud ID bylo pridano nahore automaticky, ale pridavame naseptavac, tak ho zas odebereme a pridame nove
                                    unset($cont[$fieldName]);
                                }
								$input = $cont->addText($fieldName, $field->caption, NULL, $classField->size);
                                $input->addRule('AbraHelpers::validateEncoding', __('Chybné znaky v textovém poli.'), $classField->getEncoding());
								$html = $input->getControlPrototype();
								$html->attrs['data-input-type'] = 'dtString';
                                // TODO: addRule
								if($field->display == 'readonly')
								{
									$input->setAttribute('readonly', TRUE);
								}
								break;

							case 'dtNote':
								$input = $cont->addTextArea($fieldName, $field->caption, 10, 4);
                                $input->addRule('AbraHelpers::validateEncoding', __('Chybné znaky v textovém poli.'), $classField->getEncoding());
								$html = $input->getControlPrototype();
								$html->attrs['data-input-type'] = 'dtNote';
                                // min-max properties
                                $html->attrs['data-min-max'] = '0';
                                $html->attrs['data-min-max-minimize-label'] = __('Zmenšit pole');
                                $html->attrs['data-min-max-maximize-label'] = __('Roztáhnout pole');
                                $html->attrs['data-min-max-label'] = __($field->caption);
                                // TODO: addRule
								if($field->display == 'readonly')
								{
									$input->setAttribute('readonly', TRUE);
								}
								break;

							case 'dtBoolean':
								$input = $cont->addCheckbox($fieldName, $field->caption);
								$html = $input->getControlPrototype();
								$html->attrs['data-input-type'] = 'dtBoolean';
								break;

							// Upload souboru 2013-12-29
							case 'dtFileBlob':
								$input = $cont->addMultiUpload($fieldName, $field->caption); // all uploads are multiple 2020-07-21
								$html = $input->getControlPrototype();
								$html->attrs['data-input-type'] = 'dtFileBlob';
								break;

							default:
								//dump($classField->type); die;
								$input = $cont->addText($fieldName, $field->caption, NULL, $classField->size);
                                $input->addRule('AbraHelpers::validateEncoding', __('Chybné znaky v textovém poli.'), $classField->getEncoding());
								$html = $input->getControlPrototype();
								$html->attrs['data-input-type'] = 'default';
                                // TODO: addRule
								if($field->display == 'readonly')
								{
									$input->setAttribute('readonly', TRUE);
								}
								break;
						}
					}
					// VE FIELDU je definovan ELEMENT, ktery se ma vypsat
					else
					{
						$el = $field->control->element;
						if($el instanceof \Abra\Service\Select)
						{
							// X_ApproveType - Hidden + Buttons
							if($fieldName === $this->template->APPROVE_FIELD_NAME) {
								$this->template->xApproveTypeButtons[$formName] = $el->getItems();
							}
							
							$input = $cont->addSelect($fieldName, $field->caption, $el->getItemsCodelist());
						}
					}
				}
			}

			// mame-li formularove policko ...
			if(isset($input))
			{
				if($field->display == 'viewonly')
				{
					$input->setDisabled(TRUE);
				}
//				Tracy\Debugger::$maxDepth = 3;
//				\Tracy\Debugger::barDump($field->style, $field->name);
				// styl na policko
				if($field->style !== NULL)
				{
//					$input->getControl()->attrs['style'] = (string)$field->style;
//					$input->getControlPart()->attrs['style'] = (string)$field->style;
//					dump($input, $field->style);
                    if($input instanceof Nette\Forms\Controls\Checkbox) {
                        $input->getLabelPrototype()->addAttributes(array('style' => (string)$field->style));
                    }
                    else {
                        $input->setAttribute('style', (string)$field->style);
                    }
				}

				// Pokud je definovana kaskada dame do data atributu info
				if(!empty($field->control->cascade) && !empty($classField->ref))
				{
					$input->setAttribute('data-cascade', $field->control->cascade->toArray(TRUE));
					$input->setAttribute('data-cascade-class', $classField->ref);
					$input->setAttribute('data-cascade-signal', $this->link('cascade!'));
				}
            
                // computed fields -- je to type text, nebude to zde
                if(isset($field->control->settings) && isset($field->control->settings->computedField))
                {
                    if(!empty($field->control->settings->computedField->type) && !empty($field->control->settings->computedField->value))
                    {
                        $cf = $field->control->settings->computedField;
                        $input->setAttribute('data-computed-type', $cf->type);
                        $input->setAttribute('data-computed-value', $cf->value);
                        $input->setAttribute('data-computed-field', $field->name);
                        $input->setAttribute('data-computed-datatype', $field->type);
                    }
                    else {
                        throw new InvalidArgumentException('Computed field must have both "type" and "value" properties for field "'.$field->name.'"');
                    }
                }
                
                $namePathFiltered = preg_replace('/\.[0-9]+\./Umisu', '', trim($namePrefix.'.'.$field->name, '.'));
                                
                $input->setAttribute('data-fieldinfo-path', $namePathFiltered);
                $input->setAttribute('data-fieldinfo-name', $field->name);
                $input->setAttribute('data-fieldinfo-type', $field->type);
                $input->setAttribute('data-fieldinfo-size', $classField->size);
			}
            
		} // end-foreach
	}

	/**
	 * Get POST value for fieldName
	 * @param string $fieldName Field Name (teckama oddeleny)
	 * @reutrn mixed|NULL NULL = neni v POST
	 */
	public function getPostValue($fieldName)
	{
		$parts = explode('.', $fieldName);
		$firstNamePart = array_shift($parts);
		$val = $this->getHttpRequest()->getPost($firstNamePart) ?? NULL;
		while($val !== NULL && count($parts) > 0)
		{
			$next = array_shift($parts);
			$val = isset($val[$next]) ? $val[$next] : NULL;
		}
		if(is_array($val))
		{
			return NULL;
		}
		return $val;
	}

	/**
	 * Get cascade filter - nahradi nazev pole ve value za skutecne value
	 * @param \Abra\Service\FormField $field
	 * @param Entity $formData
	 * @return NULL|Abra\Service\FilterGroup Pokud je cascade definovan, vrati filtry, pokud neni definovan vrati NULL
	 */
	public function getCascadeFilter($field, $formData)
	{
		if(!isset($field->control->cascade))
		{
			return NULL;
		}
		$cascade = Abra\Service\FilterGroup::createFrom($field->control->cascade->toArray());
		// Jednourovnove filtry - nerekurzivni
		foreach($cascade->filters as $f)
		{
			if(method_exists($formData, 'getPropertyPathData'))	// FIXME ???
			{
				$objOrValue = $formData->getPropertyPathData($f->value);
				$f->value = is_scalar($objOrValue) ? $objOrValue : $objOrValue->ID;
			}
		}
		return $cascade;
	}

	/**
	 * Handler pro chybne odeslani formulare
	 */
	public function formErrorHandler(Form $form)
	{
		$errors = $form->getErrors();
		foreach($errors as $error)
		{
			$this->flashMessage(__($error), 'error');
		}
		$this->redirect('this');
	}





	// ---------------------------------------- OBSLUHA SIGNALU -------------------------------------------------

	/**
	 * Mazani souboru z kolekce
	 * 
	 * @param string $r LINK na mazani v ABRE
	 */
	public function handleDeleteFile($r)
	{
		$url = base64_decode($r);
		if(Strings::startsWith($url, '/raw/documentcontent/delete/')) {
			$ending = str_replace('/raw/documentcontent/delete/', '', $url);
			$req = new \Abra\Service\Request('documentcontent', 'delete', $ending);
			$this->abraService->rawData($req);
			$this->redirect('this');
		}
		else {
			throw new InvalidArgumentException('Invalid URL for file deletion! URL: "'.$url.'"');
		}
	}

	/**
	 * Signal provede dotaz do ABRY na stazeni RAW dat z URL $r.
	 * Ziskana data presmeruje na vystup a skonci
	 * @param string $r LINK na data, ktera se z ABRY stahnou
	 * @param string $f Jmeno souboru
	 * @param bool $forceInline Pokud true, posle se Content-Disposition: inline
	 * @param string $forceExt Extension which is going to be forced for filename
	 */
	public function handleRawDownload($r, $f, $forceInline = FALSE, $forceExt = NULL)
	{
		$url = base64_decode($r);
		$fileName = base64_decode($f);
		// Forcing extension
		if(!empty($forceExt) && !Strings::endsWith(Strings::lower($fileName), '.'.$forceExt))
		{
			$fileName .= '.'.$forceExt;
		}
		if(Strings::startsWith($url, '/'))
		{
			$url = Strings::substring($url, 1);
		}
        
        $url = urldecode($url); // decode ~ in IDs
        
		$parts = explode('/', $url);
		if(count($parts) < 2)
		{
			$this->flashMessage(__('Invalid download request!'), 'error');
			return;
		}
		// vypada to jako validni dotaz na download, jdeme dal
		try
		{
			switch($parts[0])
			{
				// download RAW dat
				case 'raw':
					$req = new \Abra\Service\Request($parts[1], (isset($parts[2]) ? $parts[2] : NULL), (isset($parts[3]) ? $parts[3] : NULL));
					$rawData = $this->abraService->rawData($req);
					$contentDisposition = 'inline';
					header('Content-type: '.  \AbraHelpers::detectMimeType($fileName, $contentDisposition));
					if($forceInline)
					{
						header('Content-Disposition: inline; filename="'.$fileName.'"');
					}
					else
					{
						header('Content-Disposition: '.$contentDisposition.'; filename="'.$fileName.'"');
					}
					print $rawData;
					$this->terminate();
					break;
			}
		}
		// TERMINATE vyhazuje AbortException
		catch(\Nette\Application\AbortException $e)
		{
			throw $e;
		}
		// Ostatni exceptions jsou chyby
		catch(\Exception $e)
		{
			$this->flashMessage(__($e->getMessage()), 'error');
		}
	}


	/**
	 * Naseptavac - signal
	 * Naseptava se pres metodu "read"
	 *
	 * @param string $e Zdrojova entita
	 * @param string $term Vyhledavaci fraze
	 * @param string $filters Filtry
     * @param string $displayField Display field for "LABEL"
	 */
	public function handleAutocomplete($e, $term = NULL, $filters = NULL, $displayField = 'DisplayName')
	{
		$request = new \Abra\Service\Request($e, 'read', NULL);
		$request->setSearch($term);

		// Filtry v naseptavaci
		if(!empty($filters))
		{
			$filtersDecoded = \Nette\Utils\Json::decode($filters, \Nette\Utils\Json::FORCE_ARRAY);
			if(!empty($filtersDecoded))
			{
				// Pokud se predaji filtry, budeme filtrovat ciselnik
				$filterGroup = $request->setFilterGroup('AND');
				// Filtry pouze v jedne urovni (ABRA jich stejne vic neumi 2015-07-27)
				foreach($filtersDecoded as $filterName => $filterValue)
				{
					$filterGroup->addFilter($filterName, $filterValue, '=');
				}
			}
		}

		$response = $this->abraService->post($request);
		$data = array();
		if(!empty($response->data))
		{
			foreach($response->data as $id => $object)
			{
				$data[] = array(
					'id' => $id,
                    'label' => $object->{$displayField},//__toString(),
					'value' => $id,
					'item' => $object->toAssoc()
				);
			}
		}
		$this->sendResponse(new \Nette\Application\Responses\JsonResponse($data));
	}


	/**
	 * Pomocny signal pro naseptavac, ktery vytahne jeden zaznam read/ID pro predanou entitu a ID
	 * @param string $e Entita, jejiz jeden objekt se vytahne
	 * @param string $abraId Abra ID objektu, ktery se predvyplni
	 */
	public function handleAutocompleteOne($e, $abraId)
	{
		$request = new \Abra\Service\Request($e, 'read', $abraId);
		$response = $this->abraService->post($request, TRUE);
 		$item = $response->data;
		/* @var $item Entity */
		$this->sendResponse(new \Nette\Application\Responses\JsonResponse($item->toAssoc()));
	}

    
    /**
     * Fix HTTP auth from HTTP_AUTHORIZATION header
     * 
     * Tries PHP_AUTH_USER, AUTH_USER, REMOTE_USER, HTTP_AUTHORIZATION
     * Result is set to PHP_AUTH_USER (+ PW)
     */
    protected function fixHttpAuth()
    {
        if(!isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['AUTH_USER'])) {
            $_SERVER['PHP_AUTH_USER'] = $_SERVER['AUTH_USER'];
            $_SERVER['PHP_AUTH_PW'] = isset($_SERVER['AUTH_PW']) ? $_SERVER['AUTH_PW'] : NULL;
            return;
        }
        if(!isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['REMOTE_USER'])) {
            $_SERVER['PHP_AUTH_USER'] = $_SERVER['REMOTE_USER'];
            $_SERVER['PHP_AUTH_PW'] = isset($_SERVER['REMOTE_PW']) ? $_SERVER['REMOTE_PW'] : NULL;
            return;
        }
        if(!isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $auth = explode(' ', $_SERVER['HTTP_AUTHORIZATION']);
            if(strtoupper($auth[0]) == 'BASIC' && sizeof($auth) == 2) {
                $auth = @base64_decode($auth[1]);
                if($auth) {
                    $auth = explode(':', $auth, 2);
                    if(sizeof($auth) == 2) {
                        $_SERVER['AUTH_TYPE'] = 'Basic';
                        $_SERVER['PHP_AUTH_USER'] = $auth[0];
                        $_SERVER['PHP_AUTH_PW'] = $auth[1];
                    }
                }
            }
            return;
        }
    }

    
    /**
     * log response-error-****.txt
     * 
     * @param string $method
     * @param string $url
     * @param array $requestHeaders
     * @param string $request
     * @param array $responseHeaders
     * @param string $response
     */
    public function logResponseError($method, $url, $requestHeaders, $request, $responseHeaders, $response) 
    {
        $saveFile = LOG_DIR.'/response-error-'.microtime(true).'.txt';
        
        $msg = strtoupper($method)." {$url}\n\n".
                implode("\n", $requestHeaders)."\n\n".
                $request."\n\n".
                "--------------------------------------------------------------------------------\n".
                "\n\n".
                implode("\n", $responseHeaders)."\n\n".
                $response;
        
        // ulozit chybu
        file_put_contents($saveFile, $msg);
    }
    
}
