<?php

/**
 * Description of CachePresenter
 */
class CachePresenter extends BasePresenter
{

	/**
	 * @var CacheModel
	 */
	protected $cacheModel;

	/**
	 * Odmazani cache pro spojeni a objekt, nebo pro cele spojeni, nebo pro vsechna spojeni
	 * @param string|NULL $connection Spojeni, pro ktere se objekt odmaze
	 * @param string|NULL $object
	 */
	public function actionClearcache($connection = NULL, $object = NULL)
	{
		$files = $this->cacheModel->clear($connection, $object);
		dump($files);
		$this->terminate();
	}
}