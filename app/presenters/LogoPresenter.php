<?php

/**
 * LogoPresenter
 *
 * @secured
 */
class LogoPresenter extends BasePresenter
{

	/**
	 * Vyrenderuje logo
	 */
	public function renderData()
	{
        $mime = empty($this->ssUser['logo']->mime) ? 'image/jpeg' : $this->ssUser['logo']->mime;
		$this->getHttpResponse()->addHeader('Content-type', $mime);
		print $this->ssUser['logo']->data;
		$this->terminate();
	}

}