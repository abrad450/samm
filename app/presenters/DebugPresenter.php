<?php
/**
 * Debug presenter
 * 
 * @secured
 */
class DebugPresenter extends BasePresenter
{

	/**
	 * Presmerovani na debug ON
	 */
	public function actionDefault()
	{
		$this->redirect('on');
	}


	/**
	 * Zapnuti debug modu
	 */
	public function actionOn()
	{
		$this->getHttpResponse()->setCookie('debugmode', 1, 0);
		$this->redirect('Default:default');
	}


	/**
	 * Vypnuti debug modu
	 */
	public function actionOff()
	{
		$this->getHttpResponse()->setCookie('debugmode', 0, 0);
		$this->redirect('Default:default');
	}

	/**
	 * Zapnuti maintenance
	 */
	public function actionMon()
	{
		file_put_contents(WWW_DIR.'/maintenance.flag', 'maintenance');
		$this->redirect('Default:default');
	}

	/**
	 * Vypnuti maintenance
	 */
	public function actionMoff()
	{
		unlink(WWW_DIR.'/maintenance.flag');
		$this->redirect('Default:default');
	}

    
    /**
     * Znovu-nacteni modulu z ABRA
     */
    public function actionReadModules()
    {
        $allowedModules = $this->modulesModel->getAllowedModules();
        $this->ssUser['allowedModules'] = $allowedModules;
        $this->redirect('Default:default');
    }
    
    /**
     * Clear sessions
     */
    public function actionClearSessions()
    {
        $this->session->close();
        $sessionDir = TEMP_DIR.'/sessions';
        if(is_dir($sessionDir)) {
            $files = glob($sessionDir.'/*');
            foreach($files as $f) {
                if(is_file($f)) {
                    unlink($f);
                }
            }
        }
        $this->redirect('Default:default');
    }
    
    
    
    
    /**
     * Export PO file
     * 
     * @param bool $mode 1 = exclude comboboxes, 2 = only comboboxes, NULL = all
     */
    public function handleExportLang($mode = NULL)
    {
        $excludeTypes = array();
        if(!empty($mode)) {
            switch($mode) {
                case 1:
                    $excludeTypes = array(SammTranslator\Gettext::TRANSLATION_TYPE_FORM_VALUE);
                    break;
                case 2:
                    $excludeTypes = array(
                            SammTranslator\Gettext::TRANSLATION_TYPE_FORM_LABEL,
                            SammTranslator\Gettext::TRANSLATION_TYPE_FORM_VALIDATION,
                            SammTranslator\Gettext::TRANSLATION_TYPE_TEXT
                        );
                    break;
                default:
                    break;
            }
        }
        $content = $this->sammTranslator->getPOFileContent(TRUE, $excludeTypes);
        $httpResponse = $this->getHttpResponse();
		$httpResponse->setContentType('text/x-gettext-translation');
		$httpResponse->setHeader('Content-Disposition', 'attachment; filename="'.$this->lang.'.system.po"');
        $httpResponse->setHeader('Content-Length', strlen($content));        
        print $content;
        $this->terminate();
    }
    
    /**
     * Lang Export
     */
    public function actionLangExport()
    {
    }
    
    /**
     * Upload PO file form
     * 
     * @param string $name
     */
    public function createComponentUploadPoForm($name)
    {
        $form = new Nette\Application\UI\Form($this, $name);
        $form->addMultiUpload('files', 'PO soubory:')
                ->addRule(\Nette\Forms\Form::FILLED, 'Vyberte PO soubory pro aktuální jazyk:');
        $form->addSubmit('submit', 'Nahrát PO soubory pro aktuální jazyk');
        $form->onSuccess[] = array($this, 'uploadPoFormSuccess');
        return $form;
    }
    
    /**
     * PO upload form successfully submitted
     * 
     * @param \Nette\Forms\Form $form
     */
    public function uploadPoFormSuccess($form)
    {
        $values = $form->getValues();
        $files = $values['files'];
        /* @var $file Nette\Http\FileUpload */
        try {
            $tmpFiles = array();
            foreach($files as $f) {
                $tmpFiles[] = $f->getTemporaryFile();
            }
            $this->sammTranslator->saveSystemFile($this->lang, $tmpFiles);
        }
        catch(\Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
            return;
        }
        $this->flashMessage('Systémový překladový soubor pro jazyk "'.$this->lang.'" byl úspěšně uložen.', 'success');
        $this->redirect('this');
    }
    
    
}