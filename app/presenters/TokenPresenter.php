<?php

use Nette\Application\BadRequestException;

/**
 * Token Presenter
 */
class TokenPresenter extends BasePresenter
{
    
    /**
     * Token Model
     * 
     * @var TokenModel
     * @inject
     */
    public $tokenModel;
    
	/**
	 * ABRA Service
	 *
	 * @var Abra\Service\Service
     * @inject
	 */
	public $service;    
    
    
    /**
     * Save tokens from ABRA
     */
    public function actionUpload()
    {
        $httpReq = $this->getHttpRequest();
        $method = strtolower($httpReq->getMethod());
        
        if($method !== 'post') {
            throw new BadRequestException('Method not allowed', 401);
        }
        
        $auth = $httpReq->getHeader('Authorization');
        if(empty($auth)) {
            throw new BadRequestException('Invalid authorization', 403);
        }
        $authParts = explode(' ', $auth);
        if(count($authParts) < 2 || strtolower($authParts[0]) !== 'bearer' || strtolower(base64_decode($authParts[1])) !== strtolower($this->configuration['securityToken'])) {
            throw new BadRequestException('Invalid authorization', 403);
        }
        
        $httpResp = $this->getHttpResponse();
        $xml = $httpReq->getRawBody();
        try {
            $amount = $this->tokenModel->saveFromXml($xml);
            $httpResp->setCode(200);
            $output = '<'.'?xml version="1.0" encoding="utf-8"?'.'><response><saved>'.$amount.'</saved></response>';
        }
        catch(Exception $e) {
            \Tracy\Debugger::log($e);
            $httpResp->setCode(500);
            $output = '<'.'?xml version="1.0" encoding="utf-8"?'.'><response><error>'.$e->getMessage().'</error></response>';
        }
        $httpResp->setContentType('application/xml', 'UTF-8');
        echo $output;
        $this->terminate();
    }
    
    /**
     * Clear all tokens
     */
    public function actionClearAll()
    {
        if(\Tracy\Debugger::$productionMode) {
            throw new BadRequestException('Not allowed', 403);
        }        
        $this->tokenModel->clearAll();
        $this->redirect('show');
    }
    
    /**
     * Show tokens tree
     */
    public function renderShow()
    {
        if(\Tracy\Debugger::$productionMode) {
            throw new BadRequestException('Not allowed', 403);
        }
        $this->template->tree = $this->tokenModel->getTree(function($obj) {
            $obj->validity = $obj->validity->format($this->culture['dateTime']);
            $obj->created = $obj->created->format($this->culture['dateTime']);
            $obj->used = $obj->used ? $obj->used->format($this->culture['dateTime']) : NULL;
            $obj->expanded = FALSE;
        });
    }


    /**
     * Process token ID
     * 
     * @param string $guid Token GUID
     */
    public function renderProcess($guid)
    {
//        $xml = '<response>
//<tokenmodule>
//<name>TNxReceivedInvoice</name>
//<documents>
//<document>
//<id>1010000101</id>
//<displayname>FP-4/2020</displayname>
//<description>GMBH, faktura ya slu6bz</description>
//<beforecheck>A</beforecheck>
//<solved>A</solved>
//</document>
//<document>
//<id>1010000101</id>
//<displayname>FP-4/2020</displayname>
//<description>GMBH, faktura ya slu6bz</description>
//<beforecheck>A</beforecheck>
//<solved>N</solved>
//<error>chzba</error>
//</document>
//</documents>
//</tokenmodule>
//<tokenmodule>
//<name>TNxReceivedInvoice</name>
//<documents>
//<document>
//<id>1010000101</id>
//<displayname>FP-4/2020</displayname>
//<description>GMBH, faktura ya slu6bz</description>
//<beforecheck>A</beforecheck>
//<solved>A</solved>
//</document>
//<document>
//<id>1010000101</id>
//<displayname>FP-4/2020</displayname>
//<description>GMBH, faktura ya slu6bz</description>
//<beforecheck>A</beforecheck>
//<solved>N</solved>
//<error>chzba</error>
//</document>
//</documents>
//</tokenmodule>
//</response>';
//        
//        $this->template->tokenModules = \Abra\Service\TokenModuleCollection::createFromXml(simplexml_load_string($xml));
//        return;
        
        // Kontrola expirace tokenu
        $now = new \DateTime();
        $token = $this->tokenModel->getOne($guid);
        if($token->validity < $now) {
            $this->template->tokenAlreadyExpired = $token;
            return;            
        }
        
        // Neanonymni tokeny vyzaduji prihlaseni uzivatele
        if(!$this->user->isLoggedIn() && !$token->anonymous) {
            $this->redirect('Sign:in', ['ref' => $this->link('this')]);
        }

        if($token->used) {
            $this->template->tokenAlreadyUsed = $token;
            return;
        }
        
        $request = new Abra\Service\Request($token->type, $guid);
        $request->setType(Abra\Service\Request::ACTION_TOKEN);
        $response = $this->service->post($request, FALSE, TRUE);
        
        // Processing token
        $xml = simplexml_load_string($response);
        if(!empty($xml->errors) && !empty($xml->errors->error)) {
            $errors = [];
            foreach($xml->errors->error as $error) {
                $err = new Abra\Service\Error();
                $err->code = (string)$error->code;
                $err->message = (string)$error->message;
                $err->location = (string)$error->location;

                $errors[] = $err;
            }
            $this->template->isProductionMode = Tracy\Debugger::$productionMode;
            $this->template->errors = $errors;
        }
        else {
            $tokenModules = Abra\Service\TokenModuleCollection::createFromXml($xml); // $contents

            $this->template->tokenModules = $tokenModules;
            $this->template->moduleMenuCookieValue = 'collapsed';
            $this->template->customModuleName = 'Výsledek zpracování';
            // Set token as USED
            $userId = $this->user->identity->user->id;
            $userName = $this->user->identity->user->name;

            $this->tokenModel->setUsed($guid, $userId, $userName);
        }        
    }

    
}
