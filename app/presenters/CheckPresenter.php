<?php

/**
 * Check Presenter
 */
class CheckPresenter extends BasePresenter
{

    /**
     * Check SSL certificate for a given URL
     * 
     * @param string $urlb64 Base 64 encoded URL
     * @param bool $full Full output
     */    
    public function actionCertificate($urlb64, $full = 0)
    {
        $check = $this->configuration['certExpirationCheck'];
        $warn = $this->configuration['certExpirationWarn'];
        $output = array(
            'certExpirationCheck' => $check,
            'certExpirationWarn' => $warn,
            'info' => NULL,
            'validFrom' => NULL,
            'validTo' => NULL,
            'error' => NULL
        );
        if($check) {
            $url = base64_decode($urlb64);
            $host = parse_url($url, PHP_URL_HOST);
            $port = parse_url($url, PHP_URL_PORT);
            $get = stream_context_create(array(
                'ssl' => array(
                    'capture_peer_cert' => TRUE,
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE
                ),
            ));
            $read = @stream_socket_client("ssl://".$host.':'.($port ? $port : '443').'/', $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
            if($read) {
                $cert = @stream_context_get_params($read);
                if(isset($cert['options']) && isset($cert['options']['ssl']) && isset($cert['options']['ssl']['peer_certificate'])) {
                    $certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);
                    if($full) {
                        $output['info'] = $certinfo;
                    }
                    if(isset($certinfo['validTo_time_t'])) {
                        $output['validFrom'] = \DateTime::createFromFormat('U', $certinfo['validFrom_time_t'])
                                ->setTimezone(new \DateTimeZone(date_default_timezone_get()))
                                ->format(DATE_ISO8601);
                        $output['validTo'] = \DateTime::createFromFormat('U', $certinfo['validTo_time_t'])
                                ->setTimezone(new \DateTimeZone(date_default_timezone_get()))
                                ->format(DATE_ISO8601);
                    }
                    else {
                        $output['error'] = 'Unable to get certificate validity.';
                    }
                }
                else {
                    $output['error'] = 'Unable to get stream params.';
                }
            }
            else {
                $output['error'] = 'Unable to read certificate: '.$errstr;
            }
        }
        $this->sendJson($output);
    }
    
}
