<?php

use \Tracy\Debugger,
	\Nette\Application as NA;

/**
 * Error presenter.
 */
class ErrorPresenter extends BasePresenter
{

	/**
	 * @param  Exception
	 * @return void
	 */
	public function renderDefault($exception)
	{
		// AJAX request? Just note this error in payload.
		if ($this->isAjax())
		{
			$this->payload->error = TRUE;
			$this->terminate();

		}
		elseif ($exception instanceof NA\BadRequestException)
		{
			$code = $exception->getCode();
			// load template 403.latte or 404.latte or ... 4xx.latte
			$this->setView(in_array($code, array(403, 404, 405, 410, 500)) ? $code : '4xx');
			// set exception to template
			$this->template->exception = $exception;
			// log to access.log
			Debugger::log("HTTP code $code: {$exception->getMessage()} in {$exception->getFile()}:{$exception->getLine()}", 'access');
		}
		else
		{
			// load template 500.latte
			$this->setView('500');
			// set exception to template
			$this->template->exception = $exception;
			// and log exception
			Debugger::log($exception, Debugger::ERROR);
		}
	}

}
