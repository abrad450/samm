<?php

use Nette\Application\UI,
	Nette\Security as NS;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{

	/**
	 * Entita pro profil uzivatele
	 *
	 * @var TNxSecurityUser
	 */
	protected $entity = 'TNxSecurityUser';

	/**
	 * Popis formulare pro editaci
	 *
	 * @var \Abra\Service\FormDescription
	 */
	protected $formDescription;

	/**
	 * Data do formulare
	 *
	 * @var mixed
	 */
	protected $formData;


	/**
	 * Startup presenteru
	 */
	public function startup()
	{
		parent::startup();
		// Clear all filter/order/page session sections
		$filters = isset($_SESSION['__NF']['DATA']) ? preg_grep('/(^filter_|^page_|^order_)/Uiu', array_keys($_SESSION['__NF']['DATA'])) : array();
		foreach($filters as $filterSection)
		{
			$this->session->getSection($filterSection)->remove();
		}
		// Clear order session section
		$this->session->getSection('order')->remove();
	}

	public function actionDefault()
	{
		$this->redirect('Default:');
	}

	/**
	 * Prihlaseni
	 */
	public function actionIn()
	{
        $this->template->moduleMenuCookieValue = 'full';
        $this->template->showHttpWarning = $this->configuration['showHttpWarning'] ? '1' : '0';
        
        $useNtlm = $this->configuration['ntlm'];
        if($useNtlm) {
            $connections = $this->configuration['connections'];
            if(count($connections) > 1) {
                $this->setView('ntlmIn');
                $this->template->connections = $connections;
            }
            else {
                $this->ntlmLoginConnection(key($connections));
            }
        }
        
        if(isset($this->configuration['connections'])) {
            $connections = $this->configuration['connections'];
            if(count($connections) === 1) {
                $this->template->oneConnectionName = reset($connections);
            }
        }
	}
    
    /**
     * Prihlaseni - odmazani cookie na domene druheho radu
     */
    public function renderIn()
    {
        $sessionName = session_name();
        $host = $this->getHttpRequest()->getUrl()->host;
        $hostParts = explode('.', $host);
        if(count($hostParts) > 2) {
            $baseHost = array_slice($hostParts, -2);
            $baseDomain = '.'.implode('.', $baseHost);
            $this->getHttpResponse()->deleteCookie($sessionName, '/', $baseDomain);
        }
    }

    /**
     * NTLM Login to given ABRA connection
     *
     * @param string $conn Connection ident
     */
    public function handleNtlmLoginConnection($conn)
    {
        $useNtlm = $this->configuration['ntlm'];
        $connections = $this->configuration['connections'];
        if($useNtlm && isset($connections[$conn])) {
            $this->ntlmLoginConnection($conn);
        }
    }

    /**
     * Prihlasit se do predane ABRA connection s domenovym uctem
     *
     * @param string $connection Nazev ABRA connection
     * @throws \Exception
     */
    protected function ntlmLoginConnection($connection)
    {
		$ssNtlmTmp = $this->getSession()->getSection('ntlm-tmp');
		$remoteUser = $ssNtlmTmp['u4a'];

        if(empty($remoteUser)) {
            echo 'Hlavičky "REMOTE_USER", "PHP_AUTH_USER", "AUTH_USER" i "HTTP_AUTHORIZATION" jsou prázdné.<br><br>'.
                'Je SSPI modul správně nakonfigurován?<br>'.
                'Je adresa tohoto webu přidána do zóny "Intranet"?';
            $this->terminate();
        }

        $expire = $this->configuration['sessionExpiration'];

        // nastaveni connection, pokud byla nejaka definovana
        $this->abraService->setConnectionName($connection);

        // session a login
        $this->getUser()->setExpiration($expire);
        $this->getUser()->login($remoteUser);

        $this->ssUser = $this->session->getSection('user');
        $this->ssUser['connection'] = $connection;
        $this->ssUser['auth'] = $this->user->identity->data['auth'];

		$apiCreds = $this->abraService->getApiCredentials();
		$this->ssUser['loginUser'] = $apiCreds['user'];
		$this->ssUser['loginPassword'] = $apiCreds['password'];
        
        // I SEM BYCHOM MOHLI PRIDAT TOKEN - dostupne moduly se mohou vracet podle prihlaseneho uzivatele
        $this->abraService->setToken($this->ssUser['auth']->token);

		// Clear cached data foruser
		$this->codelistModel->clearForUserCache();
		
        // nacteme povolene moduly
        try {
            $allowedModules = $this->modulesModel->getAllowedModules();
            $this->ssUser['allowedModules'] = $allowedModules;
        }
        catch(\Exception $e) {
            $_ENV['repeatedError'] = 'yes';
            throw $e;
        }

        // nacteme logo
        try {
            $this->ssUser['logo'] = $this->modulesModel->getLogo();
        }
        catch(\Exception $e) {
            $this->ssUser['logo'] = NULL;
        }

        // presmerovat na homepage (nebo REF)
        $ref = $this->getParameter('ref');
        if(empty($ref)) {
            $this->redirect('Default:');
        }
        else {
            $this->redirectUrl($ref);
        }
    }




	/**
	 * Odhlaseni uzivatele
	 */
	public function actionOut()
	{
		// Clear cached data foruser
		$this->codelistModel->clearForUserCache();
		// CLEAR SESSION
		$this->ssUser = $this->session->getSection('user');
		$this->ssUser->remove();
		$this->session->destroy();
		$this->getUser()->logout(TRUE);
		// Redirect
		$this->flashMessage(__('Byl jste odhlášen.'));
		$this->redirect('in');
	}

	/**
	 * Zmena hesla
	 */
	public function actionChangePassword()
	{
	}

	/**
	 * Editace profilu
	 */
	public function actionEditProfile()
	{
        $editProfile = $this->configuration['editProfile'];
        if(!$editProfile) {
            $this->redirect('Default:default');
        }
		$form = $this->getComponent('profileForm');
		/* @var $form Form */
		if(!$form->isSubmitted())
		{
			$valz = $this->prepareFormValues($this->formData);
			$form->setDefaults($valz);
		}
	}


	/**
	 * Before render - odstranime moduly, na login se nevypisuji
	 */
	public function beforeRender()
	{
		parent::beforeRender();
		$this->template->availableModules = new \Nette\Utils\ArrayHash();
	}



	// ------------------------------------------ Komponenty ---------------------------------------------


	/**
	 * Vytvori formular pro editaci
	 * @param string $name
	 */
	public function createComponentProfileForm($name)
	{
		$this->template->description = $this->formDescription = $this->universalModel->getFormDescription($this->entity);
		$this->template->data = $this->formData = $this->universalModel->getForm($this->entity, $this->user->id);

		$form = $this->createForm($name, $this->formDescription, $this->formData);

		$form->onSuccess[] = [$this, 'formSuccessHandler'];
		$form->onError[] = [$this, 'formErrorHandler'];
		$form->setTranslator($this->sammTranslator);

		return $form;
	}


	/**
	 * Formular byl uspesne odeslan
	 * @param Form $form
	 */
	public function formSuccessHandler(UI\Form $form)
	{
		$values = $form->getValues();
		try
		{
			$this->universalModel->update($this->entity, $values);
			if(isset($values['Limit']))
			{
				$user = $this->user->identity->data['user'];
				$user->limit = $values['Limit'];
			}
			if(DEBUG_ENABLED && isset($form['dbgsubmit']) && $form['dbgsubmit']->isSubmittedBy())
			{
				return;
			}
		}
		catch(\Abra\Service\ResponseException $e)
		{
			foreach($e->getErrors() as $err)
			{
				$this->flashMessage(__($err->message), 'error');
			}
			return;
		}
		catch(\Exception $e)
		{
			$this->flashMessage(__($e->getMessage()), 'error');
			return;
		}
		$this->flashMessage(__('Váš profil byl Úspěšně uložen'), 'success');
		$this->redirect('Default:default');
	}



	/**
	 * Sign in form component factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm($name)
	{
		// Spojeni na ABRU
		$connections = NULL;
		if(isset($this->configuration['connections']))
		{
			$connections = $this->configuration['connections'];
		}

		$form = new UI\Form($this, $name);
		$form->setTranslator($this->sammTranslator);
        
        $form->addProtection('Formulář vypršel, obnovte prosím stránku');

		if($connections !== NULL)
		{
			if(count($connections) == 1)
			{
				$form->addHidden('connection', key($connections));
			}
			else
			{
				$form->addSelect('connection', 'Spojení:', (array)$connections)
					->setRequired('Zvolte spojení, se kterým chcete pracovat.');
                // Predvyplneni spojeni podle parametru v URL
                $c = $this->getParameter('c');
                if(!empty($c) && isset($connections[$c])) {
                    $form['connection']->setDefaultValue($c);
                }
			}
		}

		$form->addText('username', 'Uživatel:')
				->setRequired('Zadejte své uživatelské jméno.')
				->setDefaultValue(isset($_COOKIE['loginname']) ? $_COOKIE['loginname'] : '');

		$form->addPassword('password', 'Heslo:')
                ->setHtmlAttribute('autocomplete', 'off')
				->setRequired('Zadejte své heslo.');

		$form->addHidden('ref')
				->setDefaultValue($this->getParameter('ref', NULL));

		$form->addSubmit('submit', 'Přihlásit se');

		$form->onSuccess[] = [$this, 'signInFormSubmitted'];
		return $form;
	}


	/**
	 * Prihlaseni uzivatele
	 * @param Form $form
	 */
	public function signInFormSubmitted($form)
	{
        $expire = $this->configuration['sessionExpiration'];

		try
		{
			// hodnoty
			$values = $form->getValues();
			$user = $values->username;
			$password = $values->password;
			$connection = isset($values->connection) ? $values->connection : NULL;

			// nastaveni connection, pokud byla nejaka definovana
			if($connection !== NULL)
			{
				$this->abraService->setConnectionName($connection);
			}
			// session a login
			$this->getUser()->setExpiration($expire);
            //$this->session->close();
			$this->getUser()->login($user, $password, $connection);
			// nastavit cookie s username
			$this->getHttpResponse()->setCookie('loginname', $user, time() + 100*24*60*60); // 100 dní

			$this->ssUser = $this->session->getSection('user');
			$this->ssUser['connection'] = $connection;
			$this->ssUser['auth'] = $this->user->identity->data['auth'];
            $this->ssUser['loginUser'] = $user;
            $this->ssUser['loginPassword'] = $password;

			// I SEM BYCHOM MOHLI PRIDAT TOKEN - dostupne moduly se mohou vracet podle prihlaseneho uzivatele
			$this->abraService->setToken($this->ssUser['auth']->token);

			// Clear cached data foruser
			$this->codelistModel->clearForUserCache();
			
			// nacteme povolene moduly
			try
			{
				$allowedModules = $this->modulesModel->getAllowedModules();
				$this->ssUser['allowedModules'] = $allowedModules;
			}
			catch(\Exception $e)
			{
				$_ENV['repeatedError'] = 'yes';
				throw $e;
			}

			//dump($allowedModules); die;
			// nacteme logo
			try
			{
				$this->ssUser['logo'] = $this->modulesModel->getLogo();
			}
			catch(\Exception $e)
			{
				$this->ssUser['logo'] = NULL;
			}

			// presmerovat na homepage
			if(empty($values['ref']))
			{
				$this->redirect('Default:');
			}
			else
			{
				$this->redirectUrl($values['ref']);
			}
		}
		catch (NS\AuthenticationException $e)
		{
			$form->addError(__($e->getMessage()));
		}
        catch (Exception $ex) {
            if($ex instanceof \Nette\Application\AbortException) {
                throw $ex;
            }
            $form->addError(__($ex->getMessage()));
        }
	}


	/**
	 * Formular pro zmenu hesla
	 * @param string $name Nazev komponenty
	 * @return UI\Form
	 */
	public function createComponentChangePasswordForm($name)
	{
		$form = new UI\Form($this, $name);
		$form->addPassword('oldPassword', 'Staré heslo')
				->addRule(UI\Form::FILLED, 'Zadejte své aktuální heslo');
		$form->addPassword('newPassword', 'Nové heslo')
				->addRule(UI\Form::FILLED, 'Zadejte své nové heslo');
		$form->addPassword('checkPassword', 'Potvrzení hesla')
				->addRule(UI\Form::FILLED, 'Potvrďte své nové heslo')
				->addRule(UI\Form::EQUAL, 'Nová hesla nesouhlasí', $form['newPassword']);
		$form->addSubmit('submit', 'Změnit heslo');
		$form->onSuccess[] = [$this, 'changePasswordFormSubmitted'];
		$form->setTranslator($this->sammTranslator);
		return $form;
	}

	/**
	 * Obsluha uspesne odeslaneho formulare pro zmenu hesla
	 * @param UI\Form $form Formular
	 */
	public function changePasswordFormSubmitted($form)
	{
		$values = $form->getValues();
		try
		{
			$this->userModel->changePassword($values['oldPassword'], $values['newPassword']);
            
			$ssUser = $this->session->getSection('user');
            $ssUser['loginPassword'] = $values['newPassword'];
            
			$ok = TRUE;
			$this->flashMessage(__('Vaše heslo bylo změněno.'), 'success');
		}
		catch(\Exception $e)
		{
			$ok = FALSE;
			$form->addError(__($e->getMessage()));
		}
		if($ok)
		{
			$this->redirect('Default:');
		}
	}

}