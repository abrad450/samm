<?php

use Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route;

/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList();

		// Setup router
		//$router[] = new Route('[index.php]', 'Default:default', Route::ONE_WAY);
		$router[] = new Route('clearcache[/<connection>[/<object>]]', 'Cache:clearcache');
		$router[] = new Route('logo/data', 'Logo:data');
        
        // Tokens
		$router[] = new Route('tokens', 'Token:upload');
        $router[] = new Route('tokens/show', 'Token:show');
        $router[] = new Route('token/<guid>', 'Token:process');

		// Sign in out, change password, editProfile
		$router[] = new Route('sign/<action>', array(
			'presenter' => 'Sign',
			'action' => 'action',
		));

		// univerzalni moduly
		$router[] = new Route('[<lang=cs [a-zA-Z]{2}>/]<bo=s>/<presenter>/<action>[/<id>]', array( // [0-9a-zA-Z]{10}
			'presenter' => 'Default',
			'action' => 'default'
		));

		return $router;
	}

}