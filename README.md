http://triton.abra.eu:8555/core/debug/on

http://triton.abra.eu:8555/core/cache/clear-cache



CSS - extraslim.css

**Verzování**

v bitbucket: Edit commint > add Tags

v config.neon je potřeba povýšit verzi (parameters.version), zobrazuje se v patce systému

# Docker

- Apache

- PHP 7.1 

- composer

- xDebug: nastavení viz: [.docker/files/php/README.md](.docker/files/php/README.md)


v root složce projektu v příkazovém řádku:

    docker-compose build

v souboru `hosts` přidat (windows):

    127.0.0.1       samm.local

v root složce projektu v příkazovém řádku:

    docker-compose up


# Tunel pro testování

Použít https://github.com/localtunnel/localtunnel

- nainstalovat:  `npm install -g localtunnel`
- spustit: `lt --local-https -p 443 --allow-invalid-cert`
- přistoupit na: `https://adresa.localtunnel.me`
	
nebo

- v docker kontejneru spustit: `ssh -R 80:localhost:80 -l sammtest serveo.net`
- přistoupit na: `http://<pridelena-subomena>.serveo.net`



# URLs:
- https://samm.local
- https://samm.local/core/debug/on
- https://samm.local/core/cache/clear-cache

