<?php
namespace Abra\Service;

/**
 * JSON Decode Exception
 *
 * @author ONTU
 */
class JsonDecodeException extends \Exception 
{
	
	protected $jsonErrorCode;
	
	/**
	 *
	 * @param type $jsonErrorCode
	 * @param type $code
	 * @param type $previous 
	 */
	public function __construct($jsonErrorCode, $previous = NULL)
	{
		$this->jsonErrorCode = $jsonErrorCode;
		$message = 'Unknown error';
		switch($jsonErrorCode)
		{
			case JSON_ERROR_DEPTH:			$message = 'Maximum stack depth exceeded'; break;
			case JSON_ERROR_STATE_MISMATCH: $message = 'Underflow or the modes mismatch'; break;
			case JSON_ERROR_CTRL_CHAR:		$message = 'Unexpected control character found'; break;
			case JSON_ERROR_SYNTAX:			$message = 'Syntax error, malformed JSON'; break;
			case JSON_ERROR_UTF8:			$message = 'Malformed UTF-8 characters, possibly incorrectly encoded'; break;
		}
		parent::__construct($message, NULL, $previous);
	}
	
	
	/**
	 * Vrati chybovy kod JSONu
	 * @return type 
	 */
	public function getJsonErrorCode()
	{
		return $this->jsonErrorCode;
	}
}