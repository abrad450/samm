<?php
namespace Abra\Service;

/**
 * Vyjimka s chybou v odpovedi od ABRA sluzby
 */
class ResponseException extends ServiceException
{

	/**
	 * Response
	 * @var Response 
	 */
	protected $response;
	
	protected $errors;
	
	/**
	 * Vytvori vyjimku
	 * @param Response $response Odpoved od ABRA sluzby
	 */
	public function __construct(Response $response, $previous = NULL)
	{
		$errors = $response->getErrors();
		$this->errors = $errors;
		$message = array();
		foreach($errors as $err)
		{
			$message[] = $err->message;
		}
		parent::__construct(implode(' ', $message), 500, $errors, $previous);
		$this->response = $response;
	}
	
	/**
	 * Vrati response
	 * @return Response
	 */
	public function getResponse()
	{
		return $this->response;
	}
	
	
	/**
	 * Vrati pole chyb
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}
	
	/**
	 * Is this error fatal ? (contains at least one error/validation)
	 * @return bool
	 */
	public function isFatal()
	{
		foreach($this->errors as $err)
		{
			/* @var $err Error */
			if($err->isFatal())
			{
				return true;
			}
		}
		return false;
	}
}
