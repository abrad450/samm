<?php
namespace Abra\Service;

use \Nette\Caching\IStorage,
	\Nette\Security\User,
	\Nette\Utils\ArrayHash,
	\Tracy\Debugger;

/**
 * Komunikator s REST API ABRA pomoci JSON
 * @author ONTU
 */
class JsonService extends Service
{
	
	/**
	 * Konstruktor
	 * @param array $config Konfigurace
	 * @param User $user Uzivatel
	 * @param IStorage $cacheStorage Uloziste pro cache
	 */
	public function __construct(array $config, User $user, IStorage $cacheStorage)
	{
		parent::__construct($config, $user, $cacheStorage);
		$this->contentType = 'application/json';
	}
	
	
	public function post(Request $request, $firstOnly = FALSE, $returnRaw = FALSE)
	{
		throw new Exception('Not implemented!');
	}
	
	
	public function get(Request $request, $firstOnly = FALSE, $returnRaw = FALSE)
	{
		throw new Exception('Not implemented!');
	}
	
	
}

