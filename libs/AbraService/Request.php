<?php
namespace Abra\Service;

use \Nette\Utils\ArrayHash;

/**
 * Pozadavek na ABRA web service
 * @property string $entity Entita
 * @property string $action Akce
 * @property string $id ID zaznamu, ktery se prida za
 * @property string $type Typ pozadavku (TYPE_xxxxx)
 * @property mixed $data Data (telo pozadavku)
 * @property FilterGroup $filters filtry
 * @property string $filterXml XML filter string
 * @property array $orderBy Order by
 * @property array $limit Limit
 * @property array $offset Offset
 * @property array $search Vyhledavaci dotaz
 * @property array $groupBy Group by
 * @property-read string $url URL pozadavku
 * @property-read \Nette\Utils\ArrayHash $query QueryString
 * @property-read \Nette\Utils\ArrayHash $httpHeaders HTTP hlavicky
 */
class Request implements \ArrayAccess
{
    use \Nette\SmartObject;
    
	/**
	 * Pozadavek na login (prihlaseni)
	 */
	const TYPE_LOGIN = 'login';

    /**
     * NTLM login
     */
    const TYPE_LOGIN_NTLM = 'login'; // ntlmlogin

	/**
	 * Pozadavek na vypis modulu
	 */
	const TYPE_MODULES = 'modules';

	/**
	 * Pozadavek na forapprove
	 */
	const TYPE_FOR_APPROVE = 'forapprove';
    
	/**
	 * Pozadavek tykajici se metadat
	 */
	const TYPE_DESCRIPTION = 'description';

	/**
	 * Pozadavek tykajici se dat
	 */
	const TYPE_DATA = 'data';

	/**
	 * Pozadavek tykajici se RAW dat
	 */
	const TYPE_RAW = 'raw';

    /**
     * Aktualizace zaznamu v ABRA
     */
	const ACTION_UPDATE = 'update';

    /**
     * Kontrola dat a provedeni vypoctu v ABRA
     */
	const ACTION_VALIDATE = 'validate';
    
    /**
     * Obsluha tokenu
     */
	const ACTION_TOKEN = 'token';

	/**
	 *
	 */
	protected $type;

	/**
	 * Entita
	 * @var string
	 */
	protected $entity;

	/**
	 * Akce
	 * @var uint
	 */
	protected $action;

	/**
	 * ID
	 * @var int
	 */
	protected $id;

	/**
	 * Data
	 * @var mixed
	 */
	protected $data;




	/**
	 * Pole razeni
	 * @var array
	 */
	protected $orderby;

	/**
	 * Skupina filtracnich podminek
	 * @var FilterGroup
	 */
	protected $filters;

	/**
	 * XML filter string
	 * @var string
	 */
	protected $filterXml;

	/**
	 * Vyhledavani
	 * @var string
	 */
	protected $search;

	/**
	 * Limit
	 * @var int
	 */
	protected $limit;

	/**
	 * Offset
	 * @var int
	 */
	protected $offset;

	/**
	 * Groupovani vysledku
	 * @var array
	 */
	protected $groupby;



	/**
	 * Arrayhash QueryString parametru
	 * @var ArrayHash
	 */
	protected $query;


	/**
	 * Arrayhash HTTP hlavicek
	 * @var ArrayHash
	 */
	protected $httpHeaders;


	/**
	 * Konstruktor
	 * @param string $entity Entita
	 * @param string $action Akce
	 * @param uint $id ID konkretni entity
	 * @param array
	 */
	public function __construct($entity = NULL, $action = NULL, $id = NULL, $data = NULL)
	{
		$this->entity = $entity;
		$this->action = $action;
		$this->id = $id;
		$this->data = $data;
		$this->type = self::TYPE_DATA;
		$this->orderby = array();
		$this->filters = NULL;
		$this->filterXml = NULL;
		$this->search = NULL;
		$this->limit = NULL;
		$this->offset = NULL;
		$this->groupby = array();
		$this->query = new ArrayHash();
		$this->httpHeaders = new ArrayHash();
	}


	/**
	 * Vrati typ pozadavku
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * nastavi typ pozadavku
	 * @param string $type TYPE_xxxxx konstanty
	 * @return \Abra\Service\Request Fluent interface
	 */
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}


	/**
	 * Vrati entitu (BO o ktery se jedna)
	 * @return string
	 */
	public function getEntity()
	{
		return $this->entity;
	}

	/**
	 * Nastavit entitu o kterou se jedna v tomto pozadavku
	 * @param string $entity Entita
	 * @return \Abra\Service\Request Fluent rozhrani
	 */
	public function setEntity($entity)
	{
		$this->entity = $entity;
		return $this;
	}


	/**
	 * Akce s entitou
	 * @return string
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 * Nastavi akci s entitou
	 * @param string $action Akce s entitou
	 * @return \Abra\Service\Request Fluent rozhrani
	 */
	public function setAction($action)
	{
		$this->action = $action;
		return $this;
	}


	/**
	 * Vrati ID konkretniho zaznamu
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Nastavi ID konkretniho zaznamu pro tento pozadavek
	 * @param mixed $id ID
	 * @return \Abra\Service\Request Fluent rozhrani
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}



	public function getData()
	{
		return $this->data;
	}

	public function setData($data)
	{
		$this->data = $data;
		return $this;
	}


	/**
	 * Vrati vyhledavany retezec zaslany v tomto pozadavku
	 * @return string
	 */
	public function getSearch()
	{
		return $this->search;
	}

	/**
	 * Nastavi vyhledavany retezec zaslany v tomto pozadavku
	 * @return string $search
	 */
	public function setSearch($search)
	{
		$this->search = $search;
		return $this;
	}


	/**
	 * Vrati nastveny limit
	 * @return string
	 */
	public function getLimit()
	{
		return $this->limit;
	}

	/**
	 * Nastavi limit
	 * @return string
	 */
	public function setLimit($limit)
	{
		$this->limit = $limit;
		return $this;
	}

	/**
	 * Vrati nastaveny offset
	 * @return string
	 */
	public function getOffset()
	{
		return $this->offset;
	}

	/**
	 * Nastavi offset
	 * @return string
	 */
	public function setOffset($offset)
	{
		$this->offset = $offset;
		return $this;
	}

	/**
	 * Vrati volanou URL (type/entita/akce/id) vsechny casti jsou nepovinne
	 * @return string
	 */
	public function getUrl()
	{
		return  (isset($this->type) ? '/'.$this->type : '').
				(isset($this->entity) ? '/'.$this->entity : '').
				(isset($this->action) ? '/'.$this->action : '').
				(isset($this->id) ? '/'.$this->id : '');
	}



	/**
	 * Prida filtr... standardne se pokud neni vytvorena vytvori skupina s operatorem AND
	 * @param string $column Sloupec
	 * @param mixed $value Hodnota
	 * @param string $operator Operator
	 * @return Fil;terGroup
	 * @deprecated use setFilterGroup instead
	 */
	public function addFilter($column, $value, $operator)
	{
		if($this->filters === NULL)
		{
			$this->filters = $this->setFilterGroup(FilterGroup::OPERATOR_AND);
		}
		$this->filters->addFilter($column, $value, $operator);
		return $this->filters;
	}

	/**
	 * Prida filtracni skupinu
	 * @return FilterGroup Skupina filtracnich podminek, ktera byla vytvorena
	 */
	public function setFilterGroup($operator)
	{
		$this->filters = new FilterGroup($operator);
		return $this->filters;
	}

	/**
	 * Vrati skupinu filtru
	 * @return FilterGroup
	 */
	public function getFilterGroup()
	{
		if($this->filters === NULL) throw new ServiceException('Call SetFilterGroup with operator parameter');
		return $this->filters;
	}


	/**
	 * Get Filter XML
	 * @return string
	 */
	public function getFilterXml()
	{
		return $this->filterXml;
	}

	/**
	 * Set filter XML
	 * @param string $filterXml
	 * @return self
	 */
//	public function setFilterXml($filterXml)
//	{
//		$this->filterXml = $filterXml;
//		return $this;
//	}

	/**
	 * Prida razeni
	 * @param type $column Sloupec v db
	 * @param type $dir Smer asc / desc
	 */
	public function addOrder($column, $dir = 'asc')
	{
		$order = new \stdClass();
		$order->column = $column;
		$order->direction = $dir;
		$this->orderby[] = $order;
		return $this;
	}


	/**
	 * Vlozi sloupec pomoci ktereho se bude groupovat
	 * @param string $column Sloupec
	 */
	public function addGroupBy($column)
	{
		$this->groupby[] = $column;
	}


	/**
	 * Vrati pole razeni
	 * @return array
	 */
	public function &getOrderBy()
	{
		return $this->orderby;
	}


	/**
	 * Vrati pole razeni
	 * @return array
	 */
	public function &getGroupBy()
	{
		return $this->groupby;
	}

	/**
	 * Vrati pole nastavenych filtru
	 * @return array
	 */
	public function &getFilters()
	{
		return $this->filters;
	}


	/**
	 * Vrati referenci na ArrayHash s QueryString parametry dotazu
	 * @return ArrayHash
	 */
	public function getQuery()
	{
		return $this->query;
	}

	/**
	 * Vrati referenci na ArrayHash s HTTP hlavickami
	 * @return ArrayHash
	 */
	public function getHttpHeaders()
	{
		return $this->httpHeaders;
	}




	// ArrayAccess na QueryString parametry

	public function offsetExists($offset)
	{
		return isset($this->query[$offset]);
	}

	public function offsetGet($offset)
	{
		return $this->query[$offset];
	}

	public function offsetSet($offset, $value)
	{
		$this->query[$offset] = $value;
	}

	public function offsetUnset($offset)
	{
		unset($this->query[$offset]);
	}



}