<?php
namespace Abra\Service;

use \Nette\Caching\IStorage,
	\Nette\Security\User,
	\Nette\Utils\ArrayHash,
	\Tracy\Debugger,
    \Nette\Utils\Json;

/**
 * Rozhrani pro komunikaci s REST API ABRA
 * @property-read string $rawUrl RAW URL posledniho pozadavku
 * @property-read string $rawRequest RAW Request text
 * @property-read string $rawRequestHeaders RAW Request hlavicky
 * @property-read string $rawResponse RAW Response text
 * @property-read string $rawResponseHeaders RAW Response hlavicky
 * @property-read string $entityCachePath Cesta k ulozene cachi entit
 * @property-read string[] $apiCredentials API credentials
 * @property string $token
 */
abstract class Service
{
    use \Nette\SmartObject;

	/**
	 * Vratit RAW data vracena ze sluzby
	 */
	const RETURN_RAW = 1;

	/**
	 * Vratit data naparsovana na defaultni PHP objekty a pole
	 */
	const RETURN_PARSED = 2;

	/**
	 * Vratit data naparsovana na business objekty
	 */
	const RETURN_OBJECTS = 3;


    /**
     * Pole udalosti pro logovani vseho
     * @var array
     */
    public $onLog;
    
	/**
	 * Pole udalosti volajicich se pred odeslanim pozadavku
	 * @var array
	 */
	public $onRequest;

	/**
	 * Pole udalosti volajicich se po obdrzeni odpovedi od sluzby
	 * @var array
	 */
	public $onResponse;

	/**
	 * Pole udalosti volajicich se pri chybe obdrzene ze sluzby
	 * @var array
	 */
	public $onResponseError;

	/**
	 * Pole udalosti volajicich se po obdrzeni (a vyzadani RAW odpovedi od sluzby)
	 * @var array
	 */
	public $onRawResponse;

	/**
	 * Pole callbacku volajicich se po nacteni a nacacheovani entity
	 * @var array
	 */
	public $onEntityCached;



	/**
	 * Pole konfigurace webove sluzby
	 * @var ArrayHash
	 */
	protected $config;


	/**
	 * ContentType, se kterym se posilaji dotazy
	 * @var string
	 */
	protected $contentType;


	/**
	 * URL s querystringem, na kterou se volal posledni pozadavek na sluzbu
	 * @var string
	 */
	protected $rawUrl;

	/**
	 * Data, ktera se poslala v poslednim pozadavku
	 * @var string
	 */
	protected $rawRequest;

	/**
	 * Pole hlavicek poslanych v poslednim requestu
	 * @var array
	 */
	protected $rawRequestHeaders;

	/**
	 * Data obdrzena v posledni odpovedi
	 * @var string
	 */
	protected $rawResponse;

	/**
	 * Pole hlavicek obdrzenych v posledni odpovedi
	 * @var array
	 */
	protected $rawResponseHeaders;


	/**
	 * Internal request/resonse counter
	 * @var type
	 */
	protected $counter;

	/**
	 * Token, ktery se posila v kazdem pozadavku pro zajisteni opravneni
	 * @var string
	 */
	protected $token;

	/**
	 * Nazev spojeni
	 * @var string
	 */
	protected $connectionName;



	/**
	 * Kam se budou ukladat cache business objektu
	 * @var string
	 */
	protected $entityCachePath;


	/**
	 * O co uz jsme se pokouseli nacist
	 * @avr array
	 */
	protected $tryDescribe;
    
    /**
     * Session Section
     * 
     * @var \Nette\Http\SessionSection
     */
    protected $ssUser;
    
    /**
     * API credentials
     * 
     * @var string
     */
    protected $apiCredentials = array(
        'user' => NULL,
        'password' => NULL
    );

    /**
     * Set API credentials
     * 
     * @param string $user
     * @param string $password
     * @return $this
     */
    public function setApiCredentials($user, $password)
    {
        $this->apiCredentials['user'] = $user;
        $this->apiCredentials['password'] = $password;
        return $this;
    }
    
    /**
     * Get API credentials
     * 
     * @return string[]
     */
    public function getApiCredentials()
    {
        return $this->apiCredentials;
    }

    /**
     * Is via API ?
     * 
     * @return boolean
     */
    public function isApi()
    {
        return $this->config['api'];
    }
        
	/**
	 * Konstruktor
	 * @param array $config Konfigurace
     * @param \Nette\Http\Session $session
	 */
	public function __construct(array $config, \Nette\Http\Session $session)
	{
		$this->config = ArrayHash::from($config);
		// defaultni konfigurace
		if(!isset($this->config['header_content_type'])) $this->config['header_content_type'] = TRUE;
		if(!isset($this->config['header_accept'])) $this->config['header_accept'] = TRUE;
		if(!isset($this->config['header_cache_control'])) $this->config['header_cache_control'] = TRUE;
		if(!isset($this->config['locale'])) $this->config['locale'] = new ArrayHash();
		if(!isset($this->config['locale']['datetime_format'])) $this->config['locale']['datetime_format'] = 'Y-m-d H:i:s';
		if(!isset($this->config['locale']['date_format'])) $this->config['locale']['date_format'] = 'Y-m-d';
		if(!isset($this->config['locale']['time_format'])) $this->config['locale']['time_format'] = 'H:i:s';

		// ------------------------
		$this->token = NULL;
		$this->connectionName = NULL;
		$this->counter = 0;
		$this->tryDescribe = array();
		// Inicializace debug panelu
		ServiceBarPanel::initialize($this);
        
		// Session section - USER
		$this->ssUser = $session->getSection('user');
        if(!empty($this->ssUser['loginUser'])) {
            $this->setApiCredentials($this->ssUser['loginUser'], $this->ssUser['loginPassword']);
        }
	}

	/**
	 * Vrati konfiguraci
	 * @return array
	 */
	public function getConfig()
	{
		return $this->config;
	}


	/**
	 * Posledni URL
	 * @return string
	 */
	public function getRawUrl()
	{
		return $this->rawUrl;
	}

	/**
	 * Posledni odeslany pozadavek
	 * @return string
	 */
	public function getRawRequest()
	{
		return $this->rawRequest;
	}

	/**
	 * Hlavicky posledniho odeslaneho pozadavku
	 * @return string
	 */
	public function getRawRequestHeaders()
	{
		return $this->rawRequestHeaders;
	}

	/**
	 * Posledni obdrzena odpoved
	 * @return string
	 */
	public function getRawResponse()
	{
		return $this->rawResponse;
	}

	/**
	 * Hlavicky posledniho odeslaneho pozadavku
	 * @return string
	 */
	public function getRawResponseHeaders()
	{
		return $this->rawResponseHeaders;
	}



	/**
	 * Vrati nastaveny token pro identifikaci relace uzivatele
	 * @return string
	 */
	public function getToken()
	{
		return $this->token;
	}

	/**
	 * Nastavi token identifikujici relaci uzivatele
	 * @param string $token Token
	 * @return Service Fluent rozhrani
	 */
	public function setToken($token)
	{
		$this->token = $token;
		return $this;
	}





	/**
	 * Vrati nastaveny token pro identifikaci relace uzivatele
	 * @return string
	 */
	public function getConnectionName()
	{
		return $this->connectionName;
	}

	/**
	 * Nastavi connection name
	 * @param string $connectioName Nazev spojeni
	 * @return Service Fluent rozhrani
	 */
	public function setConnectionName($connectioName)
	{
		$this->connectionName = $connectioName;
		return $this;
	}


	/**
	 * Nastavi cestu kam se ukadaji entity (do podadresaru dle namespace / spojeni_)
	 * @param string $path Cesta kde je cache entit
	 * @return Service
	 */
	public function setEntityCachePath($path)
	{
		$this->entityCachePath = $path;
		return $this;
	}

	/**
	 * Vrati cestu kam se ukladaji entity (do podadresaru dle namespace / spojeni_)
	 * @return string
	 */
	public function getEntityCachePath()
	{
		return $this->entityCachePath;
	}



	/**
	 * Vrati nastaveny nazev webove sluzby
	 * @return string
	 */
	public function getWebServiceName()
	{
		return isset($this->config['web_service_name']) ? $this->config['web_service_name'] : NULL;
	}

	/**
	 * Nastavi nazev webove sluzby
	 * @param string $wsName Nazev webove sluzby
	 * @return Service Fluent rozhrani
	 */
	public function setWebServiceName($wsName)
	{
		$this->config['web_service_name'] = $wsName;
		return $this;
	}


	/**
	 * Vytvori response a vrati ji
	 * @param type $data
	 * @param type $headers
	 * @throws \Abra\Service\ResponseException Pri chybe v odpovedi se zavola callback a vyhodi vyjimka
	 * @return \Abra\Service\Response
	 */
	protected function createResponse($data, $headers, $url, $method, $rawRequest, $rawResponse, $requestHeaders = NULL)
	{
		try
		{
			return new Response($data, $headers);
		}
		catch(\Abra\Service\ResponseException $e)
		{
			$this->onResponseError($this, $e, $url, $method, $rawRequest, $rawResponse, $requestHeaders, $headers);
			throw $e;
		}
	}


	/**
	 * Posle pozadavek na sluzbu a vrati odpoved
	 * @param string $url URL /entita/akce[/id]
	 * @param string $method HTTP method ('get'/'post')
	 * @param ArrayHash $query Pole kic hodnota - preda se jako querystring k doatzu
	 * @param string|NULL $content RAW Content nebo NULL, pokud jde o pozadavek nevyzadujici telo (GET)
	 * @param ArrayHash Pole hlavicek, ktere se v pozadavku poslou (krome vychozich)
	 * @return string RAW Response
	 * @throws ServiceException
	 */
	protected function sendRequest($url, $method, $query = NULL, $content = NULL, $headers = NULL)
	{
//        dump($url, $type, $query, $content, $headers); die;
        $this->rawRequest = $this->rawResponse = $this->rawRequestHeaders = $this->rawResponseHeaders = NULL;
        $type = strtolower($method);
        // zvysime pocet provedenych dotazu
        $this->counter++;
        $this->rawRequest = $content;
        
        // inicializace CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->config['timeout']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);	// HTTP 1.0 to prevent chunked encoding
        curl_setopt($ch, CURLOPT_PORT, $this->config['port']);
        
        $respHeaders = array();
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, function($curl, $headerLine) use(&$respHeaders) {
            $lineLength = strlen($headerLine);
            if(trim($headerLine) != '') {
                $respHeaders[] = rtrim($headerLine);
            }
            return $lineLength;
        });

        // hlavicky pozadavku
        $reqHeaders = array();
        if($this->config['header_accept']) {
            $reqHeaders['Accept'] = $this->contentType;
        }
        if($this->config['header_cache_control']) {
            $reqHeaders['Cache-Control'] = "no-cache";
        }        

        // Pred konverzi do API callu
        $realRawRequest = NULL;
        
        if($this->config['api'])  {
            $realRawRequest = $this->rawRequest;
            
            $urlParts = explode('/', ltrim($url, '/'));
            
            $reqHeaders['Accept'] = 'application/json';
            
            // API credentials
            $credentials =  $this->apiCredentials['user'].':'.$this->apiCredentials['password'];
            
            $reqHeaders['Authorization'] = 'Basic '.base64_encode($credentials);
                        
            // RAW request = JSON
            $this->rawRequest = Json::encode(array(
                "expr" => "NxScript(".
                        "'eu.abra.tour.APIWebServices.WebLib.Api".ucfirst($type)."',".
                        "'".(isset($urlParts[0]) ? $urlParts[0] : '')."',".
                        "'".(isset($urlParts[1]) ? $urlParts[1] : '')."',".
                        "'".(isset($urlParts[2]) ? $urlParts[2] : '')."',".
                        "'".(isset($urlParts[3]) ? $urlParts[3] : '')."',".
                        "'".$this->apiEscape($this->rawRequest)."'".
                    ")"
            ));
            
            // raw URL
            $apiUrl = $this->config['protocol'].'://'.$this->config['host'].
                        (!empty($this->config['port']) ? ':'.$this->config['port'] : '').
                        $this->config['path'].
                    (!empty($this->connectionName) ? '/'.$this->connectionName : '').'/qrexpr?app=approving';
            $this->rawUrl = $apiUrl;
                        
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            
            // hlavicky pozadavku
            if($this->config['header_content_type']) {
                $reqHeaders["Content-Type"] = "application/json; charset=".strtolower($this->config['encoding']);
            }
            $reqHeaders["Content-Length"] = strlen($this->rawRequest); // mb_strlen - musi byt bytovy pocet

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->rawRequest);            

        }
        else {

            // obohatime cestu o URL prefix
            $url = $this->config['protocol'].'://'.$this->config['host'].
                        (!empty($this->config['port']) ? ':'.$this->config['port'] : '').
                        $this->config['path'].
                    (!empty($this->connectionName) ? '/'.$this->connectionName : '').
                    (!empty($this->config['web_service_name']) ? '/'.$this->config['web_service_name'] : '').
                    $url;
            // pokud mame nastaveny token, obohatime QueryString o token
            if($query === NULL) {
                $query = new ArrayHash();
            }
            if($query->count() > 0) {
                $query = http_build_query($query);
                $url .= (strpos($url, '?') === false ? '?' : '&').$query;
            }
            
            curl_setopt($ch, CURLOPT_URL, $url);
            
            // raw URL
            $this->rawUrl = $url;

            // PODLE TYPU
            switch($type)
            {
                // GET request
                case 'get':
                    break;

                // POST request
                case 'post':
                    // hlavicky pozadavku
                    if($this->config['header_content_type']) {
                        $reqHeaders["Content-Type"] = "{$this->contentType}; charset=".strtolower($this->config['encoding']);
                    }
                    $reqHeaders["Content-length"] = strlen($this->rawRequest); // mb_strlen - musi byt bytovy pocet
                        // inicializace CURL
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $this->rawRequest);
                    break;
            }
        }

        // spojime s predaymi hlavickami
        if(!empty($headers)) { 
            foreach($headers as $header => $val) { 
                $reqHeaders[$header] = $val; 
            }     
        }
        
        $this->rawRequestHeaders = array();
        foreach($reqHeaders as $header => $val)	{
            $this->rawRequestHeaders[] = $header.': '.$val;
        }
        
        // pridame halvicky
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->rawRequestHeaders);

        // udalost pred odeslanim pozadavku
        $this->onRequest($this, $type, $this->counter, $this->rawUrl, $this->rawRequestHeaders, $this->rawRequest, $realRawRequest);
		$beforeRequestSend = microtime(true);
        $this->rawResponse = curl_exec($ch);
		$afterRequestSent = microtime(true);
		$responseTime = $afterRequestSent-$beforeRequestSend; //pro tracy bar
        $this->rawResponseHeaders = $respHeaders;
        $curlHeaders = curl_getinfo($ch);
        
//        file_put_contents(LOG_DIR.'/sniff/'.\Nette\Utils\Strings::webalize($url, '.-').'.log', var_export($this->rawRequestHeaders, TRUE)."\n\n".$this->rawRequest."\n\n\n\n".var_export($this->rawResponseHeaders, TRUE)."\n\n".$this->rawResponse);
        
        // Logovani vseho
        $this->onLog($this->counter, $this->rawUrl, $this->rawRequestHeaders, $this->rawRequest, $this->rawResponseHeaders, $this->rawResponse);
        
        if($this->config['api']) {
            // TODO: headers handling
            if($curlHeaders['http_code'] >= 500) {
                \Tracy\Debugger::log(new ServiceException('Prosím, kontaktujte svého konzultanta, ABRA API neběží (chyba '.$curlHeaders['http_code'].').'));
                throw new ServiceException('Zásadní chyba komunikace, zkuste opakovat a případně informujte správce o postupu vyvolání chyby. ');
            }
        }
        else {
            if($curlHeaders['http_code'] >= 500) {
                $parts = explode('Message:', $this->rawResponse);
                $message = isset($parts[1]) ? $parts[1] : 'ABRA Web service response with header 500 and uknow message';
                $message = iconv("windows-1250", "utf-8", strip_tags($message));
                throw new ServiceException($message. '. Prosím, kontaktujte svého konzultanta, webové služby ABRA nejsou funkční (chyba '.$curlHeaders['http_code'].').');
            }
        }

        // dostali jsme odpoved ?
        if(curl_errno($ch)) {
            \Tracy\Debugger::log(new ServiceException('Cannot connect to service - URL '.$url.' ('.curl_error($ch).')'));
            throw new ServiceException('Zásadní chyba komunikace, zkuste opakovat a případně informujte správce o postupu vyvolání chyby. ');
        }
        
        // ukoncime spojeni
        curl_close($ch);

        $realRawResponse = NULL;
        // Raw response v API = v JSON property result
        if($this->config['api'] && trim($this->rawResponse) !== '') {
            $realRawResponse = $this->rawResponse;
            try {
                $decodedJson = Json::decode($this->rawResponse);
            }
            catch(\Nette\Utils\JsonException $jsonEx) {
                \Tracy\Debugger::log(new ServiceException('Unable to parse response: '.$this->rawResponse, NULL, NULL, $jsonEx));
                throw new ServiceException('Zásadní chyba komunikace, zkuste opakovat a případně informujte správce o postupu vyvolání chyby. ');
            }
            if(!(isset($decodedJson->result) || (isset($decodedJson->title) && isset($decodedJson->description)))) {
                \Tracy\Debugger::log(new ServiceException('Unable to parse response: '.$this->rawResponse));
                throw new ServiceException('Zásadní chyba komunikace, zkuste opakovat a případně informujte správce o postupu vyvolání chyby. ');
            }
            if(isset($decodedJson->title) && isset($decodedJson->description)) {
                throw new ServiceException($decodedJson->title.': '.$decodedJson->description);
            }
            $this->rawResponse = $decodedJson->result;
        }
        
        // udalost po obdrzeni odpovedi
        $this->onResponse($this, $type, $this->counter, $this->rawResponseHeaders, $this->rawResponse, $realRawResponse, $responseTime);
        // vratime odpoved
        return $this->rawResponse;
	}

    
    /**
     * Escape text
     * 
     * @param string $text
     * @return string
     */
    protected function apiEscape($text)
    {
        return str_replace(
                array(
                    "'", 
                    "&amp;"
                ), 
                array(
                    "'+NxChr(39)+'",
                    "'+NxChr(38)+'amp;"
                ),
                $text
            );
    }
    
    
	/**
	 * Metoda upravi vracena data tak, ze misto pole entit nastavi na data->ENTITA pouze prvni entitu
	 * @param stdClass $parsed Ziskany naparsovany objekt z webove sluzby
	 * @return void
	 */
	protected function filterFirstOnly($parsed)
	{
		// je tam vubec cast "data" ?
		if(!isset($parsed->data)) return;
		// pokud ano ... dej ho misto pole
		if(is_array($parsed->data) && count($parsed->data))
		{
			$parsed->data = reset($parsed->data);
		}
	}


	/**
	 * Vytvori objekty, pripadne popis metadat a vrati odpoved
	 * @param \stdClass $res Odpoved, bud pole nebo stdClass
	 * @param Request Pozadavek
	 * @param bool $firstOnly Zda vratit pouze first only zaznam
	 */
	protected function parseAndCreate($res, $request, $firstOnly)
	{
		// vytvorime ABRA objekty
		if(!empty($res->data))
		{
			if(is_object($res->data))
			{
				$entityClass = key($res->data);
				$namespace = $this->connectionName;
				$this->cacheEntities($namespace, $entityClass, FALSE);
				try
				{
					$res->data = Entity::create($res->data, $entityClass, $this->connectionName);
				}
				catch(\Exception $e)
				{
					// jakou tridu se nepodarilo najit ?
					$missingClass = $e->getEntityClass();
					if(!isset($this->tryDescribe[$missingClass]))
					{
						$this->tryDescribe[$missingClass] = true;
						$this->cacheEntities($namespace, $missingClass, TRUE);
						return $this->parseAndCreate($res, $request, $firstOnly);
					}
					throw $e;
				}
			}
			elseif(trim($res->data) == '')
			{
				$res->data = NULL;
			}
		}
		// pouze prvni ?
		if($firstOnly)
		{
			$this->filterFirstOnly($res);
		}
		// vratime
		return $res;
	}


	/**
	 * Rekurzivne cachuje entity od predane entity. Uklada je na disk do cache
	 * @param string $namespace Jmenny prostor
	 * @param string $class Trida
	 * @param $deep Zda pri existenci prvni tridy bude testovat rekurzivne nebo ne
	 */
	public function cacheEntities($namespace, $class, $deep = FALSE)
	{
		$nsClass = $namespace.'\\'.$class;
		$exists = class_exists($nsClass);
		if(($exists && !$deep) || isset($this->tryDescribe[$nsClass]))
		{
			return;
		}
		$this->tryDescribe[$nsClass] = TRUE;
		if($exists)
		{
			$description = Entity::describeEntity($nsClass);
		}
		else
		{
			try
			{
				$description = $this->describeClass(new Request($class, 'object'))->data;
				$this->saveEntityCache($description);
			}
			catch(\Abra\Service\ServiceException $e)
			{
				// ulozime fake tridu, ABRA ji odmaze, az nastane cas
				$cd = new ClassDescription($class);
				$this->saveEntityCache($cd);
				return;
			}
		}
		// projdeme vsechny properties objektu a podivame se na referencni objekty
		if(!empty($description->fields))
		{
			foreach($description->fields as $field)
			{
				if(!empty($field->list) && !class_exists($namespace.'\\'.$field->list))
				{
					$this->cacheEntities($namespace, $field->list, $deep);
				}
				if(!empty($field->ref) && !class_exists($namespace.'\\'.$field->ref))
				{
					$this->cacheEntities($namespace, $field->ref, $deep);
				}
			}
		}
	}


	// ------------------------------------------ CACHE BUSINESS OBJEKTU ----------------------------------------


	/**
	 * Metoda promaze cache a vrati pole smazanych soboru
	 * Bud promaze celou cache, nebo cache pro celou connection, nebo cache pro connection a tridu.
	 * @param string|NULL $connection Spojeni (namespace)
	 * @param string|NULL $class Trida
	 * @throws ServiceException Vyjimka
	 * @return array Ktere soubory se odmazaly, pole cest k nim
	 */
	public function clearEntityCache($connection = NULL, $class = NULL)
	{
		$removed = array();
		$dir = opendir($this->entityCachePath);
		if(!$dir) throw new ServiceException('Cannot access cache directory: "'.$this->entityCachePath.'"');
		while(($d = readdir($dir)) !== FALSE)
		{
			$namespaceDir = $this->entityCachePath.'/'.$d;
			if($d == '.' || $d == '..') continue;
			if($connection === NULL || $connection == $d)
			{
				if(is_dir($namespaceDir))
				{
					$subdir = opendir($namespaceDir);
					if(!$subdir) throw new ServiceException('Cannot access cache namespace directory: "'.$this->entityCachePath.'/'.$d.'"');
					while(($f = readdir($subdir)) !== FALSE)
					{
						$file = $namespaceDir.'/'.$f;
						if($f == '.' || $f == '..' || !is_file($file)) continue;
						if($class === NULL || strtolower($class).'.php' == strtolower($f))
						{
							$removed[] = $file;
							unlink($file);
						}
					}
					closedir($subdir);
					if($class === NULL)
					{
						rmdir($namespaceDir);
					}
				}
			}
		}
		closedir($dir);
		return $removed;
	}


	/**
	 * Ulozi PHP objekt s namespacem spojeni do adresare entityCachePath
	 * @param ClassDescription $class Metadata tridy, ktera se ulozi
	 */
	public function saveEntityCache(ClassDescription $class)
	{
		$namespace = $this->connectionName;
		if(!is_dir($this->entityCachePath))
		{
			if(!mkdir($this->entityCachePath)) throw new \Exception('Nelze vytvořit adresář pro uložení cache entit.');
		}
		$dir = $this->entityCachePath.'/'.$namespace;
		if(!is_dir($dir))
		{
			if(!mkdir($dir)) throw new \Exception('Nelze vytvořit adresář pro uložení cache entit.');
		}
		$className = $class->getClassName();
		$fileName = $dir.'/'.$className.'.php';
		$fh = fopen($fileName, 'w');
		fwrite($fh, "<?php\n");
		fwrite($fh, "namespace {$namespace};\n\n");
		fwrite($fh, "/** Entity '{$className}'. File was generated automatically at ".date('Y-m-d H:i:s').". Do not modify! */\n");
		fwrite($fh, "class {$className} extends \Abra\Service\Entity\n");
		fwrite($fh, "{\n");
		foreach($class->getFields() as $f)
		{
			/* @var $f ClassField */
            $size = str_replace('.', ',', $f->size);
			fwrite($fh, "\t/**\n");
			fwrite($fh, "\t * @type {$f->type}\n");
			fwrite($fh, "\t * @length {$size}\n");
            if($f->encoding !== NULL) {
                fwrite($fh, "\t * @encoding {$f->encoding}\n");
            }
			if($f->id !== NULL)
			{
				fwrite($fh, "\t * @id ".($f->id ? 'true' : 'false')."\n");
			}
			if($f->ref !== NULL)
			{
				fwrite($fh, "\t * @ref {$f->ref}\n");
			}
			if($f->list !== NULL)
			{
				fwrite($fh, "\t * @list {$f->list}\n");
			}
			fwrite($fh, "\t */\n");
			fwrite($fh, "\tprotected \${$f->name};\n\n");
		}
		fwrite($fh, "}\n");
		fclose($fh);
		// udalost po nacachovani
		$this->onEntityCached($this, $class, $namespace, $fileName);
	}


	/**
	 * Posle dotaz na popis tridy
	 * @param Request $request Pozadavek
	 * @return Response
	 */
	public function describeClass(Request $request)
	{
		$request->setType(Request::TYPE_DESCRIPTION);
		$response = $this->post($request, FALSE, self::RETURN_PARSED);
		if(!isset($response->data->object) || (is_array($response->data) && count($response->data) <= 0))
		{
			throw new ServiceException('Nepodarilo se vratit describe objektu');
		}
		// describe objektu
		$obj = reset($response->data->object);
		$cls = new ClassDescription($obj->name);
		if(isset($obj->fields->field))
		{
			foreach($obj->fields->field as $field)
			{
				$cfld = new ClassField();
				$cfld->id = isset($field->id) ? (strtolower($field->id) == 'a' ? TRUE : FALSE) : NULL;
				$cfld->name = isset($field->name) ? $field->name : NULL;
				$cfld->type = isset($field->type) ? $field->type : NULL;
				$cfld->encoding = isset($field->encoding) ? $field->encoding : NULL;
				$cfld->size = isset($field->size) ? $field->size : NULL;
				$cfld->ref = isset($field->ref) ? $field->ref : NULL;
				$cfld->list = isset($field->list) ? $field->list : NULL;
				$cls->addField($cfld);
			}
		}
		$response->data = $cls;
		return $response;
	}

	/**
	 * Posle pozadavek na describe listu
	 * @param Request $request Pozadavek
	 * @return Response
	 */
	public function describeList(Request $request)
	{
		$request->setType(Request::TYPE_DESCRIPTION);
		$response = $this->post($request, FALSE, self::RETURN_PARSED);
        if(empty($response->data->fields) || (is_array($response->data) && count($response->data) <= 0))
		{
			throw new ServiceException('Nepodarilo se vratit describe seznamu');
		}
		$obj = reset($response->data->fields);
		if(!isset($obj->field)) throw new ServiceException('Nepodarilo se vratit describe seznamu');
		$response->data = new ListDescription($obj);
		return $response;
	}

	/**
	 * Posle dotaz na popis formulare
	 * @param Request $request Pozadavek
	 * @return Response
	 */
	public function describeForm(Request $request)
	{
		$request->setType(Request::TYPE_DESCRIPTION);
		$response = $this->post($request, FALSE, self::RETURN_PARSED);
		if(is_array($response->data) && count($response->data) <= 0) {
            throw new ServiceException('Nepodarilo se vratit describe formulare');
        }
		if(!isset($response->data->form) || (is_array($response->data) && count($response->data->form) <= 0))
		{
			throw new ServiceException('Nepodarilo se vratit describe formulare');
		}
		$form = reset($response->data->form);
		$fd = new FormDescription($form);
		$response->data = $fd;
		return $response;
	}

	/**
	 * Posle dotaz na popis razeni
	 * @param Request $request Pozadavek
	 * @return Response
	 */
	public function describeOrder(Request $request)
	{
		$request->setType(Request::TYPE_DESCRIPTION);
		$response = $this->post($request, FALSE, self::RETURN_PARSED);
		if(is_array($response->data) && count($response->data) <= 0) 
        {
            throw new ServiceException('Nepodarilo se vratit order describe');
        }
		if(!isset($response->data->orders) || (is_array($response->data) && count($response->data->orders) <= 0))
		{
			throw new ServiceException('Nepodarilo se vratit order describe');
		}
		$orders = reset($response->data->orders);
		$od = new OrderDescription($orders);
		$response->data = $od;
		return $response;
	}


	/**
	 * Posle POST dotaz na RAW data
	 * @param Request $request
	 * @return mixed odpoved s RAW daty
	 */
	public function rawData(Request $request)
	{
		$request->setType(Request::TYPE_RAW);
		$response = $this->post($request, FALSE, self::RETURN_RAW);
		return $response;
	}

	/**
	 * Vrati raw data, ktera byla vracena v Delphi BLOB containeru
	 * @param Request $request Pozadavek
	 * @return RawDataContainer
	 */
	public function rawDataContainer(Request $request)
	{
		$response = $this->rawData($request);
		return new RawDataContainer($response);
	}


    

	// ------------------------------------------------------- ABSTRACT METHODS





	/**
	 * Posle POST pozadavek na sluzbu
	 * @param Request $request Pozadavek
	 * @param bool $firstOnly Zda vratit prvni zaznam misto pole zaznamu
	 * @param bool $returnType Zda vratit RAW response (TRUE), nebo zda ji parsovat (FALSE)
	 * @return Response Odpoved od REST API
	 * @throws ServiceException|ResponseException
	 */
	abstract public function post(Request $request, $firstOnly = FALSE, $returnType = self::RETURN_OBJECTS);

	/**
	 * Posle GET pozadavek na sluzbu
	 * @param ServiceRequest $request Pozadavek
	 * @param bool $firstOnly Zda vratit prvni zaznam misto pole zaznamu
	 * @param bool $returnRaw Zda vratit RAW response (TRUE), nebo zda ji parsovat (FALSE)
	 * @return Response Odpoved od REST API
	 * @throws ServiceException|ResponseException
	 */
	abstract public function get(Request $request, $firstOnly = FALSE, $returnType = self::RETURN_OBJECTS);






}
