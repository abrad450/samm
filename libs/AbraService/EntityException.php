<?php
namespace Abra\Service;

/**
 * Vyjimka z Entity
 */
class EntityException extends \Exception
{
	
	/**
	 * Trida, kterou se nepodarilo inicializovat
	 * @var string
	 */
	protected $entityClass;
	
	/**
	 * konstruktor vyjimky
	 * @param string $message Zprava
	 * @param int $code Kod chyby
	 * @param string $entityClass Trida, ktere se vyjimka tyka
	 * @param \Exception $previous Predchozi vyjimka
	 */
	public function __construct($message = NULL, $code = NULL, $entityClass = NULL, $previous = NULL)
	{
		parent::__construct($message, $code, $previous);
		$this->entityClass = $entityClass;
	}
	
	
	/**
	 * Vrati tridu entity
	 * @return string
	 */
	public function getEntityClass()
	{
		return $this->entityClass;
	}
}