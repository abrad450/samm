<?php
namespace Abra\Service;

/**
 * AUTH sekce
 * 
 * @property string $token
 * @property \Nette\Utils\DateTime $datetime
 * @property string $version
 */
class Auth
{
    use \Nette\SmartObject;

	/**
	 * TOKEN prihalseneho uzivatele - ID relace
	 * @var string
	 */
	protected $token;
	
	/**
	 * Datum a cas prihalseni
	 * @var \Nette\Utils\DateTime
	 */
	protected $datetime;

	/**
	 * Version
	 * @var string
	 */
	protected $version;
		
	
	/**
	 * Konstruktor
	 * @param stdClass $stdClass Data ze sekce Auth 
	 */
	public function __construct($stdClass)
	{
		if(isset($stdClass->token))
		{
			$this->setToken($stdClass->token);
		}
		if(isset($stdClass->datetime))
		{
			$this->setDatetime($stdClass->datetime);
		}
		if(isset($stdClass->version))
		{
			$this->setVersion($stdClass->version);
		}

	}
	
	/**
	 * Vrati token
	 * @return string
	 */
	public function getToken()
	{
		return $this->token;
	}
	
	/**
	 * @param string $token Token
	 * @return AuthSection Fluent rozhrani
	 */
	public function setToken($token)
	{
		$this->token = $token;
		return $this;
	}
	
	
	/**
	 * Vrati datum a cas
	 * @return \Nette\Utils\DateTime
	 */
	public function getDatetime()
	{
		return $this->datetime;
	}
	
	/**
	 * Nastavi datetime
	 * @param string|\Nette\Utils\DateTime $token 
	 * @return AuthSection Fluent rozhrani
	 */
	public function setDatetime($datetime)
	{
		if(!($datetime instanceof \Nette\Utils\DateTime))
		{
			$datetime = \Nette\Utils\DateTime::createFromFormat('Y-m-d H:i:s', $datetime);
		}
		$this->datetime = $datetime;
	}

	/**
	 * Vrati version
	 * @return string
	 */
	public function getVersion()
	{
		return $this->version;
	}
	
	/**
	 * @param string $version version
	 * @return AuthSection Fluent rozhrani
	 */
	public function setVersion($version)
	{
		$this->version = $version;
		return $this;
	}
	
}
