<?php
namespace Abra\Service;

/**
 * Container pro RAW data
 * 
 * @property-write mixed $raw RAW stream
 * @property-read string $type Delphi typ souboru
 * @property-read string $mime MIME typ soboru
 * @property-read mixed $data DATA souboru
 */
class RawDataContainer
{
    use \Nette\SmartObject;
	
	/**
	 * Datovy typ Delphi
	 * @var string
	 */
	protected $type;
	
	/**
	 * MIME typ obsahu
	 * @var string
	 */
	protected $mime;
	
	/**
	 * Binarni data souboru zbavena obalky
	 * @var mixed
	 */
	protected $data;	
	

	
	/**
	 * Prevodni tabulka delphi typu na MIME typy
	 * @var array
	 */
	public static $delphiMime = array(
		// OBRAZKY
        'TNxGDIPPicture' => 'image/jpeg',
		'TJPEGImage' => 'image/jpeg',
		'TBitmap' => 'image/bmp',
		'TGIFImage' => 'image/gif',
		'TPNGImage' => 'image/png',
		'TIcon' => 'image/x-icon',
		'TMetafile' => 'windows/metafile',
		// OSTATNI SOUBORY - TODO
	);
		
	
	
	/**
	 * Vytvori RAW data container z predanych obalenych RAW dat
	 * @property mixed $raw Binarni data
	 */
	public function __construct($raw)
	{
		$this->setRaw($raw);
	}
	
	/**
	 * Vrati Binarni data souboru
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}
	
	/**
	 * Vrati detekovany MIME typ souboru
	 * @return string
	 */
	public function getMime()
	{
		return $this->mime;
	}
	
	/**
	 * Vrati detekovany Delphi typ soboru
	 * @return string 
	 */
	public function getType()
	{
		return $this->type;
	}
	
	/**
	 * Nastavi RAW data
	 * @param mixed $raw RAW data
	 * @return RawDataContainer
	 */
	public function setRaw($raw)
	{
		$unpacked = unpack("H*", $raw);
		$unpacked = $unpacked[1];
		$four = substr($unpacked, 0, 8);
		$headerLength = hexdec($four[0].$four[1]);
		$type = substr(substr($unpacked, 0, 8 + $headerLength*2), 8);
		$this->type = pack('H*', $type);
		if(isset(self::$delphiMime[$this->type])) $this->mime = self::$delphiMime[$this->type];
		$this->data = pack('H*', substr($unpacked, 8 + $headerLength*2));
		return $this;
	}
	
}