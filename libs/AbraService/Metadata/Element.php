<?php
namespace Abra\Service;

/**
 * Element
 * 
 * @property string $name
 */
class Element
{
    use \Nette\SmartObject;

	/**
	 * Nazev elementu
	 * @var string
	 */
	protected $name;
	
	
	/**
	 * Vrati nazev elementu
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	
	/**
	 * Nastavi nazev elementu
	 * @param string $name nazev elementu
	 * @return Element
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}
	
}