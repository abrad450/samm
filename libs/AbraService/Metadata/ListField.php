<?php
namespace Abra\Service;

/**
 * Polozka tabulky (seznamu)
 *
 * @property FieldStyle $style Styl pole
 * @property bool $filter Fitrovat podle toho policka ?
 * @property string $sortfield Sort field
 * @property string $type Field type
 */
class ListField extends BaseField
{

	/**
	 * Filtrovat ?
	 * @var bool
	 */
	protected $filter;

	/**
	 * Styl
	 * @var FieldStyle
	 */
	protected $style;

	/**
	 * Sort field
	 * @var string
	 */
	protected $sortfield;
    
	/**
	 * Type
	 * @var string
	 */
	protected $type;


	/**
	 * Konstruktor
	 * @param mixed $data Data
	 */
	public function __construct($data)
	{
		parent::__construct($data);
		$this->filter = isset($data->filter) ? ($data->filter == 'A' ? TRUE : FALSE) : FALSE;
        // Hlavicku floatu zarovna doprava
		if(isset($data->type) && $data->type == "dtFloat")
        {
            if(isset($data->style)) {
                $data->style->{'text-align'} = "right";
            } else {
                $data->style = new \stdClass();
                $data->style->{'text-align'} = "right";
            }
        }
		if(isset($data->style))
		{
			$fs = new FieldStyle($data->style);
			$this->style = $fs;
		}
		if(!empty($data->sortfield))
		{
			$this->sortfield = $data->sortfield;
		}
		if(!empty($data->type))
		{
			$this->type = $data->type;
		}
	}



	/**
	 * Nastavi nazev fieldu
	 * @param string $name
	 * @return ListField Fluent interface
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * Vrati nazev fieldu
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}



	/**
	 * Nastavi popisek fieldu
	 * @param string $caption
	 * @return ListField Fluent interface
	 */
	public function setCaption($caption)
	{
		$this->caption = $caption;
		return $this;
	}

	/**
	 * Vrati popisek fieldu
	 * @return string
	 */
	public function getCaption()
	{
		return $this->caption;
	}



	/**
	 * Nastavi zda se filtruje podle tohoto pole
	 * @param bool $filter
	 * @return ListField Fluent interface
	 */
	public function setFilter($filter)
	{
		$this->filter = $filter;
		return $this;
	}

	/**
	 * Vrati zda je toto pole filtrovaci ve vypisu
	 * @return bool
	 */
	public function isFiler()
	{
		return $this->name;
	}


	/**
	 * Nastavi styl fieldu
	 * @param FieldStyle $style
	 * @return ListField Fluent interface
	 */
	public function setStyle($style)
	{
		$this->style = $style;
		return $this;
	}

	/**
	 * Vrati styl fieldu
	 * @return FieldStyle
	 */
	public function getStyle()
	{
		return $this->style;
	}


	/**
	 * Nastavi sortfield
	 * @param string $sortfield
	 * @return self Fluent interface
	 */
	public function setSortfield($sortfield)
	{
		$this->sortfield = $sortfield;
		return $this;
	}

	/**
	 * Vrati sortfield
	 * @return string
	 */
	public function getSortfield()
	{
		return $this->sortfield;
	}
    

	/**
	 * Nastavi type
	 * @param string $type
	 * @return self Fluent interface
	 */
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}

	/**
	 * Vrati type
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}
    
    
    /**
     * Is this field RIGHT ALIGNED ?
     * 
     * @return bool
     */
    public function isRightAligned()
    {
        return $this->type === 'dtFloat';
    }

}