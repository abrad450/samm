<?php
namespace Abra\Service;

/**
 * Popis tridy, metadata
 * 
 * @property string $className Nazev tridy
 * @property \Nette\Utils\ArrayHash $fields Properties tridy
 */
class ClassDescription
{
    use \Nette\SmartObject;
	
	/**
	 * Nazev tridy
	 * @var string
	 */
	protected $className;
	
	
	/** 
	 * Kolekce properties tridy
	 * @var \Nette\Utils\ArrayHash
	 */
	protected $fields;
	
	
	/**
	 * Vytvori popisny objekt pro tridu $className
	 * @param string $className nazev tridy
	 */
	public function __construct($className)
	{
		$this->className = $className;
		$this->fields = new \Nette\Utils\ArrayHash();
	}
	
	/**
	 * Nastavi nazev tridy
	 * @param string $className Nazev tridy
	 * @return ClassDescription Fluent interface
	 */
	public function setClassName($className)
	{
		$this->className = $className;
		return $this;
	}
	
	/**
	 * Vrati nazev tridy
	 * @return string
	 */
	public function getClassName()
	{
		return $this->className;
	}
	
	
	
	/**
	 * Vrati kolekci polozek typu ClassField
	 * @return \Nette\Utils\ArrayHash
	 */
	public function getFields()
	{
		return $this->fields;
	}
	
	
	
	/**
	 * Prida field do popisu tridy
	 * @param ClassField $field Field
	 * @return \Abra\Service\ClassDescription 
	 */
	public function addField(ClassField $field)
	{
		$this->fields[$field->name] = $field;
		return $this;
	}
	
}