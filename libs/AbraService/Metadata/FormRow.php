<?php
namespace Abra\Service;

/**
 * Radek formulare
 * 
 * @property-read array $columns
 */
class FormRow
{
    use \Nette\SmartObject;
	
	/**
	 * Kolekce sloupcu formulare
	 * @var \Nette\Utils\ArrayList
	 */
	protected $columns;
	
	
	/**
	 * Konstruktor 
	 * 
	 * @param mixed $data Data, ze kterych se radek naplni
	 */
	public function __construct($data)
	{
		$this->columns = new \Nette\Utils\ArrayList();
		if(isset($data->columns->column))
		{
			if(!is_array($data->columns->column)) $data->columns->column = array($data->columns->column);
			foreach($data->columns->column as $formCol)
			{
				$fc = new FormColumn($formCol);
				$this->addColumn($fc);
			}
		}		
	}
	
	
	/**
	 * Vrati kolekci sloupcu formulare
	 * @return \Nette\Utils\ArrayList
	 */
	public function getColumns()
	{
		return $this->columns;
	}
	
	/**
	 * Prida sloupec do formulare
	 * @param FormColumn $column Sloupec
	 * @return FormRow Fluent interface
	 */
	public function addColumn(FormColumn $column)
	{
		$this->columns[] = $column;
		return $this;
	}
}
