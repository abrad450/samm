<?php
namespace Abra\Service;

/**
 * Styl policka
 * 
 * @property int $width Sirka pole
 * @property int $height Vyska pole
 */
class FieldStyle
{
    use \Nette\SmartObject;
	
	/**
	 * Sirka
	 * @var int
	 */
	protected $width;
	
	/**
	 * Vyska policka
	 * @var int
	 */
	protected $height;	
	
    /**
     * Visible
     * @var string
     */
    protected $visible;

    /**
     * Barva pozadi policka
     * @var string
     */
    protected $background;

    /**
     * Barva ramecku policka
     * @var string
     */
    protected $border;

    /**
     * Zarovnání textu
     * @var string
     */
    protected $textAlign;
    
    /**
     * Other CSS array
     * @var array
     */
    protected $css;
    
	/**
	 * Konstruktor pro styl
	 * @param mixed $data 
	 */
	public function __construct($data)
	{
		if(isset($data->width)) {
			$this->width = $data->width;
		}
		if(isset($data->height)) {
			$this->height = $data->height;
		}
		if(isset($data->visible)) {
			$this->visible = $data->visible;
		}
		if(isset($data->{'background-color'})) {
			$this->background = $data->{'background-color'};
		}
        if(isset($data->{'border-color'})) {
            $this->border = $data->{'border-color'};
        }
        if(isset($data->{'text-align'})) {
            $this->textAlign = $data->{'text-align'};
        }
        
        // Other CSS
        $this->css = array();
        $skipProps = array('width', 'height', 'visible', 'background-color', 'border-color', 'text-align');
        foreach($data as $prop => $val) {
            if(in_array($prop, $skipProps)) {
                continue;
            }
            $this->css[] = $prop.': '.$val;
        }
	}

	/**
	 * Vrati sirku
	 * @return int
	 */
	public function getWidth()
	{
		return $this->width;
	}
	
	/**
	 * Nastavi sirku
	 * @param int $width Sirka
	 * @return FieldStyle
	 */
	public function setWidth($width)
	{
		$this->width = $width;
		return $this;
	}
	
	
	/**
	 * Vrati vysku
	 * @return int
	 */
	public function getHeight()
	{
		return $this->height;
	}
	
	/**
	 * Nastavi vysku
	 * @param int $height Vyska
	 * @return FieldStyle
	 */
	public function setHeight($height)
	{
		$this->height = $height;
		return $this;
	}	
	
	
	/**
	 * Render CSS
	 * @return string
	 */
	public function renderCSS()
	{
		$cssRules = array();
		if(isset($this->width)) {
			$cssRules[] = 'width:'.$this->width.(preg_match('/[0-9]/i', substr($this->width,-1)) ? 'px' : '');
		}
		if(isset($this->height)) {
			$cssRules[] = 'height:'.$this->height.(preg_match('/[0-9]/i', substr($this->height,-1)) ? 'px' : '');
		}
        if(isset($this->visible)) {
            $cssRules[] = 'display:'.(strtoupper($this->visible) === 'A' ? 'table-cell' : 'none');
        }
        if(isset($this->background)) {
            $cssRules[] = 'background-color:'.$this->background;
        }
        if(isset($this->border)) {
            $cssRules[] = 'border-color:'.$this->border;
        }
        if(isset($this->textAlign)) {
            $cssRules[] = 'text-align:'.$this->textAlign;
        }
        // Next CSS rules
        foreach($this->css as $nextRule) {
            $cssRules[] = $nextRule;
        }
		return implode(';', $cssRules);
	}

	/**
	 * Render CSS pro pouziti v bunce tabulky
	 * @return string
	 */
	public function renderTableCSS()
	{
		$cssRules = array();
		if(isset($this->width)) {
			$cssRules[] = 'width:'.$this->width.(preg_match('/[0-9]/i', substr($this->width,-1)) ? 'px' : '');
		}
		if(isset($this->height)) {
			$cssRules[] = 'height:'.$this->height.(preg_match('/[0-9]/i', substr($this->height,-1)) ? 'px' : '');
		}
        if(isset($this->visible)) {
            $cssRules[] = 'display:'.(strtoupper($this->visible) === 'A' ? 'table-cell' : 'none');
        }
        // Next CSS rules
        foreach($this->css as $nextRule) {
            $cssRules[] = $nextRule;
        }        
		return implode(';', $cssRules);
	}

    /**
     * Render CSS pro pouziti v zahlavi tabulky
     * @return string
     */
    public function renderHeaderCSS()
    {
        $cssRules = array();
        if(isset($this->visible)) {
            $cssRules[] = 'display:'.(strtoupper($this->visible) === 'A' ? 'table-cell' : 'none');
        }
        // Next CSS rules
        foreach($this->css as $nextRule) {
            $cssRules[] = $nextRule;
        }
        return implode(';', $cssRules);
    }


	/**
	 * Return CSS String
	 * @return string
	 */
	public function __toString()
	{
		return $this->renderCSS();
	}
	
}
