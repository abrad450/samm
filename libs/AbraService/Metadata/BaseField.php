<?php
namespace Abra\Service;

/**
 * predek pro fieldy
 * 
 * @property string $name Nazev fieldu
 * @property string $caption Popisek fieldu
 * @property FieldControl $control Control
 */
class BaseField
{
    use \Nette\SmartObject;
	
	/**
	 * Nazev
	 * @var string
	 */
	protected $name;
	
	/**
	 * Popisek
	 * @var string
	 */
	protected $caption;
	
	/**
	 * FieldControl
	 * @var FieldControl
	 */
	protected $control;
	
	
	/**
	 * Konstruktor 
	 */
	public function __construct($data)
	{
		$this->name = isset($data->name) ? $data->name : NULL;
		$this->caption = isset($data->caption) ? $data->caption : NULL;
		if(!empty($data->control))
		{
			$this->control = new FieldControl($data->control);
		}
	}
	
	
	/**
	 * Nastavi nazev fieldu
	 * @param string $name
	 * @return FormField Fluent interface
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}
	
	/**
	 * Vrati nazev fieldu
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	
	/**
	 * Nastavi popisek fieldu
	 * @param string $caption
	 * @return FormField Fluent interface
	 */
	public function setCaption($caption)
	{
		$this->caption = $caption;
		return $this;
	}
	
	/**
	 * Vrati popisek fieldu
	 * @return string
	 */
	public function getCaption()
	{
		return $this->caption;
	}
	
	
	/**
	 * Nastavi Field Control fieldu
	 * @param FieldControl $control
	 * @return FormField Fluent interface
	 */
	public function setControl(FieldControl $control)
	{
		$this->control = $control;
		return $this;
	}
	
	/**
	 * Vrati Field control
	 * @return FieldControl
	 */
	public function getControl()
	{
		return $this->control;
	}		
	
}