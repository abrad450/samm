<?php
namespace Abra\Service;

/**
 * Property tridy
 * 
 * @property string $name Nazev fieldu
 * @property string $type Typ fieldu
 * @property string $encoding Encoding
 * @property string $size Velikost pole
 * @property bool $id Je to ID ?
 * @property string $list Typ polozek listu
 * @property string $ref Typ odkazovane polozky
 */
class ClassField
{
    use \Nette\SmartObject;
	
	/**
	 * Nazev fieldu
	 * @var string
	 */
	protected $name;
	
	/**
	 * Datovy typ
	 * @var string
	 */
	protected $type;
	
    /**
     * Encoding
     * @var string
     */
    protected $encoding;
    
	/**
	 * Velikost pole
	 * @var int
	 */
	protected $size;
	
	/**
	 * Zda je tento field identifikujici (primarni klic)
	 * @var bool
	 */
	protected $id;
	
	/**
	 * nazev tridy, jejiz instance jsou v kolekci v etto property
	 * @var string
	 */
	protected $list;
	
	/**
	 * nazev tridy, jejiz instance je odkazovana na teto property
	 * @var string
	 */
	protected $ref;
	
	
	/**
	 * Nastavi nazev fieldu
	 * @param string $name
	 * @return ClassField Fluent interface
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}
	
	/**
	 * Vrati nazev fieldu
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	
	
	/**
	 * Nastavi typ fieldu
	 * @param string $type
	 * @return ClassField Fluent interface
	 */
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}
	
	/**
	 * Vrati typ fieldu
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}	
	
	
	/**
	 * Nastavi encoding fieldu
	 * @param string $encoding
	 * @return ClassField Fluent interface
	 */
	public function setEncoding($encoding)
	{
		$this->encoding = $encoding;
		return $this;
	}
	
	/**
	 * Vrati encoding fieldu
	 * @return string
	 */
	public function getEncoding()
	{
		return $this->encoding;
	}	
	
	
	/**
	 * Nastavi velikost fieldu
	 * @param string $size
	 * @return ClassField Fluent interface
	 */
	public function setSize($size)
	{
		$this->size = $size;
		return $this;
	}
	
	/**
	 * Vrati velikost fieldu
	 * @return string
	 */
	public function getSize()
	{
		return $this->size;
	}		
	
	
	
	/**
	 * Nastavi typ fieldu
	 * @param string $id
	 * @return ClassField Fluent interface
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}
	
	/**
	 * Vrati typ fieldu
	 * @return bool
	 */
	public function isId()
	{
		return $this->id;
	}		
	
	
	/**
	 * Nastavi typ polozek kolekce v tomto fieldu
	 * @param string $list
	 * @return ClassField Fluent interface
	 */
	public function setList($list)
	{
		$this->list = $list;
		return $this;
	}
	
	/**
	 * Vrati typ polozek kolekce v tomto fieldu
	 * @return string
	 */
	public function getList()
	{
		return $this->list;
	}
	
	
	
	/**
	 * Nastavi nazev reference na jiny objekt v tomto fieldu
	 * @param string $ref
	 * @return ClassField Fluent interface
	 */
	public function setRef($ref)
	{
		$this->ref = $ref;
		return $this;
	}
	
	/**
	 * Vrati nazev reference na jiny objekt v tomto fieldu
	 * @return string
	 */
	public function getRef()
	{
		return $this->ref;
	}	
	
	
}
