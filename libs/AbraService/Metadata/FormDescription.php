<?php
namespace Abra\Service;

/**
 * Popis formulare, metadata
 * 
 * @property-read array $sections Vsechny sekce formulare
 * @property-read array $rows
 * @property-read array $fields
 * @property-read array $allRules
 */
class FormDescription
{
    use \Nette\SmartObject;
	
	/**
	 * Kolekce radku formulare
	 * @var \Nette\Utils\ArrayList
	 */
	protected $rows;
	
	
	/**
	 * Konstruktor 
	 * @param mixed $data Data, ze kterych se objekt naplni
	 */
	public function __construct($data)
	{
		$this->rows = new \Nette\Utils\ArrayList();
		if(isset($data->rows->row))
		{
			if(!is_array($data->rows->row)) $data->rows->row = array($data->rows->row);
			foreach($data->rows->row as $formRow)
			{
				$fr = new FormRow($formRow);
				$this->addRow($fr);
			}
		}
	}
	
	
	/**
	 * Vrati kolekci radku formulare
	 * @return \Nette\Utils\ArrayList
	 */
	public function getRows()
	{
		return $this->rows;
	}
	
	/**
	 * Prida radek do formulare
	 * @param FormRow $row Radek
	 * @return FormDescription Fluent
	 */
	public function addRow(FormRow $row)
	{
		$this->rows[] = $row;
		return $this;
	}
	
	
	/**
	 * Vrati pole sekci (podle sekci se pak urci struktura formulare)
	 * @return array 
	 */
	public function getSections()
	{
		$sections = array();
		foreach($this->getRows() as $row)
		{
			/* @var $row FormRow */
			foreach($row->getColumns() as $col)
			{
				/* @var $col FormColumn */
				foreach($col->getChildren() as $section)
				{
					$sections[] = $section;
				}
			}
		}
		return $sections;
	}
	
	
	/**
	 * Vrati formularova pole
	 * @return array 
	 */
	public function getFields()
	{
		$fields = array();
		$sections = $this->getSections();
		foreach($sections as $section)
		{
			$sectionFields = $section->getFields();
			foreach($sectionFields as $field)
			{
				$fields[] = $field;
			}
		}
		return $fields;
	}
	
	
	/**
	 * Vrati vsechna validacni pravidla 
	 * @return array
	 */
	public function getAllRules()
	{
		$allRules = array();
		$fields = $this->getFields();
		foreach($fields as $field)
		{
			$this->addFieldRules($allRules, $field);
		}
		return $allRules;
	}
	
	/**
	 * Vrati pravidla pro predany field (a podrizene fieldy)
	 * @param array $allRules Kam se pravidla budou vkladat
	 * @param FormField $field Field
	 * @param string $namePrefix Prefix pro jmeno
	 */
	protected function addFieldRules(&$allRules, $field, $namePrefix = '')
	{
		/* @var $field FormField */
		$control = $field->getControl();
		if($control !== NULL)
		{
			$controlRules = $control->getRules();
			if(!empty($controlRules) && count($controlRules) > 0)
			{
				foreach($controlRules as $r)
				{
					if(!isset($allRules[$namePrefix.$field->name]))
					{
						$allRules[$namePrefix.$field->name] = array();
					}
					$allRules[$namePrefix.$field->name][] = $r;
				}
			}
		}
		// podpolozky a jejich pravidla
		$subFields = $field->getFields();
		if(count($subFields) > 0)
		{
			foreach($subFields as $subField)
			{
				$this->addFieldRules($allRules, $subField, $field->name.'.');
			}
		}
	}
	
}