<?php
namespace Abra\Service;

/**
 * sloupec formulare
 * 
 * @property-read array $sections Sekce
 * @property-read array $tabs Zalozky
 * @property-read array $children Potomci
 * @property-read string $childrenType Typ potomku
 * @property-read int $size Column size
 */
class FormColumn
{
    use \Nette\SmartObject;
    
    const CHILDREN_TYPE_SECTIONS = 'sections';
    const CHILDREN_TYPE_TABS = 'tabs';
    
	/**
	 * Kolekce sekci formulare
	 * @var \Nette\Utils\ArrayList
	 */
	protected $sections;
    
    /**
     * Kolekce zalozek
     * @var \Nette\Utils\ArrayList
     */
    protected $tabs;
    
    /**
     * Column size
     * @var int
     */
    protected $size;
	
	
	/**
	 * Konstruktor 
	 * @param mixed $data Data, ze kterych se column naplni
	 */
	public function __construct($data)
	{
        $tabs = isset($data->sections->__attributes) &&
                is_array($data->sections->__attributes) && 
                isset($data->sections->__attributes['type']) && 
                $data->sections->__attributes['type'] == 'tabs';         
        
        // Column size
        if(isset($data->__attributes) && isset($data->__attributes['size'])) {
            $this->size = $data->__attributes['size'];
        }
        
		$this->sections = new \Nette\Utils\ArrayList();
		
		if(isset($data->sections->section))
		{
			if(!is_array($data->sections->section)) 
			{
				$data->sections->section = array($data->sections->section);
			}
			foreach($data->sections->section as $formSection)
			{
				$fs = new FormSection($formSection);
                if($tabs) {
                    $this->addTab($fs);
                }
                else {
                    $this->addSection($fs);
                }
			}
		}

	}
	
	
	/**
	 * Vrati kolekci sloupcu formulare
	 * @return \Nette\Utils\ArrayList
	 */
	public function getSections()
	{
		return $this->sections;
	}
	
	/**
	 * Prida sekci do sloupce
	 * @param FormSection $section Sloupec
	 * @return FormRow Fluent interface
	 */
	public function addSection(FormSection $section)
	{
		$this->sections[] = $section;
		return $this;
	}
    
    
    
    
	/**
	 * Vrati kolekci zalozek formulare
	 * @return \Nette\Utils\ArrayList
	 */
	public function getTabs()
	{
		return $this->tabs;
	}    
    
	/**
	 * Prida zalozku do sloupce
	 * @param FormSection $tab Zalozka
	 * @return FormRow Fluent interface
	 */
	public function addTab(FormSection $tab)
	{
		$this->tabs[] = $tab;
		return $this;
	}
    
    
    /**
     * Vrati potomky, at jsou jake chteji
     * @return array
     */
    public function getChildren()
    {
        return $this->getChildrenType() === self::CHILDREN_TYPE_SECTIONS ? $this->getSections() : $this->getTabs();
    }
    
    
    
    /**
     * Vrati typ podpolozek
     * @return string
     */
    public function getChildrenType()
    {
        return empty($this->tabs) ? self::CHILDREN_TYPE_SECTIONS : self::CHILDREN_TYPE_TABS;
    }
    
    /**
     * Get columns size
     * 
     * @return int|null
     */
    public function getSize()
    {
        return $this->size;
    }


}