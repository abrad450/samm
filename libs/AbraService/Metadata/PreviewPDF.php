<?php
namespace Abra\Service;

/**
 * Preview PDF
 * @property string $href href atribut odkazu
 */
class PreviewPDF extends Element
{
	
	
	/**
	 * Kam odkaz odkazuje
	 * @var string
	 */
	protected $href;
	
	
	
	/**
	 * Konstruktor
	 * @param mixed $data Data
	 */
	public function __construct($data)
	{
		if(!isset($data->href)) throw new \Exception('Proeprty "href" is not present');
		$this->href = $data->href;
	}
	
	
	
	/**
	 * Vrati href
	 * @return string
	 */
	public function getHref()
	{
		return $this->href;
	}
	
	/**
	 * Nastavi href
	 * @param string $href HREF atribut odkazu
	 * @return Link
	 */
	public function setHref($href)
	{
		$this->href = $href;
		return $this;
	}		
	
}
