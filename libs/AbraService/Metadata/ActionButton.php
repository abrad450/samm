<?php
namespace Abra\Service;

/**
 * ActionButton
 * 
 * @property string $bo
 * @property string $action
 * @property string $id
 * @property string $ref
 * @property bool $autosave
 */
class ActionButton extends Element
{
	
	/**
	 * Agenda (Business Object - TNxStoreCard)
	 * @var string
	 */
	protected $bo;
	
	/**
	 * Action pro BO agendu
	 * @var string
	 */
	protected $action;
	
	/**
	 * ID placeholder name
	 * @var string
	 */
	protected $id;
	
	/**
	 * Reference link, kam se vratit
	 * @var string
	 */
	protected $ref;
	
	/**
	 * Autosave (po kliknuti se ulozi obsah puvodni agendy, odkud se akce vola
	 * @var bool
	 */
	protected $autosave;
	
	/**
	 * Konstruktor
	 * @param mixed $data Data
	 */
	public function __construct($data)
	{
		if(!isset($data->bo))
		{
			throw new \Exception('Property "bo" is not present');
		}
		if(!isset($data->ref))
		{
			throw new \Exception('Property "ref" is not present');
		}
		if(!isset($data->action))
		{
			throw new \Exception('Property "action" is not present');
		}
		$this->setBo($data->bo);
		$this->setRef($data->ref);
		$this->setId(isset($data->id) ? $data->id : '{ID}');
		$this->setAction($data->action);
		$this->setAutosave(isset($data->autosave) ? $data->autosave : false);
	}
	
	/**
	 * Get BO
	 * @return string
	 */
	public function getBo()
	{
		return $this->bo;
	}

	/**
	 * Get ID identificator
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Get autosave flag
	 * @return bool
	 */
	public function getAutosave()
	{
		return $this->autosave;
	}
	
	/**
	 * Vrati REF
	 * @return string
	 */
	public function getRef()
	{
		return $this->ref;
	}	
	
	/**
	 * Get action
	 * @return string
	 */
	public function getAction()
	{
		return $this->action;
	}
	

	/**
	 * Set BO
	 * @param string $bo BO
	 * @return \Abra\Service\ActionButton\ActionButton
	 */
	public function setBo($bo)
	{
		$this->bo = $bo;
		return $this;
	}

	/**
	 * set ID identifier
	 * @param string $id ID
	 * @return \Abra\Service\ActionButton\ActionButton
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * Set Autosave flag
	 * @param bool $autosave
	 * @return \Abra\Service\ActionButton\ActionButton
	 */
	public function setAutosave($autosave)
	{
		$this->autosave = $autosave;
		return $this;
	}

	/**
	 * Nastavi REF
	 * @param string $ref REF atribut odkazu
	 * @return ActionButton self
	 */
	public function setRef($ref)
	{
		$refParts = explode('/', $ref);
		if(count($refParts) > 1)
		{
			if($refParts[1] !== 'default')
			{
				array_splice($refParts, 1, 0, array('default'));
			}
			$ref = implode('/', $refParts);
		}
		$this->ref = $ref;
		return $this;
	}

	/**
	 * Set action
	 * @param string $action
	 * @return \Abra\Service\ActionButton
	 */
	public function setAction($action)
	{
		$this->action = $action;
		return $this;
	}

}