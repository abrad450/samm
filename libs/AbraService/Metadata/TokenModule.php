<?php

namespace Abra\Service;

/**
 * Token module collection
 */
class TokenModuleCollection implements \IteratorAggregate
{
    private $tokenModules;
    
    private function __construct()
    {
        $this->tokenModules = [];
    }
    
    private function addModule(TokenModule $tm)
    {
        $this->tokenModules[] = $tm;
    }
        
    public static function createFromXml(\SimpleXMLElement $xml)
    {
        $tmc = new static();
        foreach($xml as $item) {
            if(\Nette\Utils\Strings::lower($item->getName()) === 'tokenmodule') {
                $tm = TokenModule::createFromXml($item);
                $tmc->addModule($tm);
            }
        }
        return $tmc;
    }
    
    public function getIterator()
    {
        return new \ArrayIterator($this->tokenModules);
    }    
}


/**
 * Token Module
 * 
 * @property string $name Name
 * @property string $objectname Object name
 */
class TokenModule implements \IteratorAggregate
{
    use \Nette\SmartObject;
    
    private $name;
    
    private $objectname;
    
    private $documents;

    
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getObjectname()
    {
        return $this->objectname;
    }

    public function setObjectname($objectname)
    {
        $this->objectname = $objectname;
    }
            

    private function __construct($objectname, $name)
    {
        $this->objectname = $objectname;
        $this->name = $name;
        $this->documents = [];
    }
    
    private function addDocument(TokenModuleDocument $tmd)
    {
        $this->documents[] = $tmd;
    }

    
    
    public function getIterator()
    {
        return new \ArrayIterator($this->documents);
    }
    

    
    public static function createFromXml(\SimpleXMLElement $xml)
    {
        $m = new static((string)$xml->objectname, (string)$xml->name);
        if(!empty($xml->documents) && !empty($xml->documents->document)) {
            foreach($xml->documents->document as $doc) {
                $tmd = TokenModuleDocument::createFromXml($doc);
                $m->addDocument($tmd);
            }
        }
        return $m;
    }

}


/**
 * Token Module Document
 * 
 * @property string $ID
 * @property string $displayName
 * @property string $description
 * @property string $beforecheck
 * @property bool $solved
 * @property bool $error
 */
class TokenModuleDocument
{
    use \Nette\SmartObject;
    
    private $ID;
    
    private $DisplayName;
    
    private $Description;
            
    private $beforecheck;
    
    private $solved;
    
    private $error;

    public function getID()
    {
        return $this->ID;
    }

    public function getDisplayName()
    {
        return $this->DisplayName;
    }

    public function getDescription()
    {
        return $this->Description;
    }

    public function getBeforecheck()
    {
        return $this->beforecheck;
    }

    public function getSolved()
    {
        return $this->solved;
    }

    public function getError()
    {
        return $this->error;
    }

    public function setID($ID)
    {
        $this->ID = $ID;
    }

    public function setDisplayName($DisplayName)
    {
        $this->DisplayName = $DisplayName;
    }

    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    public function setBeforecheck($beforecheck)
    {
        $this->beforecheck = is_bool($beforecheck) ? $beforecheck : (strtoupper($beforecheck) === 'A' ? true : false);
    }

    public function setSolved($solved)
    {
        $this->solved = is_bool($solved) ? $solved : (strtoupper($solved) === 'A' ? true : false);
    }

    public function setError($error)
    {
        $this->error = $error;
    }

        

    public static function createFromXml(\SimpleXMLElement $xml)
    {
        $m = new static();

        $m->setID((string)$xml->id);
        $m->setDisplayName((string)$xml->displayname);
        $m->setDescription((string)$xml->description);
        $m->setBeforecheck((string)$xml->beforecheck);
        $m->setSolved((string)$xml->solved);
        $m->setError((string)$xml->error);
    
        return $m;
    }
}