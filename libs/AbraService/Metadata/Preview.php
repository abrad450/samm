<?php
namespace Abra\Service;

/**
 * Preview
 * 
 * @property-read int $tabNo Tab no (cislo stahovacich zalozek v poradi)
 * @property-read string $href HREF
 * @property-read int $height Vyska nahledu
 * @property-read bool $forceInline
 */
class Preview extends Element
{

	/**
	 * TabCounter
	 * 
	 * @var int
	 */
	protected static $tabCounter = 0;
	
	/**
	 * Tabs ID
	 * 
	 * @var string
	 */
	protected $tabNo;
	
	/**
	 * Kam odkaz odkazuje
	 * @var string
	 */
	protected $href;	
	
	/**
	 * height
	 * @var int
	 */
	protected $height;
	
	/**
	 * Get tab no
	 * 
	 * @return string
	 */
	public function getTabNo()
	{
		return $this->tabNo;
	}

	/**	
	 * Height
	 * 
	 * @return int
	 */
	public function getHeight()
	{
		return $this->height;
	}
	
	/**
	 * Constructor
	 * 
	 * @param type $data
	 */
	public function __construct($data)
	{
		if(!isset($data->href)) 
		{
			throw new \Exception('Proeprty "href" is not present!');
		}
		$this->href = $data->href;
		if(isset($data->height))
		{
			$this->height = $data->height;
		}
		self::$tabCounter++;
		$this->tabNo = self::$tabCounter;
	}
	
	/**
	 * Vrati href
	 * @return string
	 */
	public function getHref()
	{
		return $this->href;
	}
	
	/**
	 * Nastavi href
	 * @param string $href HREF atribut odkazu
	 * @return Link
	 */
	public function setHref($href)
	{
		$this->href = $href;
		return $this;
	}		
	
	/**
	 * Is force inline for given filename ?
	 * 
	 * @param string $fileName
	 * @return int 1/0
	 */
	public function isForceInline($fileName)
	{
		$fileParts = explode('.', $fileName);
		$ext = strtolower(end($fileParts));
		return in_array($ext, array('jpg','jpeg','png','gif','pdf')) ? 1 : 0;
	}
	
	/**
	 * Generate preview for
	 * 
	 * @param object $file File
	 * @param object $field Field
	 * @param string $link Link
	 * @param \Nette\Localization\ITranslator $translator
	 * @param string $loader Loader image URL
	 * @return string
	 */
	public function generatePrevewFor($file, $field, $link, \Nette\Localization\ITranslator $translator, $loader)
	{
		$fileParts = explode('.', $file->FileName);
		$ext = strtolower(end($fileParts));
		$escLink = htmlspecialchars($link);
		switch($ext)
		{
			// Image preview
			case 'gif':
			case 'png':
			case 'jpg':
			case 'jpeg':
				return
					'<img src="'.$escLink.'" alt="'.$file->FileName.'" />';
			
			// PDF preview
			case 'pdf':
				$inlineStyle = empty($field->control->element->height) ? '' : ' style="height: '.$field->control->element->height.'px"';
				return
					'<div style="min-height: 50px; background-image: url('.$loader.'); background-color: transparent; background-repeat: no-repeat; background-position: center center">'.
					'<object data="'.$escLink.'" type="application/pdf"'.$inlineStyle.' width="100%">'.
						'<p><a href="'.$escLink.'">'.$file->FileName.'</a></p>'.
					'</object>'.
					'</div>';

			default:
				// Download link
				return
					'<p class="text-center">'.$translator->translate('Náhled pro tento typ souboru není k dispozici').'</p>'.
					'<p class="text-center"><a href="'.$escLink.'">'.$file->FileName.'</a></p>';
		}
	}
	
}