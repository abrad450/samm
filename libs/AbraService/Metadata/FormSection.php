<?php
namespace Abra\Service;

/**
 * Sekce formulare
 * 
 * @property string $caption Caption sekce
 * @property bool $repeatable Zda je sekce opakovatelna
 * @property-read \Nette\Utils\ArrayList $fields Seznam poli formulare v teto sekci
 */
class FormSection
{
    use \Nette\SmartObject;

	/**
	 * Popsek sekce
	 * @var string
	 */
	protected $caption;
	
	/**
	 * Policka
	 * @var \Nette\Utils\ArrayList
	 */
	protected $fields;
	
	/**
	 * Zda je sekce s opakovanim svych fields
	 * @var bool
	 */
	protected $repeatable;
	
	
	/**
	 * Konstruktor 
	 * @param mixed $data Data, ze kterych se sekce naplni
	 */
	public function __construct($data)
	{
		$this->fields = new \Nette\Utils\ArrayList();
		
		$this->caption = isset($data->caption) ? $data->caption : NULL;
		if(isset($data->fields->field))
		{
			if(!is_array($data->fields->field))
			{
				$data->fields->field = array($data->fields->field);
			}
			foreach($data->fields->field as $formField)
			{
				$ff = new FormField($formField);
				$this->addField($ff);

				// TODO: mohou byt vnorene fieldy

				// TODO: validacni podminky pro formular

			}
		}		
	}
	
	/**
	 * Vrati nadpis sekce
	 * @return string
	 */
	public function getCaption()
	{
		return $this->caption;
	}
	
	/**
	 * Nastavi nadpis sekce
	 * @param string $caption  Nadpis sekce
	 * @return FormSection
	 */
	public function setCaption($caption)
	{
		$this->caption = $caption;
		return $this;
	}
	
	
	/**
	 * Vrati nastaveni opakovatelnosti sekce
	 * @return bool
	 */
	public function getRepeatable()
	{
		return $this->repeatable;
	}
	
	/**
	 * nastavi, zda je sekce opakovatelna
	 * @param bool $repeatable Je sekce opakovatelna ?
	 * @return FormSection
	 */
	public function setRepeatable($repeatable)
	{
		$this->repeatable = $repeatable;
		return $this;
	}	
	
	
	/**
	 * Vrati pole formulare
	 * @return \Nette\ArrayList
	 */
	public function getFields()
	{
		return $this->fields;
	}
	
	
	
	/**
	 * Prida field do sekce
	 * @param FormField $field field
	 * @return FormSection Fluent interface
	 */
	public function addField(FormField $field)
	{
		$this->fields[] = $field;
		return $this;
	}	
	
}