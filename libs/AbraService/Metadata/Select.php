<?php
namespace Abra\Service;

/**
 * SelectBox
 * @property-read ArrayList $items Polozky
 */
class Select extends Element
{

	/**
	 * Kolekce polozek selectu
	 * @var ArrayList
	 */
	protected $items;
	
	
	/**
	 * Element
	 * @param mixed $data 
	 */
	public function __construct($data)
	{
		$this->setName('select');
		$this->items = new \Nette\Utils\ArrayList();
		if(!empty($data->items) && is_array($data->items))
		{
			foreach($data->items as $item)
			{
				$className = 'Abra\\Service\\'.$item->_class;
				$this->items[] = new $className($item);
			}
		}
	}
	
	
	/**
	 * Polozky selectu (OPTIONS)
	 * @return ArrayList
	 */
	public function getItems()
	{
		return $this->items;
	}
	
	
	/**
	 * Vrati ciselnik pro select naplneny svymi hodnotami
	 * @param array $preset Prednastavene hodnoty
	 * @return array
	 */
	public function getItemsCodelist($preset = array())
	{
		$codelist = $preset;
		foreach($this->getItems() as $item)
		{
			/* @var $item SelectItem */
			$codelist[$item->getValue()] = $item->getCaption();
		}
		return $codelist;
	}
}