<?php
namespace Abra\Service;

/**
 * Control sekce pro Fieldy
 * 
 * @property-read Element $element Element
 * @property-read \Nette\Utils\ArrayList $rules Rules
 * @property-read \Nette\Utils\ArrayHash $settings Nastaveni
 * @property-read FilterGroup $cascade FilterGroup Cascade Definition
 */
class FieldControl
{
    use \Nette\SmartObject;
	
	/**
	 * Definice elementu, ktery se pouzije pro zobrazeni pole
	 * @var Element
	 */
	protected $element;
	
	/**
	 * Kolekce validacnich pravidel
	 * @var ArrayList
	 */
	protected $rules;
	
	/**
	 * Nastaveni
	 * @var \Nette\Utils\ArrayHash
	 */
	protected $settings;
	
	/**
	 * CascadeFilter - kaskadni comboboxy (a-la FilterGroup)
	 * @var FilterGroup
	 */
	protected $cascade;
	
	
	/**
	 * Konstruktor
	 * @param mixed $data Data JSON data (vkladaji se rucne v ABRE)
	 */
	public function __construct($data)
	{
		$data = json_decode($data);
		$err = json_last_error();
		if($err !== JSON_ERROR_NONE)
		{
			throw new JsonDecodeException($err);
		}
		// z univerzalniho JSON pole zjistime, pokud mame definovany "element" (select s polozkama)
		if(!empty($data->element))
		{
			$className = 'Abra\\Service\\'.$data->element->_class;
			$this->element = new $className($data->element);
		}
		// Pokud mame validacni pravidla, tak je zinicializujeme
		if(!empty($data->rules) && is_array($data->rules))
		{
			foreach($data->rules as $rule)
			{
				$className = 'Abra\\Service\\'.$rule->_class;
				$this->rules[] = new $className($rule);
			}
		}
		// Pokud mame settings, tak je sem dame (AUTOCOMPLETE = TRUE)
		if(!empty($data->settings))
		{
            $settings = json_decode(json_encode($data->settings), TRUE);
			$this->settings = \Nette\Utils\ArrayHash::from($settings);
		}
		// Pokud mame cascade, vytvorime Filters
		if(!empty($data->cascade))
		{
			$this->cascade = FilterGroup::createFrom($data->cascade);
		}
	}
	
	
	/**
	 * Vrati element
	 * @return Element
	 */
	public function getElement()
	{
		return $this->element;
	}
	
	
	/**
	 * Vrati kolekci rules
	 * @return \Nette\Utils\ArrayList
	 */
	public function getRules()
	{
		return $this->rules;
	}
	
	/**
	 * Vrati kolekci settings
	 * @return \Nette\Utils\ArrayHash
	 */
	public function getSettings()
	{
		return $this->settings;
	}
	
	/**
	 * Vrati Filter Cascade definition
	 * @return FilterGroup
	 */
	public function getCascade()
	{
		return $this->cascade;
	}


	
}