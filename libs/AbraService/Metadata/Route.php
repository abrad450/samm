<?php
namespace Abra\Service;

/**
 * Odkaz na routu
 *
 * @property-read string $module Modul
 * @property-read string $presenter Presenter
 * @property-read string $action Akce
 * @property-read string $bo Business Object
 * @property-read string $id ABRA ID
 * @property-read bool $targetBlank Target Blank
 * @property-read $href HREF
 */
class Route extends Element
{
	/**
	 * Module
	 * @var string
	 */
	protected $module;

	/**
	 * Presenter
	 * @var string
	 */
	protected $presenter;

	/**
	 * Action
	 * @var string
	 */
	protected $action;

	/**
	 * Zda se ma otevrit link v novem okne
	 * @var bool
	 */
	protected $targetBlank;

	/**
	 * Business object
	 * @var string
	 */
	protected $bo;

	/**
	 * ABRA ID
	 * @var string
	 */
	protected $id;


	/**
	 * Konstruktor
	 * @param mixed $data Data
	 */
	public function __construct($data)
	{
		if(empty($data->bo))
		{
			throw new \Exception('Proeprty "bo" is not present');
		}

		$this->bo = $data->bo;
		$this->id = isset($data->id) ? $data->id : NULL;
		$this->action = !empty($data->action) ? $data->action : 'default';
		$this->module = !empty($data->module) ? $data->module : NULL;
		$this->presenter = !empty($data->presenter) ? $data->presenter : 'Default';
		$this->targetBlank = !empty($data->targetBlank);
	}

	/**
	 * Vyrenderuje route link
	 * @param \Nette\Application\UI\Presenter $presenter
	 * @param object $id ID hodnota (NULL = pouzije se zadana z <control>)
	 * @return string HREF pro odkaz
	 */
	public function getHref(\Nette\Application\UI\Presenter $presenter, $id = NULL)
	{
		$module = !empty($this->module) ? ':'.$this->module : '';
		return $presenter->link($module.':'.$this->presenter.':'.$this->action, array(
			'bo' => $this->bo,
			'id' => strtolower($this->id) === '{id}' && $id !== NULL ? $id : $this->id,
		));
	}

	/**
	 * Is target blank ?
	 * @return bool
	 */
	public function isTargetBlank()
	{
		return $this->targetBlank;
	}

	/**
	 * Return module name
	 * @return string
	 */
	public function getModule()
	{
		return $this->module;
	}

	/**
	 * Return presenter name
	 * @return string
	 */
	public function getPresenter()
	{
		return $this->presenter;
	}

	/**
	 * Return action name
	 * @return string
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 * Get BO
	 * @return string
	 */
	public function getBo()
	{
		return $this->bo;
	}

	/**
	 * Get ID
	 * @return stgring
	 */
	public function getId()
	{
		return $this->id;
	}

}