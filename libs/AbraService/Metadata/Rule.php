<?php
namespace Abra\Service;

/**
 * Validacni pravidlo
 * 
 * @property string $field Field
 * @property string $action Action
 * @property string $value Value
 * @property string $error Error
 */
class Rule
{
    use \Nette\SmartObject;
	
	/**
	 * Pole
	 * @var string
	 */
	protected $field;
	
	/**
	 * Akce (FILLED,EQUALS,...)
	 * @var string
	 */
	protected $action;
	
	/**
	 * Hodnota
	 * @var mixed
	 */
	protected $value;
	
	/**
	 * Error message
	 * @var string
	 */
	protected $error;
	
	
	/**
	 * Pravidlo
	 * @param mixed $data Data, ze kterych se objekt vytvori
	 */
	public function __construct($data)
	{
		if(!isset($data->field)) throw new \Exception('Property "field" is not defined!');
		if(!isset($data->action)) throw new \Exception('Property "action" is not defined!');
		$this->field = $data->field;
		$this->action = $data->action;
		$this->value = isset($data->value) ? $data->value : NULL;
		$this->error = isset($data->error) ? $data->error : NULL;
	}
	
	
	/**
	 * Vrati pole
	 * @return string
	 */
	public function getField()
	{
		return $this->field;
	}
	
	/**
	 * Nastavi pole
	 * @param string $name nazev pole
	 * @return Rule
	 */
	public function setField($field)
	{
		$this->field = $field;
		return $this;
	}
	
	
	
	
	/**
	 * Vrati akci
	 * @return string
	 */
	public function getAction()
	{
		return $this->action;
	}
	
	/**
	 * Nastavi akci
	 * @param string $action akce
	 * @return Rule
	 */
	public function setAction($action)
	{
		$this->action = $action;
		return $this;
	}	
	
	
	
	
	/**
	 * Vrati pole
	 * @return string
	 */
	public function getValue()
	{
		return $this->value;
	}
	
	/**
	 * Nastavi hodnotu
	 * @param mixed $value hodnota
	 * @return Rule
	 */
	public function setValue($value)
	{
		$this->value = $value;
		return $this;
	}
	
	
	/**
	 * Vrati chzbove hlaseni
	 * @return string
	 */
	public function getError()
	{
		return $this->error;
	}
	
	/**
	 * Nastavi chzbove hlaseni
	 * @param mixed $error chzbove hlaseni
	 * @return Rule
	 */
	public function setError($error)
	{
		$this->error = $error;
		return $this;
	}		
	
	
}