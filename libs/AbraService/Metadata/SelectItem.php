<?php
namespace Abra\Service;

/**
 * SELECT OPTION
 * @property mixed $value Hodnota
 * @property string $caption Popisek
 * @property string $buttontype Button type
 */
class SelectItem extends Element
{
	
	/**
	 * @var mixed
	 */	
	protected $value;
		
	/**
	 * @var string
	 */
	protected $caption;

	/**
	 * button type (for X_ApproveType)
	 * @var string
	 */
	protected $buttontype;
	
	
	/**
	 * Element
	 * @param mixed $data 
	 */
	public function __construct($data)
	{
		$this->setName('option');
		$this->setValue(isset($data->value) ? $data->value : null);
		$this->setCaption(isset($data->caption) ? $data->caption : null);
		$this->setButtontype(isset($data->buttontype) ? $data->buttontype : null);
	}
	
	
	/**
	 * Vrati hodnotu
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}
	
	/**
	 * Nastavi hodnotu
	 * @param mixed $value 
	 * @return SelectItem
	 */
	public function setValue($value)
	{
		$this->value = $value;
		return $this;
	}

	
	
	/**
	 * Vrati popisek
	 * @return string
	 */
	public function getCaption()
	{
		return $this->caption;
	}
	
	/**
	 * Nastavi popisek
	 * @param string $value 
	 * @return SelectItem
	 */
	public function setCaption($caption)
	{
		$this->caption = $caption;
		return $this;
	}	
	
	/**
	 * Vrati button type
	 * @return string
	 */
	public function getButtontype()
	{
		return $this->buttontype;
	}
	
	/**
	 * Nastavi buttontype
	 * @param string $buttontype
	 * @return SelectItem
	 */
	public function setButtontype($buttontype)
	{
		$this->buttontype = $buttontype;
		return $this;
	}	
	
	
}