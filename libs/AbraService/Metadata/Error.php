<?php
namespace Abra\Service;

/**
 * Chyba
 * 
 * @property int $code Kod chyby
 * @property string $message Chyba zprava
 * @property string $location
 * @property-read bool $fatal
 * @property-read string $type Color of the message
 */
class Error
{
    use \Nette\SmartObject;
	
	/**
	 * Kod chyby
	 * @var int
	 */
	protected $code;
	
	/**
	 * Chybova hlaska
	 * @var string
	 */
	protected $message;
    
	protected $location;
	
	
	/**
	 * Vrati kod chyby
	 * @return int 
	 */
	public function getCode()
	{
		return $this->code;
	}
	
	/**.
	 * Nastavi kod cyby
	 * @param int $code Kod chyby
	 * @return Error Fluent rozhrani
	 */
	public function setCode($code)
	{
		$this->code = $code;
		return $this;
	}
	
	
	/**
	 * Vrati chybovou zpravu
	 * @return string 
	 */
	public function getMessage()
	{
		return $this->message;
	}
	
	
	/**
	 * Nastavi text chybove zpravy
	 * @param string $message
	 * @return \Abra\Service\Error Fluent interface
	 */
	public function setMessage($message)
	{
		$this->message = $message;
		return $this;
	}
	
    
    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

        
	/**
	 * Is this error fatal ? (error / validation)
	 * @return bool
	 */
	public function isFatal()
	{
		return $this->code < 200;
	}
	
	/**
	 * Get error type
	 * 
	 * @return string
	 */
	public function getType()
	{
		if($this->code < 100)
		{
			// Error
			return 'error';
		}
		elseif($this->code < 200)
		{
			// Validation
			return 'validation';
		}
		else // if($this->code < 300)
		{
			// Notice
			return 'notice';
		}
	}
	
}