<?php
namespace Abra\Service;

/**
 * Condition On
 * 
 * @property-read \Nette\Utils\ArrayList $rules
 */
class ConditionOn extends Rule
{
	
	/**
	 * Kolekce pravidel spadajicich do teto podminky
	 * @var ArrayList
	 */
	protected $rules;

	
	/**
	 * Konstruktgor
	 * @param mxed $data Data, ze kterych se objekt vytvori
	 */
	public function __construct($data)
	{
		parent::__construct($data);
		$this->rules = new \Nette\Utils\ArrayList();
		if(!empty($data->rules) && is_array($data->rules))
		{
			foreach($data->rules as $rule)
			{
				$className = 'Abra\\Service\\'.$rule->_class;
				$this->rules[] = new $className($rule);
			}
		}
	}
	
	
	/**
	 * Vrati pravidla
	 * @return ArrayList
	 */
	public function getRules()
	{
		return $this->rules;
	}
	
}