<?php
namespace Abra\Service;

/**
 * Property formulare
 * 
 * @property FieldStyle $style Styl pole
 * @property string $display Zpusob zobrazeni fieldu
 * @property-read string $type Typ fieldu
 * @property-read \Nette\Utils\ArrayList $fields 
 * @property string $SQLField SQL Field
 * @property-read bool $rightAligned
 */
class FormField extends BaseField
{

	/**
	 * Zpusob zobrazeni fieldu
	 * @var string
	 */
	protected $display;
		
	/**
	 * Field type
	 * @var string
	 */
	protected $type;	
    
	/**
	 * Styl
	 * @var FieldStyle
	 */
	protected $style;	
	
	/**
	 * Kolekce vnorenych Fieldu
	 * @var ArrayList 
	 */
	protected $fields;
	
	/**
	 * SQL field pro poslani zpet ve filtracnim formulari
	 * @var string
	 */
	protected $SQLField;
	
	/**
	 * Konstruktor
	 * @param mixed $data Data, ze kterych se field naplni
	 */
	public function __construct($data)
	{
		parent::__construct($data);
		$this->display = isset($data->display) ? $data->display : NULL;
        $this->type = isset($data->type) ? $data->type : NULL;
		if(isset($data->style))
		{
			$this->style = new FieldStyle($data->style);
		}
		$this->fields = new \Nette\Utils\ArrayList();
		if(isset($data->fields) && !empty($data->fields->field))
		{
            $fieldArray = is_array($data->fields->field) ? $data->fields->field : array($data->fields->field);
			foreach($fieldArray as $f)
			{
				$ff = new FormField($f);
				$this->fields[] = $ff;
			}
		}
		$this->SQLField = !empty($data->SQLField) ? $data->SQLField : NULL;
	}
	
    /**
     * Is this field RIGHT ALIGNED ?
     * 
     * @return bool
     */
    public function isRightAligned()
    {
        return $this->type === 'dtFloat';
    }
    
	
	/**
	 * Nastavi display styl fieldu
	 * @param string $display
	 * @return FormField Fluent interface
	 */
	public function setDisplay($display)
	{
		$this->display = $display;
		return $this;
	}
	
	/**
	 * Vrati display styl fieldu
	 * @return string
	 */
	public function getDisplay()
	{
		return $this->display;
	}		
	
	
	
	
	/**
	 * Nastavi styl fieldu
	 * @param FieldStyle $style
	 * @return ListField Fluent interface
	 */
	public function setStyle($style)
	{
		$this->style = $style;
		return $this;
	}
	
	/**
	 * Vrati styl fieldu
	 * @return FieldStyle
	 */
	public function getStyle()
	{
		return $this->style;
	}
	
	
	/**
	 * Vrati podpolozky polozky
	 * @return \Nette\Utils\ArrayList
	 */
	public function getFields()
	{
		return $this->fields;
	}		
    
	/**
	 * Vrati typ fieldu
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}
	
	
	/**
	 * Nastavi SQL Field
	 * @param string $display
	 * @return FormField Fluent interface
	 */
	public function setSQLField($sqlField)
	{
		$this->SQLField = $sqlField;
		return $this;
	}
	
	/**
	 * Vrati SQLField
	 * @return string
	 */
	public function getSQLField()
	{
		return $this->SQLField;
	}		
	
}
