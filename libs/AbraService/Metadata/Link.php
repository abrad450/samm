<?php
namespace Abra\Service;

/**
 * Odkaz
 * @property string $href href atribut odkazu
 * @property bool $newWindow New window
 * @property FieldStyle|string $style Link style
 * @property string $type Type
 * @property string $text Link text
 * @property string $class Link CSS class
 */
class Link extends Element
{
    const LINK_TYPES = array('link', 'download');
    const DEFAULT_LINK_TYPE = 'download';
    
	/**
	 * Kam odkaz odkazuje
	 * @var string
	 */
	protected $href;
    
	/**
	 * Is new window link?
	 * @var bool
	 */
	protected $newWindow;
	
    /**
     * Inline style for the link
     * @var string
     */
	protected $style;
    
    /**
     * Link type (download, link)
     * @var string
     */
    protected $type;
    
    /**
     * Link class
     * @var string
     */
    protected $class;
	
    /**
     * Link text
     * @var string
     */
    protected $text;
    
	/**
	 * Konstruktor
	 * @param mixed $data Data
	 */
	public function __construct($data)
	{
		if(!isset($data->href)) {
            throw new \Exception('Proeprty "href" is not present');
        }
		$this->href = $data->href;
        
        $this->newWindow = empty($data->newWindow) && empty($data->newwindow) ? FALSE : TRUE;
        
        $this->type = empty($data->type) ? self::DEFAULT_LINK_TYPE : 
                (in_array(strtolower($data->type), self::LINK_TYPES) ? strtolower($data->type) : self::DEFAULT_LINK_TYPE);
        
        $this->style = empty($data->style) ? NULL : 
                (is_object($data->style) ? new FieldStyle($data->style) : $data->style);
        
        $this->class = empty($data->class) ? NULL : $data->class;
        $this->text = empty($data->text) ? NULL : $data->text;
	}
	
	/**
	 * Vrati href
	 * @return string
	 */
	public function getClass()
	{
		return $this->class;
	}
	
	/**
	 * CSS Class setter
	 * @param string $class Class
	 * @return Link
	 */
	public function setClass($class)
	{
		$this->class = $class;
		return $this;
	}		
	    
	/**
	 * Vrati text
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}
	
	/**
	 * Link text setter
	 * @param string $text text
	 * @return Link
	 */
	public function setText($text)
	{
		$this->text = $text;
		return $this;
	}	    
    
	
	/**
	 * Vrati href
	 * @return string
	 */
	public function getHref()
	{
		return $this->href;
	}
	
	/**
	 * Nastavi href
	 * @param string $href HREF atribut odkazu
	 * @return Link
	 */
	public function setHref($href)
	{
		$this->href = $href;
		return $this;
	}		
	
    /**
     * Is link to be opened in new window
     * @return bool
     */
    public function isNewWindow() 
    {
        return $this->newWindow;
    }

    /**
     * Get slink style
     * @return FieldStyle|string
     */
    public function getStyle() 
    {
        return $this->style;
    }

    /**
     * Get link type
     * @return string
     */
    public function getType() 
    {
        return $this->type;
    }

    /**
     * Sets whetherto open the link in new window
     * @param bool $newWindow
     * @return $this
     */
    public function setNewWindow($newWindow) 
    {
        $this->newWindow = $newWindow;
        return $this;
    }

    /**
     * Set style
     * @param FieldStyle|string $style
     * @return $this
     */
    public function setStyle($style) 
    {
        $this->style = $style;
        return $this;
    }

    /**
     * Set link type
     * @param string $type
     * @return $this
     */
    public function setType($type) 
    {
        $this->type = $type;
        return $this;
    }

}
