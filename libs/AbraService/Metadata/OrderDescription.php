<?php
namespace Abra\Service;

/**
 * OrderDescription
 */
class OrderDescription
{
    use \Nette\SmartObject;

	/**
	 * Kolekce razeni
	 * @var \Nette\Utils\ArrayList
	 */
	protected $orders;	
	
	/**
	 * Konstruktor 
	 * @param mixed $data Data, ze kterych se objekt naplni
	 */
	public function __construct($data)
	{
		$this->orders = new \Nette\Utils\ArrayList();		
		if(isset($data->Order))
		{
			if(!is_array($data->Order)) $data->Order = array($data->Order);
			foreach($data->Order as $order)
			{
				$o = new Order();
				$o->name = isset($order->Name) ? $order->Name : NULL;
				$o->caption = isset($order->Caption) ? $order->Caption : NULL;
				$this->addOrder($o);
			}
		}
	}
	
	
	/**
	 * Vrati kolekci radku formulare
	 * @return \Nette\Utils\ArrayList
	 */
	public function getOrders()
	{
		return $this->orders;
	}
	
	/**
	 * Prida razeni
	 * @param Order $order razeni
	 * @return OrderDescription Fluent
	 */
	public function addOrder(Order $order)
	{
		$this->orders[] = $order;
		return $this;
	}	
	
}