<?php
namespace Abra\Service;

/**
 * Razeni
 */
class Order
{
    use \Nette\SmartObject;
	
	/**
	 * Nazev razeni
	 * @var string
	 */
	protected $name;
	
	
	/**
	 * Popisek razeni
	 * @var string
	 */
	protected $caption;
	
	
	/**
	 * Nastavi nazev razeni
	 * @param string $name
	 * @return FormField Fluent interface
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}
	
	/**
	 * Vrati nazev razeni
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	
	/**
	 * Nastavi popisek razeni
	 * @param string $caption
	 * @return FormField Fluent interface
	 */
	public function setCaption($caption)
	{
		$this->caption = $caption;
		return $this;
	}
	
	/**
	 * Vrati popisek razeni
	 * @return string
	 */
	public function getCaption()
	{
		return $this->caption;
	}	
	
	
}