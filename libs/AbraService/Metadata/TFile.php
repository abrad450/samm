<?php
/**
 * TFile
 */
class TFile extends \Abra\Service\Entity
{

	/**
	 * @type dtString
	 * @length 10
	 */
	protected $fileName;

	/**
	 * @type dtString
	 * @length 100
	 * @ref TNxCurrency
	 */
	protected $mimeType;

	/**
	 * @type dtString
	 * @length 1000000
	 */
	protected $base64data;

}
