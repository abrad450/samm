<?php
namespace Abra\Service;

/**
 * Popis seznamu (tabulky - vypisu), metadata
 * 
 * @property \Nette\Utils\ArrayList $fields Properties listu
 */
class ListDescription
{
    use \Nette\SmartObject;
	
	/** 
	 * Kolekce polozek listu (tabulky)
	 * @var \Nette\Utils\ArrayList 
	 */
	protected $fields;
	
	
	/**
	 * Vytvori popisny objekt pro tridu $className
	 * @param mixed $data Data, ze kterych se objekt sestavi
	 */
	public function __construct($data)
	{
		$this->fields = new \Nette\Utils\ArrayList();
		if(!is_array($data->field)) $data->field = array($data->field);
		foreach($data->field as $field)
		{
			$lf = new ListField($field);
			$this->addField($lf);
		}		
	}	
	
	
	/**
	 * Vrati kolekci polozek listu
	 * @return \Nette\Utils\ArrayList
	 */
	public function getFields()
	{
		return $this->fields;
	}
	
	
	
	/**
	 * Prida field do listu
	 * @param ClassField $field Field
	 * @return \Abra\Service\ListDescription 
	 */
	public function addField(ListField $field)
	{
		$this->fields[] = $field;
		return $this;
	}	
	
}
