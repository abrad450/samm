<?php
namespace Abra\Service;
/**
 * Vychozi vyjimka ABRA sluzby
 */
class ServiceException extends \Exception
{
	
	/**
	 * Obsah ze sluzby
	 * @var string
	 */
	protected $serviceContent;
	
	/**
	 * Vytvori vyjimku ServiceException
	 * @param string $message
	 * @param int $code
	 * @param string $serviceContent
	 * @param \Exception $previous 
	 */
	public function __construct($message, $code = NULL, $serviceContent = NULL, $previous = NULL)
	{
		parent::__construct($message, $code, $previous);
		$this->serviceContent = $serviceContent;
	}
	
	/**
	 * @return string
	 */
	public function getServiceContent()
	{
		return $this->serviceContent;
	}
	
	/**
	 * @return string
	 */
	public function getServiceContentText()
	{
		return strip_tags($this->serviceContent);
	}

}