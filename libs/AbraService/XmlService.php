<?php
namespace Abra\Service;

use Nette\Utils\Strings;

/**
 * Trida zabezpecujici komunikaci s REST API ABRA
 */
class XmlService extends Service
{

	/**
	 * Konstruktor
	 *
	 * @param array $config Konfigurace
	 */
	public function __construct(array $config, \Nette\Http\Session $session)
	{
		parent::__construct($config, $session);
		$this->contentType = 'text/xml';
	}

	/**
	 * Odesle GET pozadavek s telem ve formatu XML
	 * @param Request $request
	 * @param bool $firstOnly Zda vratit prvni zaznam misto pole zaznamu
	 * @param bool $returnType Zda vratit RAW response (TRUE), nebo zda ji parsovat (FALSE)
	 * @return Response
	 * @throws ServiceException|ResponseException
	 */
	public function get(Request $request, $firstOnly = FALSE, $returnType = self::RETURN_OBJECTS)
	{
		$query = $request->getQuery();
		if(isset($this->token))
		{
			$query['token'] = $this->token;
		}
		// posleme GET pozadavek
		$rawResponse = $this->sendRequest($request->url, 'get', $request->getQuery(), NULL, $request->getHttpHeaders());
		// pokud se ma vratit RAW response, vratime ji a nebudeme parsovat XML
		if($returnType == self::RETURN_RAW) {
            $isXml = FALSE;
            if($this->config['api']) {
                if(substr($rawResponse, 0, 6) === '<?xml ') {
                    $backRawResponse = $rawResponse;
                    $isXml = TRUE;
                    $parsed = $this->parseXml($rawResponse);
                    if(isset($parsed->data)) {
                        $rawResponse = base64_decode($parsed->data);
                    }                    
                    $rawResponse = base64_decode($parsed->data);
                }
                else {
                    $rawResponse = base64_decode($rawResponse);
                }
            }
            $this->onRawResponse(
                    $this, 
                    $isXml, 
                    $request->url, 
                    'post', 
                    '', 
                    (isset($backRawResponse) ? $backRawResponse : $rawResponse), 
                    $this->rawRequestHeaders, 
                    $this->rawResponseHeaders
                );
            return $rawResponse;
        }
		// vratime naparsovana data
		try
		{
			$parsed = $this->parseXml($rawResponse);
		}
		catch(ServiceException $exception)
		{
			$this->onResponseError($this, $exception, $request->url, 'get', '', $rawResponse, $this->rawRequestHeaders, $this->rawResponseHeaders);
			throw $exception;
		}		// auth sekce
		if(isset($parsed->auth)) { $parsed->auth = new Auth($parsed->auth); }
		// pokud se maji vratit PHP defaultni objekty
		if($returnType == self::RETURN_PARSED)
		{
			return $this->createResponse($parsed, $this->rawResponseHeaders, $request->url, 'get', '', $rawResponse, $this->rawRequestHeaders);
		}
		// jinak parsujeme a ziskavame objekty
		$parsedAndCreated = $this->parseAndCreate($parsed, $request, $firstOnly);
		return $this->createResponse($parsedAndCreated, $this->rawResponseHeaders, $request->url, 'get', '', $rawResponse, $this->rawRequestHeaders);
	}


	/**
	 * Provede POST pozadavek s telem ve formatu XML
	 * @param Request $request
	 * @param bool $firstOnly Zda vratit prvni zaznam misto pole zaznamu
	 * @param bool $returnType Zda vratit RAW response (TRUE), nebo zda ji parsovat (FALSE)
	 * @return Response
	 * @throws ServiceException|ResponseException
	 */
	public function post(Request $request, $firstOnly = FALSE, $returnType = self::RETURN_OBJECTS)
	{
		// TVORBA XML
		$xml = new \DOMDocument('1.0', $this->config['encoding']);
		$xml->encoding = $this->config['encoding'];
		$xml->formatOutput = true;
		$xml->preserveWhiteSpace = true;
		$xml->substituteEntities = true;

		$xml->appendChild($r = $xml->createElement('request'));
		// prdame common udaje jako limit, offset, filtry, orders ...
		$this->appendCommon($xml, $r, $request);
		// Pridame elementy s daty
		if(isset($request->data))
		{
			$this->appendData($xml, $r, $request->data);
		}
		// do retezce
		$xmlStringRaw = $xml->saveXML();
		// Remove empty filter & search
		$xmlStringRaw = preg_replace("/\s*<filter-(?:and|or)\/>/umisU",'', $xmlStringRaw);
        $xmlString = preg_replace("/\s*<search\/>/umisU",'', $xmlStringRaw);
        
        // Add GUID
        $guid = '<guid>'.$this->generateGuid().'</guid>';
        $xmlString = preg_replace("/\?>\s*<request>/umisU", "?>\n<request>\n{$guid}\n", $xmlString);
                
		// posleme pozadavek
		$rawResponse = $this->sendRequest($request->url, 'post', $request->getQuery(), $xmlString, $request->getHttpHeaders());
		// pokud se ma vratit RAW response, vratime ji a nebudeme parsovat XML
		if($returnType == self::RETURN_RAW) {
            $isXml = FALSE;
            if($this->config['api']) {
                //$r = '<?xml version="1.0" encoding="UTF-8"? ><response><guid>aaa</guid><data>'.$rawResponse.'</data></response>';
                if(substr($rawResponse, 0, 6) === '<?xml ') {
                    $isXml = TRUE;
                    $backRawResponse = $rawResponse;
                    $parsed = $this->parseXml($rawResponse);
                    if(isset($parsed->data)) {
                        $rawResponse = base64_decode($parsed->data);
                    }
                }
                else {
                    $rawResponse = base64_decode($rawResponse);
                }
            }
            $this->onRawResponse(
                    $this, 
                    $isXml, 
                    $request->url, 
                    'post', 
                    $xmlString, 
                    (isset($backRawResponse) ? $backRawResponse : $rawResponse), 
                    $this->rawRequestHeaders, 
                    $this->rawResponseHeaders
                );
            return $rawResponse;
        }
		// vratime naparsovana data
		try {
			$parsed = $this->parseXml($rawResponse);
		}
		catch(ServiceException $exception) {
			$this->onResponseError($this, $exception, $request->url, 'post', $xmlString, $rawResponse, $this->rawRequestHeaders, $this->rawResponseHeaders);
			throw $exception;
		}        
		// auth sekce
		if(isset($parsed->auth)) { $parsed->auth = new Auth($parsed->auth); }
		// pokud se maji vratit vychozi PHP objekty
		if($returnType == self::RETURN_PARSED)
		{
			return $this->createResponse($parsed, $this->rawResponseHeaders, $request->url, 'post', $xmlString, $rawResponse, $this->rawRequestHeaders);
		}
		// jinak parsujeme a vracime objekty
		$parsedAndCreated = $this->parseAndCreate($parsed, $request, $firstOnly);
		return $this->createResponse($parsedAndCreated, $this->rawResponseHeaders, $request->url, 'post', $xmlString, $rawResponse, $this->rawRequestHeaders);
	}


	/**
	 * Prida ke XML dokumentu do uzlu $r filtry, orders atd
	 * @param \DOMDocument $xml
	 * @param \DOMElement $r
	 * @param Request $request
	 */
	protected function appendCommon(\DOMDocument $xml, \DOMElement $r, Request $request)
	{
        if(!$this->config['api']) {
            if(!empty($this->token))
            {
                $r->appendChild($auth = $xml->createElement('auth'));
                $auth->appendChild($token = $xml->createElement('token', $this->token));
            }
        }
		// vyhledavani ?
		$r->appendChild($xml->createElement('search', empty($request->search) ? "" : htmlspecialchars($request->search, NULL, "UTF-8")));

		// filtr (stringovy nebo objektovy)
		if(!empty($request->filterXml))
		{
			$xmlFilter = new \DOMDocument();
			$xmlFilter->loadXML($request->filterXml);
			$r->appendChild($xml->importNode($xmlFilter->documentElement, true));
		}
		elseif(!empty($request->filters))
		{
			$r->appendChild($filters = $xml->createElement('filters'));
			$this->appendFilter($xml, $filters, $request->filters);
		}
		// razeni ?
		if(!empty($request->orderBy))
		{
			$r->appendChild($orderBy = $xml->createElement('orderby'));
			foreach($request->orderBy as $order)
			{
				$orderBy->appendChild($o = $xml->createElement('order'));
				$o->appendChild($xml->createElement('col', htmlentities($order->column)));
				$o->appendChild($xml->createElement('dir', $order->direction));
			}
		}
		// LIMIT
		if($request->limit !== NULL)
		{
			$r->appendChild($xml->createElement('limit', $request->limit));
		}
		// LIMIT
		if($request->offset !== NULL)
		{
			$r->appendChild($xml->createElement('from', $request->offset));
		}
		// GROUP BY
		if(!empty($request->groupBy))
		{
			$r->appendChild($groupBy = $xml->createElement('groupby'));
			foreach($request->groupBy as $column)
			{
				$groupBy->appendChild($xml->createElement('col', htmlentities($column)));
			}
		}
	}


	/**
	 * Prida sekci filtr
	 * @param type $xml
	 * @param type $node
	 * @param FilterGroup $group
	 */
	protected function appendFilter($xml, $node, FilterGroup $group)
	{
		// pokud ma skupina nejake filtry, pridame ji, jinak je redundantni
		$filters = $group->getFilters();
		if(!empty($filters))
		{
			$xmlGroup = $xml->createElement('filter-'.$group->getOperator());
			$node->appendChild($xmlGroup);
			foreach($filters as $filter)
			{
				if($filter instanceof FilterGroup)
				{
					$this->appendFilter($xml, $xmlGroup, $filter);
				}
				else
				{
					$xmlGroup->appendChild($f = $xml->createElement('filter'));
					$f->appendChild($xml->createElement('name', htmlentities($filter->column)));
					$f->appendChild($xml->createElement('value', htmlspecialchars($filter->value, NULL, "UTF-8")));
					$f->appendChild($xml->createElement('operator', $filter->operator));
				}
			}
		}
	}


	/**
	 * Prevede strukturu dat na retezec
	 * @param mixed $data DATA, ktera se prevedou na retezec
	 * @param DOMElement
	 * @return bool TRUE, pokud se vytvoreny element ma pridat do XML nebo FALSE, pokud e
	 */
	protected function appendData(\DOMDocument $xml, \DOMElement $r, $data, $parent = NULL)
	{
		/* @var $parent \DOMElement */
		$result = TRUE;
//		if($data !== null)
//		{
			if(is_object($data) || is_array($data) || $data instanceof \IteratorAggregate || $data instanceof \Iterator || $data instanceof \ArrayAccess)
			{
				$result = FALSE;
				foreach($data as $key => $value)
				{
					// pokud je klic pouze ciselny, jedna se urcite o kolekci
					// pokud je hodnota object ... tak todo
					if(is_numeric($key))
					{
						if(is_object($value) && !empty($value->__class__))
						{
							$parts = explode('\\', $value->__class__);
							$className = end($parts);
							$collectionElement = NULL;
							foreach($parent->childNodes as $v)
							{
								if($v->nodeName == $r->nodeName)
								{
									$collectionElement = $v;
									break;
								}
							}
							if($collectionElement === NULL)
							{
								$collectionElement = $xml->createElement($r->nodeName);
								$parent->appendChild($collectionElement);
							}
							$itemElement = $xml->createElement($className);
							$res = $this->appendData($xml, $itemElement, $value, $collectionElement);
							if($res) $collectionElement->appendChild($itemElement);
						}
						// jinak pouzijeme nadrzazeny element s nadrzenym klicem
						else
						{
							if($key != '__class__')
							{
								$node = $xml->createElement($r->nodeName);
								$res = $this->appendData($xml, $node, $value, $r);
								// pokud se vratilo FALSE, nebudeme pridavat prazdny element na konec kolekce ... je to tu potreba ?
								if($res) $parent->appendChild($node);
							}
						}
					}
					// pokud je klic textovy pouzijeme ho jako element
					else
					{
						if($key != '__class__')
						{
							$node = $xml->createElement($key);
							$res = $this->appendData($xml, $node, $value, $r);
							// pokud se vratilo FALSE, nebudeme pridavat prazdny element na konec kolekce
							if($res) $r->appendChild($node);
							$result = TRUE;
						}
					}
				}
			}
			elseif(is_scalar($data))
			{
				if(strtolower($this->config['encoding']) == 'utf-8')
				{
					$textData = $data;
				}
				else
				{
					$textData = iconv('utf-8', $this->config['encoding'].'//IGNORE', $data);
				}
				if($r->nodeName != '__class__')
				{
                    //$textDataEscaped = str_replace(array("'", '"'), array('&apos;', '&quot;'), $textData);
                    $node = $xml->createTextNode($textData);
					$r->appendChild($node);
				}
			}
			elseif($data === NULL)
			{
				if($r->nodeName != '__class__')
				{
					$r->appendChild($node = $xml->createTextNode(''));
				}
			}
			else
			{
				throw new ServiceException('Invalid input structure. Cannot create XML', 500);
			}
//		}
		return $result;
	}

	/**
	 * Prevede retezec na strukturu dat
	 * @param string $string Retezec, ze ktereho se vyparsuje datova struktura
	 * @return stdClass|array
	 * @throws ServiceException
	 */
	protected function parseXml($string)
	{
		$prev = libxml_use_internal_errors(true);
		$data = simplexml_load_string($string);
		$errors = libxml_get_errors();
		if(isset($errors[0]))
		{
//			dump($string); dump($data); dump($errors); die;
			$error = $errors[0];
			throw new ServiceException($error->message, $error->code, $string);
		}
		libxml_use_internal_errors($prev);
		if(is_bool($data))
		{
			throw new ServiceException('ABRA returned an empty response. Response headers: '.var_export($this->rawResponseHeaders, true), 500);
		}
		return $this->simpleXmlConvert($data);
	}


	/**
	 * Rekurzivne projde SimpleXML DOM a prevede ho na pole poli poli ...
	 * @param SimpleXMLElement $element Pocatecni element
	 * @return stdClass
	 */
	protected function simpleXmlConvert(\SimpleXMLElement $element, $path='', $level=1)
	{
		if($element->count() > 0)
		{
			$nodeNames = array();
			$children = new \stdClass();
			foreach($element->children() as $name => $node)
			{
				// v datech a errorech se zaznamy vrati vzdy jako pole, i kdyz tam bude jen jeden
				if(($path == '/data' || $path == '/errors') && $level == 2)
				//if(Strings::startsWith($path, '/data/') || $path == '/data' || Strings::startsWith($path, '/errors/') || $path == '/errors')
				{
					if(!isset($children->{$name})) $children->{$name} = array();
					$children->{$name}[] = $this->simpleXmlConvert($node, $path.'/'.$name, $level+1);
				}
				// jinde se nejprve zalozi zaznam samotny a pokud je druhy - stejny, zalozi se teprve pak jako pole
				else
				{
					$nodeNames[$name] = isset($nodeNames[$name]) ? ++$nodeNames[$name] : 1;
                    
                    $subNode = $this->simpleXmlConvert($node, $path.'/'.$name, $level+1);
                    // atributy pro uzel (nekompatibilni s polem poli poli ...) budou v @node => array('atribut' => 'hodnota')
                    if($node->attributes()->count() > 0)
                    {
                        $subNode->__attributes = [];
                        foreach($node->attributes() as $attr => $value) {
                            $subNode->__attributes[$attr] = trim($value->__toString());
                        }
                    }
                    
					if($nodeNames[$name] == 1)
					{
						$children->$name = $subNode;
					}
					else
					{
						if($nodeNames[$name] == 2)
						{
                            $children->{$name} = array($children->{$name});
						}
						$children->{$name}[] = $subNode;
					}
				}

			}
			return $children;
		}
        return htmlspecialchars_decode(trim($element->__toString()), ENT_QUOTES); //  | ENT_XML1
	}
    
    
    /**
     * Generate GUID
     * 
     * @return string 
     */
    protected function generateGuid()
    {
        if(function_exists('com_create_guid')) {
            $guid = com_create_guid();
        }
        else {
            mt_srand((double)microtime()*10000); // optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12)
                .chr(125);// "}"
            $guid = $uuid;
        }
        return str_replace(array('{','}'), array('',''), $guid);
    }
    
}