<?php
namespace Abra\Service;

/**
 * Odpoved od sluzby
 * @property int $total Celkovy pocet vyhovujicich zaznamu
 * @property int $from Od ktereho zaznamu se vratila odpoved
 * @property int $to Do ktereho zaznamu se odpoved vratila
 * @property mixed $auth Auth sekce
 * @property mixed $data Data
 * @property int $errorCode Kod chyby
 * @property string $errorMessage Chybova zprava
 * @property bool $error Zda nastala chyba
 * @property array $responseHeaders Pole respnse hlavicek z CURL
 */
class Response
{
    use \Nette\SmartObject;

	/**
	 * Celkovy pocet vyhovujicich zaznamu
	 * @var int
	 */
	protected $total;
	
	/**
	 * Od ktereho zaznamu se vratil vystup
	 * @var int 
	 */
	protected $from;
	
	/**
	 * Do ktereho zaznamu se vratil vystup
	 */
	protected $to;
	
	
	
	/**
	 * Autentifikacni sekce
	 * @var Auth
	 */
	protected $auth;
	
	/**
	 * Data
	 * @var mixed
	 */
	protected $data;

	/**
	 * Kolekce chyb
	 * @var \Nette\Utils\ArrayList
	 */
	protected $errors;
		
	
	/**
	 * Pole hlavicek obdrzenych z CURL odpovedi
	 * @var array
	 */
	protected $responseHeaders;
	
	/**
	 * Vytvori objekt response
	 * @param mixed $input Obsah odpovedi (rozparsovany do struktury)
	 * @param array $responseHeaders Hlavicky odpovedi
	 * @throws ResponseException Vyjimka pri vytvareni response
	 */
	public function __construct($input, $responseHeaders)
	{
		$this->errors = new \Nette\Utils\ArrayList();
		// header
		$this->to = isset($input->header->to) ? $input->header->to : null;
		$this->from = isset($input->header->from) ? $input->header->from : null;
		$this->total = isset($input->header->total) ? $input->header->total : null;		
		// body
		$this->data = isset($input->data) ? $input->data : null;
		// auth
		$this->auth = isset($input->auth) ? $input->auth : null;
		// error
		if(!empty($input->errors->error))
		{
			foreach($input->errors->error as $err)
			{
				$error = new Error();
				if(isset($err->code)) $error->code = $err->code;
				if(isset($err->message)) $error->message = $err->message;
				$this->errors[] = $error;
			}
		}
		
		// pokud se vratila chyba, vyhodime patricnou vyjimku
		if($this->isError()) throw new ResponseException($this);
		
		// http hlavicky
		$this->responseHeaders = $responseHeaders;
	}
	
	/**
	 * Celkovy pocet vyhovujicich zaznamu
	 * @return int
	 */
	public function getTotal()
	{
		return $this->total;	
	}

	/**
	 * Od ktereho zaznamu
	 * @return int
	 */
	public function getFrom()
	{
		return $this->from;	
	}

	/**
	 * Do ktereho zaznamu
	 * @return int
	 */
	public function getTo()
	{
		return $this->to;	
	}
	

	/**
	 * Auth sekce
	 * @return mixed
	 */
	public function getAuth()
	{
		return $this->auth;	
	}
	
	
	/**
	 * DATA
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;	
	}
	
	/**
	 * Nastavi data
	 * @param array $data 
	 */
	public function setData($data)
	{
		$this->data = $data;
	}
	
	
	/**
	 * Pole chyb
	 * @return Arraylist
	 */
	public function getErrors()
	{
		return $this->errors;
	}
	
	/**
	 * Byl tam error?
	 * @return bool
	 */
	public function isError()
	{
		return $this->errors->count() > 0;
	}
	
	
	/**
	 * Vrati referenci na pole obsahujioci hlavicky odpovedi
	 * @return array
	 */
	public function &getResponseHeaders()
	{
		return $this->responseHeaders;
	}
	
}