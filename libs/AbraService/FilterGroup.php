<?php
namespace Abra\Service;

use \Nette\Utils\Json;

/**
 * Filtracni skupina (AND / OR)
 * 
 * @property string $operator Operator
 * @property-read string $filters Filters
 */
class FilterGroup implements \IteratorAggregate
{
    use \Nette\SmartObject;

	/**
	 * AND
	 */
	const OPERATOR_AND = 'and';

	/**
	 * OR
	 */
	const OPERATOR_OR = 'or';



	/**
	 * operator filtru ve skupine
	 */
	protected $operator;

	/**
	 * Filtry
	 * @var array
	 */
	protected $filters;



	/**
	 * Vrati operator
	 * @return string
	 */
	public function getOperator()
	{
		return $this->operator;
	}


	/**
	 * Nastavi operator pro skupinu
	 * @param string $operator Operator
	 */
	public function setOperator($operator)
	{
		$this->operator = $operator;
	}


	/**
	 * Vrati vnorenou skupinu filtru a skupin filtru
	 * @return array
	 */
	public function &getFilters()
	{
		return $this->filters;
	}



	/**
	 * Vytvori skupinu filtracnich podminek
	 * @param string $operator Operator mezi pdminkami
	 */
	public function __construct($operator)
	{
		$this->operator = $operator;
		$this->filters = array();
	}


	/**
	 * Prida filtr
	 * @param string $column Sloupec
	 * @param mixed $value Hodnota
	 * @param string $operator Operator
     * @param string $live Live
	 * @return FilterGroup Vrati
	 */
	public function addFilter($column, $value, $operator = '=', $live = NULL)
	{
		$filter = new Filter($column, $value, $operator, $live);
		$this->filters[] = $filter;
		return $this;
	}

	/**
	 * Prida skupinu filtru
	 * @param string $operator Operator
	 * @return FilterGroup Reference na nove vlozenou slupinu filtru
	 */
	public function addFilterGroup($operator)
	{
		$group = new FilterGroup($operator);
		$this->filters[] = $group;
		return $group;
	}


	/**
	 * Iterator
	 * @return \Abra\Service\ArrayIterator
	 */
	public function getIterator()
	{
		return new \ArrayIterator($this->filters);
	}

	/**
	 * Filters To JSON
	 * @param bool $json Return as JSON string ?
	 * @return string
	 */
	public function toArray($json = FALSE)
	{
		$fg = array(
			'operator' => $this->operator,
			'filters' => array()
		);
		foreach($this->filters as $f)
		{
			$fg['filters'][] = $f->toArray();
		}
		return $json ? Json::encode($fg) : \Nette\Utils\ArrayHash::from($fg);
	}


	/**
	 * Create From Data
	 * @param mixed $data
	 */
	public static function createFrom($data)
	{
		if(!isset($data->operator) || !isset($data->filters))
		{
			throw new \Nette\InvalidArgumentException('FilterGroup musí obsahovat vlastnost "operator" a vlastnost "filters".');
		}
		$logic = strtolower($data->operator) === FilterGroup::OPERATOR_OR ? FilterGroup::OPERATOR_OR : FilterGroup::OPERATOR_AND;
		$fg = new static($logic);
		foreach($data->filters as $filter)
		{

			// Rekurzivni vkladani FilterGroup do pole filters v ramci jine FilterGroup nepodporuje ani ABRA, tedy to nyni neni nutne :) 2015-07-27

			if(!isset($filter->value) || !isset($filter->column))
			{
				throw new \Nette\InvalidArgumentException('Filter musí obsahovat vlastnost "value" a vlastnost "column".');
			}
			$op = isset($filter->operator) ? $filter->operator : '=';
			$fg->addFilter($filter->column, $filter->value, $op, isset($filter->live) ? $filter->live : NULL);
		}
		return $fg;
	}

	/**
	 * Parse and crete filter structure
	 *
	 * @param stdClass|array $data
	 * @param string $operator
	 * @return FilterGroup
	 */
	public static function parseFrom($data, $operator = NULL)
	{
		if(!is_array($data))
		{
			$data = Json::decode(Json::encode($data), Json::FORCE_ARRAY);
		}
		if(isset($data['filter-and']))
		{
			$operator = 'and';
			$filtersArray = $data['filter-and'];
		}
		elseif(isset($data['filter-or']))
		{
			$operator = 'or';
			$filtersArray = $data['filter-or'];
		}
		else
		{
			$filtersArray = $data;
		}
		if(empty($operator))
		{
			throw new \Nette\InvalidArgumentException('Operator not detected!');
		}
		// Create group
		$fg = new static($operator);
		// Loop through filtersArray and create filters
		foreach($filtersArray as $filter)
		{
			if(isset($filter['filter-and']) || isset($filter['filter-or']))
			{
				$fg->parseFilterGroup($filter);
			}
			else
			{
				$fg->addFilter($filter['name'], $filter['value'], $filter['operator']);
			}
		}
		return $fg;
	}

	/**
	 * Add filter group
	 *
	 * @param stdClass|array $filter
	 * @return FilterGroup newsly created filtergroup
	 */
	public function parseFilterGroup($filter)
	{
		$fg = FilterGroup::parseFrom($filter);
		$this->filters[] = $fg;
		return $fg;
	}


	/**
	 * Evaluate filters for given data
	 *
	 * @param Entity $data
	 * @return bool TRUE/FALSE
	 */
	public function evaluateFor($data)
	{
		$results = array();
		foreach($this->filters as $filter)
		{
			/* @var $filter Filter */
			// Find the right value from data
			$pathParts = explode('.', $filter->column);
			$finalValue = $data;
			foreach($pathParts as $part)
			{
				if(property_exists($data, $part))
				{
					$finalValue = $data->{$part};
				}
			}
			if(is_scalar($finalValue) || $finalValue instanceof \DateTime)
			{
				$results[] = $filter->evaluateFor($finalValue);
			}
			else
			{
				// Pokud se nenajde konecna skalarni hodnota nebo DateTime, tak FALSE !
				$results[] = FALSE;
			}
		}
		// AND = all results must be TRUE
		if(strtolower($this->operator) === 'and')
		{
			foreach($results as $res)
			{
				if(!$res)
				{
					return FALSE;
				}
			}
			return TRUE;
		}
		// otherwise OR operator is user ... at least one must be true
		foreach($results as $res)
		{
			if($res)
			{
				return TRUE;
			}
		}
		return FALSE;
	}

}