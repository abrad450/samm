<?php
namespace Abra\Service;

/**
 * EntityLoader
 */
class EntityLoader
{
    use \Nette\SmartObject;

	/**
	 * Path
	 * @var string
	 */
	protected $path;

	/**
	 * Constructor
	 *
	 * @param string $path
	 */
	public function __construct($path)
	{
		$this->path = $path;
		spl_autoload_register([$this, 'load'], TRUE, FALSE);
	}

	/**
	 * Load Entity
	 *
	 * @param  bool  prepend autoloader?
	 * @return self
	 */
	public function load($class)
	{
		$nsExpand = array_values(array_filter(explode('\\', $class)));
		$filePath = DIRECTORY_SEPARATOR.implode(DIRECTORY_SEPARATOR, $nsExpand).'.php';
		if(is_file($this->path.$filePath))
		{
			require_once $this->path.$filePath;
		}
	}

}