<?php
namespace Abra\Service;

/**
 * Filter
 *
 * @property string $column Column
 * @property mixed $value Value
 * @property string $live Value
 * @property string	$operator Operator
 */
class Filter
{
    use \Nette\SmartObject;

	/**
	 * Sloupec
	 * @var string
	 */
	protected $column;

	/**
	 * Hodnota
	 * @var string
	 */
	protected $value;

	/**
	 * Hodnota
	 * @var string
	 */
	protected $live;

	/**
	 * Operator pro filtr
	 * @var string
	 */
	protected $operator;



	/**
	 * Vrati sloupec
	 * @return string
	 */
	public function getColumn()
	{
		return $this->column;
	}

	/**
	 * Nastavi sloupec
	 * @param string $column Sloupec
	 */
	public function setColumn($column)
	{
		$this->column = $column;
	}



	/**
	 * Vrati sloupec
	 * @return string
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * Nastavi hodnotu
	 * @param string $value Hodnota
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}



	/**
	 * Vrati sloupec
	 * @return string
	 */
	public function getLive()
	{
		return $this->live;
	}

	/**
	 * Nastavi hodnotu
	 * @param string $value Hodnota
	 */
	public function setLive($value)
	{
		$this->live = $value;
	}




	/**
	 * Vrati operator
	 * @return string
	 */
	public function getOperator()
	{
		return $this->operator;
	}

	/**
	 * Nastavi operator
	 * @param string $operator Operator
	 */
	public function setOperator($operator)
	{
		$this->operator = $operator;
	}


	/**
	 * Konstruktor
	 * @param type $column Sloupec
	 * @param type $value Hodnota
	 * @param type $operator Operator (=, <=, >=, <, >, IN ...)
     * @param type $live
	 */
	public function __construct($column, $value, $operator = '=', $live = NULL)
	{
		$this->column = $column;
		$this->value = $value;
		$this->operator = $operator;
        $this->live = $live;
	}

	/**
	 * Filters To JSON
	 * @param bool $json TRUE = konvertovat vystup do JSONu
	 * @return string
	 */
	public function toArray($json = FALSE)
	{
		$f = array(
			'column' => $this->column,
			'value' => $this->value,
            'live' => $this->live,
			'operator' => $this->operator
		);
		return $json ? Json::encode($f) : $f;
	}

	/**
	 * Evaluate this filter for given value
	 *
	 * @param mixed $value
	 * @return bool
	 */
	public function evaluateFor($value)
	{
		// Convert value from ABRA to PHP
		$thisValue = $this->value;
		if(is_bool($value))
		{
			$thisValue = $this->value == 'A' ? TRUE : FALSE;
		}
		if($value instanceof \DateTime)
		{
			$thisValue = \DateTime::createFromFormat('Y-m-d', $this->value);
		}
		switch($this->operator)
		{
			case '=':
			case '==':
				return $value == $thisValue;
			case '<>':
			case '!=':
				return $value != $thisValue;
			case '>':
				return $value > $thisValue;
			case '<':
				return $value < $thisValue;
			case '>=':
				return $value >= $thisValue;
			case '<=':
				return $value <= $thisValue;
			default:
				return FALSE;
		}
	}

}