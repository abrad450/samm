<?php

namespace Abra\Service;

use \Nette\Reflection\Property;

/**
 * Predek pro vsechny objekty ABRY
 */
abstract class Entity implements \Iterator
{
    use \Nette\SmartObject;

	/**
	 * Pole properties objektu
	 * @var array(array(ReflectionProperty))
	 */
	protected static $__properties = array();

	/**
	 * Pole identit objektu
	 * @var array
	 */
	protected static $__identities = array();
	protected $__rawContent = NULL;
	protected $__entityNamespace = NULL;

	/**
	 * Parent Entita (pouziva se v pripade psani komentare k entite nebo uploadu souboru k entite)
	 *
	 * @var Entity
	 */
	public $_Parent;

	/**
	 * nastavi namespace entity
	 * @param string $ns namespace entity
	 */
	public function setEntityNamespace($ns)
	{
		$this->__entityNamespace = $ns;
	}

	/**
	 * Vrati entity namespace
	 * @return string
	 */
	public function getEntityNamespace()
	{
		return $this->__entityNamespace;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		if(isset($this->DisplayName))
		{
			return $this->DisplayName;
		}
		if(isset($this->Name))
		{
			return $this->Name;
		}
		if(isset($this->Code))
		{
			return $this->Code;
		}
		return $this->__rawContent;
	}

	// -------------------------------------------------------------------------------

	/**
	 * Objekt, jehoz data se naparsuji na vytvarenou tridu
	 * @param stdClass|arary $data Obecny objekt nebo pole hodnot
	 * @param string $namespace Jmenny prostor
	 */
	public function __construct($data = NULL, $namespace = NULL)
	{
		$this->__entityNamespace = $namespace;
		$class = get_class($this);
		$this->__rawContent = ''; // '[' . $class . ']'; Někdy se může stát, že TOUR pošle prázdný objekt s ID 000000000 bez DisplayName, tak se chovat, jako by nic
		if(!isset(self::$__properties[$class]))
		{
			self::buildDescription($class);
		}
		if($data !== NULL)
		{
			$this->parse($data, FALSE);
		}
	}

	/**
	 * Vrati ID (tj. hodnotu property takto oznacene)
	 * @return mixed
	 * @throws EntityException Objekt nema identitu ?
	 */
	public function getId()
	{
		$prop = isset(self::$__identities[get_class($this)]) ? self::$__identities[get_class($this)] : null;
		if($prop === NULL)
		{
			throw new EntityException('Object of class "' . get_class($this) . '" has no ID property.', 500, get_class($this));
		}
		$id = $this->$prop;
		// ID nebude prazdny string
		return $id === '' ? NULL : $id;
	}

	/**
	 * Vrati property, ktera jednoznacne identifikuje entitu
	 * @return string
	 * @throws EntityException Objekt nema identitu ?
	 */
	public function getIdProperty()
	{
		if(!isset(self::$__identities[get_class($this)]))
		{
			throw new EntityException('Object of class "' . get_class($this) . '" has no ID property.', 500, get_class($this));
		}
		return self::$__identities[get_class($this)];
	}

	/**
	 * Vrati TRUE pokud ma objekt jednoznacnou identitu, FALSE, pokud ne
	 * @return bool
	 */
	public function hasId()
	{
		return isset(self::$__identities[get_class($this)]);
	}

	/**
	 * Vrati rekurzivne hodnoty objektu v jednorozmernem poli
	 * @param string $prefix Prefix, ktery se da klicum
	 * @return array
	 */
	public function toArray($prefix = '', $separator = '_')
	{
		$arr = array();
		foreach($this as $property => $value)
		{
			if($value instanceof Entity)
			{
				$arr = array_merge($arr, $value->toArray($property . $separator, $separator));
			}
			else
			{
				$arr[$prefix . $property] = $value;
			}
		}
		return $arr;
	}

	/**
	 * Vrati objekt s hodnotami naformatovanymi pro pouziti v ABRA Service ale filtrovanymi tak,
	 * aby neobsahovaly promenne, ktere se neodeslaly formularem
	 * @param ArrayHash $values
	 * @param bool $idsOnly
	 * @return stdClass Trida
	 */
	public function toAbraFiltered($values, $idsOnly = FALSE)
	{
		$all = $this->toAbra($idsOnly);
		return $this->filterAbraData($all, $values);
	}

	/**
	 * Metoda prefiltruje data tak, aby vratila strukturu pouze toho, co bylo odeslano z formulare
	 * @param mixed $all
	 * @param mixed $form
	 */
	protected function filterAbraData($all, $form)
	{
        $formArrayCopy = $form->getIterator()->getArrayCopy();
        
		$flt = new \stdClass();
		foreach($all as $prop => $val)
		{
			if($prop === '__class__')
			{
                $flt->{$prop} = $val;
                continue;
            }
            
			if(!array_key_exists($prop, $formArrayCopy))
			{
				continue;
			}
            
			if(is_scalar($val))
			{
				$flt->{$prop} = $val;
			}
			elseif(is_array($val))
			{
				$arrFiltered = array();
				foreach($val as $index => $arrVal)
				{
					$arrFiltered[$index] = $this->filterAbraData($arrVal, $form->{$prop}[$index]);
				}
				$flt->{$prop} = $arrFiltered;
			}
			elseif($val instanceof \TFile)
			{
				/* @var $val \TFile */
				$val2 = $val->toAbra();
				unset($val2->__class__);
				$flt->{$prop} = $val2;
			}
			else
			{
				$flt->{$prop} = $this->filterAbraData($val, $form->{$prop});
			}
		}
		return $flt;
	}

	/**
	 * Vrati objekt s hodnotami naformatovanymi pro pouziti v ABRA Service
	 * @param bool $idsOnly Zda vratit pouze ID vnorenych objektu misto celych objektu
	 * @return stdClass Trida
	 */
	public function toAbra($idsOnly = FALSE)
	{
		$description = $this->getDescription();
		$obj = new \stdClass();
		$obj->__class__ = get_class($this);
		foreach($this as $property => $value)
		{
			$target = $obj;
			// Je to NULL ?  nema smysl dal pokracovat
			if($value === NULL)
			{
				$target->{$property} = $value;
				continue;
			}
			$classField = $description->fields[$property];
			/* @var $classField ClassField */
			// Je-li to reference na objekt a hodnotou je potomek Entity
			if(!empty($classField->ref) && $value instanceof Entity)
			{
				if($idsOnly)
				{
					$target->{$property} = $value->getId();
				}
				else
				{
					$target->{$property} = $value->toAbra();
				}
			}
			// je-li to kolekce objektu
			elseif(!empty($classField->list))
			{
				$collection = array();
				foreach($value as $val)
				{
					if($val instanceof Entity)
					{
						$collection[] = $val->toAbra();
					}
				}
				$target->{$property} = $collection;
			}
			else
			{
				// podle datoveho typu provedeme validaci / konverzi
				$value = self::sanitizeValue($classField->type, $value, FALSE);
				$target->{$property} = $value;
			}
		}
		return $obj;
	}

	/**
	 * Vrati popis properties tohoto objektu
	 * @return ClassDescription
	 */
	public function getDescription()
	{
		return self::$__properties[get_class($this)];
	}

	/**
	 * Naplni objekt z dat odeslanych formularem (tz. jednorozmerne pole klic => hodnota
	 * @param array $data Data z formulare
	 * @param string $prefix Prefix klicu v poli
	 * @param string $separator Oddelovac entit v klicich
	 * @param bool $clear Zda vyprazdnit predchozi obsah properties
	 * @return Entity
	 */
	public function parseArray($data, $prefix = '', $separator = '_', $clear = TRUE)
	{
		$props = self::$__properties[get_class($this)];
		// vyprazdnit ??? naplnime NULL hodnoty
		if($clear)
		{
			foreach($props as $prop => $propData)
			{
				$this->$prop = NULL;
			}
		}
		$prefixLen = strlen($prefix);
		$ignoreList = array(); // ktere properties se budou pri vicenasobnem pruchodu ignorovat
		foreach($data as $key => $value)
		{
			$property = $prefixLen > 0 ? substr($key, $prefixLen) : $key;
			// pokud je to primo property, naplnime ji
			if(isset($props[$property]))
			{
				$this->$property = $value;
			}
			// pokud to primo property neni, bude to nejaky vnoreny objekt nebo LIST, rozparsujeme podle $separator a vyzkousime
			else
			{
				$parts = explode($separator, $property);
				$propParts = array();
				foreach($parts as $part)
				{
					$propParts[] = $part;
					$propTest = implode($separator, $propParts);
					if(isset($props[$propTest]))
					{
						if(isset($props[$propTest]['ref']))
						{
							$class = $props[$propTest]['ref'];
							$subData = array();
							$propTestLen = strlen($propTest);
							foreach($data as $sdKey => $sdVal)
							{
								if(substr($sdKey, 0, $propTestLen) == $propTest)
								{
									$subData[$sdKey] = $sdVal;
								}
							}
							$obj = new $class();
							$obj->parseArray($subData, $propTest . $separator, $separator, FALSE);
							$this->$propTest = $obj;
						}
						if(isset($props[$propTest]['list']))
						{
							if(isset($ignoreList[$propTest]))
							{
								break;
							}
							$ignoreList[$propTest] = 1;
							$type = $props[$propTest]['list'];
							$scalarList = FALSE;
							// pokud je definovan listkey, je tam skalarni hodnota pod timto klicem
							if($props[$propTest]['listkey'])
							{
								$scalarList = TRUE;
							}
							// pokud je to skalarni list, vybereme vsechny s indexem, ktere nejsou zaroven nejakymi properties a naplnime je
							if($scalarList)
							{
								$subData = array();
								$propTestLen = strlen($propTest . $separator);
								foreach($data as $sdKey => $sdVal)
								{
									if(substr($sdKey, 0, $propTestLen) == $propTest . $separator && !isset($props[$sdKey]))
									{
										//$k = substr($sdKey,$propTestLen);
										// ciselne indexy od NULY
										$subData[] = $sdVal;
									}
								}
								$this->$propTest = $subData;
							}
							// je to list objektu, pro kazdy objekt zavolame instanci tridy a jeji naplneni
							else
							{

								// TODO - kolekce objektu - instanciovat stejne jako u REF, akorat v poli
								// vyuzit TYPE - zde je trida k instanci
							}
						}
						break;
					}
				}
			}
		}
		return $this;
	}

	/**
	 * Pole hodnot, nebo objekt, ze ktereho se naplni tento konkretni objekt
	 * @param stdClass|array $data
	 * @param bool $clear Zda vyprazdnit predchozi obsah properties
	 * @return Entity
	 */
	public function parse($data, $clear = TRUE)
	{
		$description = $this->describeEntity(get_class($this));
		// vyprazdnit ??? naplnime NULL hodnoty
		if($clear)
		{
			foreach($description->fields as $name => $cf)
			{
				$this->$name = NULL;
			}
		}
		// pokud je v datech neco pouzitelneho, naplnime
		if(is_array($data) || $data instanceof \stdClass || $data instanceof \Traversable)
		{
			foreach($data as $key => $value)
			{
				$this->parseValue($description, $key, $value);
			}
		}
		else
		{
			// Byl odeslan pouze stringovy obsah a nikoliv objekt
			$this->__rawContent = $data;
		}
		return $this;
	}

	// -----------------------------------------------------------------------------------

	/**
	 * Naplni jednu property
	 * @param ClassDescription $description Description
	 * @param string $key klic
	 * @param mixed $value
	 * @throws EntityException
	 */
	protected function parseValue($description, $key, $value)
	{
		$parseVal = TRUE;
		// je to odkaz na jinou tridu ?
		if(!empty($description->fields[$key]) && !empty($description->fields[$key]->ref) && is_object($value))
		{
			$parseVal = FALSE;
			$innerClass = $description->fields[$key]->ref;
			$innerClassNs = $innerClass;
			if($this->__entityNamespace !== NULL)
			{
				$innerClassNs = $this->__entityNamespace . '\\' . $innerClassNs;
			}
			if(!class_exists($innerClassNs))
			{
				throw new EntityException('Class "' . $innerClass . '" is not defined.', 500, $innerClass);
			}
			$o = new $innerClassNs($value, $this->__entityNamespace);
			$this->{$key} = $o;
		}
		if(!empty($description->fields[$key]) && !empty($description->fields[$key]->list))
		{
			$parseVal = FALSE;
			$collectionType = $description->fields[$key]->list;
			$collectionTypeNs = $collectionType;
			if($this->__entityNamespace !== NULL)
			{
				$collectionTypeNs = $this->__entityNamespace . '\\' . $collectionTypeNs;
			}
			$isClass = class_exists($collectionTypeNs);
			// Pole objektu (bezejmena kolekce)
			$collection = array();
			// pokud se vrati prazdna kolekce, tak predame jen prazdne pole
			if(!empty($value))
			{
				$arr = $value->{key($value)};
				if(!is_array($arr))
					$arr = array($arr);
				// naplnime kolekci
				foreach($arr as $v)
				{
					// je-li typ dat v kolekci trida a je-li to objekt, vytvorime / jinak vlozime jen hodnotu
					if(is_object($v))
					{
						if(!$isClass)
						{
							throw new EntityException('Class "' . $collectionType . '" is not defined.', 500, $collectionType);
						}
						$o = new $collectionTypeNs($v, $this->__entityNamespace);
						$collection[] = $o;
					}
					else
					{
						$collection[] = self::sanitizeValue($collectionType, $v);
					}
				}
			}
			$this->{$key} = $collection;
		}
		if(!empty($description->fields[$key]) && $parseVal)
		{
			// pokud je 'ref' a hodnotou je "0000000000", tak je to NULL (ABRA NULL FIX)
			if(isset($description->fields[$key]->ref) && $value === "0000000000")
			{
				$value = NULL;
			}
			// sanitizace hodnoty
			$this->{$key} = self::sanitizeValue($description->fields[$key]->type, $value);
		}
	}

	/**
	 * Sanitizuje a vrati sanitizovanou hodnotu podle predaneho typu
	 * @param string $type Jednoduchy datovy typ ABRY
	 * @param mixed|null $value Hodnota nebo NULL
	 * @param bool $fromService Zda se sanitizuje ze sluzby (TRUE) nebo do sluzby (FALSE)
	 * @return mixed|null Sanitizovane hodnota, nebo NULL
	 */
	public static function sanitizeValue($type, $value, $fromService = TRUE)
	{
		if($value === NULL)
		{
			return NULL;
		}
		// podle datoveho typu provedeme validaci / konverzi
		switch($type)
		{

			case 'dtDate':
				if($value != '')
				{
					$value = date('Y-m-d', strtotime($value)); //$this->parseTime($value);
				}
				break;

			// datum / cas
			case 'dtDateTime':
				if($value != '')
				{
					$value = date('Y-m-d H:i:s', strtotime($value)); //$this->parseTime($value);
				}
				break;

			// boolean A / N
			case 'dtBoolean':
				if($fromService)
				{
					$value = strtoupper($value) == 'A' ? true : false;
				}
				else
				{
					$value = $value ? 'A' : 'N';
				}
				break;

			// signed integer 32 bit / 64 bit
			case 'dtInteger':
				if($value != '')
				{
					$value = intval($value);
				}
				break;

			// float
			case 'dtFloat':
				if($fromService)
				{
					if($value != '')
					{
						$value = floatval($value);
					}
				}
				else
				{
					$value = str_replace(array(' ', ','), array('', '.'), $value);
				}
				break;

			// Viceradkove textove pole - predelat LF na CRLF (2014-09-15)
			case 'dtNote':
				if($fromService)
				{
					$value = preg_replace("/\r\n/i", "\n", $value);
				}
				else
				{
					$value = preg_replace("/\n/i", "\r\n", $value);
				}
				break;

			// @fixme dalsi pripadne typy
		}
		return $value;
	}

	/**
	 * Vytvori pole properties a ulozi ho do staticke promenne $properties
	 */
	protected static function buildDescription($class)
	{
		$props = array();
		$properties = \Nette\Reflection\ClassType::from($class)->getProperties();
		$description = new ClassDescription($class);
		$idSet = false;
		foreach($properties as $prop)
		{
			/* @var $prop Property */
			// pokud je to Entity, nebo zacina podtrzitkem, neni to BO
			if($prop->class == 'Entity' || \Nette\Utils\Strings::startsWith($prop->name, '_'))
			{
				continue;
			}
			$annotations = $prop->getAnnotations();
			// pokud nema nastaveny TYPE, neni to BO property
			if(empty($annotations['type']))
			{
				continue;
			}
			array_walk($annotations, function(&$val) {
				if(!isset($val[1]))
				{
					$val = $val[0];
				}
			});
			$props[$prop->name] = $annotations;
			if(!$idSet && !empty($annotations['id']))
			{
				self::$__identities[$class] = $prop->name;
				$idSet = true;
			}
			// vytvoreni ClassFieldu
			$field = new ClassField();
			$field->name = $prop->name;
			$field->size = isset($annotations['length']) ? $annotations['length'] : NULL;
			$field->list = isset($annotations['list']) ? $annotations['list'] : NULL;
			$field->ref = isset($annotations['ref']) ? $annotations['ref'] : NULL;
			$field->type = isset($annotations['type']) ? $annotations['type'] : NULL;
			$field->encoding = isset($annotations['encoding']) ? $annotations['encoding'] : 'windows-1250';
			$description->addField($field);
		}
		self::$__properties[$class] = $description;
	}

	/**
	 * Prevede excelovsky cas do timestampu
	 * @param float $excel
	 * @return uint UNix Timestamp tehoz casu
	 */
	protected function parseTime($excel)
	{
		return AbraServiceHelpers::parseExcelTime($excel);
	}

	/**
	 * Prevede UNix timestamp na excelovsky cas
	 * @param uint $timestamp Timestamp
	 * @return float excelovsky cas
	 */
	protected function buildTime($timestamp)
	{
		return AbraServiceHelpers::buildExcelTime($timestamp);
	}

	// -----------------------------------------------------------------------------------

	/**
	 * Vytvori ABRA objekt z obecneho objektu (prvni property = nazev tridy)
	 * @param stdClass $object Object s jedinnou property shodnou s nazvem tridy, jejiz instance bude vytvorena
	 * @param string $entityClass Entity class
	 * @param string $namespace Jmenny prostor
	 * @return \Entity
	 */
	static public function create(\stdClass $object, $entityClass = NULL, $namespace = NULL)
	{
		reset($object);
		$property = $className = key($object);
		// Vynucena trida ?
		if($entityClass !== NULL)
		{
			$className = $entityClass;
		}
		// pridame namespace
		$classNameNs = $className;
		if($namespace !== NULL)
		{
			$classNameNs = $namespace . '\\' . $classNameNs;
		}
		// FIX pro mnozne cislo v anglictine
		if(!class_exists($classNameNs))
		{
			if(substr($classNameNs, -3) == 'ies')
			{
				$classNameNs = substr($classNameNs, 0, -3) . 'y';
			}
			elseif(substr($classNameNs, -1) == 's')
			{
				$classNameNs = substr($classNameNs, 0, -1);
			}
		}
		// pokud ani jednotne cislo neni, tak je zle
		if(!class_exists($classNameNs))
		{
			throw new EntityException("Class '{$className}' is not declared.", 500, $className);
		}
		if(is_array($object->$property))
		{
			$arr = array();
			foreach($object->$property as $obj)
			{
				$obj = new $classNameNs($obj, $namespace);
				if($obj->hasId() && $obj->getId() !== NULL)
				{
					$arr[$obj->getId()] = $obj;
				}
				else
				{
					$arr[] = $obj;
				}
			}
			return $arr;
		}
		$o = new $classNameNs($object->$property, $namespace);
		return $o;
	}

	/**
	 * Vytvori objekt z predaneho pole
	 * @param array $data Data z formulare
	 * @param string $prefix Prefix klicu v poli
	 * @param string $separator Oddelovac entit v klicich
	 * @param string $namespace namespace
	 * @return Entity
	 */
	static public function createFromArray($data, $prefix = '', $separator = '_', $namespace = NULL)
	{
		$class = get_called_class();
		$classNs = $class;
		if($namespace !== NULL)
		{
			$classNs = $namespace . '\\' . $classNs;
		}
		$object = new $classNs();
		$object->setEntityNamespace($namespace);
		$object->parseArray($data, $prefix, $separator, FALSE);
		return $object;
	}

	/**
	 * Vytvori objekty z dat formulare
	 * @param \Nette\Utils\ArrayHash $data Data deslana z formulare
	 * @param string $class Trida
	 * @param string $namespace Jmenny prostor
	 * @return Entity
	 */
	static public function createFromForm($data, $class = NULL, $namespace = NULL)
	{
		if($class === NULL)
		{
			$class = get_called_class();
		}
		else
		{
			$class = $namespace !== NULL ? $namespace . '\\' . $class : $class;
		}
		$description = self::describeEntity($class);
		$obj = new $class();
		if($data === NULL)
		{
			//$data = array();
			//dump($data); dump($description); die;
		}
		foreach($data as $property => $value)
		{
			if(!isset($description->fields[$property]))
			{
				continue;
			}

			// Je-li hodnota NULL, poslu prazdny string
			if($value === NULL)
			{
				$value = '';
				//dump($value); dump($property); die;
			}
			// je to reference na jiny objekt a mam ho k dispozici v odeslanych hodnotach z formu ?
			if(!empty($description->fields[$property]->ref) && !is_scalar($value))
			{
				$obj->{$property} = self::createFromForm($value, $description->fields[$property]->ref, $namespace);
			}
			elseif(!empty($description->fields[$property]->list) && !is_scalar($value))
			{
				$collection = new \Nette\Utils\ArrayHash();
				foreach($value as $index => $val)
				{
					if(count($val) > 0)
					{
						$collection[$index] = self::createFromForm($val, $description->fields[$property]->list, $namespace);
					}
				}
				$obj->{$property} = $collection;
			}
			else
			{
				$obj->{$property} = $value;
			}
		}
		return $obj;
	}

	/**
	 * Vrati popis entity
	 * @param string $class Trida (i s namespacem)
	 * @return ClassDescription
	 */
	static public function describeEntity($class)
	{
		if(!isset(self::$__properties[$class]))
		{
			self::buildDescription($class);
		}
		return self::$__properties[$class];
	}

	/**
	 * Vytahne metadata property
	 * @param string $class Trida
	 * @param string $property Vlastnost, nebo cesta k vlastnosti pres vlastnene objekty (kolekci)
	 * @param string $namespace namespace
	 * @return ClassField
	 * @throws \Exception Pokud se nenalezne popis, vyhodi se vyjimka
	 */
	static public function describeField($class, $property, $namespace = NULL)
	{
		$description = Entity::describeEntity($namespace . '\\' . $class);
		$path = explode('.', $property);
		$props = NULL;
		do
		{
			$property = array_shift($path);
			// indexy vynechame, v description jsou definovane vzdy rovnou polozky bez indexu (tedy jedna polozka)
			if(is_numeric($property))
			{
				continue;
			}
			// jinak si pres teckovou notaci vyhledavame objekt
//			dump($namespace.'\\'.$class);
//			dump($description->fields);
//			dump($property);
//			echo '----------------------<br />';
			if(isset($description->fields[$property]))
			{
				$props = $description->fields[$property];
				if(!empty($props->ref))
				{
					$class = $props->ref;
					$description = Entity::describeEntity($namespace . '\\' . $props->ref);
				}
				if(!empty($props->list))
				{
					$class = $props->list;
					$description = Entity::describeEntity($namespace . '\\' . $props->list);
				}
			}
			else
			{
				throw new \Exception('property "' . $property . '" is not defined in "' . $class . '"');
			}
		}
		while(count($path) > 0);
		return $props;
	}

	/**
	 * Vrati property, ktera jednoznacne identifikuje entitu
	 * @return string
	 * @throws EntityException Objekt nema identitu ?
	 */
	static public function getClassId($class)
	{
		if(!isset(self::$__properties[$class]))
		{
			self::buildDescription($class);
		}
		if(!isset(self::$__identities[$class]))
		{
			throw new EntityException('Object of class "' . $class . '" has no ID property.', 500, $class);
		}
		return self::$__identities[$class];
	}

	/**
	 * Getter
	 * @param type $name Nazev property
	 * @return mixed
	 */
	public function &__get($name)
	{
		if(property_exists($this, $name))
		{
			return $this->$name;
		}
        $val = NULL;
		return $val; //parent::__get($name);
	}

	/**
	 * Setter
	 * @param string $name nazev property
	 * @param mixed $value hodnota
	 */
	public function __set($name, $value)
	{
		if(property_exists($this, $name))
		{
			$this->$name = $value;
		}
		else
		{
		//	parent::__set($name, $value);
		}
	}

	/**
	 * Existuje nastavena property ?
	 * @param string $name Proeprty
	 * @return bool
	 */
	public function __isset($name)
	{
		if(property_exists($this, $name))
		{
			return true;
		}
		return FALSE;// parent::__isset($name);
	}

	// ------------------------------------------------------------------------------- ITERATOR INTERFACE -------------------------------------

	public function current()
	{
		return $this->{key(self::$__properties[get_class($this)]->fields)};
	}

	public function key()
	{
		return key(self::$__properties[get_class($this)]->fields);
	}

	public function next()
	{
		return next(self::$__properties[get_class($this)]->fields);
	}

	public function rewind()
	{
		reset(self::$__properties[get_class($this)]->fields);
	}

	public function valid()
	{
		return key(self::$__properties[get_class($this)]->fields) !== null;
	}

	// ---------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * Vrati data pro teckami oddelenou konecnou property
	 * @param string $propertyPath Cesta pro dotaz na hodnotu
	 * @return mixed
	 */
	public function getPropertyPathData($propertyPath)
	{
		$parts = explode('.', $propertyPath);
		$val = $this;
		foreach($parts as $part)
		{
			if(!is_object($val) || !property_exists($val, $part))
			{
				return NULL;
			}
			$val = $val->{$part};
		}
		return $val;
	}

	/**
	 * Vrati rekurzivne hodnoty objektu jako pole poli poli
	 * @return array
	 */
	public function toAssoc()
	{
		$arr = array();
		foreach($this as $property => $value)
		{
			if($value instanceof Entity)
			{
				$arr[$property] = $value->toAssoc();
			}
			else
			{
				$arr[$property] = $value;
			}
		}
		return $arr;
	}

}
