<?php
namespace Abra\Service;

/**
 * Helpery
 * @author ONTU
 */
class AbraServiceHelpers
{
	
	/**
	 * Prevede excelovsky cas do timestampu
	 * @param float $excel 
	 * @return uint UNix Timestamp tehoz casu
	 */
	public static function parseExcelTime($excel)
	{
		$timestamp = 86400 * ($excel - 25569) - date_offset_get(new \DateTime);
		return sprintf('%u', round($timestamp));
	}
	
	/**
	 * Prevede UNix timestamp na excelovsky cas
	 * @param uint $timestamp Timestamp
	 * @return float excelovsky cas
	 */
	public static function buildExcelTime($timestamp)
	{
		return 25569 + ($timestamp + date_offset_get(new \DateTime)) / 86400;
	}
	
}