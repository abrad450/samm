<?php

namespace SammTranslator;

use Nette;
use Nette\Utils\Strings;


class FileManager
{
    use \Nette\SmartObject;
    
    /** @var array { [ key => default ] } */
    private $defaultMetadata = array(
        'Project-Id-Version' => '',
        'Report-Msgid-Bugs-To' => NULL,
        'POT-Creation-Date' => '',
        'Last-Translator' => '',
        'Language-Team' => '',
        'MIME-Version' => '1.0',
        'Content-Type' => 'text/plain; charset=UTF-8',
        'Content-Transfer-Encoding' => '8bit',
        'Plural-Forms' => 'nplurals=3; plural=((n==1) ? 0 : (n>=2 && n<=4 ? 1 : 2));',
        'X-Poedit-Language' => NULL,
        'X-Poedit-Country' => NULL,
        'X-Poedit-SourceCharset' => NULL,
        'X-Poedit-KeywordsList' => NULL,
        'Language' => NULL,
    );

    /**
     * Some basic language codes
     * @var array
     */
    private $defaultLanguageCodes = array(
        'en' => 'en_US',
        'cs' => 'cs_CZ',
        'cz' => 'cs_CZ',
        'de' => 'de_DE',
        'pl' => 'pl',
        'sr' => 'sr_CS',
        'pt' => 'pt_PT',
        'es' => 'es_ES',
        'no' => 'no_NO',
        'it' => 'it_IT',
        'fr' => 'fr_FR',
        'fi' => 'fi_FI',
        'nl' => 'nl_NL',
        'zh' => 'zh_CN',
        'hr' => 'hr_HR',
        'ru' => 'ru_RU',
        'uk' => 'uk_UA',
        'ro' => 'ro_RO',
        'he' => 'he_IL',
        'el' => 'el_GR',
        'da' => 'da_DK',
        'bg' => 'bg_BG',
        'ar' => 'ar_SA',
        'sl' => 'sl_SI',
        'sv' => 'sv_SE',
        'tr' => 'tr_TR',
        'vi' => 'vi_VN'
    );
    
    /**
     * Default plural forms for different languages
     * @var array
     */
    private $defaultPluralForms = array(
        'ach' => 'nplurals=2; plural=(n > 1);',
        'af'  => 'nplurals=2; plural=(n != 1);',
        'ak'  => 'nplurals=2; plural=(n > 1);',
        'am'  => 'nplurals=2; plural=(n > 1);',
        'an'  => 'nplurals=2; plural=(n != 1);',
        'anp' => 'nplurals=2; plural=(n != 1);',
        'ar'  => 'nplurals=6; plural=(n==0 ? 0 : (n==1 ? 1 : (n==2 ? 2 : (n%100>=3 && n%100<=10 ? 3 : (n%100>=11 ? 4 : 5)))));',
        'arn' => 'nplurals=2; plural=(n > 1);',
        'as'  => 'nplurals=2; plural=(n != 1);',
        'ast' => 'nplurals=2; plural=(n != 1);',
        'ay'  => 'nplurals=1; plural=0;',
        'az'  => 'nplurals=2; plural=(n != 1);',
        'be'  => 'nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : (n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2));',
        'bg'  => 'nplurals=2; plural=(n != 1);',
        'bn'  => 'nplurals=2; plural=(n != 1);',
        'bo'  => 'nplurals=1; plural=0;',
        'br'  => 'nplurals=2; plural=(n > 1);',
        'brx' => 'nplurals=2; plural=(n != 1);',
        'bs'  => 'nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : (n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2));',
        'ca'  => 'nplurals=2; plural=(n != 1);',
        'cgg' => 'nplurals=1; plural=0;',
        'cs'  => 'nplurals=3; plural=(n==1) ? 0 : ((n>=2 && n<=4) ? 1 : 2);',
        'cz'  => 'nplurals=3; plural=(n==1) ? 0 : ((n>=2 && n<=4) ? 1 : 2);',
        'csb' => 'nplurals=3; plural=(n==1) ? 0 : (n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);',
        'cy'  => 'nplurals=4; plural=(n==1) ? 0 : ((n==2) ? 1 : ((n != 8 && n != 11) ? 2 : 3));',
        'da'  => 'nplurals=2; plural=(n != 1);',
        'de'  => 'nplurals=2; plural=(n != 1);',
        'doi' => 'nplurals=2; plural=(n != 1);',
        'dz'  => 'nplurals=1; plural=0;',
        'el'  => 'nplurals=2; plural=(n != 1);',
        'en'  => 'nplurals=2; plural=(n != 1);',
        'eo'  => 'nplurals=2; plural=(n != 1);',
        'es'  => 'nplurals=2; plural=(n != 1);',
        'es_AR' => 'nplurals=2; plural=(n != 1);',
        'et'  => 'nplurals=2; plural=(n != 1);',
        'eu'  => 'nplurals=2; plural=(n != 1);',
        'fa'  => 'nplurals=2; plural=(n > 1);',
        'ff'  => 'nplurals=2; plural=(n != 1);',
        'fi'  => 'nplurals=2; plural=(n != 1);',
        'fil' => 'nplurals=2; plural=(n > 1);',
        'fo'  => 'nplurals=2; plural=(n != 1);',
        'fr'  => 'nplurals=2; plural=(n > 1);',
        'fur' => 'nplurals=2; plural=(n != 1);',
        'fy'  => 'nplurals=2; plural=(n != 1);',
        'ga'  => 'nplurals=5; plural=n==1 ? 0 : (n==2 ? 1 : ((n>2 && n<7) ? 2 : ((n>6 && n<11) ? 3 : 4)));',
        'gd'  => 'nplurals=4; plural=(n==1 || n==11) ? 0 : ((n==2 || n==12) ? 1 : ((n > 2 && n < 20) ? 2 : 3));',
        'gl'  => 'nplurals=2; plural=(n != 1);',
        'gu'  => 'nplurals=2; plural=(n != 1);',
        'gun' => 'nplurals=2; plural=(n > 1);',
        'ha'  => 'nplurals=2; plural=(n != 1);',
        'he'  => 'nplurals=2; plural=(n != 1);',
        'hi'  => 'nplurals=2; plural=(n != 1);',
        'hne' => 'nplurals=2; plural=(n != 1);',
        'hr'  => 'nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : (n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2));',
        'hu'  => 'nplurals=2; plural=(n != 1);',
        'hy'  => 'nplurals=2; plural=(n != 1);',
        'ia'  => 'nplurals=2; plural=(n != 1);',
        'id'  => 'nplurals=1; plural=0;',
        'is'  => 'nplurals=2; plural=(n%10!=1 || n%100==11);',
        'it'  => 'nplurals=2; plural=(n != 1);',
        'ja'  => 'nplurals=1; plural=0;',
        'jbo' => 'nplurals=1; plural=0;',
        'jv'  => 'nplurals=2; plural=(n != 0);',
        'ka'  => 'nplurals=1; plural=0;',
        'kk'  => 'nplurals=2; plural=(n != 1);',
        'kl'  => 'nplurals=2; plural=(n != 1);',
        'km'  => 'nplurals=1; plural=0;',
        'kn'  => 'nplurals=2; plural=(n != 1);',
        'ko'  => 'nplurals=1; plural=0;',
        'ku'  => 'nplurals=2; plural=(n != 1);',
        'kw'  => 'nplurals=4; plural=(n==1) ? 0 : ((n==2) ? 1 : ((n == 3) ? 2 : 3));',
        'ky'  => 'nplurals=2; plural=(n != 1);',
        'lb'  => 'nplurals=2; plural=(n != 1);',
        'ln'  => 'nplurals=2; plural=(n > 1);',
        'lo'  => 'nplurals=1; plural=0;',
        'lt'  => 'nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : (n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2));',
        'lv'  => 'nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : (n != 0 ? 1 : 2));',
        'mai' => 'nplurals=2; plural=(n != 1);',
        'me'  => 'nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : (n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);',
        'mfe' => 'nplurals=2; plural=(n > 1);',
        'mg'  => 'nplurals=2; plural=(n > 1);',
        'mi'  => 'nplurals=2; plural=(n > 1);',
        'mk'  => 'nplurals=2; plural= n==1 || n%10==1 ? 0 : 1; Can’t be correct needs a 2 somewhere',
        'ml'  => 'nplurals=2; plural=(n != 1);',
        'mn'  => 'nplurals=2; plural=(n != 1);',
        'mni' => 'nplurals=2; plural=(n != 1);',
        'mnk' => 'nplurals=3; plural=(n==0 ? 0 : (n==1 ? 1 : 2));',
        'mr'  => 'nplurals=2; plural=(n != 1);',
        'ms'  => 'nplurals=1; plural=0;',
        'mt'  => 'nplurals=4; plural=(n==1 ? 0 : (n==0 || ( n%100>1 && n%100<11) ? 1 : ((n%100>10 && n%100<20 ) ? 2 : 3)));',
        'my'  => 'nplurals=1; plural=0;',
        'nah' => 'nplurals=2; plural=(n != 1);',
        'nap' => 'nplurals=2; plural=(n != 1);',
        'nb'  => 'nplurals=2; plural=(n != 1);',
        'ne'  => 'nplurals=2; plural=(n != 1);',
        'nl'  => 'nplurals=2; plural=(n != 1);',
        'nn'  => 'nplurals=2; plural=(n != 1);',
        'no'  => 'nplurals=2; plural=(n != 1);',
        'nso' => 'nplurals=2; plural=(n != 1);',
        'oc'  => 'nplurals=2; plural=(n > 1);',
        'or'  => 'nplurals=2; plural=(n != 1);',
        'pa'  => 'nplurals=2; plural=(n != 1);',
        'pap' => 'nplurals=2; plural=(n != 1);',
        'pl'  => 'nplurals=3; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2));',
        'pms' => 'nplurals=2; plural=(n != 1);',
        'ps'  => 'nplurals=2; plural=(n != 1);',
        'pt'  => 'nplurals=2; plural=(n != 1);',
        'pt_BR' => 'nplurals=2; plural=(n > 1);',
        'rm'  => 'nplurals=2; plural=(n != 1);',
        'ro'  => 'nplurals=3; plural=(n==1 ? 0 : ((n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2));',
        'ru'  => 'nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : (n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2));',
        'rw'  => 'nplurals=2; plural=(n != 1);',
        'sah' => 'nplurals=1; plural=0;',
        'sat' => 'nplurals=2; plural=(n != 1);',
        'sco' => 'nplurals=2; plural=(n != 1);',
        'sd'  => 'nplurals=2; plural=(n != 1);',
        'se'  => 'nplurals=2; plural=(n != 1);',
        'si'  => 'nplurals=2; plural=(n != 1);',
        'sk'  => 'nplurals=3; plural=(n==1) ? 0 : ((n>=2 && n<=4) ? 1 : 2);',
        'sl'  => 'nplurals=4; plural=(n%100==1 ? 0 : (n%100==2 ? 1 : (n%100==3 || n%100==4 ? 2 : 3)));',
        'so'  => 'nplurals=2; plural=(n != 1);',
        'son' => 'nplurals=2; plural=(n != 1);',
        'sq'  => 'nplurals=2; plural=(n != 1);',
        'sr'  => 'nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : (n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2));',
        'su'  => 'nplurals=1; plural=0;',
        'sv'  => 'nplurals=2; plural=(n != 1);',
        'sw'  => 'nplurals=2; plural=(n != 1);',
        'ta'  => 'nplurals=2; plural=(n != 1);',
        'te'  => 'nplurals=2; plural=(n != 1);',
        'tg'  => 'nplurals=2; plural=(n > 1);',
        'th'  => 'nplurals=1; plural=0;',
        'ti'  => 'nplurals=2; plural=(n > 1);',
        'tk'  => 'nplurals=2; plural=(n != 1);',
        'tr'  => 'nplurals=2; plural=(n > 1);',
        'tt'  => 'nplurals=1; plural=0;',
        'ug'  => 'nplurals=1; plural=0;',
        'uk'  => 'nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : (n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2));',
        'ur'  => 'nplurals=2; plural=(n != 1);',
        'uz'  => 'nplurals=2; plural=(n > 1);',
        'vi'  => 'nplurals=1; plural=0;',
        'wa'  => 'nplurals=2; plural=(n > 1);',
        'wo'  => 'nplurals=1; plural=0;',
        'yo'  => 'nplurals=2; plural=(n != 1);',
        'zh'  => 'nplurals=1; plural=0;',
        'zh2' => 'nplurals=2; plural=(n > 1);',
    );
    

    /**
     * @param string
     * @param array
     * @param string Language code - for proper plural forms
     * @return array
     */
    public function generateMetadata($identifier, $currentMetadata, $lang)
    {
        $result = array();
        $result[] = 'PO-Revision-Date: ' . date('Y-m-d H:iO');

        foreach ($this->defaultMetadata as $key => $default) {
            
            if($key === 'Plural-Forms') {
                $default = isset($this->defaultPluralForms[$lang]) ? $this->defaultPluralForms[$lang] : $default;
            }
            
            if($key === 'Language' || $key === 'X-Poedit-Language') {
                $default = isset($this->defaultLanguageCodes[$lang]) ? $this->defaultLanguageCodes[$lang] : $default;
            }
            
            if (isset($currentMetadata[$identifier][$key])) {
                $result[] = $key . ': ' . $currentMetadata[$identifier][$key];

            } elseif ($default) {
                $result[] = $key . ': ' . $default;
            }
        }

        return $result;
    }


    /**
     * @param string
     * @param string
     * @param array
     * @return array
     */
    public function parseMetadata($input, $identifier, $metadata = array())
    {
        $inputLines = preg_split('/[\n,]+/', trim($input));
        foreach ($inputLines as $inputLine) {
            $pattern = ': ';
            $tmp = preg_split("($pattern)", $inputLine);
            $metadata[$identifier][trim($tmp[0])] = count($tmp) > 2 ? ltrim(strstr($inputLine, $pattern), $pattern) : $tmp[1];
        }

        return $metadata;
    }


    /**
     * Build PO file string
     * 
     * @param string $identifier (system/user)
     * @param array $metadata Metadata for lang
     * @param type $dictionary While translations dictionary
     * @param type $newStrings New strings from session (not stored yet)
     * @param type $addNewStrings Whether to add New strings at the end
     * @param type $variantCount If present, msgstr will be repeated $variantCount-times as array
     * @param array $excludeTypes Which types will be excluded from the string
     * @return string PO file content
     */
    public function buildPOFileString(
            $identifier, 
            $metadata, 
            $dictionary, 
            $newStrings, 
            $addNewStrings = FALSE, 
            $variantCount = NULL,
            $excludeTypes = array()
        )
    {
        $po = "# Gettext keys exported by SammTranslator and Translation Panel\n" .
                "# Created: " . date('Y-m-d H:i:s') . "\n" .
                'msgid ""' . "\n" .
                'msgstr ""' . "\n";
        $po .= '"' . implode('\n"' . "\n" . '"', $metadata) . '\n"' . "\n\n\n";

        foreach ($dictionary as $message => $data) {
            // napr. neexistuje uzivatelsky preklad teto fraze, preskocime ji
            if(empty($data[$identifier])) { // user => NULL
                continue;
            }

            $po .= 'msgid "' . str_replace(array('"'), array('\"'), $message) . '"' . "\n";

            //if (is_array($data['original']) && count($data['original']) > 1) {
            if($variantCount !== NULL) {
                $po .= 'msgid_plural "' . str_replace(array('"'), array('\"'), end($data['original'])) . '"' . "\n";
                $i = 0;
                foreach ($data[$identifier] as $string) {
                    $po .= 'msgstr[' . $i . '] "' . str_replace(array('"'), array('\"'), $string) . '"' . "\n";
                    $i++;
                }
            }
            else {
                if (!is_array($data[$identifier])) {
                    $po .= 'msgstr "' . str_replace(array('"'), array('\"'), $data[$identifier]) . '"' . "\n";
                }
                $po .= 'msgstr "' . str_replace(array('"'), array('\"'), current($data[$identifier])) . '"' . "\n";
            }
            $po .= "\n";
        }

        if($addNewStrings) {
            if (count($newStrings)) {
                foreach ($newStrings as $originalArray) {
                    
                    $original = isset($originalArray['data']) ? $originalArray['data'] : $originalArray;
                    $type = isset($originalArray['type']) ? $originalArray['type'] : Gettext::TRANSLATION_TYPE_TEXT;
                    
                    if(!in_array($type, $excludeTypes)) {
                    
                        $po .= '#. TRANSLATORS: Type='.$type."\n";

                        if (trim(current($original)) != "" && !\array_key_exists(current($original), $dictionary)) {
                            $po .= 'msgid "' . str_replace(array('"'), array('\"'), current($original)) . '"' . "\n";

                            if($variantCount !== NULL) {
                                $po .= 'msgid_plural "' . str_replace(array('"'), array('\"'), end($original)) . '"' . "\n";
                                for($vi = 0; $vi < $variantCount; $vi++) {
                                    $po .= 'msgstr[' . $vi . '] ""' . "\n";                                
                                }
                                $po .= "\n";
                            }
                            else {
                                $po .= "msgstr \"\"\n";
                                $po .= "\n";
                            }
                        }
                    }
                }
            }
        }
        return $po;
    }
    
    /**
     * @param string
     * @param string
     * @param array
     * @param array
     * @param string
     */
    public function buildPOFile($file, $identifier, $metadata, $dictionary, $newStrings)
    {
        $po = $this->buildPOFileString($identifier, $metadata, $dictionary, $newStrings);
        // Save PO File
        file_put_contents($file, $po);
    }


    /**
     * @param string
     * @param string
     * @param array
     * @param array
     */
    public function buildMOFile($file, $identifier, $metadata, $dictionary)
    {
        ksort($dictionary);
        $metadataImploded = implode("\n", $metadata);
        
        // Count of items with $identifies (system/user)
        $items = 1;
        foreach ($dictionary as $key => $value) {
            // napr. neexistuje uzivatelsky preklad teto fraze, preskocime ji
            if(!empty($value[$identifier])) { 
                $items++;
            }
        }
        //$items = count($dictionary) + 1;

        $ids = Strings::chr(0x00);
        $strings = $metadataImploded . Strings::chr(0x00);
        $idsOffsets = array(0, 28 + $items * 16);
        $stringsOffsets = array(array(0, strlen($metadataImploded)));

        foreach ($dictionary as $key => $value) {
            // napr. neexistuje uzivatelsky preklad teto fraze, preskocime ji
            if(empty($value[$identifier])) { // user => NULL
                continue;
            }
                        
            $id = $key;
            if (is_array($value['original']) && count($value['original']) > 1) {
                $id .= Strings::chr(0x00) . end($value['original']);
            }

            $string = implode(Strings::chr(0x00), $value[$identifier]);
            $idsOffsets[] = strlen($id);
            $idsOffsets[] = strlen($ids) + 28 + $items * 16;
            $stringsOffsets[] = array(strlen($strings), strlen($string));
            $ids .= $id . Strings::chr(0x00);
            $strings .= $string . Strings::chr(0x00);
        }

        $valuesOffsets = array();
        foreach ($stringsOffsets as $offset) {
            list ($all, $one) = $offset;
            $valuesOffsets[] = $one;
            $valuesOffsets[] = $all + strlen($ids) + 28 + $items * 16;
        }
        $offsets = array_merge($idsOffsets, $valuesOffsets);

        $mo = pack('Iiiiiii', 0x950412de, 0, $items, 28, 28 + $items * 8, 0, 28 + $items * 16);
        foreach ($offsets as $offset) {
            $mo .= pack('i', $offset);
        }
        
        // Save MO File
        file_put_contents($file, $mo . $ids . $strings);
    }
    
    
    /**
     * Reads PO file
     * 
     * @param string $file
     */
    public function readPOFile($file)
    {
        $parser = new \PoParser\Parser();
        $parser->read($file);
        return array(
            'headers' => $parser->getHeaders(),
            'entries' => $parser->getEntries()
        );
    }

}
