<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SammTranslator;

/**
 * Description of FormTranslator
 *
 * @author ondre
 */
class FormTranslator implements \Nette\Localization\ITranslator
{
    
    /**
     * SAMM translator
     * 
     * @var \SammTranslator\Gettext
     */
    protected $sammTranslator;
    
    /**
     * Constructor
     * 
     * @param \SammTranslator\Gettext $sammTranslator
     */
    public function __construct(\SammTranslator\Gettext $sammTranslator)
    {
        $this->sammTranslator = $sammTranslator;
    }

    
    /**
     * Translate form string
     * 
     * @param string $message
     * @param int $count
     */
    public function translate($message, ...$parameters): string
    {
        $count = $parameters[0] ?? NULL;
        $back = debug_backtrace(~DEBUG_BACKTRACE_PROVIDE_OBJECT, 3);
        $type = Gettext::TRANSLATION_TYPE_TEXT;
        
        if(count($back) >= 3) {
            $class1arr = explode('\\', $back[1]['class']);
            $class1 = strtolower(end($class1arr));
            $func1 = strtolower($back[1]['function']);
            
            $class2arr = explode('\\', $back[2]['class']);
            $class2 = strtolower(end($class2arr));
            $func2 = strtolower($back[2]['function']);
            
            // Label form. prvku
            if($class2 === 'basecontrol' && $func2 === 'getlabel') {
                $type = Gettext::TRANSLATION_TYPE_FORM_LABEL;
            }
            
            // Hodnoty selectboxu
            if($class2 === 'selectbox' && $func2 === 'getcontrol') {
                $type = Gettext::TRANSLATION_TYPE_FORM_VALUE;
            }
            
            // validacni pravidla
            if($class1 === 'validator' && $func1 === 'formatmessage') {
                $type = Gettext::TRANSLATION_TYPE_FORM_VALIDATION;
            }
        }
                
        return $this->sammTranslator->typedTranslate($type, $message, $count);
    }

}