<?php

namespace SammTranslator;

use Nette;
use Nette\Caching\Cache;
use Nette\Utils\Strings;


/**
 * @property-read array $dictionary
 * @property-read int $variantsCount
 * @property string $lang
 */
class Gettext implements Nette\Localization\ITranslator
{
    use \Nette\SmartObject;
    
    /** Typ prekladu = popisek form. pole */
    const TRANSLATION_TYPE_FORM_LABEL = 'form_label';
    /** Typ prekladu = hodnota ve formulari (selectboxes ...) */
    const TRANSLATION_TYPE_FORM_VALUE = 'form_value';
    /** Typ prekladu = zprava validacniho pravidla */
    const TRANSLATION_TYPE_FORM_VALIDATION = 'form_validation';
    /** Typ prekladu = obecny text */
    const TRANSLATION_TYPE_TEXT = 'text';
    
    
    /** @var FileManager */
    public $fileManager;

    /* @var string */
    public static $namespace = 'SammTranslator-Gettext';

    /** @var array */
    protected $files = array();

    /** @var string */
    protected $lang;

    /** @var array */
    private $dictionary = array();

    /** @var array */
    private $metadata = array();

    /** @var bool */
    private $productionMode;

    /** @var bool */
    private $loaded = FALSE;

    /** @var Nette\Http\SessionSection */
    private $sessionStorage;

    /** @var Nette\Caching\Cache */
    private $cache;

    /** @var Nette\Http\Response */
    private $httpResponse;

    /** 
     * User DIFF key for multiple translation files (multiple ABRA connections)
     * 
     * @var string
     */
    private $userDiff = NULL;
    
    /**
     * Constructor
     * 
     * @param \stdClass $config Configuration
     * @param Nette\Http\Session $session
     * @param Nette\Caching\IStorage $cacheStorage
     * @param Nette\Http\Response $httpResponse
     * @param \SammTranslator\FileManager $fileManager
     */
    public function __construct(\stdClass $config, Nette\Http\Session $session, Nette\Caching\IStorage $cacheStorage, Nette\Http\Response $httpResponse, FileManager $fileManager)
    {
        $this->sessionStorage = $sessionStorage = $session->getSection(self::$namespace);
        $this->cache = new Nette\Caching\Cache(new Nette\Caching\Storages\DevNullStorage(), self::$namespace);
        $this->httpResponse = $httpResponse;
        $this->fileManager = $fileManager;
        
        $this->addFile($config->systemFiles, 'system');
        $this->addFile($config->userFiles, 'user');
        $this->setLang($config->lang);
        
        // diff for user file by connection name stored in user section
        if(!empty($config->userDiffByConnection)) {
            $ssUser = $session->getSection('user');
            $this->userDiff = empty($ssUser['connection']) ? NULL : $ssUser['connection'];
        }
    }

    /**
     * Get dictionary
     * @return array
     */
    public function getDictionary()
    {
        $this->loadDictonary();
        return $this->dictionary;
    }
    
    /**
     * Get PO file content
     * 
     * @param bool $includingNewStrings Include new strings ?
     * @param bool $excludeTypes Which types to exclude from the string
     * @return string
     */
    public function getPOFileContent($includingNewStrings = TRUE, $excludeTypes = array())
    {
        $this->loadDictonary();
        $metadataS = $this->fileManager->generateMetadata('system', $this->metadata, $this->lang);
        $newStringsS = isset($this->sessionStorage->newStrings[$this->lang]) ? $this->sessionStorage->newStrings[$this->lang] : array();
        return $this->fileManager->buildPOFileString(
                'system', 
                $metadataS, 
                $this->dictionary, 
                $newStringsS, 
                $includingNewStrings,
                NULL, // $this->getVariantsCount()
                $excludeTypes
            );
    }

    /**
     * Add file to parse
     * @param string $dir
     * @param string $identifier
     * @return $this
     * @throws \InvalidArgumentException
     */
    protected function addFile($dir, $identifier)
    {
        if (isset($this->files[$identifier])) {
            throw new \InvalidArgumentException("Language file identified '$identifier' is already registered.");
        }

        if (is_dir($dir)) {
            $this->files[$identifier] = $dir;

        } else {
            throw new \InvalidArgumentException("Directory '$dir' doesn't exist.");
        }

        return $this;
    }


    /**
     * Get current language
     * @return string
     * @throws Nette\InvalidStateException
     */
    public function getLang()
    {
        if (empty($this->lang)) {
            throw new Nette\InvalidStateException('Language must be defined.');
        }

        return $this->lang;
    }


    /**
     * Set new language
     * @return this
     */
    public function setLang($lang)
    {
        if (empty($lang)) {
            throw new Nette\InvalidStateException('Language must be nonempty string.');
        }

        if ($this->lang === $lang) {
            return;
        }

        $this->lang = $lang;
        $this->dictionary = array();
        $this->loaded = FALSE;

        return $this;
    }


    /**
     * Set production mode (has influence on cache usage)
     * @param bool
     * @return this
     */
    public function setProductionMode($mode)
    {
        $this->productionMode = (bool) $mode;
        return $this;
    }

    
    /**
     * Typed translation (text, label, value, validation ...)
     * 
     * @param string $type One of the TRANSLATION_TYPE_.... constants
     * @param string $message Message
     * @param int $form 
     * @return string
     */
    public function typedTranslate($type, $message, $form)
    {
        $this->loadDictonary();
        

        $message = (string) $message;
        $message_plural = NULL;
        if (is_array($form) && $form !== NULL) {
            $message_plural = current($form);
            $form = (int) end($form);

        } elseif (is_numeric($form)) {
            $form = (int) $form;

        } elseif (!is_int($form) || $form === NULL) {
            $form = 1;
        }

        // Prekladana zprava je v uzivatelskych prekladech
        if (!empty($message) && 
                isset($this->dictionary[$message]) && 
                isset($this->dictionary[$message]['user']) && 
                isset($this->metadata['user']['Plural-Forms'])) {
            $tmp = preg_replace('/([a-z]+)/', '$$1', "n=$form;" . $this->metadata['user']['Plural-Forms']);
            $plural = null;
            eval($tmp);
            
            $message = $this->dictionary[$message]['user'];
            
            if (!empty($message)) {
                $message = (is_array($message) && $plural !== NULL && isset($message[$plural])) ? $message[$plural] : $message;
            }
        }
        // Prekladana zprava je v systemovych prekladech
        else if (!empty($message) && 
                isset($this->dictionary[$message]) && 
                isset($this->dictionary[$message]['system']) && 
                isset($this->metadata['system']['Plural-Forms'])) {
            $tmp = preg_replace('/([a-z]+)/', '$$1', "n=$form;" . $this->metadata['system']['Plural-Forms']);
            $plural = null;
            eval($tmp);

            $message = $this->dictionary[$message]['system'];
            if (!empty($message)) {
                $message = (is_array($message) && $plural !== NULL && isset($message[$plural])) ? $message[$plural] : $message;
            }
        }
        // Neni prelozena
        else {
            if (!$this->httpResponse->isSent() || $this->sessionStorage) {
                if (!isset($this->sessionStorage->newStrings[$this->lang])) {
                    $this->sessionStorage->newStrings[$this->lang] = array();
                }
                $this->sessionStorage->newStrings[$this->lang][$message] = array(
                    'type' => $type,
                    'data' => empty($message_plural) ? array($message) : array($message, $message_plural)
                );
            }

            if ($form > 1 && !empty($message_plural)) {
                $message = $message_plural;
            }
        }
        
        if (is_array($message)) {
            $message = current($message);
        }

        $args = func_get_args();
        
        // type
        array_shift($args);
        // message
        array_shift($args);
        // form
        array_shift($args);

        if (count($args)) {
            $message = vsprintf($message, $args);
        }

        return $message;
    }
    
    /**
     * Translate given string
     * @param string $message
     * @param int $form plural form (positive number)
     * @return string
     */
    public function translate($message, ...$parameters): string
    {
        $count = $parameters[0] ?? NULL;
        return $this->typedTranslate(self::TRANSLATION_TYPE_TEXT, $message, $count);
    }


    /**
     * Get count of plural forms
     * @return int
     */
    public function getVariantsCount()
    {
        return 1; // fixme
        
        $this->loadDictonary();
        $files = array_keys($this->files);

        if (isset($this->metadata[$files[0]]['Plural-Forms'])) {
            return (int) substr($this->metadata[$files[0]]['Plural-Forms'], 9, 1);
        }

        return 1;
    }


    /**
     * Get translations strings
     * @return array
     */
    public function getStrings()
    {
        $this->loadDictonary();

//        $newStrings = array();
        $result = array();

        if (isset($this->sessionStorage->newStrings[$this->lang])) {
            foreach($this->sessionStorage->newStrings[$this->lang] as $string => $newStringData) {
                if(trim($string) !== '') {
                    $result[$string] = array(
                            'type' => isset($newStringData['type']) ? $newStringData['type'] : self::TRANSLATION_TYPE_TEXT,
                            'data' => NULL
                        );
                }
            }
//            foreach (array_keys($this->sessionStorage->newStrings[$this->lang]) as $original) {
//                if (trim($original) != '') {
//                    $newStrings[$original] = FALSE;
//                }
//            }
        }
        
        foreach ($this->dictionary as $string => $translations) {
            if(trim($string) !== '') {
                $result[$string] = array(
                        'type' => NULL,
                        'data' => $translations
                    );
            }
        }

        return $result;
        
//        dump($result, $file, $this->dictionary, $newStrings, $this->sessionStorage->newStrings); die;

    
        

//        foreach ($this->dictionary as $original => $data) {
//            if (trim($original) != '') {
//                if ($file && $data['file'] === $file) {
//                    $result[$original] = $data['translation'];
//
//                } else {
//                    $result[$data['file']][$original] = $data['translation'];
//                }
//            }
//        }
//
//        if ($file) {
//            return array_merge($newStrings, $result);
//
//        } else {
//            foreach ($this->getFiles() as $identifier => $path) {
//                if (!isset($result[$identifier])) {
//                    $result[$identifier] = array();
//                }
//            }
//
//            return array('newStrings' => $newStrings) + $result;
//        }
    }


    /**
     * Get loaded files
     * @return array
     */
    public function getFiles()
    {
        $this->loadDictonary();
        return $this->files;
    }


    /**
     * Set translation string(s)
     * @param string|array $original original string
     * @param string|array $stringData translation strings
     * @param string
     */
    public function setTranslation($original, $stringData, $file)
    {
        $this->loadDictonary();
//        if (isset($this->sessionStorage->newStrings[$this->lang]) && array_key_exists($original, $this->sessionStorage->newStrings[$this->lang])) {
//            $message = $this->sessionStorage->newStrings[$this->lang][$message];
//        }
        $key = is_array($original) ? $original[0] : $original;
        $this->dictionary[$key]['original'] = (array) $original;        
        $this->dictionary[$key][$file] = (array) $stringData;
    }


    /**
     * Save dictionary
     * @param string
     */
    public function save()
    {
        if (!$this->loaded) {
            throw new Nette\InvalidStateException('Nothing to save, translations are not loaded.');
        }
        
        // Save system translations
        $pathS = $this->getSystemFilePath();
        $metadataS = $this->fileManager->generateMetadata('system', $this->metadata, $this->lang);
        $newStringsS = isset($this->sessionStorage->newStrings[$this->lang]) ? $this->sessionStorage->newStrings[$this->lang] : array();
        $this->fileManager->buildMOFile("$pathS.mo", 'system', $metadataS, $this->dictionary);
        $this->fileManager->buildPOFile("$pathS.po", 'system', $metadataS, $this->dictionary, $newStringsS);

        // Save user translations
        $pathU = $this->getUserFilePath();
        $metadataU = $this->fileManager->generateMetadata('user', $this->metadata, $this->lang);
        $newStringsU = isset($this->sessionStorage->newStrings[$this->lang]) ? $this->sessionStorage->newStrings[$this->lang] : array();
        $this->fileManager->buildMOFile("$pathU.mo", 'user', $metadataU, $this->dictionary);
        $this->fileManager->buildPOFile("$pathU.po", 'user', $metadataU, $this->dictionary, $newStringsU);
        
        if (isset($this->sessionStorage->newStrings[$this->lang])) {
            unset($this->sessionStorage->newStrings[$this->lang]);
        }

        if ($this->productionMode) {
            $this->cache->clean(array(
                Cache::TAGS => 'dictionary-' . $this->lang
            ));
        }
    }


    /**
     * Load data
     */
    public function loadDictonary()
    {
        if (!$this->loaded) {
            if (empty($this->files)) {
                throw new Nette\InvalidStateException('Language file(s) must be defined.');
            }

            $dLang = $this->cache->load('dictionary-' . $this->lang);
            if ($this->productionMode && isset($dLang)) {
                $this->dictionary = $this->cache->load('dictionary-' . $this->lang);

            } else {
                $files = array();

                $systemMoFile = $this->getSystemFilePath().'.mo';
                if(is_file($systemMoFile)) {
                    $this->parseFile($systemMoFile, 'system');
                    $files[] = $systemMoFile;
                }

                $userMoFile = $this->getUserFilePath().'.mo';
                if(is_file($userMoFile)) {
                    $this->parseFile($userMoFile, 'user');
                    $files[] = $userMoFile;
                }

                if ($this->productionMode) {
                    $this->cache->save('dictionary-' . $this->lang, $this->dictionary, array(
                        Cache::EXPIRE => '+ 1 hour',
                        Cache::FILES => $files,
                        Cache::TAGS => array('dictionary-' . $this->lang)
                    ));
                }
            }

            $this->loaded = TRUE;
        }
    }


    /**
     * Parse dictionary file
     * @param string $file file path
     * @param string
     */
    private function parseFile($file, $identifier)
    {
        $f = @fopen($file, 'rb');
        if (@filesize($file) < 10) {
            throw new \InvalidArgumentException("'$file' is not a gettext file.");
        }

        $endian = FALSE;
        $read = function ($bytes) use ($f, $endian) {
            $data = fread($f, 4 * $bytes);
            return $endian === FALSE ? unpack('V' . $bytes, $data) : unpack('N' . $bytes, $data);
        };

        $input = $read(1);
        if (Strings::lower(substr(dechex($input[1]), -8)) == '950412de') {
            $endian = FALSE;

        } elseif (Strings::lower(substr(dechex($input[1]), -8)) == 'de120495') {
            $endian = TRUE;

        } else {
            throw new \InvalidArgumentException("'$file' is not a gettext file.");
        }

        $input = $read(1);

        $input = $read(1);
        $total = $input[1];

        $input = $read(1);
        $originalOffset = $input[1];

        $input = $read(1);
        $translationOffset = $input[1];

        fseek($f, $originalOffset);
        $orignalTmp = $read(2 * $total);
        fseek($f, $translationOffset);
        $translationTmp = $read(2 * $total);

        for ($i = 0; $i < $total; ++$i) {
            if ($orignalTmp[$i * 2 + 1] != 0) {
                fseek($f, $orignalTmp[$i * 2 + 2]);
                $original = @fread($f, $orignalTmp[$i * 2 + 1]);

            } else {
                $original = '';
            }

            if ($translationTmp[$i * 2 + 1] != 0) {
                fseek($f, $translationTmp[$i * 2 + 2]);
                $translation = fread($f, $translationTmp[$i * 2 + 1]);
                if ($original === '') {
                    $this->metadata = $this->fileManager->parseMetadata($translation, $identifier, $this->metadata);
                    continue;
                }

                $original = explode("\0", $original);
                $translation = explode("\0", $translation);

                $key = isset($original[0]) ? $original[0] : $original;
                // prvni vyskyt prekladu ?
                if(!isset($this->dictionary[$key])) {
                    $this->dictionary[$key] = array(
                        'original' => NULL,
                        'user' => NULL,
                        'system' => NULL
                    );
                }
                $this->dictionary[$key]['original'] = $original;
                $this->dictionary[$key][$identifier] = $translation;
            }
        }
    }
    
    /**
     * Get user MO/PO file path excluding extension (.mo / .po)
     * User DIFF might be added if defined
     * 
     * @param $lang If no lang is given, current lang will be used
     * @return string
     */
    public function getUserFilePath($lang = NULL)
    {
        $useLang = $lang === NULL ? $this->lang : $lang;
        return "{$this->files['user']}/{$useLang}.user".(empty($this->userDiff) ? '' : '.'.$this->userDiff);
    }
    
    /**
     * Get system MO/PO file path excluding extension (.mo / .po)
     * 
     * @param $lang If no lang is given, current lang will be used
     * @return string
     */
    public function getSystemFilePath($lang = NULL)
    {
        $useLang = $lang === NULL ? $this->lang : $lang;
        return "{$this->files['system']}/{$useLang}.system";
    }

    /**
     * Save system file
     * 
     * @param string $lang Lang code
     * @param string|string[] $files PO File paths
     * @return void
     */
    public function saveSystemFile($lang, $files)
    {
        if(!is_array($files)) {
            $files = array($files);
        }
        
        // Parse PO files
        $final = array('headers' => array(), 'entries' => array());
        foreach($files as $file) {
            $parsed = $this->fileManager->readPOFile($file);
            // Simply fill headers from the first PO file
            if(!empty($parsed['headers'])) {
                $final['headers'] = $parsed['headers'];
            }
            // add entries that are not yet present
            if(!empty($parsed['entries'])) {
                foreach($parsed['entries'] as $pKey => $pEntry) {
                    if(!isset($final['entries'][$pKey])) {
                        $final['entries'][$pKey] = $pEntry;
                    }
                }
            }
        }
                
        // Build PO + MO files and save them
        $poContent = $this->buildPOFileString($final);
        
        $moContent = $this->buildMOFileString($final);
        // Save both files
        $targetPo = $this->getSystemFilePath($lang).'.po';
        file_put_contents($targetPo, $poContent);
        
        $targetMo = $this->getSystemFilePath($lang).'.mo';
        file_put_contents($targetMo, $moContent);
    }
    
    
    protected function buildPOFileString($parsed)
    {
        $metadata = array();
        foreach($parsed['headers'] as $hdrKey => $hdrVal) {
            $metadata[] = $hdrKey.': '.$hdrVal;
        }
        
        $po = "# Gettext keys exported by SammTranslator and Translation Panel\n" .
                "# Created: " . date('Y-m-d H:i:s') . "\n" .
                'msgid ""' . "\n" .
                'msgstr ""' . "\n";
        $po .= '"' . implode('\n"' . "\n" . '"', $metadata) . '\n"' . "\n\n\n";

        foreach ($parsed['entries'] as $message => $entry)
        {
            if(trim($message) === '') {
                continue;
            }
            /* @var $entry \PoParser\Entry */
            $translations = $entry->getTranslations();

            $po .= 'msgid "' . str_replace(array('"'), array('\"'), $message) . '"' . "\n";

            //if (is_array($data['original']) && count($data['original']) > 1) {
            if(count($translations) > 1) {
                $po .= 'msgid_plural "'.str_replace(array('"'), array('\"'), $message).'"'."\n";
                $i = 0;
                foreach ($translations as $trIndex => $string) {
                    $po .= 'msgstr['.$trIndex.'] "'.str_replace(array('"'), array('\"'), $string).'"'."\n";
                    $i++;
                }
            }
            else {
                if (!is_array($translations)) {
                    $po .= 'msgstr "'.str_replace(array('"'), array('\"'), $translations).'"'."\n";
                }
                else {
                    $po .= 'msgstr "'.str_replace(array('"'), array('\"'), reset($translations)).'"'."\n";
                }
            }
            $po .= "\n";
        }
        return $po;
    }
    
    /**
     * Builds MO file content from parsed PO file
     * 
     * @param array $parsed array with 'headers' + 'entries' keys
     */
    protected function buildMOFileString($parsed)
    {
        // Prepare metadata
        $metadata = array();
        foreach($parsed['headers'] as $headerKey => $headerVal) {
            $metadata[] = $headerKey.': '.$headerVal;
        }
        $metadataImploded = implode("\n", $metadata);

        // Count of really translated items
        $items = 1;
        foreach($parsed['entries'] as $original => $entry) {
            /* @var $entry \PoParser\Entry */
            if(trim($original) === '') {
                continue;
            }
            $translations = $entry->getTranslations();
            $allEmpty = TRUE;
            foreach($translations as $translation) {
                if(trim($translation) !== '') {
                    $allEmpty = FALSE;
                }
            }
            if($allEmpty) {
                continue;
            }
            $items++;
        }

        $ids = Strings::chr(0x00);
        $strings = $metadataImploded . Strings::chr(0x00);
        $idsOffsets = array(0, 28 + $items * 16);
        $stringsOffsets = array(array(0, strlen($metadataImploded)));

        foreach ($parsed['entries'] as $key => $entry) {
            /* @var $entry \PoParser\Entry */
            if(trim($key) === '') {
                continue;
            }
            
            $msgIdPlural = $entry->getMsgIdPlural();
            $translations = $entry->getTranslations();

            // All translations empty ???
            $allEmpty = TRUE;
            foreach($translations as $translation) {
                if(trim($translation) !== '') {
                    $allEmpty = FALSE;
                }
            }
            if($allEmpty) {
                continue;
            }

            $id = $key;
            if ($msgIdPlural !== NULL) {
                $id .= Strings::chr(0x00) . $msgIdPlural;
            }

            $string = implode(Strings::chr(0x00), $translations);
            $idsOffsets[] = strlen($id);
            $idsOffsets[] = strlen($ids) + 28 + $items * 16;
            $stringsOffsets[] = array(strlen($strings), strlen($string));
            $ids .= $id . Strings::chr(0x00);
            $strings .= $string . Strings::chr(0x00);
        }

        $valuesOffsets = array();
        foreach ($stringsOffsets as $offset) {
            list ($all, $one) = $offset;
            $valuesOffsets[] = $one;
            $valuesOffsets[] = $all + strlen($ids) + 28 + $items * 16;
        }
        $offsets = array_merge($idsOffsets, $valuesOffsets);

        $mo = pack('Iiiiiii', 0x950412de, 0, $items, 28, 28 + $items * 8, 0, 28 + $items * 16);
        foreach ($offsets as $offset) {
            $mo .= pack('i', $offset);
        }
        return $mo . $ids . $strings;
    }
    
}