<?php

namespace SammTranslator\DI;

use Nette;
use Nette\Schema\Expect;


if (!class_exists('Nette\DI\CompilerExtension')) {
    class_alias('Nette\Config\CompilerExtension', 'Nette\DI\CompilerExtension');
}


class Extension extends Nette\DI\CompilerExtension
{
	public function getConfigSchema(): Nette\Schema\Schema
	{
		return Expect::structure([
            'lang' => Expect::string('en'),
            'files' => Expect::array(),
            'userFiles' => Expect::string(),
            'systemFiles' => Expect::string(),
            'userDiffByConnection' => Expect::bool()
		]);
	} 
    
    public function loadConfiguration()
    {
        $config = $this->getConfig();

        $builder = $this->getContainerBuilder();

        $translator = $builder->addDefinition($this->prefix('translator'));
        $translator->setFactory(\SammTranslator\Gettext::class, [$config, '@session', '@cacheStorage', '@httpResponse']);
        $translator->addSetup('setProductionMode', [$builder->parameters['productionMode']]);

        $fileManager = $builder->addDefinition($this->prefix('fileManager'));
        $fileManager->setFactory(\SammTranslator\FileManager::class);

        if(empty($config->systemFiles)) {
            throw new InvalidConfigException('System Files directory must be specified (systemFiles)');
        }
        if(empty($config->userFiles)) {
            throw new InvalidConfigException('User Files directory must be specified (userFiles)');
        }
                
        $translator->addSetup('SammTranslator\Panel::register', [
                '@application', '@self', '@session', '@httpRequest'
            ]);
    }

}


class InvalidConfigException extends Nette\InvalidStateException {

}
