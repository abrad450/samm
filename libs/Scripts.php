<?php
/**
 * Skripty pro abra/samm
 * Date: 26. 1. 2016
 */
namespace Abra\Samm;

require_once __DIR__ . '/../vendor/autoload.php';

class Scripts
{
    const CUSTOM_FILE = '/../app/config/custom.neon';
    const CUSTOM_FILE_BACKUP = '/../../../../custom.neon';

    const AOS_FILE = '/../data/aos.db';
    const AOS_FILE_BACKUP = '/../../../../aos.db';

    const GUIDE_PDFS = '/../www/manual/';
    const GUIDE_PDFS_BACKUP = '/../../../../';
    
    const LANG_FILES = '/../app/lang/';
    const LANG_FILES_BACKUP = '/../../../../';
    
    const LOGO_PATH = '/../www/images/';
    const LOGO_PATH_BACKUP = '/../../../../';

    /**
     * Vytvoreni custom.neon
     */
    public static function postInstall()
    {
        $custom = array(
            'parameters' => array(
                'connections!' => array(
                    'AWS' => '',
                ),
                'service' => array(
                    'host' => '127.0.0.1',
                    'port' => 8086,
                    'path' => '',
                    'encoding' => 'UTF-8',
                    'timeout' => 300,
                    'web_service_name' => 'userwebservice',
                ),
                'lang' => 'cs',
                'langs!' => array('cs', 'en', 'pl', 'sk', 'ro'),
            ),
        );

        echo "\n\033[34m===== Instalace =====\033[0m\n";
        echo "Zadejte AWS (Adresu Webovych Sluzeb): ";
        $custom['parameters']['connections!']['AWS'] = chop(fgets(STDIN)) ?: $custom['parameters']['connections!']['AWS'];
        echo "Host nebo IP adresa, kde bezi Apache ({$custom['parameters']['service']['host']}): ";
        $custom['parameters']['service']['host'] = chop(fgets(STDIN)) ?: $custom['parameters']['service']['host'];
        echo "PORT, kde bezi Apache ({$custom['parameters']['service']['port']}): ";
        $custom['parameters']['service']['port'] = chop(fgets(STDIN)) ?: $custom['parameters']['service']['port'];
        echo "Cesta ke spojeni: ";
        $custom['parameters']['service']['path'] = chop(fgets(STDIN)) ?: $custom['parameters']['service']['path'];
        echo "Kodovani ({$custom['parameters']['service']['encoding']}): ";
        $custom['parameters']['service']['encoding'] = chop(fgets(STDIN)) ?: $custom['parameters']['service']['encoding'];
        echo "Timeout pri nefunkcnosti webovych sluzeb ({$custom['parameters']['service']['timeout']}): ";
        $custom['parameters']['service']['timeout'] = chop(fgets(STDIN)) ?: $custom['parameters']['service']['timeout'];
        echo "Nazev webovych sluzeb (konstanta pro vsechna mozna spojeni) ({$custom['parameters']['service']['web_service_name']}): ";
        $custom['parameters']['service']['web_service_name'] = chop(fgets(STDIN)) ?: $custom['parameters']['service']['web_service_name'];
        echo "Vicejazycna aplikace? (N/a): ";
        $ml = chop(fgets(STDIN));
        echo "Vychozi jazyk aplikace ({$custom['parameters']['lang']}): ";
        $custom['parameters']['lang'] = chop(fgets(STDIN)) ?: $custom['parameters']['lang'];
        if (!empty($ml) && \Nette\Utils\Strings::lower($ml) === 'a') {
            echo "Nabidka ruznych jazyku (nutno aplikaci prelozit pomoci prekladace) (" . implode(',', $custom['parameters']['langs!']) . "): ";
            $l = chop(fgets(STDIN));
            if ($l) {
                $custom['parameters']['langs!'] = explode(',', $l);
            }
        } else {
            $custom['parameters']['langs!'] = null;
        }
        echo "Vytvarim soubor custom.neon...\n";
        file_put_contents(__DIR__ . self::CUSTOM_FILE, \Nette\Neon\Neon::encode($custom));

        echo "\033[32mHotovo!\033[0m\n";

        self::chmod();
        exit(0); // Script ran OK
    }

    /**
     * Zaloha nastaveni pred aktualizaci
     */
    public static function preUpdate()
    {
        echo "\n===== Zaloha nastaveni =====\n";
        
        // logo file
		if(is_file(__DIR__ . self::CUSTOM_FILE)) {
			$config = \Nette\Neon\Neon::decode(file_get_contents(__DIR__ . self::CUSTOM_FILE));
			if(isset($config['parameters']['visual']['logo'])) {
				$logoFile = $config['parameters']['visual']['logo'];
				copy(__DIR__ . self::LOGO_PATH . $logoFile, __DIR__ . self::LOGO_PATH_BACKUP . $logoFile);
			}
		}
        
        // lang files
        $langFiles = glob(__DIR__ . self::LANG_FILES.'*.[mMpP][oO]');
        foreach($langFiles as $langFile) {
            $langFileName = pathinfo($langFile, PATHINFO_BASENAME);
            copy($langFile, __DIR__ . self::LANG_FILES_BACKUP . $langFileName);
        }
        
        // Guide PDF files
        $guideFiles = glob(__DIR__ . self::GUIDE_PDFS.'*.[pP][dD][fF]');
        foreach($guideFiles as $guideFile) {
            $guideFileName = pathinfo($guideFile, PATHINFO_BASENAME);
            copy($guideFile, __DIR__ . self::GUIDE_PDFS_BACKUP . $guideFileName);
        }

        // custom.neon
        if(is_file(__DIR__ . self::CUSTOM_FILE)) {
            copy(__DIR__ . self::CUSTOM_FILE, __DIR__ . self::CUSTOM_FILE_BACKUP);
        }
		// backup aos.db
        if(is_file(__DIR__ . self::AOS_FILE)) {
            copy(__DIR__ . self::AOS_FILE, __DIR__ . self::AOS_FILE_BACKUP);
        }

        echo "\033[32mHotovo!\033[0m\n";
    }

    /**
     * Obnova nastaveni po aktualizaci
     */
    public static function postUpdate()
    {
        echo "\n===== Obnoveni nastaveni =====\n";

        // aos.db
        if(is_file(__DIR__ . self::AOS_FILE_BACKUP)) {
            rename(__DIR__ . self::AOS_FILE_BACKUP, __DIR__ . self::AOS_FILE);
        }
        // custom.neon
        if(is_file(__DIR__ . self::CUSTOM_FILE_BACKUP)) {
            rename(__DIR__ . self::CUSTOM_FILE_BACKUP, __DIR__ . self::CUSTOM_FILE);
        }
		
        // guide PDF files
        $guideFiles = glob(__DIR__ . self::GUIDE_PDFS_BACKUP.'*.[pP][dD][fF]');
        foreach($guideFiles as $guideFile) {
            $guideFileName = pathinfo($guideFile, PATHINFO_BASENAME);
            rename($guideFile, __DIR__ . self::GUIDE_PDFS . $guideFileName);
        }        
        
        // lang files
        $langFiles = glob(__DIR__ . self::LANG_FILES_BACKUP.'*.[mMpP][oO]');
        foreach($langFiles as $langFile) {
            $langFileName = pathinfo($langFile, PATHINFO_BASENAME);
            rename($langFile, __DIR__ . self::LANG_FILES . $langFileName);
        }
        
        // logo file
		if(is_file(__DIR__ . self::CUSTOM_FILE)) {
			$config = \Nette\Neon\Neon::decode(file_get_contents(__DIR__ . self::CUSTOM_FILE));
			if(isset($config['parameters']['visual']['logo'])) {
				$logoFile = $config['parameters']['visual']['logo'];
				rename(__DIR__ . self::LOGO_PATH_BACKUP . $logoFile, __DIR__ . self::LOGO_PATH . $logoFile);
			}
		}
        
        echo "\033[32mHotovo!\033[0m\n";
        self::chmod();
    }

    /**
     * Nastaveni prav pro zapis do temp a log
     */
    private static function chmod()
    {
        echo "\n===== Nastaveni prav pro zapis =====\n";
        chmod(__DIR__ . '/../log', 777);
        chmod(__DIR__ . '/../temp', 777);
        echo "\033[32mHotovo!\033[0m\n";
    }
}
