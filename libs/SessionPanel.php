<?php
/**
 * Dumps sessions to DebugBar
 *
 * @author Jan Dolecek <juzna.cz@gmail.com>
 */
class SessionPanel implements Tracy\IBarPanel {
	
    private $sess;

    public function __construct(\Nette\Http\Session $sess)
	{
        $this->sess = $sess;
    }

    function getTab()
	{
        return $this->sess->getIterator()->count() . ' sessions';
    }

    function getPanel()
	{
        $ret = array();
        foreach($this->sess->getIterator() as $ns)
		{
			$ret[$ns] = iterator_to_array($this->sess->getSection($ns));
		}
        return \Tracy\Debugger::dump($ret, true);
    }
}