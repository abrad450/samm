#!/usr/bin/env bash
# Soubor příkazů, které se provedou v docker konteineru ihned po spuštění nebo sestavení image

# Zapnutí nebo vypnutí XDEBUGU
if [ -z ${XDEBUG_ENABLED+x} ] || [ "$XDEBUG_ENABLED" -ne "1" ];
    then
        echo 'Deaktivuji xdebug';
        phpdismod xdebug;

    else
        echo 'Aktivuji xdebug';
        phpenmod xdebug;

        # Nastavování konfigurace PHP Xdebugu dle ENV proměnných
        if [ -z ${XDEBUG_HOST+x} ];
            then
                echo 'Nebyla zadána hodnota IP pro xdebug.remote_host!';

                echo "Apache2 xdebug.ini:";
                cat /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini | xargs echo -e

                echo "CLI xdebug.ini:";
                cat /etc/php/${PHP_VER}/cli/conf.d/xdebug.ini | xargs echo -e
            else
                echo "Nastavuji IP adresu pro xdebug.remote_host na: $XDEBUG_HOST";
                sed -i "s/xdebug\.remote_host\=.*/xdebug\.remote_host\=$XDEBUG_HOST/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.remote_host\=.*/xdebug\.remote_host\=$XDEBUG_HOST/g" /etc/php/${PHP_VER}/cli/conf.d/xdebug.ini;

                echo "Apache2 xdebug.ini:";
                cat /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini | xargs echo -e

                echo "CLI xdebug.ini:";
                cat /etc/php/${PHP_VER}/cli/conf.d/xdebug.ini | xargs echo -e

                echo "Dokončeno nastavování IP pro xdebug.remote_host"
        fi

        # Docker
        if [ -z ${XDEBUG_DOCKER+x} ];
            then
                echo 'Nebyla nastavena XDEBUG_DOCKER proto nenastavuji xDebug pro docker a předchozí operace se zanechají.';
            else

                mkdir /log
                mkdir /log/xdebug

                echo "Nastavuji xDebug pro Docker a předchozí nastavení nemusí platit.";

                sed -i "s/xdebug\.remote_host\=.*/xdebug\.remote_host\=host.docker.internal/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.remote_enable\=.*/xdebug\.remote_enable\=on/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.remote_autostart\=.*/xdebug\.remote_autostart\=0/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.remote_connect_back\=.*/xdebug\.remote_connect_back\=0/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.remote_port\=.*/xdebug\.remote_port\=9000/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.profiler_enable_trigger\=.*/xdebug\.profiler_enable_trigger\=1/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.profiler_enable\=.*/xdebug\.profiler_enable\=0/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.profiler_output_dir\=.*/xdebug\.profiler_output_dir\=\/log\/xdebug/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.var_display_max_children\=.*/xdebug\.var_display_max_children\=-1/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.var_display_max_data\=.*/xdebug\.var_display_max_data\=-1/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;
                sed -i "s/xdebug\.var_display_max_depth\=.*/xdebug\.var_display_max_depth\=-1/g" /etc/php/${PHP_VER}/apache2/conf.d/xdebug.ini;

                echo "Dokončeno nastavování xDebug pro Docker.";
        fi


fi


# Spuštění Apache
apache2ctl -D FOREGROUND
