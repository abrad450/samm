# Debugging PHP with xDebug

## Run debuging:

**Phpstorm:**

Go to Run → Edit configuration → + (Add new configuration) →
PHP Remote Debug → check Filter debug connection by IDEA key.

Set `IDE key (session id):` value: `PHPSTORM`

Then open dialog by click on dots Server: → ..., then add new + →

Host: `samm.local`

Port: isSSL ? `443` : `80`

Debbuger: XDebug

check Mapping.... Projeckt files → Absolute path on server: `/var/www/samm`

Well done, save all and start debugging.
Install to
 - Chrome  [Xdebug helper](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc) 
 - Firefox [Xdebug helper](https://github.com/BrianGilbert/xdebug-helper-for-firefox)
 
extention and set mode `debug` 

Don't forget to **Start listening for PHP debug connection**
